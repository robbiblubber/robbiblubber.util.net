﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Reflection;

using Robbiblubber.Util.Debug;
using Robbiblubber.Util.Coding;



namespace Robbiblubber.Util.Localization
{
    /// <summary>This class implements locale support.</summary>
    public sealed class Locale
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>GetImages() method.</summary>
        private static MethodInfo _GetImages = null;

        /// <summary>Locales list.</summary>
        private static LocaleCollection _Locales = null;

        /// <summary>PathOp.</summary>
        private static List<string> _Paths = new List<string>();

        /// <summary>Selected locale.</summary>
        private static Locale _Selected = null;

        /// <summary>Prefixes in use.</summary>
        private static List<string> _UsedPrefixes = new List<string>();

        /// <summary>Language pack path.</summary>
        private static string _PackPath = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Internal tables dictionary.</summary>
        private Dictionary<string, Dictionary<string, string>> _InternalTables = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructrs                                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fileName">File name.</param>
        private Locale(string fileName)
        {
            IEncoder enc = Base64.Instance;
            if(fileName.ToLower().EndsWith(".locale.information")) { enc = PlainText.Instance; }

            Ddp cfg = null;

            if(fileName.ToLower().EndsWith(".locale.phil"))
            {
                cfg = _ParsePhil(fileName);
            }
            else
            {
                cfg = Ddp.Open(fileName, enc);
            }

            Key = cfg["/locale/lang"];
            Name = cfg["/locale/name"];
            Base = cfg["/locale/base"];
            IsDefault = cfg["/locale/default"];

            IconFile = cfg["/locale/icon"];
            SmallIconFile = cfg["/locale/sicon"];

            if(!File.Exists(IconFile)) { IconFile = Path.GetDirectoryName(fileName) + @"\" + IconFile; }
            if(!File.Exists(SmallIconFile)) { SmallIconFile = Path.GetDirectoryName(fileName) + @"\" + SmallIconFile; }

            _GetIcons();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a list of available locales.</summary>
        public static LocaleCollection Locales
        {
            get
            {
                if(_Locales == null)
                {
                    _Locales = new LocaleCollection();
                    Load();
                }

                return _Locales;
            }
        }


        /// <summary>Gets or sets the selected locale.</summary>
        public static Locale Selected
        {
            get
            {
                if(_Selected == null) { _Selected = Locales.Default; }
                return _Selected;
            }
            set
            {
                _Selected = value;
            }
        }


        /// <summary>Gets a list of prefixes used in this application.</summary>
        public static string[] UsedPrefixes
        {
            get { return _UsedPrefixes.ToArray(); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads the locales from a path.</summary>
        /// <param name="path">Path.</param>
        /// <param name="addPrefixes">Adds the prefixes of loaded tables to the used prefixes table.</param>
        public static void Load(string path = null, bool addPrefixes = false)
        {
            if(path == null) { path = PathOp.ApplicationDirectory; }
            path = Path.GetFullPath(path);

            if((!addPrefixes) && (_Paths.Contains(path))) return;

            if(!_Paths.Contains(path)) { _Paths.Add(path); }

            _Load(path, addPrefixes);
        }


        /// <summary>Looks up a symbol in locale tables and returns a localized expression.</summary>
        /// <param name="symbol">Symbol.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Localized string.</returns>
        public static string Lookup(string symbol, string defaultValue = null)
        {
            if(defaultValue == null) { defaultValue = symbol; }

            try
            {
                return Selected.LookupString(symbol, defaultValue);
            }
            catch(Exception) {}

            return defaultValue;
        }


        /// <summary>Clears all currently loaded locales.</summary>
        public static void Clear()
        {
            _Selected = null;
            _Locales._Items.Clear();
        }


        /// <summary>Defines a list of prefixes that will be used by this application. These will be added to UsedPrefixes.</summary>
        /// <param name="prefixes">Prefixes.</param>
        public static void UsingPrefixes(params string[] prefixes)
        {
            UsingPrefixes(false, prefixes);
        }


        /// <summary>Defines a list of prefixes that will be used by this application. These will be added to UsedPrefixes.</summary>
        /// <param name="clear">Determines if UsedPrefixes should be cleared before adding the prefixes.</param>
        /// <param name="prefixes">Prefixes.</param>
        public static void UsingPrefixes(bool clear, params string[] prefixes)
        {
            if(clear) { _UsedPrefixes = new List<string>(); }
            _UsedPrefixes.AddRange(prefixes);

            if(_Selected != null) { _Selected._InternalTables = null; }
        }


        /// <summary>Saves the locale selection.</summary>
        /// <param name="applicationKey">Application key.</param>
        public static void SaveSelection(string applicationKey = null)
        {
            if(Selected == null) return;
            if(applicationKey == null) { applicationKey = Process.GetCurrentProcess().ProcessName; }

            if(!Directory.Exists(PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\locale")) { Directory.CreateDirectory(PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\locale"); }
            Ddp cfg = Ddp.Open(PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\locale\locale.config");
            cfg["/sel/" + applicationKey].Value = Selected.Key;
            cfg.Save();
        }


        /// <summary>Loads the locale selection.</summary>
        /// <param name="applicationKey">Application key.</param>
        public static void LoadSelection(string applicationKey = null)
        {
            LoadSelection(applicationKey, new string[0]);
        }


        /// <summary>Loads the locale selection.</summary>
        /// <param name="applicationKey">Application key.</param>
        /// <param name="default">Default locale.</param>
        public static void LoadSelection(string applicationKey, params string[] @default)
        {
            if(applicationKey == null) { applicationKey = Process.GetCurrentProcess().ProcessName; }

            Ddp cfg = Ddp.Open(PathOp.SystemUserConfigurationPath + @"\robbiblubber.org\locale\locale.config");
            string test = cfg["/sel/" + applicationKey];
            Locale l = Locales[(string) cfg["/sel/" + applicationKey]];

            if(l == null)
            {
                foreach(string i in @default)
                {
                    if((l = Locales[i]) != null) break;
                }
            }

            if(l != null) { Selected = l; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads the locales from a path.</summary>
        /// <param name="path">Path.</param>
        /// <param name="addPrefixes">Adds the prefixes of loaded tables to the used prefixes table.</param>
        private static void _Load(string path, bool addPrefixes)
        {
            List<string> locs = new List<string>();

            if(path != _PackPath)
            {
                locs.AddRange(Directory.GetFiles(path, "*.lang", SearchOption.AllDirectories));
                if(locs.Count > 0)
                {
                    if(_PackPath == null)
                    {
                        _PackPath = FileOp.CreateTempPath();
                        _Paths.Add(_PackPath);
                    }

                    foreach(string i in locs)
                    {
                        ZipFile.ExtractToDirectory(i, _PackPath + @"\" + Path.GetFileName(i));
                    }

                    _Load(_PackPath, addPrefixes);
                }
            }

            locs.AddRange(Directory.GetFiles(path, "*.locale", SearchOption.AllDirectories));
            locs.AddRange(Directory.GetFiles(path, "*.locale.information", SearchOption.AllDirectories));
            locs.AddRange(Directory.GetFiles(path, "*.locale.phil", SearchOption.AllDirectories));
            
            
            foreach(string i in locs)
            {
                try
                {
                    Locale l = new Locale(i);

                    if(!Locales._Items.ContainsKey(l.Key))
                    {
                        Locales._Items.Add(l.Key, l);
                    }
                }
                catch(Exception ex) { DebugOp.Dump("LOCDLL00002", ex); }
            }

            if(addPrefixes)
            {
                locs = new List<string>();
                locs.AddRange(Directory.GetFiles(path, "*.lstab", SearchOption.AllDirectories));
                locs.AddRange(Directory.GetFiles(path, "*.lsbtab", SearchOption.AllDirectories));
                locs.AddRange(Directory.GetFiles(path, "*.lsctab", SearchOption.AllDirectories));
                foreach(string i in locs)
                {
                    try
                    {
                        IEncoder enc = Base64.Instance;

                        if(i.ToLower().EndsWith(".lsctab"))
                        {
                            enc = GZip.BASE64;
                        }
                        else if(i.ToLower().EndsWith(".lstab"))
                        {
                            enc = PlainText.Instance;
                        }

                        Ddp cfg = Ddp.Open(i, enc);

                        foreach(string j in cfg.Get<string[]>("lstab/prefixes"))
                        {
                            if(!_UsedPrefixes.Contains(j)) { _UsedPrefixes.Add(j); }
                        }
                    }
                    catch(Exception ex) { DebugOp.Dump("LOCDLL00003", ex); }
                }

                foreach(string i in Directory.GetFiles(path, "*.phil7", SearchOption.AllDirectories))
                {
                    try
                    {
                        string prefixes = File.ReadAllLines(i)[9];
                        prefixes = prefixes.Substring(1, prefixes.Length - 2);

                        foreach(string j in prefixes.Split(','))
                        {
                            if(!(string.IsNullOrWhiteSpace(j)) || _UsedPrefixes.Contains(j.Trim())) { _UsedPrefixes.Add(j.Trim()); }
                        }
                    }
                    catch(Exception ex) { DebugOp.Dump("LOCDLL00073", ex); }
                }
            }
        }


        /// <summary>Parses a PHP-implemented file.</summary>
        /// <param name="fileName">File name.</param>
        /// <returns>Configuration file.</returns>
        private static Ddp _ParsePhil(string fileName)
        {
            Ddp rval = new Ddp();
            string[] lines = File.ReadAllLines(fileName);

            if(fileName.ToLower().EndsWith(".phil7"))
            {
                rval.Set("lstab/version", lines[3].Substring(1, lines[3].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("lstab/name", lines[4].Substring(1, lines[4].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("lstab/application", lines[5].Substring(1, lines[5].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("lstab/author", lines[6].Substring(1, lines[6].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("lstab/lang", lines[7].Substring(1, lines[7].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("lstab/meta", lines[8].Substring(1, lines[8].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("lstab/prefixes", lines[9].Substring(1, lines[9].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));

                for(int i = 11; i < lines.Length; i++)
                {
                    string data = null;
                    if(lines[i].EndsWith("',"))
                    {
                        data = lines[i].Substring(1, lines[i].Length - 3);
                    }
                    else if(lines[i].EndsWith("'"))
                    {
                        data = lines[i].Substring(1, lines[i].Length - 2);
                    }
                    else if(lines[i] == "));")
                    {
                        break;
                    }
                    else continue;

                    string spc_ap = ("" + ((char) 4));
                    while(data.Contains(spc_ap)) { spc_ap += ((char) 4); }
                    data = data.Replace(@"\'", spc_ap);

                    int split = data.IndexOf("' => '");
                    rval.Set("data/" + data.Substring(0, split).Replace(spc_ap, "'").Replace(@"\\", @"\"), data.Substring(split + 6).Replace(spc_ap, "'").Replace(@"\\", @"\"));
                }
            }
            else
            {
                rval.Set("locale/version", lines[3].Substring(1, lines[3].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("locale/name", lines[4].Substring(1, lines[4].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("locale/application", lines[5].Substring(1, lines[5].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("locale/author", lines[6].Substring(1, lines[6].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("locale/lang", lines[7].Substring(1, lines[7].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("locale/meta", lines[8].Substring(1, lines[8].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("locale/base", lines[9].Substring(1, lines[9].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("locale/icon", lines[10].Substring(1, lines[10].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("locale/sicon", lines[11].Substring(1, lines[11].Length - 3).Replace(@"\\", @"\").Replace(@"\'", "'"));
                rval.Set("locale/default", lines[12].ToLower().Trim() == "true");
            }

            return rval;
        }


        /// <summary>Decodes masked characters in a string.</summary>
        /// <param name="s">String.</param>
        /// <returns>Decoded string.</returns>
        private static string _Decode(string s)
        {
            s = _Decode(s, @"\r\n", "\r\n");
            s = _Decode(s, @"\n", "\r\n");
            s = _Decode(s, @"\t", "\t");
            s = _Decode(s, @"\r", "\r");

            return s;
        }


        /// <summary>Decodes masked characters in a string.</summary>
        /// <param name="s">String.</param>
        /// <param name="enc">Encoded characters.</param>
        /// <param name="dec">Decoded characters.</param>
        /// <returns>Decoded string.</returns>
        private static string _Decode(string s, string enc, string dec)
        {
            string msk = "" + ((byte) 4);
            while(s.Contains(msk)) { msk += ((byte) 4); }

            s = s.Replace(@"\" + enc, msk);
            s = s.Replace(enc, dec);
            s = s.Replace(msk, enc);

            return s;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the language key for this locale.</summary>
        public string Key
        {
            get; private set;
        }


        /// <summary>Gets the language name for this locale.</summary>
        public string Name
        {
            get; private set;
        }


        /// <summary>Gets the base keys for this locale.</summary>
        public string[] Base
        {
            get; private set;
        }


        /// <summary>Gets the locale icon file name.</summary>
        public string IconFile
        {
            get; private set;
        }


        /// <summary>Gets the icon for this locale.</summary>
        public object Icon
        {
            get; private set;
        }


        /// <summary>Gets the locale small icon file name.</summary>
        public string SmallIconFile
        {
            get; private set;
        }


        /// <summary>Gets the small icon for this locale.</summary>
        public object SmallIcon
        {
            get; private set;
        }


        /// <summary>Gets if the locale is marked as default.</summary>
        public bool IsDefault
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Selects the locale.</summary>
        public void Select()
        {
            Selected = this;
        }


        /// <summary>Refreshes the locale tables.</summary>
        public void Refresh()
        {
            _InternalTables = new Dictionary<string, Dictionary<string, string>>();
            Dictionary<string, List<Ddp>> tabs = new Dictionary<string, List<Ddp>>();

            foreach(string i in _Paths)
            {
                foreach(string j in Directory.GetFiles(i, "*.lstab", SearchOption.AllDirectories))
                {
                    _GetTable(Ddp.Open(j), tabs);
                }
                foreach(string j in Directory.GetFiles(i, "*.lsbtab", SearchOption.AllDirectories))
                {
                    _GetTable(Ddp.Open(j, Base64.Instance), tabs);
                }
                foreach(string j in Directory.GetFiles(i, "*.lsctab", SearchOption.AllDirectories))
                {
                    _GetTable(Ddp.Open(j, GZip.BASE64), tabs);
                }
                foreach(string j in Directory.GetFiles(i, "*.phil7", SearchOption.AllDirectories))
                {
                    _GetTable(_ParsePhil(j), tabs);
                }
            }

            for(int i = Base.Length - 1; i >= 0; i--)
            {
                if(tabs.ContainsKey(Base[i])) { _LoadTables(tabs[Base[i]]); }
            }

            if(tabs.ContainsKey(Key)) { _LoadTables(tabs[Key]); }
        }


        /// <summary>Looks up a symbol in locale tables and returns a localized expression.</summary>
        /// <param name="symbol">Symbol.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Localized string.</returns>
        public string LookupString(string symbol, string defaultValue = null)
        {
            try
            {
                if(symbol.Contains("|-->"))
                {
                    int l = symbol.IndexOf("|-->");
                    return LookupString(symbol.Substring(0, l), symbol.Substring(l + 4));
                }

                if(defaultValue == null) { defaultValue = symbol; }
                string prefix = "";

                if(symbol.Contains("::"))
                {
                    int l = symbol.IndexOf(':');
                    prefix = symbol.Substring(0, l);
                    symbol = symbol.Substring(l + 2);
                }

                if(_Tables.ContainsKey(prefix))
                {
                    if(_Tables[prefix].ContainsKey(symbol)) { return _Decode(_Tables[prefix][symbol]); }
                }
            }
            catch(Exception) {}

            return defaultValue;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private properties                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the locale tables.</summary>
        private Dictionary<string, Dictionary<string, string>> _Tables
        {
            get
            {
                if(_InternalTables == null) Refresh();

                return _InternalTables;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reads a table from a file to the temporary table collection.</summary>
        /// <param name="cfg">Configuration file.</param>
        /// <param name="tabs">Temporary table collection.</param>
        private void _GetTable(Ddp cfg, Dictionary<string, List<Ddp>> tabs)
        {
            string key = cfg["/lstab/lang"];
            if((Key == key) || (Base.Contains(key)))
            {
                if(!tabs.ContainsKey(key)) { tabs.Add(key, new List<Ddp>()); }
                tabs[key].Add(cfg);
            }
        }


        /// <summary>Loads tables.</summary>
        /// <param name="cfgs">List of configuration files.</param>
        private void _LoadTables(List<Ddp> cfgs)
        {
            foreach(Ddp i in cfgs) { _LoadTable(i); }
        }


        /// <summary>Loads a table.</summary>
        /// <param name="cfg">Configuration file.</param>
        private void _LoadTable(Ddp cfg)
        {
            List<string> prefixes = new List<string>();

            foreach(string i in cfg.Get<string[]>("lstab/prefixes"))
            {
                if(string.IsNullOrWhiteSpace(i))
                {
                    if(!prefixes.Contains("")) { prefixes.Add(""); }
                }
                else if(_UsedPrefixes.Count > 0)
                {
                    if((_UsedPrefixes.Contains(i)) && (!prefixes.Contains(i))) { prefixes.Add(i); }
                }
                else if(!prefixes.Contains(i)) { prefixes.Add(i); }
            }
            if(prefixes.Count == 0) { prefixes.Add(""); }

            foreach(string i in prefixes)
            {
                if(!_InternalTables.ContainsKey(i)) { _InternalTables.Add(i, new Dictionary<string, string>()); }
            }

            foreach(DdpElement i in cfg["data"].Values)
            {
                foreach(string j in prefixes)
                {
                    if(_InternalTables[j].ContainsKey(i.Name))
                    {
                        _InternalTables[j][i.Name] = i.Value;
                    }
                    else { _InternalTables[j].Add(i.Name, i.Value); }
                }
            }
        }


        /// <summary>Loads icon images.</summary>
        private void _GetIcons()
        {
            Icon = SmallIcon = null;

            try
            {
                if(_GetImages == null)
                {
                    Type l = ClassOp.LoadType("Robbiblubber.Util.Localization.Controls.Internal.__LocaleImages");
                    _GetImages = l.GetMethod("GetImages");
                }

                if(_GetImages != null)
                {
                    object[] icons = (object[]) _GetImages.Invoke(null, new object[] { SmallIconFile, IconFile });

                    Icon = icons[0];
                    SmallIcon = icons[1];
                }
            }
            catch(Exception) {}
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // operators                                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Determines if two instances are equal.</summary>
        /// <param name="a">Locale.</param>
        /// <param name="b">Locale.</param>
        /// <returns>Returns TRUE if the objects are equal, otherwise returns FALSE.</returns>
        public static bool operator ==(Locale a, Locale b)
        {
            if(((object) a) == null) { return (((object) b) == null); }
            if(((object) b) == null) return false;

            return a.Key == b.Key;
        }


        /// <summary>Determines if two instances are not equal.</summary>
        /// <param name="a">Locale.</param>
        /// <param name="b">Locale.</param>
        /// <returns>Returns FALSE if the objects are equal, otherwise returns TRUE.</returns>
        public static bool operator !=(Locale a, Locale b)
        {
            if(((object) a) == null) { return (((object) b) != null); }
            if(((object) b) == null) return true;

            return a.Key != b.Key;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Determines if an object is equal to this instance.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Returns TRUE if the object is equal, otherwise returns FALSE.</returns>
        public override bool Equals(object obj)
        {
            if(obj == null) return false;

            if(obj is Locale)
            {
                return (((Locale) obj).Key == Key);
            }

            return false;
        }



        /// <summary>Gets a hash code for this instance.</summary>
        /// <returns>Hash.</returns>
        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }


        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return Name;
        }
    }
}
