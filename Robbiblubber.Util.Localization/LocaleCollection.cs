﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Util.Collections;



namespace Robbiblubber.Util.Localization
{
    /// <summary>This class represents a list of locales.</summary>
    public sealed class LocaleCollection: IImmutableList<Locale>, ICollection, IEnumerable<Locale>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal members                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Items.</summary>
        internal Dictionary<string, Locale> _Items = new Dictionary<string, Locale>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        internal LocaleCollection()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an element with a given key.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Element.</returns>
        public Locale this[string key]
        {
            get
            {
                if(string.IsNullOrWhiteSpace(key)) { return null; }
                
                if(_Items.ContainsKey(key)) { return _Items[key]; }
                foreach(Locale i in _Items.Values) { if(i.Key.ToLower().StartsWith(key.ToLower())) return i; }

                return null;
            }
        }


        /// <summary>Gets an array of the keys for all locales in this list.</summary>
        public string[] Keys
        {
            get { return _Items.Keys.ToArray(); }
        }


        /// <summary>Gets the default locale.</summary>
        public Locale Default
        {
            get
            {
                Locale rval = null;

                foreach(Locale i in _Items.Values)
                {
                    if(i.IsDefault) { rval = i; break; }
                    if(rval == null) { rval = i; }
                }

                return rval;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IImmutableList<Locale>                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an element with a given index.</summary>
        /// <param name="index">Index.</param>
        /// <returns>Element.</returns>
        public Locale this[int index]
        {
            get
            {
                try
                {
                    return _Items.Values.ElementAt(index);
                }
                catch(Exception) {}

                return null;
            }
        }


        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        public bool Contains(Locale item)
        {
            return _Items.Values.Contains(item);
        }


        /// <summary>Gets the index of an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Index.</returns>
        public int IndexOf(Locale item)
        {
            int n = 0;
            foreach(Locale i in _Items.Values)
            {
                if(i == item) { return n; }
                n++;
            }

            return -1;
        }


        /// <summary>Gets the number of locales in this list.</summary>
        public int Count
        {
            get { return _Items.Count; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICollection                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a synchronization object.</summary>
        object ICollection.SyncRoot
        {
            get { return ((ICollection) _Items).SyncRoot; }
        }


        /// <summary>Gets if the object is synchronized.</summary>
        bool ICollection.IsSynchronized
        {
            get { return ((ICollection) _Items).IsSynchronized; }
        }


        /// <summary>Copies the elements to an array.</summary>
        /// <param name="array">Array.</param>
        /// <param name="index">Index.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            ((ICollection) _Items).CopyTo(array, index);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.Values.OrderBy(x => x.Name).GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<Locale>                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<Locale> IEnumerable<Locale>.GetEnumerator()
        {
            return _Items.Values.OrderBy(x => x.Name).GetEnumerator();
        }
    }
}
