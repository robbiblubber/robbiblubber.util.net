﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;



namespace Robbiblubber.Util.Dynamic
{
    /// <summary>This class provides a event handler list for members.</summary>
    /// <typeparam name="T">Type.</typeparam>
    /// <typeparam name="TEventHandler">Event handler type.</typeparam>
    public class __InterceptionEventList<T, TEventHandler> where TEventHandler: Delegate
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>List of event handlers.</summary>
        private Dictionary<MemberInfo, List<TEventHandler>> _Handlers;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public __InterceptionEventList()
        {
            Type t = typeof(T);
            _Handlers = new Dictionary<MemberInfo, List<TEventHandler>>();

            foreach(MemberInfo i in t.GetMethods(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance))
            {
                _Handlers.Add(i, new List<TEventHandler>());
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the event handler list for a member.</summary>
        /// <param name="member">Member.</param>
        /// <returns>Event handlers.</returns>
        public List<TEventHandler> this[MemberInfo member]
        {
            get { return _Handlers[member]; }
        }


        /// <summary>Adds an event handler to this class.</summary>
        /// <param name="handler">Event handler.</param>
        /// <param name="members">Members.</param>
        public void Add(TEventHandler handler, IEnumerable<MemberInfo> members)
        {
            if((members == null) || (members.Count() == 0)) { members = _Handlers.Keys; }
            foreach(MemberInfo i in members) 
            {
                if(i is PropertyInfo)
                {
                    _Handlers[((PropertyInfo) i).GetGetMethod(true)].Add(handler);
                    _Handlers[((PropertyInfo) i).GetSetMethod(true)].Add(handler);
                }
                else
                {
                    _Handlers[(MethodInfo) i].Add(handler);
                }
            }
        }


        /// <summary>Removes an event handler from this class.</summary>
        /// <param name="handler">Event handler.</param>
        /// <param name="members">Members.</param>
        public void Remove(TEventHandler handler, IEnumerable<MemberInfo> members)
        {
            if((members == null) || (members.Count() == 0)) { members = _Handlers.Keys; }
            foreach(MemberInfo i in members)
            {
                if(i is PropertyInfo)
                {
                    if(_Handlers[((PropertyInfo) i).GetGetMethod(true)].Contains(handler)) { _Handlers[((PropertyInfo) i).GetGetMethod(true)].Remove(handler); }
                    if(_Handlers[((PropertyInfo) i).GetSetMethod(true)].Contains(handler)) { _Handlers[((PropertyInfo) i).GetSetMethod(true)].Remove(handler); }
                }
                else
                {
                    if(_Handlers[(MethodInfo) i].Contains(handler)) { _Handlers[(MethodInfo) i].Remove(handler); }
                }
            }
        }
    }
}
