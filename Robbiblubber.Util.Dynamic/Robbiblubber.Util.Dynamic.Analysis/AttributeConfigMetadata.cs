﻿using System;



namespace Robbiblubber.Util.Dynamic.Analysis
{
    /// <summary>This class provides attribute configuration metadata.</summary>
    public sealed class AttributeConfigMetadata
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Default attribute metadata.</summary>
        public static readonly AttributeConfigMetadata DEFAULT = new AttributeConfigMetadata(false, false, null, null);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="hidden">Hidden flag.</param>
        /// <param name="suppressed">Suppressed flag.</param>
        /// <param name="refs">Referenced members.</param>
        /// <param name="defaults">Member defaults.</param>
        internal AttributeConfigMetadata(bool hidden, bool suppressed, string[] refs, string[] defaults)
        {
            if(Suppressed = suppressed)
            {
                Hidden = true;
            }
            else { Hidden = hidden; }

            ReferencedProperties = refs;

            if(refs != null)
            {
                Defaults = new string[refs.Length];
                for(int i = 0; i < refs.Length; i++)
                {
                    if(i < defaults.Length)
                    {
                        Defaults[i] = defaults[i];
                    }
                    else { Defaults[i] = "-+---"; }
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the attribute is hidden.</summary>
        /// <remarks>Hidden attributes do not occur in the item declaration.</remarks>
        public bool Hidden
        {
            get; private set;
        }


        /// <summary>Gets if the attribute is suppressed.</summary>
        /// <remarks>Suppressed attributes do not occur in the item declaration or attribute list.</remarks>
        public bool Suppressed
        {
            get; private set;
        }


        /// <summary>Gets the referenced property names.</summary>
        public string[] ReferencedProperties
        {
            get; private set;
        }


        /// <summary>Gets the referenced property defaults.</summary>
        public string[] Defaults
        {
            get; private set;
        }


        /// <summary>Gets if attribute metadata are defined.</summary>
        public bool IsDefined
        {
            get { return ReferencedProperties != null; }
        }
    }
}
