﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;



namespace Robbiblubber.Util.Dynamic.Analysis
{
    /// <summary>This class provides extension methods for member analysis.</summary>
    public static class MemberAnalysis
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the member visibility.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Visibility.</returns>
        public static MemberVisibility GetVisibility(this MemberInfo mem)
        {
            if(mem is PropertyInfo)
            {
                mem = ((PropertyInfo) mem).BaseMethod();
            }

            if(mem.GetPropertyValue("IsVirtual", false) && mem.GetPropertyValue("IsFinal", false))
            {
                return MemberVisibility.PUBLIC_INTERFACE_IMPLEMENTATION;
            }

            if(mem is EventInfo)
            {
                mem = ((EventInfo) mem).AddMethod;
            }

            if(mem.GetPropertyValue("IsPublic", false) || mem.GetPropertyValue("IsNestedPublic", false))
            {
                return MemberVisibility.PUBLIC;
            }
            else if(mem.GetPropertyValue("IsFamilyOrAssembly", false) || mem.GetPropertyValue("IsNestedFamORAssem", false))
            {
                return MemberVisibility.PROTECTED_INTERNAL;
            }
            else if(mem.GetPropertyValue("IsFamilyAndAssembly", false) || mem.GetPropertyValue("IsNestedFamANDAssem", false))
            {
                return MemberVisibility.PROTECTED_PRIVATE;
            }
            else if(mem.GetPropertyValue("IsFamily", false) || mem.GetPropertyValue("IsNestedFamily", false))
            {
                return MemberVisibility.PROTECTED;
            }
            else if(mem.GetPropertyValue("IsAssembly", false) || mem.GetPropertyValue("IsNestedAssembly", false))
            {
                return MemberVisibility.INTERNAL;
            }

            return MemberVisibility.PRIVATE;
        }


        /// <summary>Gets if the member is a indexer property.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Returns TRUE for indexers, otherwise returns FALSE.</returns>
        public static bool IsIndexer(this MemberInfo mem)
        {
            if(mem is PropertyInfo)
            {
                return (((PropertyInfo) mem).GetIndexParameters().Length > 0);
            }

            return false;
        }


        /// <summary>Returns if the attribute is inherited from the base type.</summary>
        /// <param name="attr">Attribure.</param>
        /// <param name="m">Member.</param>
        /// <returns>Returns TRUE if the attribute is inherited, otherwise returns FALSE.</returns>
        public static bool IsInheritedAttributeIn(this Attribute attr, MemberInfo m)
        {
            if(m is FieldInfo) { return ((FieldInfo) m).IsInherited(); }

            Type t = ((m is Type) ? ((Type) m) : m.ReflectedType);
            while(true)
            {
                if((t = t.BaseType) == null) return false;

                try
                {
                    if(m is Type)
                    {
                        if(t.GetCustomAttribute(attr.GetType()) != null) return true;
                    }
                    else if(m is MethodInfo)
                    {
                        MethodInfo n = t.GetMethod(m.Name, ((MethodInfo) m).GetParameters().GetParameterTypes());
                        if((n != null) && (n.GetCustomAttribute(attr.GetType()) != null)) return true;
                    }
                    else if(m is PropertyInfo)
                    {
                        PropertyInfo n;

                        if(((PropertyInfo) m).GetIndexParameters().Length == 0)
                        {
                            n = t.GetProperty(m.Name, ((PropertyInfo) m).PropertyType);
                        }
                        else
                        {
                            n = t.GetProperty(m.Name, ((PropertyInfo) m).PropertyType, ((PropertyInfo) m).GetIndexParameters().GetParameterTypes());
                        }

                        if((n != null) && (n.GetCustomAttribute(attr.GetType()) != null)) return true;
                    }
                    else if(m is EventInfo)
                    {
                        EventInfo n = t.GetEvent(m.Name);
                        if((n != null) && (n.GetCustomAttribute(attr.GetType()) != null)) return true;
                    }
                    else { return false; }
                }
                catch(AmbiguousMatchException) { return true; }
            }
        }


        /// <summary>Returns if the method is implemented in its class.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Returns TRUE if the method is implemented, otherwise returns FALSE.</returns>
        public static bool IsImplemented(this MemberInfo mem)
        {
            return (mem.DeclaringType == mem.ReflectedType);
        }


        /// <summary>Returns if the member is declared in a base class.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Returns TRUE if the method is ihnerited, otherwise returns FALSE.</returns>
        public static bool IsInherited(this MemberInfo mem)
        {
            return mem.GetDefiningType() != mem.ReflectedType;
        }


        /// <summary>Gets the underlying method for an event or property.</summary>
        /// <param name="mem">Property.</param>
        /// <returns>Get or set method.</returns>
        /// <remarks>This method returns the most visible get or set method for a property, or the add method for an event.</remarks>
        public static MethodInfo GetUnderlyingMethod(this MemberInfo mem)
        {
            if(mem is MethodInfo)
            {
                return (MethodInfo) mem;
            }
            else if(mem is EventInfo)
            {
                return ((EventInfo) mem).AddMethod;
            }
            if(mem is PropertyInfo)
            {
                if(((PropertyInfo) mem).GetMethod == null) { return ((PropertyInfo) mem).SetMethod; }
                if(((PropertyInfo) mem).SetMethod == null) { return ((PropertyInfo) mem).GetMethod; }

                if(((PropertyInfo) mem).GetMethod.IsPublic) { return ((PropertyInfo) mem).GetMethod; }
                if(((PropertyInfo) mem).SetMethod.IsPublic) { return ((PropertyInfo) mem).SetMethod; }

                if(((PropertyInfo) mem).GetMethod.IsFamilyOrAssembly) { return ((PropertyInfo) mem).GetMethod; }
                if(((PropertyInfo) mem).SetMethod.IsFamilyOrAssembly) { return ((PropertyInfo) mem).SetMethod; }

                if(((PropertyInfo) mem).GetMethod.IsFamily) { return ((PropertyInfo) mem).GetMethod; }
                if(((PropertyInfo) mem).SetMethod.IsFamily) { return ((PropertyInfo) mem).SetMethod; }

                if(((PropertyInfo) mem).GetMethod.IsAssembly) { return ((PropertyInfo) mem).GetMethod; }
                if(((PropertyInfo) mem).SetMethod.IsAssembly) { return ((PropertyInfo) mem).SetMethod; }

                return ((PropertyInfo) mem).GetMethod;
            }

            return null;
        }


        /// <summary>Gets the base implementation for a member.</summary>
        /// <param name="m">Member.</param>
        /// <returns>Base implementation.</returns>
        public static MemberInfo GetMemberBaseImplementation(this MemberInfo m)
        {
            if(m is MethodInfo)   { return ((MethodInfo) m).GetBaseImplementation(); }
            if(m is PropertyInfo) { return ((PropertyInfo) m).GetBaseImplementation(); }
            if(m is EventInfo)    { return ((EventInfo) m).GetBaseImplementation(); }
            if(m is FieldInfo)    { return ((FieldInfo) m).GetBaseImplementation(); }

            return m;
        }


        /// <summary>Gets the base implementation for a method.</summary>
        /// <param name="m">Method.</param>
        /// <returns>Base implementation.</returns>
        public static MethodInfo GetBaseImplementation(this MethodBase m)
        {
            return m.GetDefiningType().GetMethod(m.Name, m.GetParameters().GetParameterTypes());
        }


        /// <summary>Gets the base implementation for a method.</summary>
        /// <param name="p">Property.</param>
        /// <returns>Base implementation.</returns>
        public static PropertyInfo GetBaseImplementation(this PropertyInfo p)
        {
            return ((p.GetMethod == null) ? p.SetMethod : p.GetMethod).GetDefiningType().GetProperty(p.Name, p.PropertyType, p.GetIndexParameters().GetParameterTypes());
        }


        /// <summary>Gets the base implementation for a method.</summary>
        /// <param name="ev">Event.</param>
        /// <returns>Base implementation.</returns>
        public static EventInfo GetBaseImplementation(this EventInfo ev)
        {
            return ev.AddMethod.GetDefiningType().GetEvent(ev.Name);
        }


        /// <summary>Gets the base implementation for a method.</summary>
        /// <param name="f">Field.</param>
        /// <returns>Base implementation.</returns>
        public static FieldInfo GetBaseImplementation(this FieldInfo f)
        {
            return f.DeclaringType.GetField(f.Name);
        }


        /// <summary>Gets the parameter types for a set of parameters.</summary>
        /// <param name="p">Parameters.</param>
        /// <returns>Array of parameter types.</returns>
        public static Type[] GetParameterTypes(this ParameterInfo[] p)
        {
            Type[] rval = new Type[p.Length];
            for(int i = 0; i < rval.Length; i++) { rval[i] = p[i].ParameterType; }

            return rval;
        }


        /// <summary>Gets a corresponding method for a method in the type base type.</summary>
        /// <param name="t">Type.</param>
        /// <param name="m">Method.</param>
        /// <returns></returns>
        public static MethodInfo GetBaseTypeMethod(this Type t, MethodBase m)
        {
            return t.BaseType.GetMethod(m.Name, m.GetParameters().GetParameterTypes());
        }


        /// <summary>Gets the type that initially declared the member.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Declaring type.</returns>
        public static Type GetDefiningType(this MemberInfo mem)
        {
            if(mem is PropertyInfo)
            {
                mem = ((PropertyInfo) mem).BaseMethod();
            }
            else if(mem is EventInfo) { mem = ((EventInfo) mem).AddMethod; }

            if(!(mem is MethodInfo))
            {
                return mem.DeclaringType;
            }

            Type rval = mem.ReflectedType;

            while(rval.BaseType != null)
            {
                if(rval.GetBaseTypeMethod((MethodInfo) mem) == null) { return rval; }
                rval = rval.BaseType;
            }

            return rval;
        }


        /// <summary>Returns if the member is non-virtual inherited that is re-implemented in its class.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Returns TRUE if the member is new, otherwise returns FALSE.</returns>
        public static bool IsNew(this MemberInfo mem)
        {
            if(mem is PropertyInfo)
            {
                mem = ((PropertyInfo) mem).BaseMethod();
            }
            else if(mem is EventInfo) { mem = ((EventInfo) mem).AddMethod; }

            if(!(mem is MethodInfo)) return false;

            if(((MethodInfo) mem).IsVirtual || (!mem.IsImplemented())) return false;

            Type t = mem.ReflectedType;
            while(true)
            {
                if((t = t.BaseType) == null) return false;
                if(t.GetMethod(mem.Name, ((MethodInfo) mem).GetParameters().GetParameterTypes()) != null) return true;
            }
        }


        /// <summary>Returns if the member is inherited and overridden in its class.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Returns TRUE if the member is overridden, otherwise returns FALSE.</returns>
        public static bool IsOverride(this MemberInfo mem)
        {
            if(mem is PropertyInfo)
            {
                mem = ((PropertyInfo) mem).BaseMethod();
            }
            else if(mem is EventInfo) { mem = ((EventInfo) mem).AddMethod; }

            if(!(mem is MethodInfo)) return false;

            return (mem.IsInherited() && mem.IsImplemented() && ((MethodInfo) mem).IsVirtual);
        }


        /// <summary>Returns if the member is new or overridden in its class.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Returns TRUE if the member is new or overridden, otherwise returns FALSE.</returns>
        public static bool IsNewOrOverride(this MemberInfo mem)
        {
            return (mem.IsNew() || mem.IsOverride());
        }


        /// <summary>Returns if the interface is inherited from the base type.</summary>
        /// <param name="intfc">Interface.</param>
        /// <param name="t">Type.</param>
        /// <returns>Returns TRUE if the interface is inherited, otherwise returns FALSE.</returns>
        public static bool IsInheritedInterfaceIn(this Type intfc, Type t)
        {
            if(t.BaseType == null) return false;

            return (t.BaseType.GetInterfaces().Contains(intfc));
        }


        /// <summary>Returns the member modifiers.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Modifiers.</returns>
        public static MemberModifiers GetModifiers(this MemberInfo mem)
        {
            if(mem is Type)
            {
                if(((Type) mem).IsInterface) { return MemberModifiers.NONE; }
                if(((Type) mem).IsEnum) { return MemberModifiers.NONE; }
                if(typeof(Delegate).IsAssignableFrom((Type) mem)) { return MemberModifiers.NONE; }

                if(((Type) mem).IsAbstract)
                {
                    return (((Type) mem).IsSealed ? MemberModifiers.STATIC : MemberModifiers.ABSTRACT);
                }
                else if(((Type) mem).IsSealed)
                {
                    return (typeof(ValueType).IsAssignableFrom(((Type) mem)) ? MemberModifiers.NONE : MemberModifiers.SEALED);
                }
                else
                {
                    return MemberModifiers.NONE;
                }
            }

            if(mem is ConstructorInfo)
            {
                return MemberModifiers.NONE;
            }

            if(mem is FieldInfo)
            {
                if((mem is FieldInfo) && (typeof(Enum).IsAssignableFrom(mem.ReflectedType))) { return MemberModifiers.NONE; }
                if(((FieldInfo) mem).IsLiteral) return MemberModifiers.CONST;
                if(((FieldInfo) mem).IsStatic)
                {
                    return (((FieldInfo) mem).IsInitOnly ? MemberModifiers.STATIC_READONLY : MemberModifiers.STATIC);
                }

                return (((FieldInfo) mem).IsInitOnly ? MemberModifiers.READONLY : MemberModifiers.NONE);
            }

            if(mem is PropertyInfo)
            {
                mem = ((PropertyInfo) mem).BaseMethod();
            }
            else if(mem is EventInfo) { mem = ((EventInfo) mem).AddMethod; }
            
            if(((MethodBase) mem).IsStatic) { return MemberModifiers.STATIC; }
            if(((MethodBase) mem).IsAbstract)
            {
                return (mem.ReflectedType.IsInterface ? MemberModifiers.NONE : MemberModifiers.ABSTRACT);
            }

            if(((MethodBase) mem).IsVirtual)
            {
                return (((MethodBase) mem).IsOverride() ? MemberModifiers.OVERRIDE : MemberModifiers.VIRTUAL);
            }

            return (((MethodBase) mem).IsNew() ? MemberModifiers.NEW: MemberModifiers.NONE);
        }
        
        
        /// <summary>Gets the most visible accessor method for a property.</summary>
        /// <param name="prop">Property.</param>
        /// <returns>Accessor method.</returns>
        public static MethodInfo BaseMethod(this PropertyInfo prop)
        {
            if(prop.GetMethod == null) { return prop.SetMethod; }
            if(prop.SetMethod == null) { return prop.GetMethod; }

            if(prop.GetMethod.IsPublic)
            {
                return prop.GetMethod;
            }
            else if(prop.SetMethod.IsPublic) { return prop.SetMethod; }

            if(prop.GetMethod.IsFamilyOrAssembly)
            {
                return prop.GetMethod;
            }
            else if(prop.SetMethod.IsFamilyOrAssembly) { return prop.SetMethod; }

            if(prop.GetMethod.IsFamily)
            {
                return prop.GetMethod;
            }
            else if(prop.SetMethod.IsFamily) { return prop.SetMethod; }

            if(prop.GetMethod.IsAssembly)
            {
                return prop.GetMethod;
            }
            else if(prop.SetMethod.IsAssembly) { return prop.SetMethod; }

            if(prop.GetMethod.IsFamilyAndAssembly)
            {
                return prop.GetMethod;
            }
            else if(prop.SetMethod.IsFamilyAndAssembly) { return prop.SetMethod; }

            return prop.GetMethod;
        }


        /// <summary>Returns if a method has a return value.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Return TRUE if the method has a return value, otherwie returns FALSE.</returns>
        public static bool HasReturnValue(this MethodBase mem)
        {
            if(!(mem is MethodInfo)) { return false; }
            return (((MethodInfo) mem).ReturnType != typeof(void));
        }


        /// <summary>Returns a marshalled value expression for an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Value expression</returns>
        public static string MarshalledValue(this object obj)
        {
            return obj.MarshalledValue().ToString();
        }


        /// <summary>Gets a minimal set of interfaces for a type.</summary>
        /// <param name="t">Type.</param>
        /// <returns>Interfaces.</returns>
        public static Type[] GetEssentialInterfaces(this Type t)
        {
            List<Type> rval = new List<Type>();

            foreach(Type i in t.GetInterfaces())
            {
                if(i.IsInheritedInterfaceIn(t)) continue;

                bool isEssential = true;
                foreach(Type k in t.GetInterfaces())
                {
                    if(k.GetInterfaces().Contains(i)) { isEssential = false; break; }
                }

                if(isEssential) { rval.Add(i); }
            }

            return rval.ToArray();
        }


        /// <summary>Gets all generic arguments that are defined in this member.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Returns the generic arguments that are defined in the member.</returns>
        /// <remarks>While GetGenericArguments() returns all generic arguments, including those from generic parent classes, this method returns only those declared in the reflected member.</remarks>
        public static Type[] GetLocalGenericArguments(this MemberInfo mem)
        {
            List<Type> rval = new List<Type>();
            Type[] gens;
            Type[] pgens;
            Type par = mem.DeclaringType;

            if(mem is MethodInfo)
            {
                gens = ((MethodInfo) mem).GetGenericArguments();
            }
            else if(mem is Type)
            {
                gens = ((Type) mem).GetGenericArguments();
            }
            else { return new Type[0]; }

            if(par == null)
            {
                return gens;
            }

            pgens = par.GetGenericArguments();
            foreach(Type i in gens)
            {
                bool ok = true;
                foreach(Type k in pgens)
                {
                    if(k.Name == i.Name) { ok = false; break; }
                }

                if(ok) rval.Add(i);
            }

            return rval.ToArray();
        }
    }
}
