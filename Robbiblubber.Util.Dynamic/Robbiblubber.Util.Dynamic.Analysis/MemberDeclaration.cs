﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

using Robbiblubber.Util.Dynamic.CodeMarkup;



namespace Robbiblubber.Util.Dynamic.Analysis
{
    /// <summary>This class provides extension methods for member declarations.</summary>
    public static class MemberDeclaration
    {
        /// <summary>Returns a marshalled value expression for an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Value expression</returns>
        public static BDeclaration MarshalledValueDecl(this object obj)
        {
            BDeclaration rval = new BDeclaration();

            if(obj == null)
            {
                rval.Append(BKind.KEYWORD, "null");
            }
            else if(obj.GetType() == typeof(string))
            {
                rval.Append(BKind.STRING, '"' + ((string) obj) + '"');
            }
            else if(obj.GetType() == typeof(char))
            {
                rval.Append(BKind.STRING, "'" + ((char) obj) + "'");
            }
            else if(obj.GetType().IsSubclassOf(typeof(Enum)))
            {
                rval.Append(obj.GetType().CommonNameDecl());
                rval.Append(".");
                rval.Append(BKind.IDENTIFIER, obj.ToString());
            }
            else if(obj.GetType() == typeof(bool))
            {
                rval.Append(BKind.KEYWORD, ((bool) obj) ? "true" : "false");
            }
            else if(obj.GetType() == typeof(Type))
            {
                rval.Append(BKind.KEYWORD, "typeof");
                rval.Append("(");
                rval.Append(((Type) obj).CommonNameDecl());
                rval.Append(")");
            }
            else
            {
                rval.Append(BKind.LITERAL, obj.ToString());
            }

            return rval;
        }


        /// <summary>Gets the type common name.</summary>
        /// <param name="t">Type.</param>
        /// <param name="ignoreRef">Determines if the reference sign will be ignored.</param>
        /// <param name="parentTypes">Parent type.</param>
        /// <param name="additionalParentTypes">Additional parent types.</param>
        /// <returns>Type name.</returns>
        public static BDeclaration CommonNameDecl(this Type t, bool ignoreRef, Type[] parentTypes, params Type[] additionalParentTypes)
        {
            List<Type> rval = new List<Type>(parentTypes);
            rval.AddRange(additionalParentTypes);

            return t.CommonNameDecl(ignoreRef, false, rval.ToArray());
        }


        /// <summary>Gets the type common name.</summary>
        /// <param name="t">Type.</param>
        /// <param name="parentTypes">Parent type.</param>
        /// <param name="additionalParentTypes">Additional parent types.</param>
        /// <returns>Type name.</returns>
        public static BDeclaration CommonNameDecl(this Type t, Type[] parentTypes, params Type[] additionalParentTypes)
        {
            return t.CommonNameDecl(true, parentTypes, additionalParentTypes);
        }


        /// <summary>Gets the type common name.</summary>
        /// <param name="t">Type.</param>
        /// <param name="parentTypes">Parent type.</param>
        /// <returns>Type name.</returns>
        public static BDeclaration CommonNameDecl(this Type t, params Type[] parentTypes)
        {
            return t.CommonNameDecl(true, false, parentTypes);
        }


        /// <summary>Gets the type common name.</summary>
        /// <param name="t">Type.</param>
        /// <param name="ignoreRef">Determines if the reference sign will be ignored.</param>
        /// <param name="fullGenericNames">Determines if generic type names will be fully displayed.</param>
        /// <param name="parentTypes">Parent type.</param>
        /// <returns>Type name.</returns>
        public static BDeclaration CommonNameDecl(this Type t, bool ignoreRef, bool fullGenericNames, params Type[] parentTypes)
        {
            if(t == null)
            {
                return new BDeclaration(BKind.KEYWORD, "null");
            }

            BDeclaration rval = null;

            if(t.Name.Contains("&"))
            {
                rval = t.GetElementType().CommonNameDecl();
                if(!ignoreRef) { rval.Append("&"); }

                return rval;
            }

            if(parentTypes == null) { parentTypes = new Type[0]; }

            if(t.FullName != null)
            {
                switch(t.FullName)
                {
                    case "System.Void": return new BDeclaration(BKind.KEYWORD, "void");
                    case "System.Object": return new BDeclaration(BKind.KEYWORD, "object");
                    case "System.Char": return new BDeclaration(BKind.KEYWORD, "char");
                    case "System.String": return new BDeclaration(BKind.KEYWORD, "string");
                    case "System.Boolean": return new BDeclaration(BKind.KEYWORD, "bool");
                    case "System.Byte": return new BDeclaration(BKind.KEYWORD, "byte");
                    case "System.SByte": return new BDeclaration(BKind.KEYWORD, "sbyte");
                    case "System.Int16": return new BDeclaration(BKind.KEYWORD, "short");
                    case "System.UInt16": return new BDeclaration(BKind.KEYWORD, "ushort");
                    case "System.Int32": return new BDeclaration(BKind.KEYWORD, "int");
                    case "System.UInt32": return new BDeclaration(BKind.KEYWORD, "uint");
                    case "System.Int64": return new BDeclaration(BKind.KEYWORD, "long");
                    case "System.UInt64": return new BDeclaration(BKind.KEYWORD, "ulong");
                    case "System.Float": return new BDeclaration(BKind.KEYWORD, "float");
                    case "System.Double": return new BDeclaration(BKind.KEYWORD, "double");
                    case "System.Decimal": return new BDeclaration(BKind.KEYWORD, "decimal");
                }
            }

            rval = new BDeclaration();

            if(t.IsArray)
            {
                rval.Append(t.GetElementType().CommonNameDecl(ignoreRef, parentTypes, t));
                rval.Append("[]");

                return rval;
            }

            if(t.IsNested && (!t.IsGenericParameter))
            {
                bool hasPrefix = true;

                foreach(Type i in parentTypes)
                {
                    if(t.DeclaringType.IsAssignableFrom(i)) { hasPrefix = false; break; }
                }

                if(hasPrefix)
                {
                    rval.Append(t.DeclaringType.CommonNameDecl(ignoreRef, parentTypes, t));
                    rval.Append(".");
                }
            }

            rval.Append(fullGenericNames ? t.GetFullNameDecl() : t.GetSimpleNameDecl());

            for(int i = 0; i < t.GenericTypeArguments.Length; i++)
            {
                if(i == 0)
                {
                    rval.Append("<");
                }
                else { rval.Append(", "); }

                rval.Append(t.GenericTypeArguments[i].CommonNameDecl(parentTypes, t));

                if(i == t.GenericTypeArguments.Length - 1) { rval.Append(">"); }
            }

            return rval;
        }


        /// <summary>Returns the visibility modifier for a member.</summary>
        /// <param name="mem">Member.</param>
        /// <param name="addBlank">Determines if a blank as appended if a modifier exists.</param>
        /// <returns>Visibility modifier declaration.</returns>
        public static BDeclaration GetVisibilityDecl(this MemberInfo mem, bool addBlank = false)
        {
            if((mem is FieldInfo) && (typeof(Enum).IsAssignableFrom(mem.ReflectedType)))
            {
                return new BDeclaration();
            }

            BDeclaration rval = new BDeclaration();

            switch(mem.GetVisibility())
            {
                case MemberVisibility.PUBLIC:
                    rval.Append(BKind.KEYWORD, "public"); break;
                case MemberVisibility.PROTECTED_INTERNAL:
                    rval.Append(BKind.KEYWORD, "protected");
                    rval.AppendBlank();
                    rval.Append(BKind.KEYWORD, "internal"); break;
                case MemberVisibility.PROTECTED:
                    rval.Append(BKind.KEYWORD, "protected"); break;
                case MemberVisibility.INTERNAL:
                    rval.Append(BKind.KEYWORD, "internal"); break;
                case MemberVisibility.PROTECTED_PRIVATE:
                    rval.Append(BKind.KEYWORD, "protected");
                    rval.AppendBlank();
                    rval.Append(BKind.KEYWORD, "private"); break;
                case MemberVisibility.PRIVATE:
                    rval.Append(BKind.KEYWORD, "private"); break;
                default:
                    return rval;
            }
            if(addBlank) { rval.AppendBlank(); }

            return rval;
        }


        /// <summary>Returns the member abstracts.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Member abstracts.</returns>
        /// <remarks>The member abstracts is the declaration without modifiers, return value, parameter names, and property accessors.</remarks>
        public static BDeclaration GetAbstractsDecl(this MemberInfo mem)
        {
            return GetDeclarationDecl(mem, false, false, false, false);
        }


        /// <summary>Returns the modifiers for a member.</summary>
        /// <param name="mem">Member.</param>
        /// <param name="addBlank">Determines if a blank as appended if a modifier exists.</param>
        /// <returns>Modifier declaration.</returns>
        public static BDeclaration GetModifiersDecl(this MemberInfo mem, bool addBlank = false)
        {
            BDeclaration rval = new BDeclaration();

            switch(mem.GetModifiers())
            {
                case MemberModifiers.ABSTRACT:
                    rval.Append(BKind.KEYWORD, "abstract"); break;
                case MemberModifiers.CONST:
                    rval.Append(BKind.KEYWORD, "const"); break;
                case MemberModifiers.NEW:
                    rval.Append(BKind.KEYWORD, "new"); break;
                case MemberModifiers.OVERRIDE:
                    rval.Append(BKind.KEYWORD, "override"); break;
                case MemberModifiers.READONLY:
                    rval.Append(BKind.KEYWORD, "readonly"); break;
                case MemberModifiers.SEALED:
                    rval.Append(BKind.KEYWORD, "sealed"); break;
                case MemberModifiers.STATIC:
                    rval.Append(BKind.KEYWORD, "static"); break;
                case MemberModifiers.STATIC_READONLY:
                    rval.Append(BKind.KEYWORD, "static readonly"); break;
                case MemberModifiers.VIRTUAL:
                    rval.Append(BKind.KEYWORD, "virtual"); break;
                default:
                    return rval;
            }
            if(addBlank) { rval.AppendBlank(); }

            return rval;
        }


        /// <summary>Returns the prefixes (namespaces, types) for a member.</summary>
        /// <param name="mem">Member.</param>
        /// <param name="addDot">Determines if a dot will be appended to the prefixes if any exist.</param>
        /// <returns>Member prefixes.</returns>
        public static BDeclaration GetNamePrefixesDecl(this MemberInfo mem, bool addDot = false)
        {
            BDeclaration rval = new BDeclaration();

            if(mem.ReflectedType == null)
            {
                if(!string.IsNullOrWhiteSpace(((Type) mem).Namespace)) { rval.Append(BKind.IDENTIFIER, ((Type) mem).Namespace); }
            }
            else
            {
                rval.Append(mem.ReflectedType.GetFullNameDecl());
            }
            if(addDot)
            {
                if(rval.Length > 0) { rval.Append("."); }
            }

            return rval;
        }


        /// <summary>Gets the member name without any appendices.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Member name.</returns>
        public static BDeclaration GetSimpleNameDecl(this MemberInfo mem)
        {
            BDeclaration rval;

            if((mem is MethodInfo) && (((MethodInfo) mem).IsSpecialName) && (mem.Name.StartsWith("op_")))
            {
                rval = new BDeclaration();

                if(mem.Name == "op_Explicit")
                {
                    rval.Append(BKind.KEYWORD, "explicit ");
                    rval.Append(((MethodInfo) mem).ReturnParameter.ParameterType.CommonNameDecl(mem.ReflectedType));
                }
                else if(mem.Name == "op_Implicit")
                {
                    rval.Append(BKind.KEYWORD, "implicit ");
                    rval.Append(((MethodInfo) mem).ReturnParameter.ParameterType.CommonNameDecl(mem.ReflectedType));
                }
                else
                {
                    rval.Append(BKind.SYNTAX, Metadata.GetOperatorMetadata(mem).Name);
                }
            }
            else if((mem is PropertyInfo) && (((PropertyInfo) mem).GetIndexParameters().Length > 0))
            {
                rval = new BDeclaration(BKind.KEYWORD, "this");
            }
            else if(mem is ConstructorInfo)
            {
                rval = new BDeclaration(BKind.IDENTIFIER, mem.ReflectedType.GetSimpleNameDecl());
            }
            else if(mem == null)
            {
                rval = new BDeclaration();
            }
            else
            {
                rval = new BDeclaration(mem is Type ? BKind.TYPE : BKind.IDENTIFIER, (mem.Name.Contains("`") ? mem.Name.Substring(0, mem.Name.IndexOf("`")) : mem.Name));
            }

            return rval;
        }



        /// <summary>Returns the member name without any appendices including namespace and parent type.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Full member name.</returns>
        public static BDeclaration GetSimpleFullNameDecl(this MemberInfo mem)
        {
            BDeclaration rval = mem.GetNamePrefixesDecl(true);
            rval.Append(mem.GetSimpleNameDecl());

            return rval;
        }



        /// <summary>Gets the member name including generic arguments.</summary>
        /// <param name="mem">Member.</param>
        /// <param name="fullGenericNames">Determines if full generic names should be used.</param>
        /// <returns>Member name.</returns>
        public static BDeclaration GetNameDecl(this MemberInfo mem, bool fullGenericNames = false)
        {
            BDeclaration rval = mem.GetSimpleNameDecl();

            rval.Append(mem.GetGenericParametersDecl(fullGenericNames));
            rval.Append(mem.GetParameterListDecl(false));
            
            return rval;
        }



        /// <summary>Gets the full member name including generic arguments, base types, and namespace.</summary>
        /// <param name="mem">Member.</param>
        /// <param name="fullGenericNames">Determines if generic parameters will be addressed with full name.</param>
        /// <returns>Member name.</returns>
        public static BDeclaration GetFullNameDecl(this MemberInfo mem, bool fullGenericNames = false)
        {
            BDeclaration rval = mem.GetNamePrefixesDecl(true);
            rval.Append(mem.GetNameDecl(true));

            return rval;
        }


        /// <summary>Gets the generic parameters for a type or method.</summary>
        /// <param name="mem">Member.</param>
        /// <param name="fullGenericNames">Determines if generic parameters will be addressed with full name.</param>
        /// <returns>Generic parameters declaration.</returns>
        public static BDeclaration GetGenericParametersDecl(this MemberInfo mem, bool fullGenericNames = false)
        {
            BDeclaration rval = new BDeclaration();

            Type[] gen = new Type[0];

            if(mem is Type)
            {
                gen = ((Type) mem).GetLocalGenericArguments();
            }
            else if(mem is MethodInfo) { gen = ((MethodInfo) mem).GetLocalGenericArguments(); }

            if(gen.Length > 0)
            {
                rval.Append("<");
                for(int i = 0; i < gen.Length; i++)
                {
                    if(i > 0) { rval.Append(", "); }
                    if(fullGenericNames)
                    {
                        rval.Append(gen[i].CommonNameDecl(false, true, mem is Type ? (Type) mem : mem.ReflectedType));
                    }
                    else { rval.Append(gen[i].CommonNameDecl(mem is Type ? (Type) mem : mem.ReflectedType)); }
                }
                rval.Append(">");
            }

            return rval;
        }


        /// <summary>Gets the parameter list for a property or method.</summary>
        /// <param name="mem">Member.</param>
        /// <param name="parameterNames">Determines if parameter names will be included in the declaration.</param>
        /// <param name="fullParameterTypes">Determines if parameter types will be displayed with their full name.</param>
        /// <returns>Parameter list declaration.</returns>
        public static BDeclaration GetParameterListDecl(this MemberInfo mem, bool parameterNames = true, bool fullParameterTypes = false)
        {
            if(mem is MethodBase)
            {
                return ((MethodBase) mem).GetParameters().GetParameterListDecl(mem.ReflectedType, parameterNames, fullParameterTypes, "()");
            }
            else if(mem is PropertyInfo)
            {
                if(((PropertyInfo) mem).IsIndexer())
                {
                    return ((PropertyInfo) mem).GetIndexParameters().GetParameterListDecl(mem.ReflectedType, parameterNames, fullParameterTypes, "[]");
                }
            }

            return new BDeclaration();
        }


        /// <summary>Gets the parameter list declaration for a parameter list.</summary>
        /// <param name="parameters">Parameters.</param>
        /// <param name="parentType">Parent type.</param>
        /// <param name="parameterNames">Determines if parameter names will be included in the declaration.</param>
        /// <param name="fullParameterTypes">Determines if parameter types will be displayed with their full name.</param>
        /// <param name="enclosingBrackets">Enclosing brackets string.</param>
        /// <returns>Parameter list declaration.</returns>
        public static BDeclaration GetParameterListDecl(this ParameterInfo[] parameters, Type parentType, bool parameterNames = true, bool fullParameterTypes = false, string enclosingBrackets = "()")
        {
            BDeclaration rval = new BDeclaration();

            rval.Append(enclosingBrackets[0].ToString());

            for(int i = 0; i < parameters.Length; i++)
            {
                if(i == 0)
                {
                    if(parameterNames && (parameters[i].Member.GetCustomAttribute<ExtensionAttribute>() != null))
                    {
                        rval.Append(BKind.KEYWORD, "this");
                        rval.AppendBlank();
                    }
                }
                else { rval.Append(", "); }

                if((parameters[i].ParameterType.FullName != null) && parameters[i].ParameterType.FullName.Contains("&"))
                {
                    rval.Append(BKind.KEYWORD, parameters[i].IsOut ? "out" : "ref");
                    rval.AppendBlank();
                }
                else if(parameterNames && (parameters[i].GetCustomAttribute<ParamArrayAttribute>() != null))
                {
                    rval.Append(BKind.KEYWORD, "params");
                    rval.AppendBlank();
                }

                if(fullParameterTypes)
                {
                    rval.Append(parameters[i].ParameterType.GetFullNameDecl());
                }
                else { rval.Append(parameters[i].ParameterType.CommonNameDecl(parentType)); }

                if(parameterNames)
                {
                    rval.AppendBlank();
                    rval.Append(BKind.IDENTIFIER, parameters[i].Name);

                    if(parameters[i].HasDefaultValue)
                    {
                        rval.Append(" = ");
                        rval.Append(parameters[i].DefaultValue.MarshalledValueDecl());
                    }
                }
            }
            rval.Append(enclosingBrackets[1].ToString());

            return rval;
        }


        /// <summary>Gets the base class and implemented interfaces declaration.</summary>
        /// <param name="mem">Member.</param>
        /// <param name="includeIhnerited">Determines if iherited interfaces will be included.</param>
        /// <returns>Declaration.</returns>
        public static BDeclaration GetInheritanceDecl(this MemberInfo mem, bool includeIhnerited = true)
        {
            BDeclaration rval = new BDeclaration();
            if(!(mem is Type)) { return rval; }

            if(((Type) mem).IsEnum)
            {
                if(((Type) mem).GetEnumUnderlyingType() != typeof(int))
                {
                    rval.Append(": ");
                    rval.Append(((Type) mem).GetEnumUnderlyingType().CommonNameDecl(mem.ReflectedType));
                }
            }
            else if((((Type) mem).BaseType == null) || (!((Type) mem).BaseType.IsSubclassOf(typeof(Delegate))))
            {
                List<Type> b = new List<Type>();
                string delim = ": ";

                if(((Type) mem).BaseType != null)
                {
                    if((((Type) mem).BaseType != typeof(object)) && (((Type) mem).BaseType != typeof(ValueType)))
                    {
                        rval.Append(delim);
                        rval.Append(((Type) mem).BaseType.CommonNameDecl(mem.ReflectedType));
                        delim = ", ";
                    }
                }

                foreach(Type i in (includeIhnerited ? ((Type) mem).GetInterfaces() : ((Type) mem).GetEssentialInterfaces()))
                {
                    rval.Append(delim);
                    rval.Append(i.CommonNameDecl(mem.ReflectedType));
                    delim = ", ";
                }
            }

            return rval;
        }


        /// <summary>Gets the attribute declaration.</summary>
        /// <param name="mem">Member.</param>
        /// <param name="includeInherited">Determines if inherited attributes will be included.</param>
        /// <returns>Declaration.</returns>
        public static BDeclaration GetAttributesDecl(this MemberInfo mem, bool includeInherited = true)
        {
            BDeclaration rval = new BDeclaration();

            foreach(Attribute i in mem.GetCustomAttributes())
            {
                if((!includeInherited) && i.IsInheritedAttributeIn(mem)) continue;

                AttributeConfigMetadata meta;
                if((meta = i.GetMetadata()).Hidden) continue;

                rval.Append("[");

                string attname = i.GetType().CommonNameDecl(mem.ReflectedType).ToString();
                if(attname.EndsWith("Attribute")) { attname = attname.Substring(0, attname.Length - 9); }
                rval.Append(BKind.TYPE, attname);

                if((meta.IsDefined) && (meta.ReferencedProperties.Length > 0))
                {
                    bool def = false;
                    BDeclaration pam = new BDeclaration();
                    pam.Append("(");
                    for(int k = 0; k < meta.ReferencedProperties.Length; k++)
                    {
                        if(k > 0) { pam.Append(", "); }
                        PropertyInfo p = i.GetType().GetProperty(meta.ReferencedProperties[k]);

                        object xx = p.GetValue(i);
                        BDeclaration v = p.GetValue(i).MarshalledValueDecl();
                        pam.Append(v);

                        if(meta.Defaults[k] != v.ToString()) { def = true; }
                    }
                    pam.Append(")");

                    if(def) { rval.Append(pam); }
                }
                else
                {
                    bool hasprop = false;
                    foreach(PropertyInfo k in i.GetType().GetProperties())
                    {
                        if(k.DeclaringType != i.GetType()) continue;

                        if(!hasprop)
                        {
                            rval.Append("(");
                            hasprop = true;
                        }
                        else { rval.Append(", "); }

                        rval.Append(BKind.IDENTIFIER, k.Name);
                        rval.Append(" = ");

                        rval.Append(k.GetValue(i).MarshalledValueDecl());
                    }
                    if(hasprop) { rval.Append(")"); }
                }

                rval.Append("]\r\n");
            }

            return rval;
        }


        /// <summary>Returns a generic where clause for a generic attribute.</summary>
        /// <param name="mem">Member.</param>
        public static BDeclaration GetGenericWhereClauseDecl(this MemberInfo mem)
        {
            Type[] gen = null;

            if(mem is Type)
            {
                if(((Type) mem).ContainsGenericParameters) { gen = ((Type) mem).GetLocalGenericArguments(); }
            }
            else if(mem is MethodInfo)
            {
                if(((MethodInfo) mem).ContainsGenericParameters) { gen = ((MethodInfo) mem).GetLocalGenericArguments(); }
            }

            BDeclaration rval = new BDeclaration();

            if(gen != null)
            {
                foreach(Type i in gen)
                {
                    if(rval.Length > 0) { rval.AppendBlank(); }

                    BDeclaration init = new BDeclaration();
                    init.AppendBlank();
                    init.Append(BKind.KEYWORD, "where");
                    init.AppendBlank();
                    init.Append(BKind.TYPE, i.Name);
                    init.Append(": ");

                    if((i.GenericParameterAttributes & GenericParameterAttributes.ReferenceTypeConstraint) == GenericParameterAttributes.ReferenceTypeConstraint)
                    {
                        rval.Append(init);
                        init = new BDeclaration(BKind.SYNTAX, ", ");

                        rval.Append(BKind.KEYWORD, "class");
                    }

                    if((i.GenericParameterAttributes & GenericParameterAttributes.NotNullableValueTypeConstraint) == GenericParameterAttributes.NotNullableValueTypeConstraint)
                    {
                        rval.Append(init);
                        init = new BDeclaration(BKind.SYNTAX, ", ");

                        rval.Append(BKind.KEYWORD, "struct");
                    }

                    foreach(Type k in i.GetGenericParameterConstraints())
                    {
                        if(k.FullName == "System.ValueType") continue;

                        rval.Append(init);
                        init = new BDeclaration(BKind.SYNTAX, ", ");

                        rval.Append(k.CommonNameDecl(i));
                    }

                    if(((i.GenericParameterAttributes & GenericParameterAttributes.DefaultConstructorConstraint) == GenericParameterAttributes.DefaultConstructorConstraint) &&
                       ((i.GenericParameterAttributes & GenericParameterAttributes.NotNullableValueTypeConstraint) != GenericParameterAttributes.NotNullableValueTypeConstraint))
                    {
                        rval.Append(init);
                        init = new BDeclaration(BKind.SYNTAX, ", ");

                        rval.Append(BKind.LITERAL, "new()");
                    }
                }
            }
            return rval;
        }


        /// <summary>Returns the member declaration.</summary>
        /// <param name="mem">Member.</param>
        /// <param name="propertyAccessors">Determines if property accessors (get, set) will be included in the declaration.</param>
        /// <param name="modifiers">Determines if member visibility and modifiers will be included in the declaration.</param>
        /// <param name="returnValue">Determines if the return value will be included in the declaration.</param>
        /// <param name="parameterNames">Determines if parameter names, default values, and generic where clauses will be included in the declaration.</param>
        /// <param name="inheritedInterfaces">Determines if inherited interfaces will be included in the declaration.</param>
        /// <param name="fullParameterTypes">Determines if paremeters will be shown with their full names.</param>
        /// <returns>Member abstracts.</returns>
        public static BDeclaration GetDeclarationDecl(this MemberInfo mem, bool propertyAccessors = false, bool modifiers = true, bool returnValue = true, bool parameterNames = true, bool inheritedInterfaces = true, bool fullParameterTypes = false)
        {
            BDeclaration rval = new BDeclaration();
            
            if(modifiers)
            {
                rval.Append(mem.GetVisibilityDecl(true));
                rval.Append(mem.GetModifiersDecl(true));
            }

            if(mem is Type)
            {
                if(typeof(Delegate).IsAssignableFrom((Type) mem))
                {
                    rval.Append(BKind.KEYWORD, "delegate");
                    rval.AppendBlank();

                    MethodInfo invoke = ((Type) mem).GetMethod("Invoke");

                    if(invoke != null)
                    {
                        rval.Append(invoke.ReturnType.CommonNameDecl((Type) mem));
                        rval.AppendBlank();
                    }

                    rval.Append(mem.Name);

                    if(invoke != null)
                    {
                        rval.Append(invoke.GetParameterListDecl());
                    }
                }
                else
                {
                    if(((Type) mem).IsInterface)
                    {
                        rval.Append(BKind.KEYWORD, "interface");
                    }
                    else if(((Type) mem).IsEnum)
                    {
                        rval.Append(BKind.KEYWORD, "enum");
                    }
                    else if(((Type) mem).IsValueType)
                    {
                        rval.Append(BKind.KEYWORD, "struct");
                    }
                    else { rval.Append(BKind.KEYWORD, "class"); }

                    rval.AppendBlank();
                    rval.Append(mem.GetSimpleNameDecl());
                    rval.Append(mem.GetGenericParametersDecl());
                    if(parameterNames)
                    {
                        rval.Append(mem.GetInheritanceDecl(inheritedInterfaces));
                        rval.Append(mem.GetGenericWhereClauseDecl());
                    }
                }
            }
            else if((mem is MethodInfo) && (((MethodInfo) mem).IsSpecialName) && (mem.Name.StartsWith("op_")))
            {
                bool cv = true;
                if(mem.Name == "op_Explicit")
                {
                    rval.Append(BKind.KEYWORD, "explicit");
                    rval.AppendBlank();
                    rval.Append(BKind.KEYWORD, "opertator");
                    rval.AppendBlank();
                }
                else if(mem.Name == "op_Implicit")
                {
                    rval.Append(BKind.KEYWORD, "implicit");
                    rval.AppendBlank();
                    rval.Append(BKind.KEYWORD, "opertator");
                    rval.AppendBlank();
                }
                else { cv = false; }

                rval.Append(((MethodInfo) mem).ReturnParameter.ParameterType.CommonNameDecl(mem.ReflectedType));

                if(!cv)
                {
                    rval.AppendBlank();
                    rval.Append(BKind.KEYWORD, "operator");
                    rval.AppendBlank();
                    rval.Append(BKind.NAME, Metadata.GetOperatorMetadata(mem).Name);
                }

                rval.Append(mem.GetParameterListDecl());
            }
            else if(mem is MethodBase)
            {
                if(mem is MethodInfo)
                {
                    if(modifiers && ((((MethodInfo) mem).MethodImplementationFlags & MethodImplAttributes.InternalCall) != 0) || (mem.GetCustomAttribute<DllImportAttribute>() != null))
                    {
                        rval.Append(BKind.KEYWORD, "extern");
                        rval.AppendBlank();
                    }

                    if(returnValue)
                    {
                        if(fullParameterTypes)
                        {
                            rval.Append(((MethodInfo) mem).ReturnType.GetFullNameDecl());
                        }
                        else { rval.Append(((MethodInfo) mem).ReturnType.CommonNameDecl(mem.ReflectedType)); }
                        rval.AppendBlank();
                    }
                }

                rval.Append(mem.GetSimpleNameDecl());
                rval.Append(mem.GetGenericParametersDecl(fullParameterTypes));
                rval.Append(mem.GetParameterListDecl(parameterNames, fullParameterTypes));
                if(parameterNames) { rval.Append(mem.GetGenericWhereClauseDecl()); }
            }
            else if(mem is PropertyInfo)
            {
                if(returnValue)
                {
                    rval.Append(((PropertyInfo) mem).PropertyType.CommonNameDecl(mem.ReflectedType));
                    rval.AppendBlank();
                }
                rval.Append(mem.GetSimpleNameDecl());
                if(mem.IsIndexer()) { rval.Append(mem.GetParameterListDecl(parameterNames)); }

                if(propertyAccessors)
                {
                    rval.Append("\r\n{\r\n    ");

                    if(((PropertyInfo) mem).GetMethod != null)
                    {
                        if(((PropertyInfo) mem).GetMethod.GetVisibility() != ((PropertyInfo) mem).BaseMethod().GetVisibility()) { rval.Append(((PropertyInfo) mem).GetMethod.GetVisibilityDecl(true)); }
                        rval.Append(BKind.KEYWORD, "get");
                        rval.Append(";");

                        if(((PropertyInfo) mem).SetMethod != null) { rval.AppendBlank(); }
                    }

                    if(((PropertyInfo) mem).SetMethod != null)
                    {
                        if(((PropertyInfo) mem).SetMethod.GetVisibility() != ((PropertyInfo) mem).BaseMethod().GetVisibility()) { rval.Append(((PropertyInfo) mem).SetMethod.GetVisibilityDecl(true)); }
                        rval.Append(BKind.KEYWORD, "set");
                        rval.Append(";");
                    }
                    rval.Append("\r\n}");
                }
            }
            else if(mem is EventInfo)
            {
                rval.Append(BKind.KEYWORD, "event");
                rval.AppendBlank();

                if(returnValue)
                {
                    rval.Append(((EventInfo) mem).EventHandlerType.CommonNameDecl(mem.ReflectedType));
                    rval.AppendBlank();
                }

                rval.Append(mem.Name);
            }
            else if((mem is FieldInfo) && (typeof(Enum).IsAssignableFrom(mem.ReflectedType)))
            {
                rval.Append(BKind.IDENTIFIER, mem.Name);
                rval.Append(BKind.SYNTAX, " = ");

                if(mem.ReflectedType.GetEnumUnderlyingType() == typeof(ulong))
                {
                    rval.Append(BKind.LITERAL, ((ulong) ((FieldInfo) mem).GetValue(null)).ToString());
                }
                else
                {
                    rval.Append(BKind.LITERAL, Convert.ToInt64(((FieldInfo) mem).GetValue(null)).ToString());
                }
            }
            else if(mem is FieldInfo)
            {
                rval.Append(((FieldInfo) mem).FieldType.CommonNameDecl(mem.ReflectedType));
                rval.AppendBlank();
                rval.Append(BKind.IDENTIFIER, mem.Name);

                if(((FieldInfo) mem).IsLiteral)
                {
                    rval.Append(BKind.SYNTAX, " = ");
                    rval.Append(((FieldInfo) mem).GetValue(null).MarshalledValueDecl());
                }
            }

            return rval;
        }
    }
}
