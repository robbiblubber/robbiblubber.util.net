﻿using System;



namespace Robbiblubber.Util.Dynamic.Analysis
{
    /// <summary>This class provides operator configuration metadata.</summary>
    public sealed class OperatorConfigMetadata
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="name">Operator name.</param>
        /// <param name="linkName">Link name.</param>
        /// <param name="methodName">Method name.</param>
        internal OperatorConfigMetadata(string name, string linkName, string methodName)
        {
            Name = name;
            LinkName = linkName;
            MethodName = methodName;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the operator name.</summary>
        public string Name
        {
            get; private set;
        }


        /// <summary>Gets the operator link name.</summary>
        public string LinkName
        {
            get; private set;
        }


        /// <summary>Gets the operator method name.</summary>
        public string MethodName
        {
            get; private set;
        }
    }
}
