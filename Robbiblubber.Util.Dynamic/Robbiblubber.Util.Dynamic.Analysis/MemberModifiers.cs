﻿using System;
using System.Collections.Generic;
using System.Text;



namespace Robbiblubber.Util.Dynamic.Analysis
{
    /// <summary>This enumeration defines member modifiers.</summary>
    public enum MemberModifiers
    {
        /// <summary>The member has no modifiers.</summary>
        NONE = 0,
        /// <summary>The member is sealed.</summary>
        SEALED = 200,
        /// <summary>The member is abstract.</summary>
        ABSTRACT = 300,
        /// <summary>The member is virtual.</summary>
        VIRTUAL = 400,
        /// <summary>The member is override.</summary>
        OVERRIDE = 500,
        /// <summary>The member is new.</summary>
        NEW = 600,
        /// <summary>The member is readonly (initonly).</summary>
        READONLY = 700,
        /// <summary>The member is static.</summary>
        STATIC = 800,
        /// <summary>The member is constant (literal).</summary>
        CONST = 900,
        /// <summary>The member is readonly (initonly) and static.</summary>
        STATIC_READONLY = 1500
    }
}
