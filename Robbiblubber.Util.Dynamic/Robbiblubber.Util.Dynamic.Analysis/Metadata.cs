﻿using System;
using System.Collections.Generic;
using System.Reflection;



namespace Robbiblubber.Util.Dynamic.Analysis
{
    /// <summary>This class provides metadata for analysis methods.</summary>
    public static class Metadata
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Operator configuration metadata.</summary>
        private static Dictionary<string, OperatorConfigMetadata> _OperatorMetadata = null;
        
        /// <summary>Attribute configuration metadata.</summary>
        private static Dictionary<string, AttributeConfigMetadata> _AttributeMetadata = null;

        /// <summary>Operators definition file.</summary>
        private static string _OperatorsDefinitionFile = null;

        /// <summary>Attributes definition file.</summary>
        private static string _AttributesDefinitionFile = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the attribute metadata.</summary>
        /// <param name="attr">Attribute.</param>
        /// <returns>Metadata.</returns>
        public static AttributeConfigMetadata GetMetadata(this Attribute attr)
        {
            if(__AttributeMetadata.ContainsKey(attr.GetType().FullName)) { return __AttributeMetadata[attr.GetType().FullName]; }

            return AttributeConfigMetadata.DEFAULT;
        }


        /// <summary>Returns if a member is an operator method.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Returns TRUE if the member is an operator method, otherwise returns FALSE.</returns>
        public static bool IsOperator(this MemberInfo mem)
        {
            return (mem.GetOperatorMetadata() != null);
        }


        /// <summary>Gets the operator metadata for an operator method.</summary>
        /// <param name="mem">Member.</param>
        /// <returns>Operator metadata. Returns NULL if the member is not an operator method.</returns>
        public static OperatorConfigMetadata GetOperatorMetadata(this MemberInfo mem)
        {
            if(mem is MethodInfo)
            {
                if(((MethodInfo) mem).IsSpecialName)
                {
                    if(__OperatorMetadata.ContainsKey(mem.Name)) { return __OperatorMetadata[mem.Name]; }
                }
            }

            return null;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the attributes definition file name.</summary>
        public static string AttributesDefinitionFile
        {
            get
            {
                if(_AttributesDefinitionFile == null) { _AttributesDefinitionFile = PathOp.ApplicationDirectory + @"\meta\attributes.meta"; }

                return _AttributesDefinitionFile;
            }
            set { _AttributesDefinitionFile = value; }
        }


        /// <summary>Gets or sets the attributes definition file name.</summary>
        public static string OperatorsDefinitionFile
        {
            get
            {
                if(_OperatorsDefinitionFile == null) { _OperatorsDefinitionFile = PathOp.ApplicationDirectory + @"\meta\operators.meta"; }

                return _OperatorsDefinitionFile;
            }
            set { _OperatorsDefinitionFile = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal static methods                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets attribute configuration metadata.</summary>
        internal static Dictionary<string, AttributeConfigMetadata> __AttributeMetadata
        {
            get
            {
                if(_AttributeMetadata == null)
                {
                    _AttributeMetadata = new Dictionary<string, AttributeConfigMetadata>();
                    Ddp cfg = Ddp.Open(_AttributesDefinitionFile);

                    foreach(DdpElement i in cfg.Sections)
                    {
                        try
                        {
                            _AttributeMetadata.Add(i.Name, new AttributeConfigMetadata(i["hide"], i["suppress"], i["ref"], i["default"]));
                        }
                        catch(Exception) {}
                    }
                }

                return _AttributeMetadata;
            }
        }


        /// <summary>Gets operator configuration metadata.</summary>
        internal static Dictionary<string, OperatorConfigMetadata> __OperatorMetadata
        {
            get
            {
                if(_OperatorMetadata == null)
                {
                    _OperatorMetadata = new Dictionary<string, OperatorConfigMetadata>();
                    Ddp cfg = Ddp.Open(_OperatorsDefinitionFile);

                    foreach(DdpElement i in cfg.Sections)
                    {
                        try
                        {
                            _OperatorMetadata.Add(i["method"], new OperatorConfigMetadata(i["operator"], i.Name, i["method"]));
                        }
                        catch(Exception) {}
                    }
                }

                return _OperatorMetadata;
            }
        }
    }
}
