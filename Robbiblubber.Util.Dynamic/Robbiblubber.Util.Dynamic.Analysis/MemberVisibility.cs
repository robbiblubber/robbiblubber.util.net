﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Robbiblubber.Util.Dynamic.Analysis
{
    /// <summary>This enumeration defines member visibilities.</summary>
    public enum MemberVisibility
    {
        /// <summary>The member is private.</summary>
        PRIVATE = 0,
        /// <summary>The member is protected private (family AND assembly).</summary>
        PROTECTED_PRIVATE = 1,
        /// <summary>The member is internal (assembly).</summary>
        INTERNAL = 2,
        /// <summary>The member is protected (family).</summary>
        PROTECTED = 3,
        /// <summary>The member is protected internal (family OR assembly)</summary>
        PROTECTED_INTERNAL = 4,
        /// <summary>The member is public.</summary>
        PUBLIC = 5,
        /// <summary>The member is public in explicit interface implementation.</summary>
        PUBLIC_INTERFACE_IMPLEMENTATION = 6
    }
}
