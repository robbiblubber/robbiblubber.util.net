﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Robbiblubber.Util.Dynamic
{
    /// <summary>This class provides an abstract class builder implementation.</summary>
    public abstract class __ClassBuilder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static members                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Counter.</summary>
        protected static int _N = 101;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>List of used namespaces.</summary>
        protected List<string> _Usings = new List<string>();

        /// <summary>Code.</summary>
        protected string _Code = null;

        /// <summary>Prefix.</summary>
        protected string _Prefix = ("__Dyn" + _N.ToString() + "_");

        /// <summary>Class name.</summary>
        protected string _ClassName;
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="t">Target type.</param>
        /// <param name="template">Code template.</param>
        protected __ClassBuilder(Type t, string template)
        {
            Type = t;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected properties                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the prefix for private members.</summary>
        protected string _PrivatePrefix
        {
            get { return ("_" + _Prefix); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the target type.</summary>
        public Type Type
        {
            get; protected set;
        }


        /// <summary>Gets the class code.</summary>
        public virtual string Code
        {
            get
            {
                if(_Code == null) { _Code = _GenerateCode(); }

                return _Code;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Generates the class code.</summary>
        /// <returns>Code.</returns>
        protected abstract string _GenerateCode();


        /// <summary>Creates a used namespaces list and applies it.</summary>
        /// <param name="code">Code string builder.</param>
        protected void _ApplyUsings(StringBuilder code)
        {
            StringBuilder usg = new StringBuilder();
            foreach(string i in _Usings)
            {
                usg.AppendLine("using " + i.Trim() + ";");
            }

            code.Replace("{$Usings}", usg.ToString());
        }
    }
}
