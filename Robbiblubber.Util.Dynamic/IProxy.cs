﻿using System;



namespace Robbiblubber.Util.Dynamic
{
    /// <summary>Proxy implementations implement this interface.</summary>
    public interface IProxy: IInterceptable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the proxy base subject.</summary>
        object Subject { get; }
    }
}
