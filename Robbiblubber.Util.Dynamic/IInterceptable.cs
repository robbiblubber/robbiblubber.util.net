﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Util.Dynamic
{
    /// <summary>Interceptable objects implement this interface.</summary>
    public interface IInterceptable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // events                                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Raised before a member is invoked.</summary>
        event InterceptionCancelEventHandler BeforeInvocation;


        /// <summary>Raised after a member has been invoked.</summary>
        event InterceptionEventHandler AfterInvocation;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the invocation handler.</summary>
        IInvocationHandler Handler { get; set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an interception event handler for a specific members.</summary>
        /// <param name="handler">Event handler.</param>
        /// <param name="members">Members.</param>
        void AddBeforeInvocationHandler(InterceptionCancelEventHandler handler, params MemberInfo[] members);


        /// <summary>Adds an interception event handler for a specific members.</summary>
        /// <param name="handler">Event handler.</param>
        /// <param name="members">Members.</param>
        void AddAfterInvocationHandler(InterceptionEventHandler handler, params MemberInfo[] members);


        /// <summary>Removes an interception event handler for a specific members.</summary>
        /// <param name="handler">Event handler.</param>
        /// <param name="members">Members.</param>
        void RemoveBeforeInvocationHandler(InterceptionCancelEventHandler handler, params MemberInfo[] members);


        /// <summary>Removes an interception event handler for a specific members.</summary>
        /// <param name="handler">Event handler.</param>
        /// <param name="members">Members.</param>
        void RemoveAfterInvocationHandler(InterceptionEventHandler handler, params MemberInfo[] members);
    }
}
