﻿using System;
using System.Reflection;



namespace Robbiblubber.Util.Dynamic
{
    /// <summary>Invocation handlers implement this interface.</summary>
    public interface IInvocationHandler
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Handles a method invocation.</summary>
        /// <param name="method">Called method.</param>
        /// <param name="obj">Target object.</param>
        /// <param name="parameters">Parameters.</param>
        /// <returns>Returns method result.</returns>
        object Handle(MethodInfo method, object obj, params object[] parameters);
    }
}
