﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using Robbiblubber.Util.Dynamic.Analysis;



namespace Robbiblubber.Util.Dynamic
{
    /// <summary>This class provides extensions for dynamic invokation.</summary>
    public static class InvokeOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Strict wrapper cache.</summary>
        private static Dictionary<Type, Type> _StrictCache = new Dictionary<Type, Type>();

        /// <summary>Strict wrapper cache.</summary>
        private static Dictionary<Type, Type> _Cache = new Dictionary<Type, Type>();

        /// <summary>Counter.</summary>
        private static int _N = 0;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Invokes a method.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="parameters">Method parameters.</param>
        /// <param name="types">Parameters types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <param name="bindingAttr">Binding attributes.</param>
        /// <param name="binder">Binder.</param>
        /// <param name="callConvention">Calling convention.</param>
        /// <returns>Returns the method result or NULL if the return type is VOID.</returns>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the method.</exception>
        public static object InvokeMethodStrict(this object obj, string methodName, object[] parameters, Type[] types, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null, CallingConventions callConvention = CallingConventions.HasThis)
        {
            MethodInfo m;

            if(parameters == null) { parameters = new object[0]; }
            if(types == null)
            {
                m = obj.GetType().GetMethod(methodName, bindingAttr);
            }
            else
            {
                m = obj.GetType().GetMethod(methodName, bindingAttr, binder, callConvention, types, modifiers);
            }
            
            if(m == null) { throw new NotImplementedException(); }

            return m.Invoke(obj, parameters);
        }


        /// <summary>Invokes a method.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="parameters">Method parameters.</param>
        /// <param name="types">Parameters types.</param>
        /// <param name="bindingAttr">Binding attributes.</param>
        /// <param name="binder">Binder.</param>
        /// <param name="callConvention">Calling convention.</param>
        /// <returns>Returns the method result or NULL if the return type is VOID.</returns>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the method.</exception>
        public static object InvokeMethodStrict(this object obj, string methodName, object[] parameters = null, Type[] types = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null, CallingConventions callConvention = CallingConventions.HasThis)
        {
            return InvokeMethodStrict(obj, methodName, parameters, types, null, bindingAttr, binder, callConvention);
        }


        /// <summary>Invokes a method.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="parameters">Method parameters.</param>
        /// <param name="defaultValue">Default return value.</param>
        /// <param name="types">Parameters types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <param name="bindingAttr">Binding attributes.</param>
        /// <param name="binder">Binder.</param>
        /// <param name="callConvention">Calling convention.</param>
        /// <returns>Returns the method result or NULL if the return type is VOID.</returns>
        public static object InvokeMethod(this object obj, string methodName, object[] parameters, object defaultValue, Type[] types, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null, CallingConventions callConvention = CallingConventions.HasThis)
        {
            try
            {
                return InvokeMethodStrict(obj, methodName, parameters, types, modifiers, bindingAttr, binder, callConvention);
            }
            catch(Exception) { return defaultValue; }
        }


        /// <summary>Invokes a method.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="parameters">Method parameters.</param>
        /// <param name="defaultValue">Default return value.</param>
        /// <param name="types">Parameters types.</param>
        /// <param name="bindingAttr">Binding attributes.</param>
        /// <param name="binder">Binder.</param>
        /// <param name="callConvention">Calling convention.</param>
        /// <returns>Returns the method result or NULL if the return type is VOID.</returns>
        public static object InvokeMethod(this object obj, string methodName, object[] parameters = null, object defaultValue = null, Type[] types = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null, CallingConventions callConvention = CallingConventions.HasThis)
        {
            try
            {
                return InvokeMethodStrict(obj, methodName, parameters, types, null, bindingAttr, binder, callConvention);
            }
            catch(Exception) { return defaultValue; }
        }


        /// <summary>Invokes a method.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="parameters">Method parameters.</param>
        /// <param name="types">Parameters types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <param name="bindingAttr">Binding attributes.</param>
        /// <param name="binder">Binder.</param>
        /// <param name="callConvention">Calling convention.</param>
        /// <returns>Returns the method result or NULL if the return type is VOID.</returns>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the method.</exception>
        public static T InvokeMethodResultStrict<T>(this object obj, string methodName, object[] parameters, Type[] types, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null, CallingConventions callConvention = CallingConventions.HasThis)
        {
            return (T) InvokeMethodStrict(obj, methodName, parameters, types, modifiers, bindingAttr, binder, callConvention);
        }


        /// <summary>Invokes a method.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="parameters">Method parameters.</param>
        /// <param name="types">Parameters types.</param>
        /// <param name="bindingAttr">Binding attributes.</param>
        /// <param name="binder">Binder.</param>
        /// <param name="callConvention">Calling convention.</param>
        /// <returns>Returns the method result or NULL if the return type is VOID.</returns>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the method.</exception>
        public static T InvokeMethodResultStrict<T>(this object obj, string methodName, object[] parameters = null, Type[] types = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null, CallingConventions callConvention = CallingConventions.HasThis)
        {
            return (T) InvokeMethodStrict(obj, methodName, parameters, types, null, bindingAttr, binder, callConvention);
        }


        /// <summary>Invokes a method.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="parameters">Method parameters.</param>
        /// <param name="defaultValue">Default return value.</param>
        /// <param name="types">Parameters types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <param name="bindingAttr">Binding attributes.</param>
        /// <param name="binder">Binder.</param>
        /// <param name="callConvention">Calling convention.</param>
        /// <returns>Returns the method result or NULL if the return type is VOID.</returns>
        public static T InvokeMethodResult<T>(this object obj, string methodName, object[] parameters, T defaultValue, Type[] types, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null, CallingConventions callConvention = CallingConventions.HasThis)
        {
            try
            {
                return (T) InvokeMethodStrict(obj, methodName, parameters, types, modifiers, bindingAttr, binder, callConvention);
            }
            catch(Exception) { return defaultValue; }
        }


        /// <summary>Invokes a method.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="methodName">Method name.</param>
        /// <param name="parameters">Method parameters.</param>
        /// <param name="defaultValue">Default return value.</param>
        /// <param name="types">Parameters types.</param>
        /// <param name="bindingAttr">Binding attributes.</param>
        /// <param name="binder">Binder.</param>
        /// <param name="callConvention">Calling convention.</param>
        /// <returns>Returns the method result or NULL if the return type is VOID.</returns>
        public static T InvokeMethodResult<T>(this object obj, string methodName, object[] parameters = null, T defaultValue = default(T), Type[] types = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null, CallingConventions callConvention = CallingConventions.HasThis)
        {
            try
            {
                return (T) InvokeMethodStrict(obj, methodName, parameters, types, null, bindingAttr, binder, callConvention);
            }
            catch(Exception) { return defaultValue; }
        }


        /// <summary>Returns the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="type">Property type.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <returns>Property value.</returns>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the property.</exception>
        public static object GetPropertyStrict(this object obj, string propertyName, Type type, object[] indexes, Type[] indexTypes, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            return _GetProperty(obj, propertyName, type, indexes, indexTypes, modifiers, bindingAttr, binder, true, null);
        }


        /// <summary>Returns the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="type">Property type.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <returns>Property value.</returns>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the property.</exception>
        public static object GetPropertyStrict(this object obj, string propertyName, Type type = null, object[] indexes = null, Type[] indexTypes = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            return _GetProperty(obj, propertyName, type, indexes, indexTypes, null, bindingAttr, binder, true, null);
        }


        /// <summary>Returns the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="type">Property type.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <returns>Property value.</returns>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the property.</exception>
        public static object GetProperty(this object obj, string propertyName, object defaultValue, Type type, object[] indexes, Type[] indexTypes, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            return _GetProperty(obj, propertyName, type, indexes, indexTypes, modifiers, bindingAttr, binder, false, defaultValue);
        }


        /// <summary>Returns the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="type">Property type.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <returns>Property value.</returns>
        public static object GetProperty(this object obj, string propertyName, object defaultValue = null, Type type = null, object[] indexes = null, Type[] indexTypes = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            return _GetProperty(obj, propertyName, type, indexes, indexTypes, null, bindingAttr, binder, false, defaultValue);
        }


        /// <summary>Returns the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <returns>Property value.</returns>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the property.</exception>
        public static T GetPropertyValueStrict<T>(this object obj, string propertyName, object[] indexes, Type[] indexTypes, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            return (T) _GetProperty(obj, propertyName, typeof(T), indexes, indexTypes, modifiers, bindingAttr, binder, true, null);
        }


        /// <summary>Returns the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <returns>Property value.</returns>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the property.</exception>
        public static T GetPropertyValueStrict<T>(this object obj, string propertyName, object[] indexes = null, Type[] indexTypes = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            return (T) _GetProperty(obj, propertyName, typeof(T), indexes, indexTypes, null, bindingAttr, binder, true, null);
        }


        /// <summary>Returns the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <returns>Property value.</returns>
        public static T GetPropertyValue<T>(this object obj, string propertyName, T defaultValue, object[] indexes, Type[] indexTypes, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            return (T) _GetProperty(obj, propertyName, typeof(T), indexes, indexTypes, modifiers, bindingAttr, binder, false, defaultValue);
        }


        /// <summary>Returns the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <returns>Property value.</returns>
        public static T GetPropertyValue<T>(this object obj, string propertyName, T defaultValue = default(T), object[] indexes = null, Type[] indexTypes = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            return (T) _GetProperty(obj, propertyName, typeof(T), indexes, indexTypes, null, bindingAttr, binder, false, defaultValue);
        }


        /// <summary>Sets the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="value">Property value.</param>
        /// <param name="type">Property type.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the property.</exception>
        public static void SetPropertyStrict(this object obj, string propertyName, object value, Type type, object[] indexes, Type[] indexTypes, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            PropertyInfo p;

            if(type == null)
            {
                p = obj.GetType().GetProperty(propertyName, bindingAttr);
            }
            else
            {
                if(indexTypes == null) { indexTypes = new Type[0]; }
                if(modifiers == null) { modifiers = new ParameterModifier[] { new ParameterModifier(indexTypes.Length) }; }

                p = obj.GetType().GetProperty(propertyName, bindingAttr, binder, type, indexTypes, modifiers);
            }
            if(p == null) { throw new NotImplementedException(); }

            if(indexes == null)
            {
                p.SetValue(obj, value);
            }
            else { p.SetValue(obj, value, indexes); }
        }


        /// <summary>Sets the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="value">Value.</param>
        /// <param name="type">Property type.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the property.</exception>
        public static void SetPropertyStrict(this object obj, string propertyName, object value, Type type = null, object[] indexes = null, Type[] indexTypes = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            SetPropertyStrict(obj, propertyName, value, type, indexes, indexTypes, null, bindingAttr, binder);
        }


        /// <summary>Sets the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="value">Property value.</param>
        /// <param name="type">Property type.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        public static void SetProperty(this object obj, string propertyName, object value, Type type, object[] indexes, Type[] indexTypes, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            try
            {
                SetPropertyStrict(obj, propertyName, value, type, indexes, indexTypes, modifiers, bindingAttr, binder);
            }
            catch(Exception) {}
        }


        /// <summary>Sets the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="value">Value.</param>
        /// <param name="type">Property type.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        public static void SetProperty(this object obj, string propertyName, object value, Type type = null, object[] indexes = null, Type[] indexTypes = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            try
            {
                SetPropertyStrict(obj, propertyName, value, type, indexes, indexTypes, null, bindingAttr, binder);
            }
            catch(Exception) {}
        }


        /// <summary>Sets the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="value">Property value.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the property.</exception>
        public static void SetPropertyValueStrict<T>(this object obj, string propertyName, T value, object[] indexes, Type[] indexTypes, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            SetPropertyStrict(obj, propertyName, value, typeof(T), indexes, indexTypes, modifiers, bindingAttr, binder);
        }


        /// <summary>Sets the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="value">Value.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the property.</exception>
        public static void SetPropertyValueStrict<T>(this object obj, string propertyName, T value, object[] indexes = null, Type[] indexTypes = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            SetPropertyStrict(obj, propertyName, value, typeof(T), indexes, indexTypes, null, bindingAttr, binder);
        }


        /// <summary>Sets the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="value">Property value.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the property.</exception>
        public static void SetPropertyValue<T>(this object obj, string propertyName, T value, object[] indexes, Type[] indexTypes, ParameterModifier[] modifiers, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            try
            {
                SetPropertyStrict(obj, propertyName, value, typeof(T), indexes, indexTypes, modifiers, bindingAttr, binder);
            }
            catch(Exception) {}
        }


        /// <summary>Sets the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="value">Value.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        public static void SetPropertyValue<T>(this object obj, string propertyName, T value, object[] indexes = null, Type[] indexTypes = null, BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance, Binder binder = null)
        {
            try
            {
                SetPropertyStrict(obj, propertyName, value, typeof(T), indexes, indexTypes, null, bindingAttr, binder);
            }
            catch(Exception) {}
        }


        /// <summary>Returns if the object supports a given property</summary>
        /// <param name="m">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <returns>Returns TRUE if the object support the property, otherwise returns FALSE.</returns>
        public static bool HasProperty(this object m, string propertyName)
        {
            return (m.GetType().GetProperty(propertyName) != null);
        }


        /// <summary>Returns if the object supports a given property</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="m">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <returns>Returns TRUE if the object support the property, otherwise returns FALSE.</returns>
        public static bool HasProperty<T>(this object m, string propertyName)
        {
            PropertyInfo p = m.GetType().GetProperty(propertyName);

            if(p == null)
            {
                return false;
            }

            return (p.PropertyType == typeof(T));
        }


        /// <summary>Adds an event handler to an event.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="eventName">Event name.</param>
        /// <param name="eventHandler">Event handler.</param>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the event.</exception>
        public static void AddEventHandlerStrict(this object obj, string eventName, Delegate eventHandler)
        {
            obj.GetType().GetEvent(eventName).AddEventHandler(obj, eventHandler);
        }


        /// <summary>Adds an event handler to an event.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="eventName">Event name.</param>
        /// <param name="eventHandler">Event handler.</param>
        public static void AddEventHandler(this object obj, string eventName, Delegate eventHandler)
        {
            try
            {
                AddEventHandlerStrict(obj, eventName, eventHandler);
            }
            catch(Exception) {}
        }


        /// <summary>Removes an event handler from an event.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="eventName">Event name.</param>
        /// <param name="eventHandler">Event handler.</param>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the event.</exception>
        public static void RemoveventHandlerStrict(this object obj, string eventName, Delegate eventHandler)
        {
            obj.GetType().GetEvent(eventName).RemoveEventHandler(obj, eventHandler);
        }


        /// <summary>Adds an event handler to an event.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="eventName">Event name.</param>
        /// <param name="eventHandler">Event handler.</param>
        public static void RemoveEventHandler(this object obj, string eventName, Delegate eventHandler)
        {
            try
            {
                AddEventHandlerStrict(obj, eventName, eventHandler);
            }
            catch(Exception) {}
        }


        /// <summary>Disposes a dynamic interface.</summary>
        /// <param name="obj">Object.</param>
        /// <remarks>Disposing a dynamic interface is required if an iterface accesses events in a base class.</remarks>
        public static void DisposeDynamicInterface(this object obj)
        {
            obj.InvokeMethod("__DisposeDynamicImpl__");
        }


        /// <summary>Returns the object as an implementation of an interface.</summary>
        /// <param name="intfc">Interface type.</param>
        /// <param name="obj">Object.</param>
        /// <param name="strict">Determines if an interface member will throw an exception if not implemented.</param>
        /// <returns>Object implementing the interface.</returns>
        /// <exception cref="ArgumentException">Thrown when the interface type is not an interface.</exception>
        public static object AsInterface(object obj, Type intfc, bool strict = false)
        {
            if(!intfc.IsInterface) { throw new ArgumentException(); }

            if(obj.GetType().GetInterfaces().Contains(intfc)) { return obj; }

            Dictionary<Type, Type> cache = (strict ? _StrictCache : _Cache);
            if(!cache.ContainsKey(intfc))
            {
                CodeDomProvider cdp = CodeDomProvider.CreateProvider("C#");
                CompilerParameters pam = new CompilerParameters();
                pam.ReferencedAssemblies.Add("mscorlib.dll");
                pam._AddAssembly(obj.GetType().Assembly);
                pam._AddAssembly(intfc.Assembly);
                pam._AddAssembly(Assembly.GetExecutingAssembly());

                pam.GenerateInMemory = true;
                pam.IncludeDebugInformation = false;

                StringBuilder source = new StringBuilder();
                string className = intfc.GetSimpleNameDecl();
                try
                {
                    if(className.StartsWith("I") && (className.Substring(1,1) == className.Substring(1,1).ToUpper())) { className = className.Substring(1); }
                }
                catch(Exception) {}
                className = "__Dynamic" + className + "ImplW" + _N++.ToString() + "__";
                
                List<string> usings = new List<string>();

                usings.Add("System");
                usings.Add("System.Reflection");
                usings.Add("Robbiblubber.Util.Dynamic");

                foreach(MethodInfo i in intfc.GetMethods())
                {
                    if(!usings.Contains(i.ReturnType.Namespace)) { usings.Add(i.ReturnType.Namespace); }
                    pam._AddAssembly(i.ReturnType.Assembly);
                    foreach(ParameterInfo k in i.GetParameters())
                    {
                        if(!usings.Contains(k.ParameterType.Namespace)) { usings.Add(k.ParameterType.Namespace); }
                        pam._AddAssembly(k.ParameterType.Assembly);
                    }
                }

                foreach(PropertyInfo i in intfc.GetProperties())
                {
                    if(!usings.Contains(i.PropertyType.Namespace)) { usings.Add(i.PropertyType.Namespace); }
                    pam._AddAssembly(i.PropertyType.Assembly);
                    foreach(ParameterInfo k in i.GetIndexParameters())
                    {
                        if(!usings.Contains(k.ParameterType.Namespace)) { usings.Add(k.ParameterType.Namespace); }
                        pam._AddAssembly(k.ParameterType.Assembly);
                    }
                }

                foreach(EventInfo i in intfc.GetEvents())
                {
                    if(!usings.Contains(i.EventHandlerType.Namespace)) { usings.Add(i.EventHandlerType.Namespace); }
                    pam._AddAssembly(i.EventHandlerType.Assembly);
                    foreach(ParameterInfo k in i.EventHandlerType.GetMethod("Invoke").GetParameters())
                    {
                        if(!usings.Contains(k.ParameterType.Namespace)) { usings.Add(k.ParameterType.Namespace); }
                        pam._AddAssembly(k.ParameterType.Assembly);
                    }
                }

                foreach(string i in usings.OrderBy(m => m))
                {
                    source.AppendLine("using " + i + ";");
                }
                source.AppendLine();

                source.AppendLine("namespace Robbiblubber.Util.Dynamic.__DynamicGenerated__");
                source.AppendLine("{");

                source.AppendLine("    public class " + className + ": " + intfc.GetFullNameDecl());
                source.AppendLine("    {");
                source.AppendLine("        private object _Obj;");
                source.AppendLine();
                source.AppendLine("        public " + className + "(object obj)");
                source.AppendLine("        {");
                source.AppendLine("            _Obj = obj;");
                source.AppendLine("            __InitDynamicImpl__();");
                source.AppendLine("        }");
                source.AppendLine();

                
                foreach(MethodInfo i in intfc.GetMethods())
                {
                    string accf = (strict ? "_Obj.InvokeMethodStrict(" : "_Obj.InvokeMethod(");
                    if(i.HasReturnValue())
                    {
                        accf = (strict ? "_Obj.InvokeMethodResultStrict<T>(" : "_Obj.InvokeMethodResult<T>(")._ApplyType(i.ReturnType);
                    }
                    if(i.IsSpecialName) continue;
                    source.AppendLine("        public " + i.GetDeclarationDecl(false, false));
                    source.AppendLine("        {");

                    _ParamStrings mpam = new _ParamStrings(i.GetParameters());

                    source.AppendLine("            ParameterModifier pam = new ParameterModifier(" + i.GetParameters().Length + ");");

                    for(int k = 0; k < i.GetParameters().Length; k++)
                    {
                        if(i.GetParameters()[k].IsOut)
                        {
                            source.AppendLine("            pam[" + k.ToString() + "] = true;");
                        }
                    }
                    source.AppendLine();
                    source.Append("            ");
                    if(i.HasReturnValue()) { source.Append("return "); }
                    source.Append(accf);
                    source.Append("\"" + i.GetSimpleNameDecl() + "\", " + mpam.Values);
                    if(!strict) { source.Append(i.HasReturnValue() ? (", default(" + ((string) i.ReturnType.CommonNameDecl()) + ")") : ", null"); }
                    source.Append(", " + mpam.Types + ", new ParameterModifier[] { pam });");
                    source.AppendLine("        }");
                    source.AppendLine();
                }
                
                string accget = (strict ? "_Obj.GetPropertyValueStrict<T>(" : "_Obj.GetPropertyValue<T>(");
                string accset = (strict ? "_Obj.SetPropertyValueStrict<T>(" : "_Obj.SetPropertyValue<T>(");
                foreach(PropertyInfo i in intfc.GetProperties())
                {
                    if(i.IsSpecialName) continue;
                    source.AppendLine("        public " + i.GetDeclarationDecl(false, false));
                    source.AppendLine("        {");

                    if(i.GetMethod != null)
                    {
                        source.AppendLine("            get");
                        source.AppendLine("            {");
                        source.Append("                return " + accget._ApplyType(i.PropertyType));
                        source.Append("\"" + i.Name + "\"");

                        if(i.IsIndexer())
                        {
                            if(!strict) { source.Append(", default(" + i.PropertyType.CommonNameDecl() + ")"); }
                            _ParamStrings mpam = new _ParamStrings(i.GetIndexParameters());
                            source.Append(", " + mpam.Values);
                            source.Append(", " + mpam.Types);
                        }
                        source.AppendLine(");");
                        source.AppendLine("            }");
                    }

                    if(i.SetMethod != null)
                    {
                        source.AppendLine("            set");
                        source.AppendLine("            {");
                        source.Append("                " + accset._ApplyType(i.PropertyType));
                        source.Append("\"" + i.Name + "\"");
                        source.Append(", value");

                        if(i.IsIndexer())
                        {
                            _ParamStrings mpam = new _ParamStrings(i.GetIndexParameters());
                            source.Append(", " + mpam.Values);
                            source.Append(", " + mpam.Types);
                        }
                        source.AppendLine(");");
                        source.AppendLine("            }");
                    }

                    source.AppendLine("        }");
                    source.AppendLine();
                }
                
                foreach(EventInfo i in intfc.GetEvents())
                {
                    MethodInfo m = i.EventHandlerType.GetMethod("Invoke");

                    source.AppendLine("        public " + i.GetDeclarationDecl(false, false) + ";");
                    source.AppendLine("        private " + i.EventHandlerType.CommonNameDecl() + " __DynamicEventHandler" + i.Name + "__;");
                    source.Append("        private ");
                    source.AppendLine(((string) m.GetDeclarationDecl(false, false)).Replace("Invoke", "__OnDynamicEvent" + i.Name + "__"));
                    source.AppendLine("        {");
                    source.Append("            if(" + i.Name + " != null) { " + i.Name + ".Invoke(");
                    for(int k = 0; k < m.GetParameters().Length; k++)
                    {
                        if(k > 0) { source.Append(", "); }
                        source.Append(m.GetParameters()[k].Name);
                    }
                    source.AppendLine("); }");
                    source.AppendLine("        }");
                    source.AppendLine();
                }
                
                source.AppendLine("        private void __InitDynamicImpl__()");
                source.AppendLine("        {");
                foreach(EventInfo i in intfc.GetEvents())
                {
                    source.AppendLine("            _Obj.AddEventHandler" + (strict ? "Strict" : "") + "(\"" + i.Name + "\", __DynamicEventHandler" + i.Name + "__ = new " + i.EventHandlerType.CommonNameDecl() + "(__OnDynamicEvent" + i.Name + "__));");
                }
                source.AppendLine("        }");
                source.AppendLine();

                source.AppendLine("        public void __DisposeDynamicImpl__()");
                source.AppendLine("        {");
                foreach(EventInfo i in intfc.GetEvents())
                {
                    source.AppendLine("            _Obj.RemoveEventHandler(\"" + i.Name + "\", __DynamicEventHandler" + i.Name + "__);");
                }
                source.AppendLine("        }");

                source.AppendLine("    }");

                source.AppendLine("}");

                cache.Add(intfc, cdp.CompileAssemblyFromSource(pam, new string[] { source.ToString() }).CompiledAssembly.GetType("Robbiblubber.Util.Dynamic.__DynamicGenerated__." + className));
                return source.ToString();
            }
            return Activator.CreateInstance(cache[intfc], obj);
        }


        /*/// <summary>Returns the object as an implementation of an interface.</summary>
        /// <typeparam name="T">Interface type.</typeparam>
        /// <param name="obj">Object.</param>
        /// <returns>Object implementing the interface.</returns>
        /// <exception cref="ArgumentException">Thrown when the interface type is not an interface.</exception>
        /*public static T AsInterface<T>(this object obj)
        {
            return (T) AsInterface(obj, typeof(T));
        }*/


        /// <summary>Returns the object as an implementation of an interface.</summary>
        /// <param name="intfc">Interface type.</param>
        /// <param name="obj">Object.</param>
        /// <returns>Object implementing the interface.</returns>
        /// <exception cref="ArgumentException">Thrown when the interface type is not an interface.</exception>
        public static object AsInterfaceStrict(this object obj, Type intfc)
        {
            return AsInterface(obj, intfc, true);
        }


        /// <summary>Returns the object as an implementation of an interface.</summary>
        /// <typeparam name="T">Interface type.</typeparam>
        /// <param name="obj">Object.</param>
        /// <returns>Object implementing the interface.</returns>
        /// <exception cref="ArgumentException">Thrown when the interface type is not an interface.</exception>
        public static T AsInterfaceStrict<T>(this object obj)
        {
            return (T) AsInterfaceStrict(obj, typeof(T));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Applies a type to a invocation string.</summary>
        /// <param name="s">String.</param>
        /// <param name="type">Type.</param>
        /// <param name="fullTypeName"></param>
        internal static string _ApplyType(this string s, Type type, bool fullTypeName = false)
        {
            if(type == typeof(void)) { return s.Replace("<T>", ""); }

            return s.Replace("<T>", "<" + (fullTypeName ? type.GetFullNameDecl(true) : type.CommonNameDecl()) + ">");
        }


        /// <summary>Adds an assembly to the list of referenced assemblies.</summary>
        /// <param name="pam">Compiler parameters.</param>
        /// <param name="asm">Assembly.</param>
        private static void _AddAssembly(this CompilerParameters pam, Assembly asm)
        {
            string fileName = asm.Location.Replace("file:", "").TrimStart('/').Replace('/', '\\');

            if(pam.ReferencedAssemblies.Contains(asm.FullName)) return;
            if(pam.ReferencedAssemblies.Contains(fileName)) return;

            pam.ReferencedAssemblies.Add(fileName);
        }


        /// <summary>Returns the value of a property on an object.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="propertyName">Property name.</param>
        /// <param name="type">Property type.</param>
        /// <param name="indexes">Indexes.</param>
        /// <param name="bindingAttr">Binding flags.</param>
        /// <param name="binder">Binder object.</param>
        /// <param name="indexTypes">Parameter types.</param>
        /// <param name="modifiers">Parameter modifiers.</param>
        /// <param name="defaultValue">Strict mode.</param>
        /// <param name="strict">Default value.</param>
        /// <returns>Property value.</returns>
        /// <exception cref="NotImplementedException">Thrown when the object does not support the property.</exception>
        private static object _GetProperty(object obj, string propertyName, Type type, object[] indexes, Type[] indexTypes, ParameterModifier[] modifiers, BindingFlags bindingAttr, Binder binder, bool strict, object defaultValue)
        {
            PropertyInfo p;

            if(type == null)
            {
                p = obj.GetType().GetProperty(propertyName, bindingAttr);
            }
            else
            {
                if(indexTypes == null) { indexTypes = new Type[0]; }
                if(modifiers == null)
                {
                    modifiers = ((indexTypes.Length == 0) ? new ParameterModifier[0] : new ParameterModifier[] { new ParameterModifier(indexTypes.Length) });
                }

                p = obj.GetType().GetProperty(propertyName, bindingAttr, binder, type, indexTypes, modifiers);
            }

            if(p == null)
            {
                if(strict) { throw new NotImplementedException(); }

                return defaultValue;
            }

            if(indexes == null)
            {
                return p.GetValue(obj);
            }
            else { return p.GetValue(obj, indexes); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] _ParamsStrings                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class provides parameter strings.</summary>
        private class _ParamStrings
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="pam">Parameters.</param>
            public _ParamStrings(ParameterInfo[] pam)
            {
                if(pam.Length == 0)
                {
                    Values = "new object[0]";
                    Types = "new Type[0]";
                }
                else
                {
                    Values = "new object[] { ";
                    Types = "new Type[] { ";
                    for(int i = 0; i < pam.Length; i++)
                    {
                        if(i > 0) { Values += ", "; Types += ", "; }
                        Values += pam[i].Name;
                        Types += ("typeof(" + pam[i].ParameterType.CommonNameDecl() + ")");
                    }
                    Values += " }";
                    Types += " }";
                }
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public properties                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets the parameter types.</summary>
            public string Types { get; private set; }


            /// <summary>Gets the parameter values.</summary>
            public string Values { get; private set; }
        }
    }
}
