﻿using System;
using System.Reflection;



namespace Robbiblubber.Util.Dynamic
{
    /// <summary>This class provides interception event arguments that allows cancellation.</summary>
    public class InterceptionCancelEventArgs: InterceptionEventArgs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="member">Member.</param>
        /// <param name="parameters">Parameters.</param>
        public InterceptionCancelEventArgs(MethodInfo member, object[] parameters): base(member, parameters)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if the invocation will be cancelled.</summary>
        public bool Cancel { get; set; } = false;
    }
}
