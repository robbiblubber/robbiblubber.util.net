﻿using System;
using System.Reflection;

using Robbiblubber.Util.Dynamic.Analysis;



namespace Robbiblubber.Util.Dynamic
{
    /// <summary>This class provides parameter strings.</summary>
    internal class __ParamStrings
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="pam">Parameters.</param>
        public __ParamStrings(ParameterInfo[] pam)
        {
            if(pam.Length == 0)
            {
                Values = "new object[0]";
                Types = "new Type[0]";
            }
            else
            {
                Values = "new object[] { ";
                Types = "new Type[] { ";
                for(int i = 0; i < pam.Length; i++)
                {
                    if(i > 0) { Values += ", "; Types += ", "; }
                    Values += pam[i].Name;
                    Types += ("typeof(" + pam[i].ParameterType.GetFullNameDecl(true) + ")");
                }
                Values += " }";
                Types += " }";
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parameter types.</summary>
        public string Types { get; private set; }


        /// <summary>Gets the parameter values.</summary>
        public string Values { get; private set; }
    }
}