﻿using System;



namespace Robbiblubber.Util.Dynamic
{
    /// <summary>Proxy implementations implement this interface.</summary>
    public interface IProxy<T>: IInterceptable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the proxy base subject.</summary>
        T Subject { get; }
    }
}
