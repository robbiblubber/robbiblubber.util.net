﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Util.Dynamic.Analysis;



namespace Robbiblubber.Util.Dynamic
{
    /// <summary>This class provides a class builder for proxies implementing a specific interface.</summary>
    public sealed class __InterfaceProxyBuilder: __ClassBuilder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Cache for strict implementations.</summary>
        private static Dictionary<Type, Type> _StrictCache = new Dictionary<Type, Type>();

        /// <summary>Cache for standard implementations</summary>
        private static Dictionary<Type, Type> _Cache = new Dictionary<Type, Type>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Subject variable name.</summary>
        private string _SubjectName;

        /// <summary>Result class.</summary>
        private Type _Class = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="t">Type.</param>
        /// <param name="interfaceType">Interface type.</param>
        /// <param name="strict">Determines if the interface implementation is strict.</param>
        public __InterfaceProxyBuilder(Type t, Type interfaceType, bool strict): base(t, Resources.__Proxy)
        {
            InterfaceType = interfaceType;
            Strict = strict;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the target interface type.</summary>
        public Type InterfaceType
        {
            get; private set;
        }


        /// <summary>Gets if the interface implementation is strict.</summary>
        public bool Strict
        {
            get; private set;
        }


        /// <summary>Gets the interface class.</summary>
        public Type Class
        {
            get
            {
                if(_Class == null) { _Class = _Compile(); }

                return _Class;
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Compiles the class.</summary>
        /// <returns>Compiled class.</returns>
        private Type _Compile()
        {
            // TODO: implement!
            return null;
        }



        /// <summary>Writes event registration code.</summary>
        /// <param name="code">Code string builder.</param>
        private void _RegisterEvents(StringBuilder code)
        {
            StringBuilder reg = new StringBuilder();
            StringBuilder hdl = new StringBuilder();

            bool first = true;
            foreach(EventInfo i in InterfaceType.GetEvents())
            {
                string hname = _PrivatePrefix + i.Name + "EventHandler__";
                
                if(first)
                {
                    first = false;
                }
                else { hdl.AppendLine(); }

                reg.AppendLine(_SubjectName + ".AddEventHandler(\"" + i.Name + "\", new " + i.EventHandlerType.GetFullNameDecl(true) + "(" + hname + "));");

                MethodInfo m = i.EventHandlerType.GetMethod("Invoke");
                hdl.Append("private ");
                hdl.AppendLine(m.GetDeclarationDecl(false, false, true, true, true, true).ToString().Replace(" Invoke(", " " + hname + "("));
                hdl.AppendLine("{");
                hdl.Append("    " + i.Name + "?.Invoke(");
                for(int k = 0; k < m.GetParameters().Length; k++)
                {
                    if(k > 0) { hdl.Append(", "); }
                    hdl.Append(m.GetParameters()[k].Name);
                }
                hdl.AppendLine(");");
                hdl.AppendLine("}");
            }

            reg.Replace("\r\n", "\n").Replace("\n", "\r\n            ");
            hdl.Replace("\r\n", "\n").Replace("\n", "\r\n        ");

            code.Replace("{$EventRegister}", reg.ToString());
            code.Replace("{$EventHandlers}", hdl.ToString());
        }


        /// <summary>Writes the interface implementation code.</summary>
        /// <param name="code">Code string builder.</param>
        private void _ImplementInterface(StringBuilder code)
        {
            StringBuilder imp = new StringBuilder();

            _Usings.Add("System");
            _Usings.Add("System.Reflection");
            _Usings.Add("Robbiblubber.Util.Dynamic");

            bool first = true;
            foreach(MethodInfo i in InterfaceType.GetMethods())
            {
                if(i.IsSpecialName) continue;
                if(first) { first = false; } else { imp.AppendLine(); }

                string func = (_SubjectName + (Strict ? ".InvokeMethodStrict(" : ".InvokeMethod("));
                if(i.HasReturnValue())
                {
                    func = (_SubjectName + (Strict ? ".InvokeMethodResultStrict<T>(" : ".InvokeMethodResult<T>(")._ApplyType(i.ReturnType, true));
                }

                imp.AppendLine(i.GetDeclarationDecl(false, true, true, true, true, true));
                imp.AppendLine("{");

                __ParamStrings mpam = new __ParamStrings(i.GetParameters());
                imp.AppendLine("        ParameterModifier pam = new ParameterModifier(" + i.GetParameters().Length + ");");
                
                for(int k = 0; k < i.GetParameters().Length; k++)
                {
                    if(i.GetParameters()[k].IsOut) { imp.AppendLine("        pam[" + k.ToString() + "] = true;"); }
                }
                imp.AppendLine("        ");

                imp.Append("        ");
                if(i.HasReturnValue()) { imp.Append("return "); }
                imp.Append(func);
                imp.Append("\"" + i.GetSimpleNameDecl() + "\", " + mpam.Values);
                if(!Strict) { imp.Append(i.HasReturnValue() ? (", default(" + ((string) i.ReturnType.GetFullNameDecl(true)) + ")") : ", null"); }
                imp.AppendLine(", " + mpam.Types + ", new ParameterModifier[] { pam });");

                imp.AppendLine("}");
            }

            string pget = (_SubjectName + (Strict ? ".GetPropertyValueStrict<T>(" : ".GetPropertyValue<T>("));
            string pset = (_SubjectName + (Strict ? ".SetPropertyValueStrict<T>(" : ".SetPropertyValue<T>("));
            foreach(PropertyInfo i in InterfaceType.GetProperties())
            {
                if(i.IsSpecialName) continue;
                if(first) { first = false; } else { imp.AppendLine(); }

                imp.AppendLine(i.GetDeclarationDecl(false, true, true, true, true, true));
                imp.AppendLine("{");

                if(i.GetMethod != null)
                {
                    imp.Append("        get { return ");
                    imp.Append(pget._ApplyType(i.PropertyType, true));
                    imp.Append("\"" + i.Name + "\"");

                    if(i.IsIndexer())
                    {
                        if(!Strict) { imp.Append(", default(" + i.PropertyType.CommonNameDecl() + ")"); }
                        __ParamStrings mpam = new __ParamStrings(i.GetIndexParameters());
                        imp.Append(", " + mpam.Values);
                        imp.Append(", " + mpam.Types);
                    }
                    imp.AppendLine("); }");
                }

                if(i.SetMethod != null)
                {
                    imp.Append("        set { ");
                    imp.Append(pget._ApplyType(i.PropertyType, true));
                    imp.Append("\"" + i.Name + "\", value");

                    if(i.IsIndexer())
                    {
                        __ParamStrings mpam = new __ParamStrings(i.GetIndexParameters());
                        imp.Append(", " + mpam.Values);
                        imp.Append(", " + mpam.Types);
                    }
                    imp.AppendLine("); }");
                }

                imp.AppendLine("}");
            }

            imp.Replace("\r\n", "\n").Replace("\n", "\r\n        ");

            _ApplyUsings(code);
            code.Replace("{$Implementation}", imp.ToString());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __ClassBuilder                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Generates the class code.</summary>
        /// <returns>Code.</returns>
        protected override string _GenerateCode()
        {
            StringBuilder code = new StringBuilder(Resources.__InterfaceProxy);

            _ClassName = InterfaceType.GetSimpleNameDecl();
            if(_ClassName.StartsWith("I") && (_ClassName.Substring(1, 1) == _ClassName.Substring(1, 1).ToUpper()))
            {
                _ClassName = _Prefix + _ClassName.Substring(1) + "IntfcProxy__";
            }
            else { _ClassName = _Prefix + _ClassName + "IntfcProxy__"; }
            code.Replace("{$ClassName}", _ClassName);

            code.Replace("{$TargetClass}", InterfaceType.GetFullNameDecl(true));
            code.Replace("{$Subject}", _SubjectName = _PrivatePrefix + "Subject__");

            _RegisterEvents(code);
            _ImplementInterface(code);

            return code.ToString();
        }
    }
}
