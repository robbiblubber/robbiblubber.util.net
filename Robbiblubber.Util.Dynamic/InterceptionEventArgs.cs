﻿using System;
using System.Reflection;



namespace Robbiblubber.Util.Dynamic
{
    /// <summary>This class provides interception event arguments.</summary>
    public class InterceptionEventArgs: EventArgs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="member">Member.</param>
        /// <param name="parameters">Parameters.</param>
        public InterceptionEventArgs(MethodInfo member, object[] parameters)
        {
            Member = member;
            Parameters = parameters;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="member">Member.</param>
        /// <param name="parameters">Parameters.</param>
        /// <param name="result">Result.</param>
        public InterceptionEventArgs(MethodInfo member, object[] parameters, object result)
        {
            Member = member;
            Parameters = parameters;
            Result = result;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the invoked method.</summary>
        public MethodInfo Member { get; protected set; }


        /// <summary>Gets the method parameters.</summary>
        public object[] Parameters { get; protected set; }


        /// <summary>Gets or sets the result that has been returned or will be returned for a cancelled invocation.</summary>
        public object Result { get; set; }
    }
}
