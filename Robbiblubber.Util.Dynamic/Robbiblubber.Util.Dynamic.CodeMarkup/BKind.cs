﻿using System;



namespace Robbiblubber.Util.Dynamic.CodeMarkup
{
    /// <summary>This enumeration contains possible declaration string types.</summary>
    public enum BKind: int
    {
        /// <summary>Syntactical elements and signs.</summary>
        SYNTAX = 0,
        /// <summary>Element name.</summary>
        NAME = 1,
        /// <summary>Language key word.</summary>
        KEYWORD = 2,
        /// <summary>Type name.</summary>
        TYPE = 3,
        /// <summary>Identifier.</summary>
        IDENTIFIER = 4,
        /// <summary>Literal.</summary>
        LITERAL = 5,
        /// <summary>String literal.</summary>
        STRING = 6,
        /// <summary>Comment.</summary>
        COMMENT = 7
    }
}
