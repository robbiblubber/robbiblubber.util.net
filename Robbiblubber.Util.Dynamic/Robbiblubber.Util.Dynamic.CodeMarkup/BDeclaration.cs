﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Util.Dynamic.CodeMarkup
{
    /// <summary>This class defines a declaration.</summary>
    public class BDeclaration: IEnumerable<BString>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Items.</summary>
        protected List<BString> _Items = new List<BString>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public BDeclaration()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="kind">String kind.</param>
        /// <param name="text">String text.</param>
        public BDeclaration(BKind kind, string text)
        {
            _Items.Add(new BString(kind, text));
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="str">Strings.</param>
        public BDeclaration(params BString[] str)
        {
            _Items.AddRange(str);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="decl">Declarations.</param>
        public BDeclaration(params BDeclaration[] decl)
        {
            Append(decl);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the number of items in this instance.</summary>
        public int Count
        {
            get { return _Items.Count; }
        }


        /// <summary>Gets the declaration length.</summary>
        public int Length
        {
            get { return Text.Length; }
        }


        /// <summary>Gets the declaration as plain text.</summary>
        public string Text
        {
            get
            {
                string rval = "";
                foreach(BString i in _Items)
                {
                    rval += i.Text;
                }

                return rval;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Appends strings.</summary>
        /// <param name="str">Strings.</param>
        public void Append(params BString[] str)
        {
            _Items.AddRange(str);
        }


        /// <summary>Appends declarations.</summary>
        /// <param name="decl">Declarations.</param>
        public void Append(params BDeclaration[] decl)
        {
            foreach(BDeclaration i in decl)
            {
                _Items.AddRange(i._Items);
            }
        }


        /// <summary>Appends a string.</summary>
        /// <param name="text">String text.</param>
        public void Append(string text)
        {
            _Items.Add(new BString(text));
        }


        /// <summary>Appends a string.</summary>
        /// <param name="kind">String kind.</param>
        /// <param name="text">String text.</param>
        public void Append(BKind kind, string text)
        {
            _Items.Add(new BString(kind, text));
        }


        /// <summary>Appends a syntax space string.</summary>
        public void AppendBlank()
        {
            _Items.Add(new BString(" "));
        }


        /// <summary>Appends a syntax line break string.</summary>
        public void AppendBreak()
        {
            _Items.Add(new BString("\r\n"));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<BString>                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<BString> IEnumerable<BString>.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return Text;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // operators                                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Implicitly converts a declaration to a string.</summary>
        /// <param name="decl">Declaration.</param>
        public static implicit operator string(BDeclaration decl)
        {
            return decl.Text;
        }


        /// <summary>Implicitly converts a string to a declaration.</summary>
        /// <param name="str">String.</param>
        public static implicit operator BDeclaration(string str)
        {
            return new BDeclaration(BKind.SYNTAX, str);
        }


        /// <summary>Adds a declaration to a declaration.</summary>
        /// <param name="a">Declaration.</param>
        /// <param name="b">Declaration.</param>
        /// <returns>Declaration.</returns>
        public static BDeclaration operator +(BDeclaration a, BDeclaration b)
        {
            return new BDeclaration(a, b);
        }
    }
}
