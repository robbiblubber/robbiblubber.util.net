﻿using System;



namespace Robbiblubber.Util.Dynamic.CodeMarkup
{
    /// <summary>This class represents a string as part of a declaraion.</summary>
    public class BString
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public BString(): this(BKind.SYNTAX, "")
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="text">String text</param>
        public BString(string text): this(BKind.SYNTAX, text)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="kind">String kind.</param>
        public BString(BKind kind): this(kind, "")
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="kind">String kind.</param>
        /// <param name="text">String text</param>
        public BString(BKind kind, string text)
        {
            Kind = kind;
            Text = text;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the string kind.</summary>
        public BKind Kind
        {
            get; set;
        }


        /// <summary>Gets or sets the string text.</summary>
        public string Text
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return Text;
        }
    }
}
