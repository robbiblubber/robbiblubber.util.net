﻿using System;



namespace Robbiblubber.Util.Dynamic
{
    /// <summary>Handles an interception event.</summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Event arguments.</param>
    public delegate void InterceptionCancelEventHandler(object sender, InterceptionCancelEventArgs e);
}
