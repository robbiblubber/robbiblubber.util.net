using System;
using System.Reflection;



namespace Robbiblubber.Util.Dynamic.__DynamicGenerated__
{
    /// <summary>This class provides a proxy for the Foo class.</summary>
    public class {$ClassName}: {$TargetNamespace}.{$TargetClass}, IProxy<{$TargetClass}>, IProxy, IInterceptable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>List of event handlers for Before events.</summary>
        public __InterceptionEventList<{$TargetClass}, InterceptionCancelEventHandler> _Before = new __InterceptionEventList<{$TargetClass}, InterceptionCancelEventHandler>();

        /// <summary>List of event handlers for After events.</summary>
        public __InterceptionEventList<{$TargetClass}, InterceptionEventHandler> _After = new __InterceptionEventList<{$TargetClass}, InterceptionEventHandler>();
        
        /// <summary>Subject.</summary>
        private {$TargetClass} _Subject;

        /// <summary>Invocation handler.</summary>
        private IInvocationHandler _Handler = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="subject">Subject.</param>
        public __FooProxy({$TargetClass} subject)
        {
            _Subject = subject;
			
			{$EventRegister}
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Invokes a method.</summary>
        /// <param name="member">Method.</param>
        /// <param name="parameters">Method parameters.</param>
        /// <returns>Returns the method result.</returns>
        private object _Invoke(MethodInfo member, params object[] parameters)
        {
            InterceptionCancelEventArgs ce = new InterceptionCancelEventArgs(member, parameters);
            foreach(InterceptionCancelEventHandler i in _Before[member])
            {
                i.Invoke(_Subject, ce);
                if(ce.Cancel) { return ce.Result; }
            }

            object rval = ((_Handler == null) ? member.Invoke(_Subject, parameters) : _Handler.Handle(member, _Subject, parameters));

            InterceptionEventArgs me = new InterceptionEventArgs(member, parameters, rval);
            foreach(InterceptionEventHandler i in _After[member])
            {
                i.Invoke(_Subject, me);
            }

            return me.Result;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Foo                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        {$Overloads}
		
		
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		{$EventHandlers}
		
		

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IInterceptable                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Raised before a member is invoked.</summary>
        event InterceptionCancelEventHandler IInterceptable.BeforeInvocation
        {
            add    { ((IInterceptable) this).AddBeforeInvocationHandler(value); }
            remove { ((IInterceptable) this).RemoveBeforeInvocationHandler(value); }
        }


        /// <summary>Raised after a member has been invoked.</summary>
        event InterceptionEventHandler IInterceptable.AfterInvocation
        {
            add    { ((IInterceptable) this).AddAfterInvocationHandler(value); }
            remove { ((IInterceptable) this).RemoveAfterInvocationHandler(value); }
        }


        /// <summary>Gets or sets the invocation handler.</summary>
        IInvocationHandler IInterceptable.Handler 
        { 
            get { return _Handler; }
            set { _Handler = value; }
        }


        /// <summary>Adds an interception event handler for a specific members.</summary>
        /// <param name="handler">Event handler.</param>
        /// <param name="members">Members.</param>
        void IInterceptable.AddBeforeInvocationHandler(InterceptionCancelEventHandler handler, params MemberInfo[] members)
        {
            _Before.Add(handler, null);
        }


        /// <summary>Adds an interception event handler for a specific members.</summary>
        /// <param name="handler">Event handler.</param>
        /// <param name="members">Members.</param>
        void IInterceptable.AddAfterInvocationHandler(InterceptionEventHandler handler, params MemberInfo[] members)
        {
            _After.Add(handler, null);
        }


        /// <summary>Removes an interception event handler for a specific members.</summary>
        /// <param name="handler">Event handler.</param>
        /// <param name="members">Members.</param>
        void IInterceptable.RemoveBeforeInvocationHandler(InterceptionCancelEventHandler handler, params MemberInfo[] members)
        {
            _Before.Remove(handler, null);
        }


        /// <summary>Removes an interception event handler for a specific members.</summary>
        /// <param name="handler">Event handler.</param>
        /// <param name="members">Members.</param>
        void IInterceptable.RemoveAfterInvocationHandler(InterceptionEventHandler handler, params MemberInfo[] members)
        {
            _After.Remove(handler, null);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProxy                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the proxy base subject.</summary>
        object IProxy.Subject 
        { 
            get { return _Subject; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProxy<{$TargetClass}>                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the proxy base subject.</summary>
        {$TargetClass} IProxy<{$TargetClass}>.Subject
        {
            get { return _Subject; }
        }
    }
}
