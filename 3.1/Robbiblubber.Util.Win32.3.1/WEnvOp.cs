﻿using System;
using System.Runtime.Versioning;
using System.Security.Principal;

using Microsoft.Win32;



namespace Robbiblubber.Util.Win32
{
    /// <summary>This class provides environment information.</summary>
    /// <remarks>3.1 version.</remarks>
    public static class WEnvOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if the current identity is administrator.</summary>
        public static bool IsAdmin
        {
            get { return (new WindowsPrincipal(WindowsIdentity.GetCurrent())).IsInRole(WindowsBuiltInRole.Administrator); }
        }


        /// <summary>Registers an application.</summary>
        /// <param name="key">Application key.</param>
        /// <param name="name">Display name.</param>
        /// <param name="installDir">Installation directory.</param>
        /// <param name="icon">Display icon.</param>
        /// <param name="version">Version.</param>
        /// <param name="uninstall">Uninstall string.</param>
        /// <param name="modify">Modify string.</param>
        public static void RegisterApplication(string key, string name, string installDir, string icon, Version version, string uninstall = null, string modify = null)
        {
            RegisterApplication(key, name, null, installDir, icon, version, uninstall, modify);
        }


        /// <summary>Registers an application.</summary>
        /// <param name="key">Application key.</param>
        /// <param name="name">Display name.</param>
        /// <param name="publisher">Publisher name.</param>
        /// <param name="installDir">Installation directory.</param>
        /// <param name="icon">Display icon.</param>
        /// <param name="version">Version.</param>
        /// <param name="uninstall">Uninstall string.</param>
        /// <param name="modify">Modify string.</param>
        public static void RegisterApplication(string key, string name, string publisher, string installDir, string icon, Version version, string uninstall = null, string modify = null)
        {
            RegistryKey k = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Uninstall", true).CreateSubKey(key);

            k.SetValue("DisplayName", name, RegistryValueKind.String);
            if(!string.IsNullOrWhiteSpace(publisher)) { k.SetValue("Publisher", publisher, RegistryValueKind.String); }
            k.SetValue("InstallLocation", installDir, RegistryValueKind.ExpandString);
            if(!string.IsNullOrWhiteSpace(icon)) { k.SetValue("DisplayIcon", icon, RegistryValueKind.String); }
            k.SetValue("DisplayVersion", version.ToVersionString(), RegistryValueKind.String);
            k.SetValue("VersionMajor", version.Major, RegistryValueKind.DWord);
            k.SetValue("VersionMinor", version.Minor, RegistryValueKind.DWord);
            k.SetValue("VersionBuild", version.Build, RegistryValueKind.DWord);
            k.SetValue("VersionRevision", version.Revision, RegistryValueKind.DWord);

            if(!string.IsNullOrWhiteSpace(uninstall)) { k.SetValue("UninstallString", uninstall, RegistryValueKind.ExpandString); }
            if(!string.IsNullOrWhiteSpace(modify)) { k.SetValue("ModifyPath", modify); }
            k.Close();
            k.Dispose();
        }
    }
}
