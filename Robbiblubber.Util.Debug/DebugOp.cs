﻿using System;
using System.IO;
using System.Net;



namespace Robbiblubber.Util.Debug
{
    /// <summary>This class provides debug utility methods.</summary>
    public static class DebugOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Divider constant.</summary>
        private const string _DIVIDER = "------------------------------------------------------------------------------------------------------------------------";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Debug file name.</summary>
        private static string _File = null;


        /// <summary>Enable debugging.</summary>
        private static bool _Enabled = false;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Dumps an exception to the debug log and displays a message.</summary>
        /// <param name="exception">Exception.</param>
        /// <param name="debugKey">Debug key.</param>
        /// <param name="rethrow">Determines if the exception should be re-thrown.</param>
        public static void DumpMessage(this Exception exception, string debugKey, bool rethrow = false)
        {
            __Dialogs.ShowMessageBox(debugKey, exception.Message);
            Dump(debugKey, exception, rethrow);
        }


        /// <summary>Dumps an exception to the debug log.</summary>
        /// <param name="exception">Exception.</param>
        /// <param name="debugKey">Debug key.</param>
        /// <param name="rethrow">Determines if the exception should be re-thrown.</param>
        public static void Dump(this Exception exception, string debugKey, bool rethrow = false)
        {
            if(!_Enabled) return;
            if(_File != null)
            {
                File.AppendAllText(_File, "\r\n\r\n" + _DIVIDER + "\r\n[" + debugKey + "]   " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\r\n" + exception.ToString());
            }

            if(rethrow) throw exception;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the debug text.</summary>
        public static string Text
        {
            get
            {
                if(_Enabled && File.Exists(_File))
                {
                    return File.ReadAllText(_File);
                }

                return "";
            }
        }


        /// <summary>Gets or sets if the debugging logger is enabled.</summary>
        public static bool Enabled
        {
            get { return _Enabled; }
            set
            {
                if(_Enabled == value) return;

                string n = "";

                if(_Enabled = value)
                {
                    while(true)
                    {
                        _File = Path.GetTempPath() + "\\" + (n = StringOp.Random(24)) + ".debug.raw";
                        if(!File.Exists(_File)) break;
                    }

                    string init = "DUMP #" + n + "\r\n" +
                                  "Time:      " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\r\n" +
                                  "Computer:  " + Environment.MachineName;

                    try
                    {
                        init += " (" + Dns.GetHostName() + ")\r\n";
                    }
                    catch(Exception) {}

                    try
                    {
                        init += "IP:        " + Dns.GetHostAddresses(Dns.GetHostName())[0].ToString() + "\r\n";
                    }
                    catch(Exception) {}

                    init += "User:      " + Environment.UserDomainName + "\\" + Environment.UserName + "\r\n" +
                            "Command:   " + Environment.CommandLine + "\r\n";

                    File.WriteAllText(_File, init);
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Dumps an exception to the debug log and displays a message.</summary>
        /// <param name="debugKey">Debug key.</param>
        /// <param name="exception">Exception.</param>
        /// <param name="rethrow">Determines if the exception should be re-thrown.</param>
        public static void DumpMessage(string debugKey, Exception exception, bool rethrow = false)
        {
            DumpMessage(exception, debugKey, rethrow);
        }


        /// <summary>Dumps an exception to the debug log.</summary>
        /// <param name="debugKey">Debug key.</param>
        /// <param name="exception">Exception.</param>
        /// <param name="rethrow">Determines if the exception should be re-thrown.</param>
        public static void Dump(string debugKey, Exception exception, bool rethrow = false)
        {
            Dump(exception, debugKey, rethrow);
        }


        /// <summary>Dumps an exception to the debug log.</summary>
        /// <param name="debugKey">Debug key.</param>
        /// <param name="text">Debug text.</param>
        public static void Dump(string debugKey, string text)
        {
            if(!_Enabled) return;

            File.AppendAllText(_File, "\r\n\r\n" + _DIVIDER + "\r\n[" + debugKey + "]   " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "\r\n" + text);
        }


        /// <summary>Creates a dump file.</summary>
        /// <param name="fileName">File name.</param>
        public static void CreateDumpFile(string fileName)
        {
            File.WriteAllText(fileName, Text);
        }


        /// <summary>Uploads debug data.</summary>
        /// <param name="targetURL">Target URL.</param>
        /// <param name="metaData">Metadata.</param>
        /// <param name="dialog">Determines if a dialog should be used.</param>
        public static void UploadData(string targetURL, string metaData, bool dialog)
        {
            if(metaData == null) { metaData = ""; }

            __DumpUploader d = new __DumpUploader(targetURL, Text);
            if(dialog)
            {
                __Dialogs.SendData(Text, new Action<string>(d.UploadMetaData), new Action(d.UploadData), new Action(d.Dispose));
            }
            else
            {
                d.UploadMetaData(metaData);
                d.UploadData();
                d.Dispose();
            }
        }


        /// <summary>Uploads debug data.</summary>
        /// <param name="targetURL">Target URL.</param>
        /// <param name="metaData">Metadata.</param>
        public static void UploadData(string targetURL, string metaData)
        {
            UploadData(targetURL, metaData, metaData == null ? true : false);
        }


        /// <summary>Uploads debug data.</summary>
        /// <param name="targetURL">Target URL.</param>
        public static void UploadData(string targetURL)
        {
            UploadData(targetURL, null, true);
        }
        
        
        /// <summary>Shows the log data.</summary>
        public static void ShowLog()
        {
            __Dialogs.ShowLog(Text);
        }
    }
}
