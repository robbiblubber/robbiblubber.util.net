﻿using System;
using System.IO;
using System.Net;



namespace Robbiblubber.Util.Debug
{
    /// <summary>This class uploads debug information.</summary>
    internal class __DumpUploader: IDisposable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Temporary directory.</summary>
        private string _TempDir = null;

        /// <summary>Metadata file.</summary>
        private string _MetaFile = null;

        /// <summary>Data file.</summary>
        private string _DataFile = null;

        /// <summary>Target URL.</summary>
        private string _URL;

        /// <summary>ID.</summary>
        private string _ID;

        /// <summary>Web client.</summary>
        private WebClient _WC;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="targetUrl">Target URL.</param>
        /// <param name="data">Data.</param>
        public __DumpUploader(string targetUrl, string data = null)
        {
            _URL = (targetUrl.TrimEnd('/') + "/");

            _TempDir = FileOp.CreateTempPath();

            if(data == null) { data = ""; }
            File.WriteAllText(_DataFile = (_TempDir + @"\data"), data);

            _WC = new WebClient();
            _WC.Proxy.Credentials = CredentialCache.DefaultCredentials;
            _WC.Headers.Add("user-agent", "Robbiblubber.Util.Debug");
            Ddp result = new Ddp(_WC.DownloadString(_URL + "createid.php"));

            _ID = result["result"];
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Uploads metadata.</summary>
        /// <param name="meta">Metadata.</param>
        public void UploadMetaData(string meta)
        {
            File.WriteAllText(_MetaFile = (_TempDir + @"\meta"), meta);

            _WC.Headers.Add("user-agent", "Robbiblubber.Util.Debug");
            _WC.Headers.Add("Referer", _URL + "receive.php?id=" + _ID + "&opcode=upload");
            _WC.UploadFile(_URL + "receive.php?id=" + _ID + "&target=meta", "POST", _MetaFile);

            try { File.Delete(_MetaFile); } catch(Exception) {}
        }


        /// <summary>Uploads the chunk with the given number.</summary>
        public void UploadData()
        {
            _WC.Headers.Add("user-agent", "Robbiblubber.Util.Debug");
            _WC.Headers.Add("Referer", _URL + "receive.php?id=" + _ID + "&opcode=upload");
            _WC.UploadFile(_URL + "receive.php?id=" + _ID + "&target=data", "POST", _DataFile);

            try { File.Delete(_DataFile); } catch(Exception) {}
            try { Directory.Delete(_TempDir, true); } catch(Exception) {}
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Disposes the object.</summary>
        public void Dispose()
        {
            _WC.Dispose();
        }
    }
}
