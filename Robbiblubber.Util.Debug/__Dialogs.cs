﻿using System;
using System.Reflection;



namespace Robbiblubber.Util.Debug
{
    /// <summary>This class provides stubs for dialog classes.</summary>
    internal static class __Dialogs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>DebugDialogs class type.</summary>
        private static Type _Dialogs = null;

        /// <summary>ShowMessageBox() method.</summary>
        private static MethodInfo _ShowMessageBox = null;

        /// <summary>ShowLog() method.</summary>
        private static MethodInfo _ShowLog = null;

        /// <summary>SendData() method.</summary>
        private static MethodInfo _SendData = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Shows a message box.</summary>
        /// <param name="debugKey">Debug key.</param>
        /// <param name="msg">Exception message.</param>
        public static void ShowMessageBox(string debugKey, string msg)
        {
            _Load();
            _ShowMessageBox.Invoke(null, new object[] { debugKey, msg });
        }

        
        /// <summary>Shows debug log.</summary>
        /// <param name="text">Log text.</param>
        public static void ShowLog(string text)
        {
            _Load();
            _ShowLog.Invoke(null, new object[] { text });
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Debug data.</param>
        /// <param name="sndMeta">Metadata sending method.</param>
        /// <param name="sndData">Data sending method.</param>
        /// <param name="cleanup">Cleanup method.</param>
        public static void SendData(string data, Action<string> sndMeta, Action sndData, Action cleanup)
        {
            _Load();
            _SendData.Invoke(null, new object[] { data, sndMeta, sndData, cleanup });
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads the debug dialog library and methods.</summary>
        private static void _Load()
        {
            if(_Dialogs == null)
            {
                _Dialogs = ClassOp.LoadType("Robbiblubber.Util.Debug.Dialogs.DebugDialogs");

                if(_Dialogs != null)
                {
                    _ShowMessageBox = _Dialogs.GetMethod("ShowMessageBox");
                    _ShowLog = _Dialogs.GetMethod("ShowLog");
                    _SendData = _Dialogs.GetMethod("SendData");
                }
            }
        }
    }
}
