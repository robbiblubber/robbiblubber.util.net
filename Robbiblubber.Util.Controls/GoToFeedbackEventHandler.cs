﻿using System;



namespace Robbiblubber.Util.Controls
{
    /// <summary>Event handler delegate for GoTo-events.</summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Event arguments.</param>
    public delegate void GoToFeedbackEventHandler(object sender, GoToFeedbackEventArgs e);



    /// <summary>Event argument class.</summary>
    public class GoToFeedbackEventArgs: EventArgs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="line">Currently chosen line.</param>
        public GoToFeedbackEventArgs(int line): base()
        {
            Line = line;
            Allowed = true;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the line value chosen.</summary>
        public int Line { get; }


        /// <summary>Gets or sets if the line value is allowed.</summary>
        public bool Allowed { get; set; }
    }
}
