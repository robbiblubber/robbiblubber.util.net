﻿namespace Robbiblubber.Util.Controls
{
    partial class WaitControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WaitControl));
            this._LabelAnimation = new System.Windows.Forms.Label();
            this._LabelText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _LabelAnimation
            // 
            this._LabelAnimation.Image = ((System.Drawing.Image)(resources.GetObject("_LabelAnimation.Image")));
            this._LabelAnimation.Location = new System.Drawing.Point(23, 15);
            this._LabelAnimation.Name = "_LabelAnimation";
            this._LabelAnimation.Size = new System.Drawing.Size(35, 30);
            this._LabelAnimation.TabIndex = 0;
            // 
            // _LabelText
            // 
            this._LabelText.Location = new System.Drawing.Point(85, 21);
            this._LabelText.Name = "_LabelText";
            this._LabelText.Size = new System.Drawing.Size(336, 19);
            this._LabelText.TabIndex = 0;
            // 
            // WaitControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this._LabelText);
            this.Controls.Add(this._LabelAnimation);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "WaitControl";
            this.Size = new System.Drawing.Size(450, 63);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label _LabelAnimation;
        private System.Windows.Forms.Label _LabelText;
    }
}
