﻿using System;
using System.Windows.Forms;

using Microsoft.Win32;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class provides application layout functionality.</summary>
    public static class LayoutOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the configuration file name.</summary>
        public static string FileName { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Initializes the layout provider.</summary>
        /// <param name="fileName">Configuration file name.</param>
        public static void Initialize(string fileName = null)
        {
            if(fileName == null)
            {
                FileName = PathOp.UserConfigurationPath + @"\application.layout";
            }
            else
            {
                FileName = fileName;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the current form layout.</summary>
        /// <param name="form">Form.</param>
        /// <param name="name">Form name.</param>
        public static void SaveLayout(this Form form, string name = null)
        {
            if(FileName == null) Initialize();

            if(name == null) { name = form.Name; }
            

            Ddp cfg;
            try
            {
                cfg = Ddp.Open(FileName);
            }
            catch(Exception) { cfg = new Ddp(); }

            if(form.WindowState != FormWindowState.Minimized)
            {
                cfg.Set(name + "/state", (int) form.WindowState);
            }

            if(form.WindowState != FormWindowState.Maximized)
            {
                cfg.Set(name + "/pos:x", form.Left);
                cfg.Set(name + "/pos:y", form.Top);
                cfg.Set(name + "/size:x", form.Width);
                cfg.Set(name + "/size:y", form.Height);
            }

            cfg.Save(FileName);
        }


        /// <summary>Restore the form layout.</summary>
        /// <param name="form">Form.</param>
        /// <param name="name">Form name.</param>
        public static void RestoreLayout(this Form form, string name = null)
        {
            if(FileName == null) Initialize();

            if(name == null) { name = form.Name; }
            try
            {
                Ddp cfg = Ddp.Open(FileName);

                form.Left = cfg.Get(name + "/pos:x", form.Left);
                form.Top = cfg.Get(name + "/pos:y", form.Top);
                form.Width = cfg.Get(name + "/size:x", form.Width);
                form.Height = cfg.Get(name + "/size:y", form.Height);
                form.WindowState = (FormWindowState) cfg.Get(name + "/state", (int) form.WindowState);
            }
            catch(Exception) {}
        }


        /// <summary>Saves the current grid view layout.</summary>
        /// <param name="gridview">Grid view control.</param>
        /// <param name="name">Control name.</param>
        /// <param name="formName">Form name.</param>
        public static void SaveLayout(this DataGridView gridview, string name = null, string formName = null)
        {
            Control p = gridview.Parent;
            while(!(p is Form)) { p = p.Parent; }

            if(formName == null) { formName = p.Name; }
            if(name == null) { name = gridview.Name; }

            Ddp cfg = null;
            cfg = Ddp.Open(FileName);

            for(int i = 0; i < gridview.Columns.Count; i++)
            {
                cfg[formName + '/' + name + "/col:" + i.ToString()].Value = gridview.Columns[i].Width;
            }

            cfg.Save();
        }


        /// <summary>Restores the grid view layout.</summary>
        /// <param name="gridview">Grid view control.</param>
        /// <param name="name">Control name.</param>
        /// <param name="formName">Form name.</param>
        public static void RestoreLayout(this DataGridView gridview, string name = null, string formName = null)
        {
            Control p = gridview.Parent;
            while(!(p is Form)) { p = p.Parent; }

            if(((Form) p).WindowState == FormWindowState.Maximized) return;

            if(formName == null) { formName = p.Name; }
            if(name == null) { name = gridview.Name; }

            Ddp cfg = null;
            cfg = Ddp.Open(FileName);

            for(int i = 0; i < gridview.Columns.Count; i++)
            {
                try
                {
                    gridview.Columns[i].Width = cfg.Get(formName + '/' + name + "/col:" + i.ToString(), gridview.Columns[i].Width);
                }
                catch(Exception) {}
            }
        }


        /// <summary>Saves the current split container layout.</summary>
        /// <param name="split">Split control.</param>
        /// <param name="name">Control name.</param>
        /// <param name="formName">Form name.</param>
        public static void SaveLayout(this SplitContainer split, string name = null, string formName = null)
        {
            Control p = split.Parent;
            while(!(p is Form)) { p = p.Parent; }

            if(((Form) p).WindowState == FormWindowState.Maximized) return;

            if(formName == null) { formName = p.Name; }
            if(name == null) { name = split.Name; }
            
            Ddp cfg = Ddp.Open(FileName);

            cfg[formName + '/' + name + "/div:x"].Value = split.SplitterDistance;
            cfg.Save();
        }


        /// <summary>Restores the split container layout.</summary>
        /// <param name="split">Split control.</param>
        /// <param name="name">Control name.</param>
        /// <param name="formName">Form name.</param>
        public static void RestoreLayout(this SplitContainer split, string name = null, string formName = null)
        {
            Control p = split.Parent;
            while(!(p is Form)) { p = p.Parent; }

            if(((Form) p).WindowState == FormWindowState.Maximized) return;

            if(formName == null) { formName = p.Name; }
            if(name == null) { name = split.Name; }
            
            try
            {
                Ddp cfg = Ddp.Open(FileName);
                split.SplitterDistance = cfg.Get(formName + '/' + name + "/div:x", split.SplitterDistance);
            }
            catch(Exception) {}
        }


        /// <summary>Sets data.</summary>
        /// <param name="form">Form.</param>
        /// <param name="key">Data key.</param>
        /// <param name="value">Data value.</param>
        /// <param name="name">Form name.</param>
        public static void SetData(this Form form, string key, string value, string name = null)
        {
            if(FileName == null) return;

            if(name == null) { name = form.Name; }
            
            Ddp cfg = Ddp.Open(FileName);

            cfg[name + "/" + key].Value = value;
            cfg.Save();
        }


        /// <summary>Sets data.</summary>
        /// <param name="form">Form.</param>
        /// <param name="key">Data key.</param>
        /// <param name="value">Data value.</param>
        /// <param name="name">Form name.</param>
        public static void SetData(this Form form, string key, int value, string name = null)
        {
            if(FileName == null) return;

            if(name == null) { name = form.Name; }
            
            Ddp cfg = Ddp.Open(FileName);

            cfg[name + "/" + key].Value = value;
            cfg.Save();
        }


        /// <summary>Gets data.</summary>
        /// <param name="form">Form.</param>
        /// <param name="key">Data key.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="name">Form name.</param>
        /// <returns>Value.</returns>
        public static int GetInteger(this Form form, string key, int defaultValue = 0, string name = null)
        {
            if(FileName == null) Initialize();

            if(name == null) { name = form.Name; }
            
            Ddp cfg = Ddp.Open(FileName);
            return cfg.Get(name + "/" + key, defaultValue);
        }


        /// <summary>Gets data.</summary>
        /// <param name="form">Form.</param>
        /// <param name="key">Data key.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="name">Form name.</param>
        /// <returns>Value.</returns>
        public static bool GetBoolean(this Form form, string key, bool defaultValue = false, string name = null)
        {
            if(FileName == null) Initialize();

            if(name == null) { name = form.Name; }
            
            Ddp cfg = Ddp.Open(FileName);
            return cfg.Get(name + "/" + key, defaultValue);
        }


        /// <summary>Gets data.</summary>
        /// <param name="form">Form.</param>
        /// <param name="key">Data key.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="name">Form name.</param>
        /// <returns>Value.</returns>
        public static string GetString(this Form form, string key, string defaultValue = "", string name = null)
        {
            if(FileName == null) Initialize();

            if(name == null) { name = form.Name; }
            
            Ddp cfg = Ddp.Open(FileName);
            return cfg.Get(name + "/" + key, defaultValue);
        }
    }
}
