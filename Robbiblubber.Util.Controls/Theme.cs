﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Robbiblubber.Util.Controls
{
    /// <summary>This class represents a theme.</summary>
    public sealed class Theme
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>File name.</summary>
        private string _FileName = null;

        /// <summary>Color scheme.</summary>
        private ThemeColorScheme _ColorScheme = null;

        /// <summary>Color table.</summary>
        private ThemeColorTable _ColorTable = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="fileName">File name.</param>
        internal Theme(string fileName)
        {
            Ddp ddp = Ddp.Open(_FileName = fileName);

            Name = ddp["Theme/Name"];
            Type = ddp["Theme/Type"].GetValue<ThemeType>();
        }


        /// <summary>Creates a new instance of this class.</summary>
        public Theme()
        {
            Name = "System";
            Type = ThemeType.DEFAULT;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the theme name.</summary>
        public string Name { get; private set; }


        /// <summary>Gets the basic theme type.</summary>
        public ThemeType Type { get; private set; }


        /// <summary>Gets the theme color scheme for this theme.</summary>
        public ThemeColorScheme ColorScheme
        {
            get
            {
                if(_ColorScheme == null)
                {
                    _ColorScheme = new ThemeColorScheme(Ddp.Open(_FileName)["Scheme"]);
                }

                return _ColorScheme;
            }
        }


        /// <summary>Gets a color table for this theme.</summary>
        public ThemeColorTable ColorTable
        {
            get
            {
                if(_ColorTable == null)
                {
                    _ColorTable = new ThemeColorTable(Ddp.Open(_FileName)["ColorTable"]);
                }

                return _ColorTable;
            }
        }


        /// <summary>Saves the theme.</summary>
        /// <param name="file">File name.</param>
        public void Save(string file)
        {
            Ddp ddp = new Ddp();
            ColorScheme._Save(ddp["Scheme"]);
            ColorTable._Save(ddp["ColorTable"]);
            ddp.Save(file);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representsion for this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return Name;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] ThemeColorScheme                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This cass provides theme color data.</summary>
        public sealed class ThemeColorScheme
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="ddp">Theme definition.</param>
            internal ThemeColorScheme(DdpElement ddp)
            {
                ActiveBorder = ColorOp.FromHex(ddp.Get("ActiveBorder", SystemColors.ActiveBorder.ToHex()));
                ActiveCaption = ColorOp.FromHex(ddp.Get("ActiveCaption", SystemColors.ActiveCaption.ToHex()));
                ActiveCaptionText = ColorOp.FromHex(ddp.Get("ActiveCaptionText", SystemColors.ActiveCaptionText.ToHex()));
                AppWorkspace = ColorOp.FromHex(ddp.Get("AppWorkspace", SystemColors.AppWorkspace.ToHex()));
                ButtonFace = ColorOp.FromHex(ddp.Get("ButtonFace", SystemColors.ButtonFace.ToHex()));
                ButtonHighlight = ColorOp.FromHex(ddp.Get("ButtonHighlight", SystemColors.ButtonHighlight.ToHex()));
                ButtonShadow = ColorOp.FromHex(ddp.Get("ButtonShadow", SystemColors.ButtonShadow.ToHex()));
                Control = ColorOp.FromHex(ddp.Get("Control", SystemColors.Control.ToHex()));
                ControlDark = ColorOp.FromHex(ddp.Get("ControlDark", SystemColors.ControlDark.ToHex()));
                ControlDarkDark = ColorOp.FromHex(ddp.Get("ControlDarkDark", SystemColors.ControlDarkDark.ToHex()));
                ControlLight = ColorOp.FromHex(ddp.Get("ControlLight", SystemColors.ControlLight.ToHex()));
                ControlLightLight = ColorOp.FromHex(ddp.Get("ControlLightLight", SystemColors.ControlLightLight.ToHex()));
                ControlText = ColorOp.FromHex(ddp.Get("ControlText", SystemColors.ControlText.ToHex()));
                Desktop = ColorOp.FromHex(ddp.Get("Desktop", SystemColors.Desktop.ToHex()));
                GradientActiveCaption = ColorOp.FromHex(ddp.Get("GradientActiveCaption", SystemColors.GradientActiveCaption.ToHex()));
                GradientInactiveCaption = ColorOp.FromHex(ddp.Get("GradientInactiveCaption", SystemColors.GradientInactiveCaption.ToHex()));
                GrayText = ColorOp.FromHex(ddp.Get("GrayText", SystemColors.GrayText.ToHex()));
                Highlight = ColorOp.FromHex(ddp.Get("Highlight", SystemColors.Highlight.ToHex()));
                HighlightText = ColorOp.FromHex(ddp.Get("HighlightText", SystemColors.HighlightText.ToHex()));
                HotTrack = ColorOp.FromHex(ddp.Get("HotTrack", SystemColors.HotTrack.ToHex()));
                InactiveBorder = ColorOp.FromHex(ddp.Get("InactiveBorder", SystemColors.InactiveBorder.ToHex()));
                InactiveCaption = ColorOp.FromHex(ddp.Get("InactiveCaption", SystemColors.InactiveCaption.ToHex()));
                InactiveCaptionText = ColorOp.FromHex(ddp.Get("InactiveCaptionText", SystemColors.InactiveCaptionText.ToHex()));
                Info = ColorOp.FromHex(ddp.Get("Info", SystemColors.Info.ToHex()));
                InfoText = ColorOp.FromHex(ddp.Get("InfoText", SystemColors.InfoText.ToHex()));
                Menu = ColorOp.FromHex(ddp.Get("Menu", SystemColors.Menu.ToHex()));
                MenuBar = ColorOp.FromHex(ddp.Get("MenuBar", SystemColors.MenuBar.ToHex()));
                MenuHighlight = ColorOp.FromHex(ddp.Get("MenuHighlight", SystemColors.MenuHighlight.ToHex()));
                MenuText = ColorOp.FromHex(ddp.Get("MenuText", SystemColors.MenuText.ToHex()));
                ScrollBar = ColorOp.FromHex(ddp.Get("ScrollBar", SystemColors.ScrollBar.ToHex()));
                Window = ColorOp.FromHex(ddp.Get("Window", SystemColors.Window.ToHex()));
                WindowFrame = ColorOp.FromHex(ddp.Get("WindowFrame", SystemColors.WindowFrame.ToHex()));
                WindowText = ColorOp.FromHex(ddp.Get("WindowText", SystemColors.WindowText.ToHex()));
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // internal methods                                                                                             //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Saves the theme to a ddp element.</summary>
            /// <param name="ddp">Theme definition.</param>
            internal void _Save(DdpElement ddp)
            {
                ddp.Set("ActiveBorder", ActiveBorder.ToHex());
                ddp.Set("ActiveCaption", ActiveCaption.ToHex());
                ddp.Set("ActiveCaptionText", ActiveCaptionText.ToHex());
                ddp.Set("AppWorkspace", AppWorkspace.ToHex());
                ddp.Set("ButtonFace", ButtonFace.ToHex());
                ddp.Set("ButtonHighlight", ButtonHighlight.ToHex());
                ddp.Set("ButtonShadow", ButtonShadow.ToHex());
                ddp.Set("Control", Control.ToHex());
                ddp.Set("ControlDark", ControlDark.ToHex());
                ddp.Set("ControlDarkDark", ControlDarkDark.ToHex());
                ddp.Set("ControlLight", ControlLight.ToHex());
                ddp.Set("ControlLightLight", ControlLightLight.ToHex());
                ddp.Set("ControlText", ControlText.ToHex());
                ddp.Set("Desktop", Desktop.ToHex());
                ddp.Set("GradientActiveCaption", GradientActiveCaption.ToHex());
                ddp.Set("GradientInactiveCaption", GradientInactiveCaption.ToHex());
                ddp.Set("GrayText", GrayText.ToHex());
                ddp.Set("Highlight", Highlight.ToHex());
                ddp.Set("HighlightText", HighlightText.ToHex());
                ddp.Set("HotTrack", HotTrack.ToHex());
                ddp.Set("InactiveBorder", InactiveBorder.ToHex());
                ddp.Set("InactiveCaption", InactiveCaption.ToHex());
                ddp.Set("InactiveCaptionText", InactiveCaptionText.ToHex());
                ddp.Set("Info", Info.ToHex());
                ddp.Set("InfoText", InfoText.ToHex());
                ddp.Set("Menu", Menu.ToHex());
                ddp.Set("MenuBar", MenuBar.ToHex());
                ddp.Set("MenuHighlight", MenuHighlight.ToHex());
                ddp.Set("MenuText", MenuText.ToHex());
                ddp.Set("ScrollBar", ScrollBar.ToHex());
                ddp.Set("Window", Window.ToHex());
                ddp.Set("WindowFrame", WindowFrame.ToHex());
                ddp.Set("WindowText", WindowText.ToHex());
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public properties                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets the active border color.</summary>
            public Color ActiveBorder
            {
                get; private set; 
            }


            /// <summary>Gets the active caption color.</summary>
            public Color ActiveCaption
            {
                get; private set;
            }


            /// <summary>Gets the active caption text color.</summary>
            public Color ActiveCaptionText
            {
                get; private set;
            }


            /// <summary>Gets the application workspace color.</summary>
            public Color AppWorkspace
            {
                get; private set;
            }


            /// <summary>Gets the button face color.</summary>
            public Color ButtonFace
            {
                get; private set;
            }


            /// <summary>Gets the button highlight color.</summary>
            public Color ButtonHighlight
            {
                get; private set;
            }


            /// <summary>Gets the button shadow color.</summary>
            public Color ButtonShadow
            {
                get; private set;
            }


            /// <summary>Gets the control color.</summary>
            public Color Control
            {
                get; private set;
            }


            /// <summary>Gets the control dark color.</summary>
            public Color ControlDark
            {
                get; private set;
            }


            /// <summary>Gets the control dark (dark) color.</summary>
            public Color ControlDarkDark
            {
                get; private set;
            }


            /// <summary>Gets the control light color.</summary>
            public Color ControlLight
            {
                get; private set;
            }


            /// <summary>Gets the control light (light) color.</summary>
            public Color ControlLightLight
            {
                get; private set;
            }


            /// <summary>Gets the control text color.</summary>
            public Color ControlText
            {
                get; private set;
            }


            /// <summary>Gets the desktop color.</summary>
            public Color Desktop
            {
                get; private set;
            }


            /// <summary>Gets the gradient active caption color.</summary>
            public Color GradientActiveCaption
            {
                get; private set;
            }


            /// <summary>Gets the gradient inactive caption color.</summary>
            public Color GradientInactiveCaption
            {
                get; private set;
            }


            /// <summary>Gets the gray text color.</summary>
            public Color GrayText
            {
                get; private set;
            }


            /// <summary>Gets the highlight color.</summary>
            public Color Highlight
            {
                get; private set;
            }


            /// <summary>Gets the highlight text color.</summary>
            public Color HighlightText
            {
                get; private set;
            }


            /// <summary>Gets the hot track color.</summary>
            public Color HotTrack
            {
                get; private set;
            }


            /// <summary>Gets the inactive border color.</summary>
            public Color InactiveBorder
            {
                get; private set;
            }


            /// <summary>Gets the inactive caption color.</summary>
            public Color InactiveCaption
            {
                get; private set;
            }


            /// <summary>Gets the inactive caption text color.</summary>
            public Color InactiveCaptionText
            {
                get; private set;
            }


            /// <summary>Gets the tool tip info color.</summary>
            public Color Info
            {
                get; private set;
            }


            /// <summary>Gets the tool tip info text color.</summary>
            public Color InfoText
            {
                get; private set;
            }


            /// <summary>Gets the menu color.</summary>
            public Color Menu
            {
                get; private set;
            }


            /// <summary>Gets the menu bar color.</summary>
            public Color MenuBar
            {
                get; private set;
            }


            /// <summary>Gets the menu highlight color.</summary>
            public Color MenuHighlight
            {
                get; private set;
            }


            /// <summary>Gets the menu text color.</summary>
            public Color MenuText
            {
                get; private set;
            }


            /// <summary>Gets the scroll bar color.</summary>
            public Color ScrollBar
            {
                get; private set;
            }


            /// <summary>Gets the window color.</summary>
            public Color Window
            {
                get; private set;
            }


            /// <summary>Gets the window frame color.</summary>
            public Color WindowFrame
            {
                get; private set;
            }


            /// <summary>Gets the window text color.</summary>
            public Color WindowText
            {
                get; private set;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] ThemeColorTable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>This class provides a color table</summary>
        public sealed class ThemeColorTable: ProfessionalColorTable
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private members                                                                                              //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>ButtonSelectedHighlight color.</summary>
            private Color _ButtonSelectedHighlight;

            /// <summary>ButtonSelectedHighlightBorder color.</summary>
            private Color _ButtonSelectedHighlightBorder;

            /// <summary>ButtonPressedHighlight color.</summary>
            private Color _ButtonPressedHighlight;

            /// <summary>ButtonPressedHighlightBorder color.</summary>
            private Color _ButtonPressedHighlightBorder;

            /// <summary>ButtonCheckedHighlight color.</summary>
            private Color _ButtonCheckedHighlight;

            /// <summary>ButtonCheckedHighlightBorder color.</summary>
            private Color _ButtonCheckedHighlightBorder;

            /// <summary>ButtonPressedBorder color.</summary>
            private Color _ButtonPressedBorder;

            /// <summary>ButtonSelectedBorder color.</summary>
            private Color _ButtonSelectedBorder;

            /// <summary>ButtonCheckedGradientBegin color.</summary>
            private Color _ButtonCheckedGradientBegin;

            /// <summary>ButtonCheckedGradientMiddle color.</summary>
            private Color _ButtonCheckedGradientMiddle;

            /// <summary>ButtonCheckedGradientEnd color.</summary>
            private Color _ButtonCheckedGradientEnd;

            /// <summary>ButtonSelectedGradientBegin color.</summary>
            private Color _ButtonSelectedGradientBegin;

            /// <summary>ButtonSelectedGradientMiddle color.</summary>
            private Color _ButtonSelectedGradientMiddle;

            /// <summary>ButtonSelectedGradientEnd color.</summary>
            private Color _ButtonSelectedGradientEnd;

            /// <summary>ButtonPressedGradientBegin color.</summary>
            private Color _ButtonPressedGradientBegin;

            /// <summary>ButtonPressedGradientMiddle color.</summary>
            private Color _ButtonPressedGradientMiddle;

            /// <summary>ButtonPressedGradientEnd color.</summary>
            private Color _ButtonPressedGradientEnd;

            /// <summary>CheckBackground color.</summary>
            private Color _CheckBackground;

            /// <summary>CheckSelectedBackground color.</summary>
            private Color _CheckSelectedBackground;

            /// <summary>CheckPressedBackground color.</summary>
            private Color _CheckPressedBackground;

            /// <summary>GripDark color.</summary>
            private Color _GripDark;

            /// <summary>GripLight color.</summary>
            private Color _GripLight;

            /// <summary>ImageMarginGradientBegin color.</summary>
            private Color _ImageMarginGradientBegin;

            /// <summary>ImageMarginGradientMiddle color.</summary>
            private Color _ImageMarginGradientMiddle;

            /// <summary>ImageMarginGradientEnd color.</summary>
            private Color _ImageMarginGradientEnd;

            /// <summary>ImageMarginRevealedGradientBegin color.</summary>
            private Color _ImageMarginRevealedGradientBegin;

            /// <summary>ImageMarginRevealedGradientMiddle color.</summary>
            private Color _ImageMarginRevealedGradientMiddle;

            /// <summary>ImageMarginRevealedGradientEnd color.</summary>
            private Color _ImageMarginRevealedGradientEnd;

            /// <summary>MenuStripGradientBegin color.</summary>
            private Color _MenuStripGradientBegin;

            /// <summary>MenuStripGradientEnd color.</summary>
            private Color _MenuStripGradientEnd;

            /// <summary>MenuItemSelected color.</summary>
            private Color _MenuItemSelected;

            /// <summary>MenuItemBorder color.</summary>
            private Color _MenuItemBorder;

            /// <summary>MenuBorder color.</summary>
            private Color _MenuBorder;

            /// <summary>MenuItemSelectedGradientBegin color.</summary>
            private Color _MenuItemSelectedGradientBegin;

            /// <summary>MenuItemSelectedGradientEnd color.</summary>
            private Color _MenuItemSelectedGradientEnd;

            /// <summary>MenuItemPressedGradientBegin color.</summary>
            private Color _MenuItemPressedGradientBegin;

            /// <summary>MenuItemPressedGradientMiddle color.</summary>
            private Color _MenuItemPressedGradientMiddle;

            /// <summary>MenuItemPressedGradientEnd color.</summary>
            private Color _MenuItemPressedGradientEnd;

            /// <summary>RaftingContainerGradientBegin color.</summary>
            private Color _RaftingContainerGradientBegin;

            /// <summary>RaftingContainerGradientEnd color.</summary>
            private Color _RaftingContainerGradientEnd;

            /// <summary>SeparatorDark color.</summary>
            private Color _SeparatorDark;

            /// <summary>SeparatorLight color.</summary>
            private Color _SeparatorLight;

            /// <summary>StatusStripGradientBegin color.</summary>
            private Color _StatusStripGradientBegin;

            /// <summary>StatusStripGradientEnd color.</summary>
            private Color _StatusStripGradientEnd;

            /// <summary>ToolStripBorder color.</summary>
            private Color _ToolStripBorder;

            /// <summary>ToolStripDropDownBackground color.</summary>
            private Color _ToolStripDropDownBackground;

            /// <summary>ToolStripGradientBegin color.</summary>
            private Color _ToolStripGradientBegin;

            /// <summary>ToolStripGradientMiddle color.</summary>
            private Color _ToolStripGradientMiddle;

            /// <summary>ToolStripGradientEnd color.</summary>
            private Color _ToolStripGradientEnd;

            /// <summary>ToolStripContentPanelGradientBegin color.</summary>
            private Color _ToolStripContentPanelGradientBegin;

            /// <summary>ToolStripContentPanelGradientEnd color.</summary>
            private Color _ToolStripContentPanelGradientEnd;

            /// <summary>ToolStripPanelGradientBegin color.</summary>
            private Color _ToolStripPanelGradientBegin;

            /// <summary>ToolStripPanelGradientEnd color.</summary>
            private Color _ToolStripPanelGradientEnd;

            /// <summary>OverflowButtonGradientBegin color.</summary>
            private Color _OverflowButtonGradientBegin;

            /// <summary>OverflowButtonGradientMiddle color.</summary>
            private Color _OverflowButtonGradientMiddle;

            /// <summary>OverflowButtonGradientEnd color.</summary>
            private Color _OverflowButtonGradientEnd;



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="ddp">ddp element.</param>
            public ThemeColorTable(DdpElement ddp)
            {
                ProfessionalColorTable ctab = new ProfessionalColorTable();
                _ButtonSelectedHighlight = ColorOp.FromHex(ddp.Get("ButtonSelectedHighlight", ctab.ButtonSelectedHighlight.ToHex()));
                _ButtonSelectedHighlightBorder = ColorOp.FromHex(ddp.Get("ButtonSelectedHighlightBorder", ctab.ButtonSelectedHighlightBorder.ToHex()));
                _ButtonPressedHighlight = ColorOp.FromHex(ddp.Get("ButtonPressedHighlight", ctab.ButtonPressedHighlight.ToHex()));
                _ButtonPressedHighlightBorder = ColorOp.FromHex(ddp.Get("ButtonPressedHighlightBorder", ctab.ButtonPressedHighlightBorder.ToHex()));
                _ButtonCheckedHighlight = ColorOp.FromHex(ddp.Get("ButtonCheckedHighlight", ctab.ButtonCheckedHighlight.ToHex()));
                _ButtonCheckedHighlightBorder = ColorOp.FromHex(ddp.Get("ButtonCheckedHighlightBorder", ctab.ButtonCheckedHighlightBorder.ToHex()));
                _ButtonPressedBorder = ColorOp.FromHex(ddp.Get("ButtonPressedBorder", ctab.ButtonPressedBorder.ToHex()));
                _ButtonSelectedBorder = ColorOp.FromHex(ddp.Get("ButtonSelectedBorder", ctab.ButtonSelectedBorder.ToHex()));
                _ButtonCheckedGradientBegin = ColorOp.FromHex(ddp.Get("ButtonCheckedGradientBegin", ctab.ButtonCheckedGradientBegin.ToHex()));
                _ButtonCheckedGradientMiddle = ColorOp.FromHex(ddp.Get("ButtonCheckedGradientMiddle", ctab.ButtonCheckedGradientMiddle.ToHex()));
                _ButtonCheckedGradientEnd = ColorOp.FromHex(ddp.Get("ButtonCheckedGradientEnd", ctab.ButtonCheckedGradientEnd.ToHex()));
                _ButtonSelectedGradientBegin = ColorOp.FromHex(ddp.Get("ButtonSelectedGradientBegin", ctab.ButtonSelectedGradientBegin.ToHex()));
                _ButtonSelectedGradientMiddle = ColorOp.FromHex(ddp.Get("ButtonSelectedGradientMiddle", ctab.ButtonSelectedGradientMiddle.ToHex()));
                _ButtonSelectedGradientEnd = ColorOp.FromHex(ddp.Get("ButtonSelectedGradientEnd", ctab.ButtonSelectedGradientEnd.ToHex()));
                _ButtonPressedGradientBegin = ColorOp.FromHex(ddp.Get("ButtonPressedGradientBegin", ctab.ButtonPressedGradientBegin.ToHex()));
                _ButtonPressedGradientMiddle = ColorOp.FromHex(ddp.Get("ButtonPressedGradientMiddle", ctab.ButtonPressedGradientMiddle.ToHex()));
                _ButtonPressedGradientEnd = ColorOp.FromHex(ddp.Get("ButtonPressedGradientEnd", ctab.ButtonPressedGradientEnd.ToHex()));
                _CheckBackground = ColorOp.FromHex(ddp.Get("CheckBackground", ctab.CheckBackground.ToHex()));
                _CheckSelectedBackground = ColorOp.FromHex(ddp.Get("CheckSelectedBackground", ctab.CheckSelectedBackground.ToHex()));
                _CheckPressedBackground = ColorOp.FromHex(ddp.Get("CheckPressedBackground", ctab.CheckPressedBackground.ToHex()));
                _GripDark = ColorOp.FromHex(ddp.Get("GripDark", ctab.GripDark.ToHex()));
                _GripLight = ColorOp.FromHex(ddp.Get("GripLight", ctab.GripLight.ToHex()));
                _ImageMarginGradientBegin = ColorOp.FromHex(ddp.Get("ImageMarginGradientBegin", ctab.ImageMarginGradientBegin.ToHex()));
                _ImageMarginGradientMiddle = ColorOp.FromHex(ddp.Get("ImageMarginGradientMiddle", ctab.ImageMarginGradientMiddle.ToHex()));
                _ImageMarginGradientEnd = ColorOp.FromHex(ddp.Get("ImageMarginGradientEnd", ctab.ImageMarginGradientEnd.ToHex()));
                _ImageMarginRevealedGradientBegin = ColorOp.FromHex(ddp.Get("ImageMarginRevealedGradientBegin", ctab.ImageMarginRevealedGradientBegin.ToHex()));
                _ImageMarginRevealedGradientMiddle = ColorOp.FromHex(ddp.Get("ImageMarginRevealedGradientMiddle", ctab.ImageMarginRevealedGradientMiddle.ToHex()));
                _ImageMarginRevealedGradientEnd = ColorOp.FromHex(ddp.Get("ImageMarginRevealedGradientEnd", ctab.ImageMarginRevealedGradientEnd.ToHex()));
                _MenuStripGradientBegin = ColorOp.FromHex(ddp.Get("MenuStripGradientBegin", ctab.MenuStripGradientBegin.ToHex()));
                _MenuStripGradientEnd = ColorOp.FromHex(ddp.Get("MenuStripGradientEnd", ctab.MenuStripGradientEnd.ToHex()));
                _MenuItemSelected = ColorOp.FromHex(ddp.Get("MenuItemSelected", ctab.MenuItemSelected.ToHex()));
                _MenuItemBorder = ColorOp.FromHex(ddp.Get("MenuItemBorder", ctab.MenuItemBorder.ToHex()));
                _MenuBorder = ColorOp.FromHex(ddp.Get("MenuBorder", ctab.MenuBorder.ToHex()));
                _MenuItemSelectedGradientBegin = ColorOp.FromHex(ddp.Get("MenuItemSelectedGradientBegin", ctab.MenuItemSelectedGradientBegin.ToHex()));
                _MenuItemSelectedGradientEnd = ColorOp.FromHex(ddp.Get("MenuItemSelectedGradientEnd", ctab.MenuItemSelectedGradientEnd.ToHex()));
                _MenuItemPressedGradientBegin = ColorOp.FromHex(ddp.Get("MenuItemPressedGradientBegin", ctab.MenuItemPressedGradientBegin.ToHex()));
                _MenuItemPressedGradientMiddle = ColorOp.FromHex(ddp.Get("MenuItemPressedGradientMiddle", ctab.MenuItemPressedGradientMiddle.ToHex()));
                _MenuItemPressedGradientEnd = ColorOp.FromHex(ddp.Get("MenuItemPressedGradientEnd", ctab.MenuItemPressedGradientEnd.ToHex()));
                _RaftingContainerGradientBegin = ColorOp.FromHex(ddp.Get("RaftingContainerGradientBegin", ctab.RaftingContainerGradientBegin.ToHex()));
                _RaftingContainerGradientEnd = ColorOp.FromHex(ddp.Get("RaftingContainerGradientEnd", ctab.RaftingContainerGradientEnd.ToHex()));
                _SeparatorDark = ColorOp.FromHex(ddp.Get("SeparatorDark", ctab.SeparatorDark.ToHex()));
                _SeparatorLight = ColorOp.FromHex(ddp.Get("SeparatorLight", ctab.SeparatorLight.ToHex()));
                _StatusStripGradientBegin = ColorOp.FromHex(ddp.Get("StatusStripGradientBegin", ctab.StatusStripGradientBegin.ToHex()));
                _StatusStripGradientEnd = ColorOp.FromHex(ddp.Get("StatusStripGradientEnd", ctab.StatusStripGradientEnd.ToHex()));
                _ToolStripBorder = ColorOp.FromHex(ddp.Get("ToolStripBorder", ctab.ToolStripBorder.ToHex()));
                _ToolStripDropDownBackground = ColorOp.FromHex(ddp.Get("ToolStripDropDownBackground", ctab.ToolStripDropDownBackground.ToHex()));
                _ToolStripGradientBegin = ColorOp.FromHex(ddp.Get("ToolStripGradientBegin", ctab.ToolStripGradientBegin.ToHex()));
                _ToolStripGradientMiddle = ColorOp.FromHex(ddp.Get("ToolStripGradientMiddle", ctab.ToolStripGradientMiddle.ToHex()));
                _ToolStripGradientEnd = ColorOp.FromHex(ddp.Get("ToolStripGradientEnd", ctab.ToolStripGradientEnd.ToHex()));
                _ToolStripContentPanelGradientBegin = ColorOp.FromHex(ddp.Get("ToolStripContentPanelGradientBegin", ctab.ToolStripContentPanelGradientBegin.ToHex()));
                _ToolStripContentPanelGradientEnd = ColorOp.FromHex(ddp.Get("ToolStripContentPanelGradientEnd", ctab.ToolStripContentPanelGradientEnd.ToHex()));
                _ToolStripPanelGradientBegin = ColorOp.FromHex(ddp.Get("ToolStripPanelGradientBegin", ctab.ToolStripPanelGradientBegin.ToHex()));
                _ToolStripPanelGradientEnd = ColorOp.FromHex(ddp.Get("ToolStripPanelGradientEnd", ctab.ToolStripPanelGradientEnd.ToHex()));
                _OverflowButtonGradientBegin = ColorOp.FromHex(ddp.Get("OverflowButtonGradientBegin", ctab.OverflowButtonGradientBegin.ToHex()));
                _OverflowButtonGradientMiddle = ColorOp.FromHex(ddp.Get("OverflowButtonGradientMiddle", ctab.OverflowButtonGradientMiddle.ToHex()));
                _OverflowButtonGradientEnd = ColorOp.FromHex(ddp.Get("OverflowButtonGradientEnd", ctab.OverflowButtonGradientEnd.ToHex()));
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // internal methods                                                                                             //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Saves the color table data.</summary>
            /// <param name="ddp">ddp element.</param>
            internal void _Save(DdpElement ddp)
            {
                ddp.Set("ButtonSelectedHighlight", _ButtonSelectedHighlight.ToHex());
                ddp.Set("ButtonSelectedHighlightBorder", _ButtonSelectedHighlightBorder.ToHex());
                ddp.Set("ButtonPressedHighlight", _ButtonPressedHighlight.ToHex());
                ddp.Set("ButtonPressedHighlightBorder", _ButtonPressedHighlightBorder.ToHex());
                ddp.Set("ButtonCheckedHighlight", _ButtonCheckedHighlight.ToHex());
                ddp.Set("ButtonCheckedHighlightBorder", _ButtonCheckedHighlightBorder.ToHex());
                ddp.Set("ButtonPressedBorder", _ButtonPressedBorder.ToHex());
                ddp.Set("ButtonSelectedBorder", _ButtonSelectedBorder.ToHex());
                ddp.Set("ButtonCheckedGradientBegin", _ButtonCheckedGradientBegin.ToHex());
                ddp.Set("ButtonCheckedGradientMiddle", _ButtonCheckedGradientMiddle.ToHex());
                ddp.Set("ButtonCheckedGradientEnd", _ButtonCheckedGradientEnd.ToHex());
                ddp.Set("ButtonSelectedGradientBegin", _ButtonSelectedGradientBegin.ToHex());
                ddp.Set("ButtonSelectedGradientMiddle", _ButtonSelectedGradientMiddle.ToHex());
                ddp.Set("ButtonSelectedGradientEnd", _ButtonSelectedGradientEnd.ToHex());
                ddp.Set("ButtonPressedGradientBegin", _ButtonPressedGradientBegin.ToHex());
                ddp.Set("ButtonPressedGradientMiddle", _ButtonPressedGradientMiddle.ToHex());
                ddp.Set("ButtonPressedGradientEnd", _ButtonPressedGradientEnd.ToHex());
                ddp.Set("CheckBackground", _CheckBackground.ToHex());
                ddp.Set("CheckSelectedBackground", _CheckSelectedBackground.ToHex());
                ddp.Set("CheckPressedBackground", _CheckPressedBackground.ToHex());
                ddp.Set("GripDark", _GripDark.ToHex());
                ddp.Set("GripLight", _GripLight.ToHex());
                ddp.Set("ImageMarginGradientBegin", _ImageMarginGradientBegin.ToHex());
                ddp.Set("ImageMarginGradientMiddle", _ImageMarginGradientMiddle.ToHex());
                ddp.Set("ImageMarginGradientEnd", _ImageMarginGradientEnd.ToHex());
                ddp.Set("ImageMarginRevealedGradientBegin", _ImageMarginRevealedGradientBegin.ToHex());
                ddp.Set("ImageMarginRevealedGradientMiddle", _ImageMarginRevealedGradientMiddle.ToHex());
                ddp.Set("ImageMarginRevealedGradientEnd", _ImageMarginRevealedGradientEnd.ToHex());
                ddp.Set("MenuStripGradientBegin", _MenuStripGradientBegin.ToHex());
                ddp.Set("MenuStripGradientEnd", _MenuStripGradientEnd.ToHex());
                ddp.Set("MenuItemSelected", _MenuItemSelected.ToHex());
                ddp.Set("MenuItemBorder", _MenuItemBorder.ToHex());
                ddp.Set("MenuBorder", _MenuBorder.ToHex());
                ddp.Set("MenuItemSelectedGradientBegin", _MenuItemSelectedGradientBegin.ToHex());
                ddp.Set("MenuItemSelectedGradientEnd", _MenuItemSelectedGradientEnd.ToHex());
                ddp.Set("MenuItemPressedGradientBegin", _MenuItemPressedGradientBegin.ToHex());
                ddp.Set("MenuItemPressedGradientMiddle", _MenuItemPressedGradientMiddle.ToHex());
                ddp.Set("MenuItemPressedGradientEnd", _MenuItemPressedGradientEnd.ToHex());
                ddp.Set("RaftingContainerGradientBegin", _RaftingContainerGradientBegin.ToHex());
                ddp.Set("RaftingContainerGradientEnd", _RaftingContainerGradientEnd.ToHex());
                ddp.Set("SeparatorDark", _SeparatorDark.ToHex());
                ddp.Set("SeparatorLight", _SeparatorLight.ToHex());
                ddp.Set("StatusStripGradientBegin", _StatusStripGradientBegin.ToHex());
                ddp.Set("StatusStripGradientEnd", _StatusStripGradientEnd.ToHex());
                ddp.Set("ToolStripBorder", _ToolStripBorder.ToHex());
                ddp.Set("ToolStripDropDownBackground", _ToolStripDropDownBackground.ToHex());
                ddp.Set("ToolStripGradientBegin", _ToolStripGradientBegin.ToHex());
                ddp.Set("ToolStripGradientMiddle", _ToolStripGradientMiddle.ToHex());
                ddp.Set("ToolStripGradientEnd", _ToolStripGradientEnd.ToHex());
                ddp.Set("ToolStripContentPanelGradientBegin", _ToolStripContentPanelGradientBegin.ToHex());
                ddp.Set("ToolStripContentPanelGradientEnd", _ToolStripContentPanelGradientEnd.ToHex());
                ddp.Set("ToolStripPanelGradientBegin", _ToolStripPanelGradientBegin.ToHex());
                ddp.Set("ToolStripPanelGradientEnd", _ToolStripPanelGradientEnd.ToHex());
                ddp.Set("OverflowButtonGradientBegin", _OverflowButtonGradientBegin.ToHex());
                ddp.Set("OverflowButtonGradientMiddle", _OverflowButtonGradientMiddle.ToHex());
                ddp.Set("OverflowButtonGradientEnd", _OverflowButtonGradientEnd.ToHex());
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [override] ProfessionalColorTable                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets the ButtonSelectedHighlight color.</summary>
            public override Color ButtonSelectedHighlight
            {
                get { return _ButtonSelectedHighlight; }
            }


            /// <summary>Gets the ButtonSelectedHighlightBorder color.</summary>
            public override Color ButtonSelectedHighlightBorder
            {
                get { return _ButtonSelectedHighlightBorder; }
            }


            /// <summary>Gets the ButtonPressedHighlight color.</summary>
            public override Color ButtonPressedHighlight
            {
                get { return _ButtonPressedHighlight; }
            }


            /// <summary>Gets the ButtonPressedHighlightBorder color.</summary>
            public override Color ButtonPressedHighlightBorder
            {
                get { return _ButtonPressedHighlightBorder; }
            }


            /// <summary>Gets the ButtonCheckedHighlight color.</summary>
            public override Color ButtonCheckedHighlight
            {
                get { return _ButtonCheckedHighlight; }
            }


            /// <summary>Gets the ButtonCheckedHighlightBorder color.</summary>
            public override Color ButtonCheckedHighlightBorder
            {
                get { return _ButtonCheckedHighlightBorder; }
            }


            /// <summary>Gets the ButtonPressedBorder color.</summary>
            public override Color ButtonPressedBorder
            {
                get { return _ButtonPressedBorder; }
            }


            /// <summary>Gets the ButtonSelectedBorder color.</summary>
            public override Color ButtonSelectedBorder
            {
                get { return _ButtonSelectedBorder; }
            }


            /// <summary>Gets the ButtonCheckedGradientBegin color.</summary>
            public override Color ButtonCheckedGradientBegin
            {
                get { return _ButtonCheckedGradientBegin; }
            }


            /// <summary>Gets the ButtonCheckedGradientMiddle color.</summary>
            public override Color ButtonCheckedGradientMiddle
            {
                get { return _ButtonCheckedGradientMiddle; }
            }


            /// <summary>Gets the ButtonCheckedGradientEnd color.</summary>
            public override Color ButtonCheckedGradientEnd
            {
                get { return _ButtonCheckedGradientEnd; }
            }


            /// <summary>Gets the ButtonSelectedGradientBegin color.</summary>
            public override Color ButtonSelectedGradientBegin
            {
                get { return _ButtonSelectedGradientBegin; }
            }


            /// <summary>Gets the ButtonSelectedGradientMiddle color.</summary>
            public override Color ButtonSelectedGradientMiddle
            {
                get { return _ButtonSelectedGradientMiddle; }
            }


            /// <summary>Gets the ButtonSelectedGradientEnd color.</summary>
            public override Color ButtonSelectedGradientEnd
            {
                get { return _ButtonSelectedGradientEnd; }
            }


            /// <summary>Gets the ButtonPressedGradientBegin color.</summary>
            public override Color ButtonPressedGradientBegin
            {
                get { return _ButtonPressedGradientBegin; }
            }


            /// <summary>Gets the ButtonPressedGradientMiddle color.</summary>
            public override Color ButtonPressedGradientMiddle
            {
                get { return _ButtonPressedGradientMiddle; }
            }


            /// <summary>Gets the ButtonPressedGradientEnd color.</summary>
            public override Color ButtonPressedGradientEnd
            {
                get { return _ButtonPressedGradientEnd; }
            }


            /// <summary>Gets the CheckBackground color.</summary>
            public override Color CheckBackground
            {
                get { return _CheckBackground; }
            }


            /// <summary>Gets the CheckSelectedBackground color.</summary>
            public override Color CheckSelectedBackground
            {
                get { return _CheckSelectedBackground; }
            }


            /// <summary>Gets the CheckPressedBackground color.</summary>
            public override Color CheckPressedBackground
            {
                get { return _CheckPressedBackground; }
            }


            /// <summary>Gets the GripDark color.</summary>
            public override Color GripDark
            {
                get { return _GripDark; }
            }


            /// <summary>Gets the GripLight color.</summary>
            public override Color GripLight
            {
                get { return _GripLight; }
            }


            /// <summary>Gets the ImageMarginGradientBegin color.</summary>
            public override Color ImageMarginGradientBegin
            {
                get { return _ImageMarginGradientBegin; }
            }


            /// <summary>Gets the ImageMarginGradientMiddle color.</summary>
            public override Color ImageMarginGradientMiddle
            {
                get { return _ImageMarginGradientMiddle; }
            }


            /// <summary>Gets the ImageMarginGradientEnd color.</summary>
            public override Color ImageMarginGradientEnd
            {
                get { return _ImageMarginGradientEnd; }
            }


            /// <summary>Gets the ImageMarginRevealedGradientBegin color.</summary>
            public override Color ImageMarginRevealedGradientBegin
            {
                get { return _ImageMarginRevealedGradientBegin; }
            }


            /// <summary>Gets the ImageMarginRevealedGradientMiddle color.</summary>
            public override Color ImageMarginRevealedGradientMiddle
            {
                get { return _ImageMarginRevealedGradientMiddle; }
            }


            /// <summary>Gets the ImageMarginRevealedGradientEnd color.</summary>
            public override Color ImageMarginRevealedGradientEnd
            {
                get { return _ImageMarginRevealedGradientEnd; }
            }


            /// <summary>Gets the MenuStripGradientBegin color.</summary>
            public override Color MenuStripGradientBegin
            {
                get { return _MenuStripGradientBegin; }
            }


            /// <summary>Gets the MenuStripGradientEnd color.</summary>
            public override Color MenuStripGradientEnd
            {
                get { return _MenuStripGradientEnd; }
            }


            /// <summary>Gets the MenuItemSelected color.</summary>
            public override Color MenuItemSelected
            {
                get { return _MenuItemSelected; }
            }


            /// <summary>Gets the MenuItemBorder color.</summary>
            public override Color MenuItemBorder
            {
                get { return _MenuItemBorder; }
            }


            /// <summary>Gets the MenuBorder color.</summary>
            public override Color MenuBorder
            {
                get { return _MenuBorder; }
            }


            /// <summary>Gets the MenuItemSelectedGradientBegin color.</summary>
            public override Color MenuItemSelectedGradientBegin
            {
                get { return _MenuItemSelectedGradientBegin; }
            }


            /// <summary>Gets the MenuItemSelectedGradientEnd color.</summary>
            public override Color MenuItemSelectedGradientEnd
            {
                get { return _MenuItemSelectedGradientEnd; }
            }


            /// <summary>Gets the MenuItemPressedGradientBegin color.</summary>
            public override Color MenuItemPressedGradientBegin
            {
                get { return _MenuItemPressedGradientBegin; }
            }


            /// <summary>Gets the MenuItemPressedGradientMiddle color.</summary>
            public override Color MenuItemPressedGradientMiddle
            {
                get { return _MenuItemPressedGradientMiddle; }
            }


            /// <summary>Gets the MenuItemPressedGradientEnd color.</summary>
            public override Color MenuItemPressedGradientEnd
            {
                get { return _MenuItemPressedGradientEnd; }
            }


            /// <summary>Gets the RaftingContainerGradientBegin color.</summary>
            public override Color RaftingContainerGradientBegin
            {
                get { return _RaftingContainerGradientBegin; }
            }


            /// <summary>Gets the RaftingContainerGradientEnd color.</summary>
            public override Color RaftingContainerGradientEnd
            {
                get { return _RaftingContainerGradientEnd; }
            }


            /// <summary>Gets the SeparatorDark color.</summary>
            public override Color SeparatorDark
            {
                get { return _SeparatorDark; }
            }


            /// <summary>Gets the SeparatorLight color.</summary>
            public override Color SeparatorLight
            {
                get { return _SeparatorLight; }
            }


            /// <summary>Gets the StatusStripGradientBegin color.</summary>
            public override Color StatusStripGradientBegin
            {
                get { return _StatusStripGradientBegin; }
            }


            /// <summary>Gets the StatusStripGradientEnd color.</summary>
            public override Color StatusStripGradientEnd
            {
                get { return _StatusStripGradientEnd; }
            }


            /// <summary>Gets the ToolStripBorder color.</summary>
            public override Color ToolStripBorder
            {
                get { return _ToolStripBorder; }
            }


            /// <summary>Gets the ToolStripDropDownBackground color.</summary>
            public override Color ToolStripDropDownBackground
            {
                get { return _ToolStripDropDownBackground; }
            }


            /// <summary>Gets the ToolStripGradientBegin color.</summary>
            public override Color ToolStripGradientBegin
            {
                get { return _ToolStripGradientBegin; }
            }


            /// <summary>Gets the ToolStripGradientMiddle color.</summary>
            public override Color ToolStripGradientMiddle
            {
                get { return _ToolStripGradientMiddle; }
            }


            /// <summary>Gets the ToolStripGradientEnd color.</summary>
            public override Color ToolStripGradientEnd
            {
                get { return _ToolStripGradientEnd; }
            }


            /// <summary>Gets the ToolStripContentPanelGradientBegin color.</summary>
            public override Color ToolStripContentPanelGradientBegin
            {
                get { return _ToolStripContentPanelGradientBegin; }
            }


            /// <summary>Gets the ToolStripContentPanelGradientEnd color.</summary>
            public override Color ToolStripContentPanelGradientEnd
            {
                get { return _ToolStripContentPanelGradientEnd; }
            }


            /// <summary>Gets the ToolStripPanelGradientBegin color.</summary>
            public override Color ToolStripPanelGradientBegin
            {
                get { return _ToolStripPanelGradientBegin; }
            }


            /// <summary>Gets the ToolStripPanelGradientEnd color.</summary>
            public override Color ToolStripPanelGradientEnd
            {
                get { return _ToolStripPanelGradientEnd; }
            }


            /// <summary>Gets the OverflowButtonGradientBegin color.</summary>
            public override Color OverflowButtonGradientBegin
            {
                get { return _OverflowButtonGradientBegin; }
            }


            /// <summary>Gets the OverflowButtonGradientMiddle color.</summary>
            public override Color OverflowButtonGradientMiddle
            {
                get { return _OverflowButtonGradientMiddle; }
            }


            /// <summary>Gets the OverflowButtonGradientEnd color.</summary>
            public override Color OverflowButtonGradientEnd
            {
                get { return _OverflowButtonGradientEnd; }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] ThemeList                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class provides a theme list.</summary>
        public sealed class ThemeList: IEnumerable<Theme>
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private members                                                                                              //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Themes.</summary>
            private Dictionary<string, Theme> _Themes = new Dictionary<string, Theme>();



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            internal ThemeList()
            {
                string[] paths = new string[] { PathOp.SystemLocalConfigurationPath + @"\robbiblubber.org\themes", PathOp.ApplicationDirectory };

                foreach(string i in paths)
                {
                    if(Directory.Exists(i))
                    {
                        foreach(string k in Directory.GetFiles(i, "*.thx", SearchOption.AllDirectories))
                        {
                            Theme t = new Theme(k);
                            if(!_Themes.ContainsKey(t.Name)) { _Themes.Add(t.Name, t); }
                        }
                    }
                }

                if(_Themes.Count == 0)
                {
                    Theme t = new Theme();
                    _Themes.Add(t.Name, t);
                }
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public properties                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Gets a theme by its name.</summary>
            /// <param name="name">Name.</param>
            /// <returns>Theme.</returns>
            public Theme this[string name]
            {
                get { return _Themes[name]; }
            }


            /// <summary>Gets a theme by its index.</summary>
            /// <param name="i">Index.</param>
            /// <returns>Theme.</returns>
            public Theme this[int i]
            {
                get { return _Themes.Values.ElementAt(i); }
            }


            /// <summary>Gets the number of items for this instance.</summary>
            public int Count
            {
                get { return _Themes.Count; }
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [interface] IEnumerable<Theme>                                                                               //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets an enumerator for this instance.</summary>
            /// <returns>Enumerator.</returns>
            IEnumerator<Theme> IEnumerable<Theme>.GetEnumerator()
            {
                return _Themes.Values.GetEnumerator();
            }


            /// <summary>Gets an enumerator for this instance.</summary>
            /// <returns>Enumerator.</returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Themes.Values.GetEnumerator();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [enum] ThemeType                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This enumeration defines basic theme types.</summary>
        public enum ThemeType: int
        {
            /// <summary>Default theme type.</summary>
            DEFAULT = 0,
            /// <summary>Light theme type.</summary>
            LIGHT = 1,
            /// <summary>Dark theme type.</summary>
            DARK = 2,
            /// <summary>Special theme type.</summary>
            SPECIAL = 4

        }
    }
}
