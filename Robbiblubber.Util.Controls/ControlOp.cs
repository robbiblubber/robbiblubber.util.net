﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class provides control extension methods.</summary>
    public static class ControlOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the control that has the focus.</summary>
        /// <param name="control">Control.</param>
        /// <returns>Focus control.</returns>
        public static Control GetFocusControl(this ContainerControl control)
        {
            Control rval = control.ActiveControl;

            while(rval is ContainerControl)
            {
                rval = ((ContainerControl) rval).ActiveControl;
            }

            return rval;
        }
    }
}
