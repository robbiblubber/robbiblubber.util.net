﻿namespace Robbiblubber.Util.Controls
{
    partial class ConsoleControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._Console = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // _Console
            // 
            this._Console.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._Console.Dock = System.Windows.Forms.DockStyle.Fill;
            this._Console.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._Console.Location = new System.Drawing.Point(0, 0);
            this._Console.Name = "_Console";
            this._Console.Size = new System.Drawing.Size(720, 460);
            this._Console.TabIndex = 0;
            this._Console.Text = "";
            this._Console.KeyDown += new System.Windows.Forms.KeyEventHandler(this._Console_KeyDown);
            this._Console.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._Console_KeyPress);
            this._Console.MouseUp += new System.Windows.Forms.MouseEventHandler(this._Console_MouseUp);
            // 
            // ConsoleWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._Console);
            this.Name = "ConsoleWindow";
            this.Size = new System.Drawing.Size(720, 460);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox _Console;
    }
}
