﻿using System;
using System.Drawing;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class implements a rich combobox.</summary>
    public class RichComboBox: ComboBox
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public RichComboBox()
        {
            DrawMode = DrawMode.OwnerDrawFixed;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the image list for this control.</summary>
        public ImageList ImageList
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an item by its tag.</summary>
        /// <param name="tag">Tag.</param>
        /// <returns>Item.</returns>
        public RichComboItem GetItemByTag(object tag)
        {
            foreach(object i in Items)
            {
                if(i is RichComboItem)
                {
                    if(tag == null)
                    {
                        if(((RichComboItem) i).Tag == null) { return (RichComboItem) i; }
                    }
                    else if(tag.Equals(((RichComboItem) i).Tag)) { return (RichComboItem) i; }
                }
            }

            return null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] ComboBox                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Raises the OnDraw event.</summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            base.OnDrawItem(e);

            e.DrawBackground();
            e.DrawFocusRectangle();

            if(e.Index >= 0)
            {
                if((Items[e.Index] is RichComboItem) && (ImageList != null))
                {
                    RichComboItem item = (RichComboItem) Items[e.Index];

                    int img = item.ImageIndex;
                    if(img == -1) { img = ImageList.Images.IndexOfKey(item.ImageKey); }
                    if(img >= ImageList.Images.Count) { img = -1; }

                    Font font = ((item.AlternateStyle && (item.Font != null)) ? item.Font : Font);
                    Color c = (item.AlternateStyle ? item.ForeColor : ForeColor);

                    if(img != -1)
                    {
                        ImageList.Draw(e.Graphics, e.Bounds.Left, e.Bounds.Top, img);
                        e.Graphics.DrawString(item.Text, ((item.AlternateStyle && (item.Font != null)) ? item.Font : e.Font), new SolidBrush(item.AlternateStyle ? item.ForeColor : e.ForeColor), e.Bounds.Left + ImageList.ImageSize.Width, e.Bounds.Top);
                    }
                    else
                    {
                        e.Graphics.DrawString(item.Text, ((item.AlternateStyle && (item.Font != null)) ? item.Font : e.Font), new SolidBrush(item.AlternateStyle ? item.ForeColor : e.ForeColor), e.Bounds.Left + ImageList.ImageSize.Width, e.Bounds.Top);
                    }
                }
                else
                {
                    e.Graphics.DrawString(this.Items[e.Index].ToString(), e.Font, new SolidBrush(e.ForeColor), e.Bounds.Left, e.Bounds.Top);
                }
            }
        }
    }
}
