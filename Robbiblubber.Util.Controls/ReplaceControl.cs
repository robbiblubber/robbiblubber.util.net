﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>Replace control.</summary>
    public partial class ReplaceControl: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public ReplaceControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the current search text.</summary>
        public static string CurrentSearchText
        {
            get { return FindControl._ControlsSearchText; }
        }


        /// <summary>Gets the current search text.</summary>
        public static string CurrentReplaceText
        {
            get { return FindControl._ControlsReplaceText; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public events                                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when search text has changed.</summary>
        public event EventHandler SearchTextChanged;

        /// <summary>Occurs when replace text has changed.</summary>
        public event EventHandler ReplaceTextChanged;

        /// <summary>Occures when a search is performed.</summary>
        public event EventHandler Search;

        /// <summary>Occures when a replace operation is performed.</summary>
        public event EventHandler Replace;

        /// <summary>Occures when a replace all operation is performed.</summary>
        public event EventHandler ReplaceAll;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the control pin state.</summary>
        public bool Pinned
        {
            get { return PinButton.Checked; }
            set { PinButton.Checked = value; }
        }


        /// <summary>Gets or sets the search text.</summary>
        public string SearchText
        {
            get { return SearchTextBox.Text; }
            set { SearchTextBox.Text = value; }
        }


        /// <summary>Gets or sets the replace text.</summary>
        public string ReplaceText
        {
            get { return ReplaceTextBox.Text; }
            set { ReplaceTextBox.Text = value; }
        }


        /// <summary>Gets or sets if the current text can be replaced.</summary>
        public bool AllowReplace
        {
            get { return ReplaceButton.Enabled; }
            set { ReplaceButton.Enabled = value; }
        }


        /// <summary>Gets or sets if the current text can be replaced.</summary>
        public bool AllowReplaceAll
        {
            get { return ReplaceAllButton.Enabled; }
            set { ReplaceAllButton.Enabled = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Control                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the control text.</summary>
        public override string Text
        {
            get { return SearchTextBox.Text; }
            set { SearchTextBox.Text = value; }
        }


        /// <summary>Shows the control.</summary>
        public new void Show()
        {
            SearchTextBox.Text = FindControl._ControlsSearchText;
            ReplaceTextBox.Text = FindControl._ControlsReplaceText;
            Pinned = FindControl._ControlsPinned;

            base.Show();
            Focus();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Moves the current selection in the text box to the Clipboard.</summary>
        public void Cut()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Cut(); }
        }


        /// <summary>Copies the current selection in the text box to the Clipboard.</summary>
        public void Copy()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Copy(); }
        }


        /// <summary>Replaces the current selection in the text box with the contents of the Clipboard.</summary>
        public void Paste()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Paste(); }
        }


        /// <summary>Selects all text.</summary>
        public void SelectAll()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).SelectAll(); }
        }


        /// <summary>Deselects all text.</summary>
        public void DeselectAll()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).DeselectAll(); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Search text changed.</summary>
        private void SearchTextBox_TextChanged(object sender, EventArgs e)
        {
            NextButton.Enabled = ReplaceAllButton.Enabled = (SearchTextBox.TextLength > 0);
            if(SearchTextBox.TextLength == 0) { ReplaceButton.Enabled = false; }
            FindControl._ControlsSearchText = SearchTextBox.Text;

            OnTextChanged(e);
            SearchTextChanged?.Invoke(this, e);
        }


        /// <summary>Replace text changed.</summary>
        private void ReplaceTextBox_TextChanged(object sender, EventArgs e)
        {
            FindControl._ControlsReplaceText = ReplaceTextBox.Text;
            ReplaceTextChanged?.Invoke(this, e);
        }


        /// <summary>Pin check changed.</summary>
        private void PinButton_CheckedChanged(object sender, EventArgs e)
        {
            FindControl._ControlsPinned = PinButton.Checked;
        }


        /// <summary>Button "Close" click.</summary>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            Visible = false;
        }


        /// <summary>Button "Next" click.</summary>
        private void NextButton_Click(object sender, EventArgs e)
        {
            FindControl._ControlsSearchText = SearchTextBox.Text;
            FindControl._ControlsReplaceText = ReplaceTextBox.Text;
            Search?.Invoke(this, EventArgs.Empty);
        }


        /// <summary>Button "Replace" click.</summary>
        private void ReplaceButton_Click(object sender, EventArgs e)
        {
            FindControl._ControlsSearchText = SearchTextBox.Text;
            FindControl._ControlsReplaceText = ReplaceTextBox.Text;
            Replace?.Invoke(this, EventArgs.Empty);
        }


        /// <summary>Button "Replace all" click.</summary>
        private void ReplaceAllButton_Click(object sender, EventArgs e)
        {
            FindControl._ControlsSearchText = SearchTextBox.Text;
            FindControl._ControlsReplaceText = ReplaceTextBox.Text;

            if(!Pinned) { Hide(); }

            ReplaceAll?.Invoke(this, EventArgs.Empty);
        }


        /// <summary>Control key down.</summary>
        private void ReplaceControl_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if(NextButton.Enabled) { NextButton_Click(null, null); }
            }
            else if(e.KeyCode == Keys.Escape) { Hide(); }
        }
    }
}
