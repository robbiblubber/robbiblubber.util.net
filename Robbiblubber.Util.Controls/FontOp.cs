﻿using System;
using System.Drawing;
using System.Runtime.Versioning;
using Robbiblubber.Util.Coding;



namespace Robbiblubber.Util.Controls
{
    /// <summary>Font serialization utility class.</summary>
    public static class FontOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Default prototype font.</summary>
        private static Font _DefaultPrototype = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the default prototype font.</summary>
        public static Font DefaultPrototype
        {
            get
            {
                if(_DefaultPrototype == null)
                {
                    _DefaultPrototype = new Font("SegueUI", 10, FontStyle.Regular);
                }

                return _DefaultPrototype;
            }
            set { _DefaultPrototype = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Serializes the font to a Base64-encoded ddp string.</summary>
        /// <param name="font">Font.</param>
        /// <returns>Serialized string.</returns>
        public static string Serialize(this Font font)
        {
            Ddp ddp = new Ddp();

            ddp["name"].Value = font.Name;
            ddp["size"].Value = (int) (font.Size * 100);
            ddp["style"].Value = (int) font.Style;

            return ddp.ToString(Base64.Instance);
        }


        /// <summary>Deserializes a Base64-encode string to a font object.</summary>
        /// <param name="value">Serialized string.</param>
        /// <param name="prototype">Font prototype.</param>
        /// <returns>Font.</returns>
        public static Font Deserialize(string value, Font prototype = null)
        {
            Ddp ddp = new Ddp(value, Base64.Instance);

            if(prototype == null) { prototype = DefaultPrototype; }
            return new Font(ddp.Get("name", prototype.Name), Convert.ToSingle(ddp.Get("size", (int) (prototype.Size * 100))) / 100, (FontStyle) ddp.Get("style", (int) prototype.Style));
        }
    }
}
