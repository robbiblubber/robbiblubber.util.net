﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class provides AutoCompletion functionality.</summary>
    public class AutoCompletion: IDisposable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>List view.</summary>
        protected ListView _ListProvider;

        /// <summary>Image list.</summary>
        protected ImageList _IlistProvider;

        /// <summary>Parent offset.</summary>
        protected Point _Offset;

        /// <summary>Timer;</summary>
        protected Timer _Timer;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent control.</param>
        /// <param name="source">AutoCompletion source.</param>
        public AutoCompletion(TextBoxBase parent, IAutoCompletionSource source)
        {
            Parent = parent;
            Source = source;

            AutoStart = CtrlSpace = true;
            ApplySpace = true;

            _IlistProvider = new ImageList();
            _ListProvider = new ListView();

            _ListProvider.Visible = false;

            _ListProvider.Font = Parent.Font;
            _ListProvider.BackColor = Parent.BackColor;
            _ListProvider.ForeColor = Parent.ForeColor;
            _ListProvider.SmallImageList = _IlistProvider;

            _ListProvider.Columns.Add(new ColumnHeader());
            _ListProvider.HeaderStyle = ColumnHeaderStyle.None;
            _ListProvider.MultiSelect = false;
            _ListProvider.FullRowSelect = true;
            _ListProvider.HideSelection = false;

            _ListProvider.DoubleClick += _ListProvider_DoubleClick;
            _ListProvider.MouseUp += _ListProvider_MouseUp;

            Size = new Size(220, 120);
            _ListProvider.View = View.Details;
            _ListProvider.BorderStyle = BorderStyle.FixedSingle;

            Graphics g = Parent.CreateGraphics();
            Point parentOffset = Parent.FindForm().PointToClient(Parent.Parent.PointToScreen(Parent.Location));
            _Offset = new Point(parentOffset.X, parentOffset.Y + TextRenderer.MeasureText(g, "X", Parent.Font).Height + 2);
            g.Dispose();

            Parent.FindForm().Controls.Add(_ListProvider);

            _Timer = new Timer();
            _Timer.Enabled = false;
            _Timer.Interval = 40;
            _Timer.Tick += _Timer_Tick;

            Parent.KeyDown += _KeyDown;
            Parent.KeyPress += _KeyPress;
            Parent.MouseDown += _MouseDown;
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent control.</summary>
        public TextBoxBase Parent { get; protected set; }


        /// <summary>Gets the InstelliSense source.</summary>
        public IAutoCompletionSource Source { get; protected set; }


        /// <summary>Gets or sets the AutoCompletion control size.</summary>
        public Size Size
        {
            get { return _ListProvider.Size; }
            set
            {
                _ListProvider.Size = value;
                _ListProvider.Columns[0].Width = (Size.Width - 20);
            }
        }


        /// <summary>Gets if the control is visible.</summary>
        public bool Visible
        {
            get { return _ListProvider.Visible; }
        }


        /// <summary>Gets or sets if [Ctrl]+[Space] triggers auto-completion.</summary>
        public bool CtrlSpace
        {
            get; set;
        }


        /// <summary>Gets or sets if AutoCompletion starts automatically.</summary>
        public bool AutoStart
        {
            get; set;
        }


        /// <summary>Gets or sets if [Space] will apply the selected value.</summary>
        public bool ApplySpace
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an image to the control.</summary>
        /// <param name="key">Image key.</param>
        /// <param name="image">Image.</param>
        public void AddImage(string key, Image image)
        {
            _IlistProvider.Images.Add(key, image);
        }


        /// <summary>Shows the AutoCompletion control.</summary>
        /// <param name="key">Current key.</param>
        public void Show(char key)
        {
            Show(true, key);
        }


        /// <summary>Shows the AutoCompletion control.</summary>
        /// <param name="sorted">Determines if the items will be sorted.</param>
        /// <param name="key">Current key.</param>
        public void Show(bool sorted = true, char key = ' ')
        {
            ListViewItem l;
            string pretext = Parent.__GetPrecedingLine();

            if(Source.UpdateRequired(pretext, key))
            {
                _ListProvider.Items.Clear();
                foreach(IAutoCompletionKeyword i in (sorted ? Source.UpdateWords(pretext, key).OrderBy(m => m.Word) : Source.UpdateWords(pretext, key)))
                {
                    l = _ListProvider.Items.Add(i.Word, i.ImageKey);

                    if(i.Italic) { l.Font = (new Font(l.Font, FontStyle.Italic)); }
                    if(i.Bold) { l.Font = (new Font(l.Font, FontStyle.Bold)); }
                }
            }
            
            Point x = Parent.GetPositionFromCharIndex(Parent.SelectionStart);
            _ListProvider.Location = new Point(x.X + _Offset.X , x.Y + _Offset.Y);

            _ListProvider.Visible = true;
            _ListProvider.BringToFront();
            _Update();
        }


        /// <summary>Hides the AutoCompletion control.</summary>
        public void Hide()
        {
            _ListProvider.Hide();
        }


        /// <summary>Accepts the selected term.</summary>
        /// <param name="force">Force acception.</param>
        public void Accept(bool force = true)
        {
            if(_ListProvider.SelectedItems.Count > 0)
            {
                bool accept = true;

                if(!force)
                {
                    string needle = Parent.__GetPrecedingWord().ToLower();

                    if((_ListProvider.SelectedIndices[0] > 0) && _ListProvider.Items[_ListProvider.SelectedIndices[0] - 1].Text.ToLower().StartsWith(needle)) { accept = false; }
                    if((_ListProvider.SelectedIndices[0] < (_ListProvider.Items.Count - 1)) && _ListProvider.Items[_ListProvider.SelectedIndices[0] + 1].Text.ToLower().StartsWith(needle)) { accept = false; }
                }

                if(accept)
                {
                    Parent.__SelectPrecedingWord();
                    Parent.SelectedText = _ListProvider.SelectedItems[0].Text;
                }
            }

            Hide();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Updates the AutoCompletion control.</summary>
        protected virtual void _Update()
        {
            _Timer.Enabled = true;
        }


        /// <summary>Updates the AutoCompletion control.</summary>
        protected virtual void _Update2(bool force = false)
        {
            int match = 0;
            int c;
            ListViewItem hit = null;
            string needle = Parent.__GetPrecedingWord().ToLower();

            foreach(ListViewItem i in _ListProvider.Items)
            {
                if(hit == null)
                {
                    hit = i;
                    match = i.Text.ToLower().MatchesFromStart(needle);
                    continue;
                }

                c = i.Text.ToLower().MatchesFromStart(needle);

                if((c > match) || ((c == match) && (i.Text.Length == needle.Length)))
                {
                    hit = i;
                    match = c;
                }
                else if(c < match) break;
            }

            if(hit == null)
            {
                _ListProvider.SelectedItems.Clear();
            }
            if(hit != null)
            {
                if((needle.Length > match) && (!force))
                {
                    _ListProvider.SelectedItems.Clear();
                }
                else { hit.Selected = true; }

                hit.EnsureVisible();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Key down event.</summary>
        protected void _KeyDown(object sender, KeyEventArgs e)
        {
            if(_ListProvider.Visible)
            {
                if(e.KeyCode == Keys.Up)
                {
                    e.Handled = e.SuppressKeyPress = true;

                    if(_ListProvider.SelectedItems.Count == 0)
                    {
                        _Update2(true);
                    }
                    else { _ListProvider.__SelectPrevious(); }
                }
                else if(e.KeyCode == Keys.Down)
                {
                    e.Handled = e.SuppressKeyPress = true;

                    if(_ListProvider.SelectedItems.Count == 0)
                    {
                        _Update2(true);
                    }
                    else { _ListProvider.__SelectNext(); }
                }
                else if(e.KeyCode == Keys.Left)
                {
                    Hide();
                }
                else if((e.KeyCode == Keys.Back) && (Parent.SelectionStart > 0) && char.IsWhiteSpace(Parent.Text[Parent.SelectionStart - 1]))
                {
                    Hide();
                }
                else if(e.KeyCode == Keys.Escape)
                {
                    e.Handled = e.SuppressKeyPress = true;
                    Hide();
                }
                else if((e.KeyCode == Keys.Right) || (e.KeyCode == Keys.OemPeriod))
                {
                    Accept(false);
                }
                else if(e.KeyCode == Keys.Space)
                {
                    if(e.Alt || ApplySpace)
                    {
                        Accept(false);
                    }
                    else Hide();
                }
                else if(((e.KeyCode == Keys.Tab) || (e.KeyCode == Keys.Enter)) && (!e.Control))
                {
                    Accept(true);
                    e.Handled = e.SuppressKeyPress = true;
                }
            }
            else if(CtrlSpace && e.Control && (e.KeyCode == Keys.Space))
            {
                e.Handled = e.SuppressKeyPress = true;
                Show((char) 27);
            }
        }


        /// <summary>Key pressed event.</summary>
        private void _KeyPress(object sender, KeyPressEventArgs e)
        {
            if(_ListProvider.Visible)
            {
                if((e.KeyChar == ';') || (e.KeyChar == ':') || (e.KeyChar == ',') || (e.KeyChar == '(') || (e.KeyChar == ')') || (e.KeyChar == '\''))
                {
                    Accept(false);
                }
                else
                {
                    _Update();
                }
            }
            else if(AutoStart && (!char.IsWhiteSpace(e.KeyChar)) && ((Parent.SelectionStart == 0) || (char.IsWhiteSpace(Parent.Text[Parent.SelectionStart - 1]))))
            {
                Show(e.KeyChar);
            }
            else if(AutoStart && (e.KeyChar == '.'))
            {
                Show(e.KeyChar);
            }
        }


        /// <summary>Mouse down event.</summary>
        private void _MouseDown(object sender, MouseEventArgs e)
        {
            if(_ListProvider.Visible) Hide();
        }


        /// <summary>Timer tick event.</summary>
        private void _Timer_Tick(object sender, EventArgs e)
        {
            _Timer.Enabled = false;
            Application.DoEvents();

            _Update2();
        }


        /// <summary>Doubleclick on list.</summary>
        private void _ListProvider_DoubleClick(object sender, EventArgs e)
        {
            Accept(true);
        }


        /// <summary>Click on list.</summary>
        private void _ListProvider_MouseUp(object sender, EventArgs e)
        {
            Parent.Focus();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Releases all resources used and disposes the object.</summary>
        public void Dispose()
        {
            try
            {
                _Timer.Tick -= _Timer_Tick;

                Parent.KeyDown -= _KeyDown;
                Parent.KeyPress -= _KeyPress;
                Parent.MouseDown -= _MouseDown;

                _ListProvider.DoubleClick -= _ListProvider_DoubleClick;
                _ListProvider.MouseUp -= _ListProvider_MouseUp;

                Parent = null;
                Source = null;
                Disposal.Recycle(_Timer, _ListProvider, _IlistProvider);
            }
            catch(Exception) {}
        }
    }
}
