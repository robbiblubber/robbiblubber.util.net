﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>Find control.</summary>
    public partial class GoToControl: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occures when a goto function is called.</summary>
        public event EventHandler GoTo;

        /// <summary>Occurs when search text has changed.</summary>
        public event GoToFeedbackEventHandler LineChanged;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public GoToControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the control pin state.</summary>
        public bool Pinned
        {
            get { return PinButton.Checked; }
            set { PinButton.Checked = value; }
        }


        /// <summary>Gets or sets the line number.</summary>
        public int Line
        {
            get
            {
                int rval = 1;
                int.TryParse(GotoTextBox.Text, out rval);

                return rval;
            }
            set { GotoTextBox.Text = value.ToString(); }
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Moves the current selection in the text box to the Clipboard.</summary>
        public void Cut()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Cut(); }
        }


        /// <summary>Copies the current selection in the text box to the Clipboard.</summary>
        public void Copy()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Copy(); }
        }


        /// <summary>Replaces the current selection in the text box with the contents of the Clipboard.</summary>
        public void Paste()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Paste(); }
        }


        /// <summary>Selects all text.</summary>
        public void SelectAll()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).SelectAll(); }
        }


        /// <summary>Deselects all text.</summary>
        public void DeselectAll()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).DeselectAll(); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Control                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the control text.</summary>
        public override string Text
        {
            get { return GotoTextBox.Text; }
            set { GotoTextBox.Text = value; }
        }


        /// <summary>Shows the control.</summary>
        public new void Show()
        {
            base.Show();
            Pinned = FindControl._ControlsPinned;
            Focus();

            GotoTextBox_TextChanged(null, null);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Button "Close" click.</summary>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            Visible = false;
        }


        /// <summary>Button "Find" click.</summary>
        private void GotoButton_Click(object sender, EventArgs e)
        {
            if(!Pinned) Hide();

            GoTo?.Invoke(this, EventArgs.Empty);
        }


        /// <summary>Control key down.</summary>
        private void KeyControl_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if(GotoButton.Enabled) { GotoButton_Click(null, null); }
            }
            else if(e.KeyCode == Keys.Escape) { CloseButton_Click(null, null); }
        }


        /// <summary>Pin check changed.</summary>
        private void PinButton_CheckedChanged(object sender, EventArgs e)
        {
            FindControl._ControlsPinned = PinButton.Checked;
        }


        /// <summary>Line number changed.</summary>
        private void GotoTextBox_TextChanged(object sender, EventArgs e)
        {
            OnTextChanged(e);
            GoToFeedbackEventArgs a = new GoToFeedbackEventArgs(Line);

            LineChanged?.Invoke(this, a);

            GotoButton.Enabled = a.Allowed;
        }
    }
}
