﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class provides extensions to the RichTextBox control.</summary>
    public static class AutoCompletionExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal constants                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Break characters.</summary>
        internal const string __BREAKS = " \r\n\t.,;:()\'\"";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the last word of a string.</summary>
        /// <param name="s">String.</param>
        /// <returns>Last word.</returns>
        public static string LastWord(this string s)
        {
            for(int i = s.Length - 1; i >= 0; i--)
            {
                if(__BREAKS.Contains(s.Substring(i, 1))) { return s.Substring(i + 1, s.Length - i - 1); }
            }

            return s;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal static methods                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the preceding line.</summary>
        /// <param name="tb">Control.</param>
        /// <returns>Preceding line.</returns>
        internal static string __GetPrecedingLine(this TextBoxBase tb)
        {
            int pos = tb.SelectionStart - 1;
            while(pos > 0)
            {
                if(tb.Text.Substring(pos, 1) == "\n") break;
                pos--;
            }

            if(pos < 0) { pos = 0; }

            return tb.Text.Substring(pos, tb.SelectionStart - pos);
        }


        /// <summary>Gets the preceding word.</summary>
        /// <param name="tb">Control.</param>
        /// <returns>Preceding word.</returns>
        internal static string __GetPrecedingWord(this TextBoxBase tb)
        {
            int pos = tb.SelectionStart - 1;
            while(pos >= 0)
            {
                if(__BREAKS.Contains(tb.Text.Substring(pos, 1))) break;
                pos--;
            }

            pos++;

            return tb.Text.Substring(pos, tb.SelectionStart - pos);
        }


        /// <summary>Selects the preceding word.</summary>
        /// <param name="tb">Control.</param>
        internal static void __SelectPrecedingWord(this TextBoxBase tb)
        {
            int pos = tb.SelectionStart - 1;
            while(pos >= 0)
            {
                if(__BREAKS.Contains(tb.Text.Substring(pos, 1))) break;
                pos--;
            }

            tb.Select(pos + 1, tb.SelectionStart - pos - 1);
        }
    }
}
