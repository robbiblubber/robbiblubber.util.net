﻿namespace Robbiblubber.Util.Controls
{
    partial class GoToControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GoToControl));
            this.GotoLabel = new System.Windows.Forms.Label();
            this.GotoTextBox = new System.Windows.Forms.TextBox();
            this.GotoButton = new System.Windows.Forms.Button();
            this.PinButton = new System.Windows.Forms.CheckBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // GotoLabel
            // 
            this.GotoLabel.AutoSize = true;
            this.GotoLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GotoLabel.Location = new System.Drawing.Point(13, 29);
            this.GotoLabel.Name = "GotoLabel";
            this.GotoLabel.Size = new System.Drawing.Size(104, 13);
            this.GotoLabel.TabIndex = 0;
            this.GotoLabel.Tag = "utctrl::common.text.gotoline";
            this.GotoLabel.Text = "Go to &line number:";
            // 
            // GotoTextBox
            // 
            this.GotoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.GotoTextBox.Location = new System.Drawing.Point(16, 45);
            this.GotoTextBox.Name = "GotoTextBox";
            this.GotoTextBox.Size = new System.Drawing.Size(423, 25);
            this.GotoTextBox.TabIndex = 0;
            this.GotoTextBox.TextChanged += new System.EventHandler(this.GotoTextBox_TextChanged);
            this.GotoTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyControl_KeyDown);
            // 
            // GotoButton
            // 
            this.GotoButton.Enabled = false;
            this.GotoButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GotoButton.Location = new System.Drawing.Point(314, 85);
            this.GotoButton.Name = "GotoButton";
            this.GotoButton.Size = new System.Drawing.Size(125, 28);
            this.GotoButton.TabIndex = 1;
            this.GotoButton.Tag = "utctrl::common.button.ok";
            this.GotoButton.Text = "&OK";
            this.GotoButton.UseVisualStyleBackColor = true;
            this.GotoButton.Click += new System.EventHandler(this.GotoButton_Click);
            this.GotoButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyControl_KeyDown);
            // 
            // PinButton
            // 
            this.PinButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.PinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PinButton.Image = ((System.Drawing.Image)(resources.GetObject("PinButton.Image")));
            this.PinButton.Location = new System.Drawing.Point(390, 7);
            this.PinButton.Name = "PinButton";
            this.PinButton.Size = new System.Drawing.Size(23, 23);
            this.PinButton.TabIndex = 0;
            this.PinButton.TabStop = false;
            this.PinButton.Tag = "||utctrl::common.button.pin";
            this.PinButton.UseVisualStyleBackColor = true;
            this.PinButton.CheckedChanged += new System.EventHandler(this.PinButton_CheckedChanged);
            this.PinButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyControl_KeyDown);
            // 
            // CloseButton
            // 
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseButton.Image = ((System.Drawing.Image)(resources.GetObject("CloseButton.Image")));
            this.CloseButton.Location = new System.Drawing.Point(416, 7);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(1);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(23, 23);
            this.CloseButton.TabIndex = 0;
            this.CloseButton.TabStop = false;
            this.CloseButton.Tag = "||utctrl::common.button.close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            this.CloseButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyControl_KeyDown);
            // 
            // GoToControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.PinButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.GotoButton);
            this.Controls.Add(this.GotoTextBox);
            this.Controls.Add(this.GotoLabel);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "GoToControl";
            this.Size = new System.Drawing.Size(452, 125);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyControl_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>Goto label control.</summary>
        public System.Windows.Forms.Label GotoLabel;

        /// <summary>Goto text box control.</summary>
        public System.Windows.Forms.TextBox GotoTextBox;

        /// <summary>Goto button control.</summary>
        public System.Windows.Forms.Button GotoButton;

        /// <summary>Close button control.</summary>
        public System.Windows.Forms.Button CloseButton;

        /// <summary>Pin button control.</summary>
        public System.Windows.Forms.CheckBox PinButton;
    }
}
