﻿using System;
using System.Drawing;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class defines a richt combobox item.</summary>
    public class RichComboItem
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instnace of this class.</summary>
        protected RichComboItem()
        {
            ImageIndex = -1;
            ImageKey = null;

            AlternateStyle = false;
            ForeColor = SystemColors.WindowText;
            Font = null;
        }


        /// <summary>Creates a new instnace of this class.</summary>
        /// <param name="text">Text.</param>
        public RichComboItem(string text): this()
        {
            Text = text;
        }


        /// <summary>Creates a new instnace of this class.</summary>
        /// <param name="text">Text.</param>
        /// <param name="imageIndex">Image index.</param>
        public RichComboItem(string text, int imageIndex): this()
        {
            Text = text;
            ImageIndex = imageIndex;
        }


        /// <summary>Creates a new instnace of this class.</summary>
        /// <param name="text">Text.</param>
        /// <param name="imageKey">Image key.</param>
        public RichComboItem(string text, string imageKey): this()
        {
            Text = text;
            ImageKey = imageKey;
        }


        /// <summary>Creates a new instnace of this class.</summary>
        /// <param name="text">Text.</param>
        /// <param name="imageIndex">Image index.</param>
        /// <param name="tag">Tag.</param>
        public RichComboItem(string text, int imageIndex, object tag): this()
        {
            Text = text;
            ImageIndex = imageIndex;
            Tag = tag;
        }


        /// <summary>Creates a new instnace of this class.</summary>
        /// <param name="text">Text.</param>
        /// <param name="imageKey">Image key.</param>
        /// <param name="tag">Tag.</param>
        public RichComboItem(string text, string imageKey, object tag): this()
        {
            Text = text;
            ImageKey = imageKey;
            Tag = tag;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the item image text.</summary>
        public string Text
        {
            get; set;
        }


        /// <summary>Gets or sets the item image index.</summary>
        public int ImageIndex
        {
            get; set;
        }


        /// <summary>Gets or sets the item image key.</summary>
        public string ImageKey
        {
            get; set;
        }


        /// <summary>Gets or sets if the item uses its own font and color.</summary>
        public bool AlternateStyle
        {
            get; set;
        }


        /// <summary>Gets or sets the item color.</summary>
        public Color ForeColor
        {
            get; set;
        }


        /// <summary>Gets or sets the item font.</summary>
        public Font Font
        {
            get; set;
        }


        /// <summary>Gets or sets the item tag.</summary>
        public object Tag
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return Text;
        }
    }
}
