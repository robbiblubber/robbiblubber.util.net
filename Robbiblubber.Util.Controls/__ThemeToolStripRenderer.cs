﻿using System;
using System.Drawing;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    internal sealed class __ThemeToolStripRenderer: ToolStripProfessionalRenderer
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instence of this class.</summary>
        /// <param name="theme">Theme.</param>
        internal __ThemeToolStripRenderer(Theme theme): base(theme.ColorTable)
        {
            Theme = theme;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the current theme.</summary>
        public Theme Theme
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] ToolStripProfessionalRenderer                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when a menu item background is rendered.</summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnRenderMenuItemBackground(ToolStripItemRenderEventArgs e)
        {
            using(SolidBrush brush = new SolidBrush(Theme.ColorScheme.ButtonFace))
            {
                e.Graphics.FillRectangle(brush, new Rectangle(new Point(2, 1), new Size(e.Item.Size.Width - 3, e.Item.Size.Height - 2)));
            }

            base.OnRenderMenuItemBackground(e);
        }
    }
}
