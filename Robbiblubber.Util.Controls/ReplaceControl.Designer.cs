﻿namespace Robbiblubber.Util.Controls
{
    partial class ReplaceControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReplaceControl));
            this.ReplaceAllButton = new System.Windows.Forms.Button();
            this.SearchTextBox = new System.Windows.Forms.TextBox();
            this.SearchLabel = new System.Windows.Forms.Label();
            this.PinButton = new System.Windows.Forms.CheckBox();
            this.CloseButton = new System.Windows.Forms.Button();
            this.ReplaceTextBox = new System.Windows.Forms.TextBox();
            this.ReplaceLabel = new System.Windows.Forms.Label();
            this.ReplaceButton = new System.Windows.Forms.Button();
            this.NextButton = new System.Windows.Forms.Button();
            this.CustomCheck0 = new System.Windows.Forms.CheckBox();
            this.CustomCheck1 = new System.Windows.Forms.CheckBox();
            this.CustomCheck2 = new System.Windows.Forms.CheckBox();
            this.CustomCheck3 = new System.Windows.Forms.CheckBox();
            this.CustomCheck4 = new System.Windows.Forms.CheckBox();
            this.CustomCheck5 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // ReplaceAllButton
            // 
            this.ReplaceAllButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReplaceAllButton.Location = new System.Drawing.Point(314, 143);
            this.ReplaceAllButton.Name = "ReplaceAllButton";
            this.ReplaceAllButton.Size = new System.Drawing.Size(125, 28);
            this.ReplaceAllButton.TabIndex = 4;
            this.ReplaceAllButton.Tag = "utctrl::common.button.replaceall";
            this.ReplaceAllButton.Text = "Replace &All";
            this.ReplaceAllButton.UseVisualStyleBackColor = true;
            this.ReplaceAllButton.Click += new System.EventHandler(this.ReplaceAllButton_Click);
            this.ReplaceAllButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReplaceControl_KeyDown);
            // 
            // SearchTextBox
            // 
            this.SearchTextBox.Location = new System.Drawing.Point(16, 47);
            this.SearchTextBox.Name = "SearchTextBox";
            this.SearchTextBox.Size = new System.Drawing.Size(423, 25);
            this.SearchTextBox.TabIndex = 0;
            this.SearchTextBox.TextChanged += new System.EventHandler(this.SearchTextBox_TextChanged);
            this.SearchTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReplaceControl_KeyDown);
            // 
            // SearchLabel
            // 
            this.SearchLabel.AutoSize = true;
            this.SearchLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SearchLabel.Location = new System.Drawing.Point(13, 31);
            this.SearchLabel.Name = "SearchLabel";
            this.SearchLabel.Size = new System.Drawing.Size(44, 13);
            this.SearchLabel.TabIndex = 0;
            this.SearchLabel.Tag = "utctrl::common.text.search";
            this.SearchLabel.Text = "&Search:";
            // 
            // PinButton
            // 
            this.PinButton.Appearance = System.Windows.Forms.Appearance.Button;
            this.PinButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PinButton.Image = ((System.Drawing.Image)(resources.GetObject("PinButton.Image")));
            this.PinButton.Location = new System.Drawing.Point(390, 9);
            this.PinButton.Name = "PinButton";
            this.PinButton.Size = new System.Drawing.Size(23, 23);
            this.PinButton.TabIndex = 0;
            this.PinButton.TabStop = false;
            this.PinButton.Tag = "||utctrl::common.button.pin";
            this.PinButton.UseVisualStyleBackColor = true;
            this.PinButton.CheckedChanged += new System.EventHandler(this.PinButton_CheckedChanged);
            this.PinButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReplaceControl_KeyDown);
            // 
            // CloseButton
            // 
            this.CloseButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CloseButton.Image = ((System.Drawing.Image)(resources.GetObject("CloseButton.Image")));
            this.CloseButton.Location = new System.Drawing.Point(416, 9);
            this.CloseButton.Margin = new System.Windows.Forms.Padding(1);
            this.CloseButton.Name = "CloseButton";
            this.CloseButton.Size = new System.Drawing.Size(23, 23);
            this.CloseButton.TabIndex = 0;
            this.CloseButton.TabStop = false;
            this.CloseButton.Tag = "||utctrl::common.button.close";
            this.CloseButton.UseVisualStyleBackColor = true;
            this.CloseButton.Click += new System.EventHandler(this.CloseButton_Click);
            this.CloseButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReplaceControl_KeyDown);
            // 
            // ReplaceTextBox
            // 
            this.ReplaceTextBox.Location = new System.Drawing.Point(15, 98);
            this.ReplaceTextBox.Name = "ReplaceTextBox";
            this.ReplaceTextBox.Size = new System.Drawing.Size(423, 25);
            this.ReplaceTextBox.TabIndex = 1;
            this.ReplaceTextBox.TextChanged += new System.EventHandler(this.ReplaceTextBox_TextChanged);
            this.ReplaceTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReplaceControl_KeyDown);
            // 
            // ReplaceLabel
            // 
            this.ReplaceLabel.AutoSize = true;
            this.ReplaceLabel.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ReplaceLabel.Location = new System.Drawing.Point(12, 82);
            this.ReplaceLabel.Name = "ReplaceLabel";
            this.ReplaceLabel.Size = new System.Drawing.Size(76, 13);
            this.ReplaceLabel.TabIndex = 1;
            this.ReplaceLabel.Tag = "utctrl::common.text.replacewith";
            this.ReplaceLabel.Text = "Replace &with:";
            // 
            // ReplaceButton
            // 
            this.ReplaceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ReplaceButton.Location = new System.Drawing.Point(183, 143);
            this.ReplaceButton.Name = "ReplaceButton";
            this.ReplaceButton.Size = new System.Drawing.Size(125, 28);
            this.ReplaceButton.TabIndex = 3;
            this.ReplaceButton.Tag = "utctrl::common.button.replace";
            this.ReplaceButton.Text = "&Replace";
            this.ReplaceButton.UseVisualStyleBackColor = true;
            this.ReplaceButton.Click += new System.EventHandler(this.ReplaceButton_Click);
            this.ReplaceButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReplaceControl_KeyDown);
            // 
            // NextButton
            // 
            this.NextButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.NextButton.Location = new System.Drawing.Point(52, 143);
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(125, 28);
            this.NextButton.TabIndex = 2;
            this.NextButton.Tag = "utctrl::common.button.next";
            this.NextButton.Text = "&Next";
            this.NextButton.UseVisualStyleBackColor = true;
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            this.NextButton.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReplaceControl_KeyDown);
            // 
            // CustomCheck0
            // 
            this.CustomCheck0.Appearance = System.Windows.Forms.Appearance.Button;
            this.CustomCheck0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CustomCheck0.Image = ((System.Drawing.Image)(resources.GetObject("CustomCheck0.Image")));
            this.CustomCheck0.Location = new System.Drawing.Point(349, 9);
            this.CustomCheck0.Name = "CustomCheck0";
            this.CustomCheck0.Size = new System.Drawing.Size(23, 23);
            this.CustomCheck0.TabIndex = 0;
            this.CustomCheck0.TabStop = false;
            this.CustomCheck0.Tag = "||utctrl::common.button.matchwhole";
            this.CustomCheck0.UseVisualStyleBackColor = true;
            this.CustomCheck0.Visible = false;
            // 
            // CustomCheck1
            // 
            this.CustomCheck1.Appearance = System.Windows.Forms.Appearance.Button;
            this.CustomCheck1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CustomCheck1.Image = ((System.Drawing.Image)(resources.GetObject("CustomCheck1.Image")));
            this.CustomCheck1.Location = new System.Drawing.Point(321, 9);
            this.CustomCheck1.Name = "CustomCheck1";
            this.CustomCheck1.Size = new System.Drawing.Size(23, 23);
            this.CustomCheck1.TabIndex = 0;
            this.CustomCheck1.TabStop = false;
            this.CustomCheck1.Tag = "||utctrl::common.button.casesensitive";
            this.CustomCheck1.UseVisualStyleBackColor = true;
            this.CustomCheck1.Visible = false;
            // 
            // CustomCheck2
            // 
            this.CustomCheck2.Appearance = System.Windows.Forms.Appearance.Button;
            this.CustomCheck2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CustomCheck2.Image = ((System.Drawing.Image)(resources.GetObject("CustomCheck2.Image")));
            this.CustomCheck2.Location = new System.Drawing.Point(294, 9);
            this.CustomCheck2.Name = "CustomCheck2";
            this.CustomCheck2.Size = new System.Drawing.Size(23, 23);
            this.CustomCheck2.TabIndex = 0;
            this.CustomCheck2.TabStop = false;
            this.CustomCheck2.UseVisualStyleBackColor = true;
            this.CustomCheck2.Visible = false;
            // 
            // CustomCheck3
            // 
            this.CustomCheck3.Appearance = System.Windows.Forms.Appearance.Button;
            this.CustomCheck3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CustomCheck3.Image = ((System.Drawing.Image)(resources.GetObject("CustomCheck3.Image")));
            this.CustomCheck3.Location = new System.Drawing.Point(267, 9);
            this.CustomCheck3.Name = "CustomCheck3";
            this.CustomCheck3.Size = new System.Drawing.Size(23, 23);
            this.CustomCheck3.TabIndex = 0;
            this.CustomCheck3.TabStop = false;
            this.CustomCheck3.UseVisualStyleBackColor = true;
            this.CustomCheck3.Visible = false;
            // 
            // CustomCheck4
            // 
            this.CustomCheck4.Appearance = System.Windows.Forms.Appearance.Button;
            this.CustomCheck4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CustomCheck4.Image = ((System.Drawing.Image)(resources.GetObject("CustomCheck4.Image")));
            this.CustomCheck4.Location = new System.Drawing.Point(240, 9);
            this.CustomCheck4.Name = "CustomCheck4";
            this.CustomCheck4.Size = new System.Drawing.Size(23, 23);
            this.CustomCheck4.TabIndex = 0;
            this.CustomCheck4.TabStop = false;
            this.CustomCheck4.UseVisualStyleBackColor = true;
            this.CustomCheck4.Visible = false;
            // 
            // CustomCheck5
            // 
            this.CustomCheck5.Appearance = System.Windows.Forms.Appearance.Button;
            this.CustomCheck5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CustomCheck5.Image = ((System.Drawing.Image)(resources.GetObject("CustomCheck5.Image")));
            this.CustomCheck5.Location = new System.Drawing.Point(213, 9);
            this.CustomCheck5.Name = "CustomCheck5";
            this.CustomCheck5.Size = new System.Drawing.Size(23, 23);
            this.CustomCheck5.TabIndex = 0;
            this.CustomCheck5.TabStop = false;
            this.CustomCheck5.UseVisualStyleBackColor = true;
            this.CustomCheck5.Visible = false;
            // 
            // ReplaceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.CustomCheck0);
            this.Controls.Add(this.CustomCheck1);
            this.Controls.Add(this.CustomCheck2);
            this.Controls.Add(this.CustomCheck3);
            this.Controls.Add(this.CustomCheck4);
            this.Controls.Add(this.CustomCheck5);
            this.Controls.Add(this.NextButton);
            this.Controls.Add(this.ReplaceButton);
            this.Controls.Add(this.ReplaceTextBox);
            this.Controls.Add(this.ReplaceLabel);
            this.Controls.Add(this.PinButton);
            this.Controls.Add(this.CloseButton);
            this.Controls.Add(this.ReplaceAllButton);
            this.Controls.Add(this.SearchTextBox);
            this.Controls.Add(this.SearchLabel);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ReplaceControl";
            this.Size = new System.Drawing.Size(452, 186);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReplaceControl_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>Pin button control.</summary>
        public System.Windows.Forms.CheckBox PinButton;

        /// <summary>Close button control.</summary>
        public System.Windows.Forms.Button CloseButton;

        /// <summary>Custom check button (0) control.</summary>
        public System.Windows.Forms.CheckBox CustomCheck0;

        /// <summary>Custom check button (1) control.</summary>
        public System.Windows.Forms.CheckBox CustomCheck1;

        /// <summary>Custom check button (2) control.</summary>
        public System.Windows.Forms.CheckBox CustomCheck2;

        /// <summary>Custom check button (3) control.</summary>
        public System.Windows.Forms.CheckBox CustomCheck3;

        /// <summary>Custom check button (4) control.</summary>
        public System.Windows.Forms.CheckBox CustomCheck4;

        /// <summary>Custom check button (5) control.</summary>
        public System.Windows.Forms.CheckBox CustomCheck5;

        /// <summary>Search label control.</summary>
        public System.Windows.Forms.Label SearchLabel;

        /// <summary>Search text box control.</summary>
        public System.Windows.Forms.TextBox SearchTextBox;

        /// <summary>Replace text box control.</summary>
        public System.Windows.Forms.TextBox ReplaceTextBox;

        /// <summary>Replace label control.</summary>
        public System.Windows.Forms.Label ReplaceLabel;

        /// <summary>Next button control.</summary>
        public System.Windows.Forms.Button NextButton;

        /// <summary>Replace button control.</summary>
        public System.Windows.Forms.Button ReplaceButton;

        /// <summary>Replace all button control.</summary>
        public System.Windows.Forms.Button ReplaceAllButton;
    }
}
