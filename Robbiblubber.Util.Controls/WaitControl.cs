﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class implements a wait control.</summary>
    public partial class WaitControl: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public WaitControl()
        {
            InitializeComponent();
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Shows the control.</summary>
        /// <param name="text">Control text.</param>
        public void Show(string text)
        {
            _LabelText.Text = text;
            Show();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] UserControl                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the control text.</summary>
        public override string Text
        {
            get { return _LabelText.Text; }
            set { _LabelText.Text = value; }
        }
    }
}
