﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using Robbiblubber.Util.Coding;



namespace Robbiblubber.Util.Controls
{
    /// <summary>Console control.</summary>
    public partial class ConsoleControl: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Password character.</summary>
        protected string _PasswordChar = null;

        /// <summary>Determines if the console is currently reading.</summary>
        protected bool _Reading = false;

        /// <summary>Current reading start position.</summary>
        protected int _ReadPos = 0;

        /// <summary>Currently read value.</summary>
        protected string _ReadValue = "";

        /// <summary>Command stack.</summary>
        protected List<string> _CommandStack = new List<string>();

        /// <summary>Command stack position.</summary>
        protected int _StackPos = -1;

        /// <summary>Foreground color.</summary>
        protected ConsoleColor _FgColor;

        /// <summary>Background color.</summary>
        protected ConsoleColor _BgColor;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public ConsoleControl()
        {
            InitializeComponent();

            EOF = false;
            Colors = new ColorMap("sAAAAB+LCAAAAAAABABNjtsJxEAMAytasPwOYvtvK05y4fJjBluMLHuBmJFVJZHUm92tnfayCn2vtu5wZ8xao7UmknuZljjrukcUlP3H4zHnkdMiHzVwS0IESUxphksTdpH2pP01I37v6URPux48srAAAAA=");
            BgColor = ConsoleColor.White;
            FgColor = ConsoleColor.Black;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the color map.</summary>
        public ColorMap Colors
        {
            get; set;
        }


        /// <summary>Gets or sets the background color.</summary>
        public ConsoleColor BgColor
        {
            get { return _BgColor; }
            set
            {
                _BgColor = value;
                BackColor = Colors[value];
            }
        }


        /// <summary>Gets or sets the foreground color.</summary>
        public ConsoleColor FgColor
        {
            get { return _FgColor; }
            set
            {
                _FgColor = value;
                ForeColor = Colors[value];
            }
        }


        /// <summary>Gets if it is possible to cut from the console.</summary>
        public bool CanCut
        {
            get { return (_Console.SelectionStart >= _ReadPos); }
        }


        /// <summary>Gets or sets the type of scroll bars.</summary>
        public RichTextBoxScrollBars ScrollBars
        {
            get { return _Console.ScrollBars; }
            set { _Console.ScrollBars = value; }
        }


        /// <summary>Gets or sets whether the control automatically wraps words to the beginning of the next line when necessary.</summary>
        public bool WordWrap
        {
            get { return _Console.WordWrap; }
            set { _Console.WordWrap = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Copies the selected text to the clipboard.</summary>
        public virtual void Copy()
        {
            Clipboard.SetDataObject(_Console.SelectedText, true);
        }


        /// <summary>Pastes the clipboard contents into the console.</summary>
        public virtual void Paste()
        {
            if(_Console.SelectionStart < _ReadPos) _Console.SelectionStart = _Console.TextLength;

            try
            {
                _Console.SelectedText = ((string) Clipboard.GetDataObject().GetData(typeof(string))).Replace("\r\n", " ").Replace("\n", " ");
            }
            catch(Exception) {}
        }


        /// <summary>Pastes a text into the console.</summary>
        /// <param name="text">Text.</param>
        public virtual void Paste(string text)
        {
            if(_Console.SelectionStart < _ReadPos) _Console.SelectionStart = _Console.TextLength;

            try
            {
                _Console.SelectedText = text.Replace("\r\n", " ").Replace("\n", " ");
            }
            catch(Exception) {}
        }


        /// <summary>Cuts the selected text to the clipboard.</summary>
        public virtual void Cut()
        {
            if(_Console.SelectionStart < _ReadPos) return;

            try
            {
                Clipboard.SetDataObject(_Console.SelectedText, true);
                _Console.SelectedText = "";
            }
            catch(Exception) {}
        }


        /// <summary>Aborts the shell window.</summary>
        public virtual void Abort()
        {
            _Reading = false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IInput                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if the stream has reached EOF.</summary>
        public virtual bool EOF { get; set; }


        /// <summary>Reads a string from the stream.</summary>
        /// <param name="passwordChar">Password character.</param>
        /// <returns>String.</returns>
        public virtual string ReadLine(string passwordChar)
        {
            _ReadPos = _GetStartPos();
            _Reading = true;
            _PasswordChar = passwordChar;

            while(_Reading) Application.DoEvents();

            string rval = _ReadValue.Trim('\r', '\n');
            _ReadValue = "";

            return rval;
        }

        /// <summary>Reads a string from the stream.</summary>
        /// <returns>String.</returns>
        public virtual string ReadLine()
        {
            return ReadLine(null);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IOutput                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a string into the stream.</summary>
        /// <param name="text">Text.</param>
        /// <param name="color">Color.</param>
        public virtual void Write(string text, ConsoleColor color)
        {
            if(_Console.InvokeRequired)
            {
                _Console.Invoke(new _InvokeWrite(Write));
            }
            else
            {
                try
                {
                    _Console.AppendText(text);

                    _Console.Select(_Console.TextLength - text.Length, text.Length);
                    _Console.SelectionColor = Colors[color];
                    _Console.Select(_Console.TextLength, 0);
                }
                catch(Exception) {}
            }
        }


        /// <summary>Writes a string into the stream.</summary>
        /// <param name="text">Text.</param>
        public virtual void Write(string text)
        {
            Write(text, FgColor);
        }


        /// <summary>Writes a string into the stream.</summary>
        /// <param name="text">Text.</param>
        /// <param name="color">Color.</param>
        public virtual void WriteLine(string text, ConsoleColor color)
        {
            Write(text + '\n', color);
        }


        /// <summary>Writes a string into the stream.</summary>
        /// <param name="text">Text.</param>
        public virtual void WriteLine(string text)
        {
            Write(text + '\n', FgColor);
        }


        /// <summary>Writes a newline into the stream.</summary>
        public virtual void WriteLine()
        {
            Write("\n", FgColor);
        }


        /// <summary>Clears the console screen.</summary>
        public virtual void Clear()
        {
            if(_Console.InvokeRequired)
            {
                _Console.Invoke(new _InvokeVoid(Clear));
            }
            else
            {
                _Console.Clear();
            }
        }


        /// <summary>Sets the stream foreground color.</summary>
        /// <param name="color">Color.</param>
        public virtual void PaintFg(ConsoleColor color)
        {
            FgColor = color;
        }


        /// <summary>Sets the stream background color.</summary>
        /// <param name="color">Color.</param>
        public virtual void PaintBg(ConsoleColor color)
        {
            BgColor = color;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected delegates                                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Void delegate.</summary>
        protected delegate void _InvokeVoid();

        /// <summary>Write delegate.</summary>
        protected delegate void _InvokeWrite(string text, ConsoleColor color);

        /// <summary>Start position delegate.</summary>
        protected delegate int _InvokeStartPos();


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the starting position.</summary>
        /// <returns>Start position.</returns>
        protected virtual int _GetStartPos()
        {
            int rval;

            if(_Console.InvokeRequired)
            {
                rval = (int) _Console.Invoke(new _InvokeStartPos(_GetStartPos));
            }
            else
            {
                rval = _Console.SelectionStart;
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Console key press.</summary>
        private void _Console_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(_PasswordChar != null)
            {
                _ReadValue += e.KeyChar;

                if(_PasswordChar == "")
                {
                    e.Handled = true;
                }
                else
                {
                    if(e.KeyChar != '\r') e.KeyChar = _PasswordChar[0];
                }

            }
            else if(!_Reading) e.Handled = true;
        }


        /// <summary>Console key down.</summary>
        private void _Console_KeyDown(object sender, KeyEventArgs e)
        {
            string rval;

            if((e.KeyCode != Keys.Alt) && (e.KeyCode != Keys.ControlKey) && (e.KeyCode != Keys.ShiftKey) && (_Console.SelectionStart < _ReadPos))
            {
                if((((e.KeyCode == Keys.C) && e.Control) || (e.KeyCode == Keys.Insert) && e.Control))
                {
                    Copy();
                    e.Handled = true;
                    _Console.Select(_Console.TextLength, 0);
                }
                else if(((e.KeyCode == Keys.X) && e.Control) || ((e.KeyCode == Keys.V) && e.Control))
                {
                    e.Handled = true;
                    _Console.Select(_Console.TextLength, 0);
                }

                _Console.Select(_Console.TextLength, 0);
                return;
            }

            if(((e.KeyCode == Keys.V) && e.Control) || ((e.KeyCode == Keys.Insert) && e.Shift))
            {
                Paste();
                e.Handled = true;
                return;
            }

            switch(e.KeyCode)
            {
                case Keys.Return:
                    _Console.Select(_Console.TextLength, 0);

                    if(_PasswordChar == null)
                    {
                        try
                        {
                            if(_Console.Text.IndexOf("\n", _ReadPos) >= 0)
                                _ReadValue = _Console.Text.Substring(_ReadPos, _Console.TextLength - _Console.Text.IndexOf("\r", _ReadPos));
                            else
                                _ReadValue = _Console.Text.Substring(_ReadPos);

                            if(_ReadValue != "")
                            {
                                _CommandStack.Remove(_ReadValue);
                                _CommandStack.Insert(0, _ReadValue);
                            }
                        }
                        catch(Exception) { _ReadValue = ""; }
                    }

                    _StackPos = -1;
                    _Reading = false;
                    break;
                case Keys.Up:
                    try
                    {
                        rval = _CommandStack[++_StackPos];
                    }
                    catch(Exception)
                    {
                        rval = "";
                        _StackPos = _CommandStack.Count;
                    }

                    try
                    {
                        _Console.Select(_ReadPos, _Console.TextLength - _ReadPos);
                        _Console.SelectedText = rval;
                    }
                    catch(Exception) {};

                    e.Handled = true;
                    break;
                case Keys.Down:
                    try
                    {
                        rval = _CommandStack[--_StackPos];
                    }
                    catch(Exception)
                    {
                        rval = "";
                        _StackPos = -1;
                    }

                    try
                    {
                        _Console.Select(_ReadPos, _Console.TextLength - _ReadPos);
                        _Console.SelectedText = rval;
                    }
                    catch(Exception) {};

                    e.Handled = true;
                    break;
                case Keys.Left:
                    if(_Console.SelectionStart <= _ReadPos) e.Handled = true;
                    break;
                case Keys.Back:
                    if(_Console.SelectionStart <= _ReadPos) e.Handled = true;
                    break;
                case Keys.Delete:
                    if(_Console.SelectionStart < _ReadPos) e.Handled = true;
                    break;
                case Keys.PageUp:
                    if(_CommandStack.Count > 0)
                    {
                        _StackPos = _CommandStack.Count - 1;

                        try
                        {
                            _Console.Select(_ReadPos, _Console.TextLength - _ReadPos);
                            _Console.SelectedText = (string) _CommandStack[_CommandStack.Count - 1];
                        }
                        catch(Exception) {};
                    }
                    e.Handled = true;
                    break;
                case Keys.PageDown:
                    if(_CommandStack.Count > 0)
                    {
                        _StackPos = 0;

                        try
                        {
                            _Console.Select(_ReadPos, _Console.TextLength - _ReadPos);
                            _Console.SelectedText = (string) _CommandStack[0];
                        }
                        catch(Exception) {};
                    }
                    e.Handled = true;
                    break;
                case Keys.Home:
                    try
                    {
                        int cpos = _Console.SelectionStart;
                        _Console.Select(_ReadPos, e.Shift ? (cpos - _ReadPos) : 0);
                    }
                    catch(Exception) {};

                    e.Handled = true;
                    break;
                case Keys.Escape:
                    try
                    {
                        _Console.Select(_ReadPos, _Console.TextLength - _ReadPos);
                        _Console.SelectedText = "";
                    }
                    catch(Exception) {}

                    e.Handled = true;
                    break;
            }
        }


        /// <summary>Console mouse up.</summary>
        private void _Console_MouseUp(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Right && _Console.SelectionLength > 0)
            {
                _Console.AppendText(_Console.SelectedText.Replace("\r\n", " ").Replace("\n", " "));
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] ColorMap                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class maps console colors to GUI colors.</summary>
        public sealed class ColorMap
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private members                                                                                              //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Color map.</summary>
            private Dictionary<ConsoleColor, Color> _Map;



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="data">Mapping data as Base64 string.</param>
            public ColorMap(string data = null)
            {
                Dictionary<int, int> map = new Dictionary<int, int>();

                if(data != null)
                {
                    try
                    {
                        data = data.Decompress();

                        foreach(string i in data.Split(';'))
                        {
                            try
                            {
                                string[] j = i.Split('=');
                                map.Add(Convert.ToInt32(j[0]), Convert.ToInt32(j[1]));
                            }
                            catch(Exception) {}
                        }

                    }
                    catch(Exception) {}
                }

                _Map = new Dictionary<ConsoleColor, Color>();

                foreach(int i in Enum.GetValues(typeof(ConsoleColor)))
                {
                    if(map.ContainsKey(i))
                    {
                        _Map.Add((ConsoleColor) i, Color.FromArgb(map[i]));
                    }
                    else
                    {
                        _Map.Add((ConsoleColor) i, Color.FromName(((ConsoleColor) i).ToString()));
                    }
                }
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // public properties                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets or sets a color for a console color.</summary>
            /// <param name="c">Console color.</param>
            /// <returns>Color.</returns>
            public Color this[ConsoleColor c]
            {
                get
                {
                    try
                    {
                        return _Map[c];
                    }
                    catch(Exception) { return Color.Black; }
                }
                set
                {
                    try
                    {
                        _Map[c] = value;
                    }
                    catch(Exception) { _Map.Add(c, value); }
                }
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [override] object                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Returns a string representation of this object.</summary>
            /// <returns>String.</returns>
            public override string ToString()
            {
                string rval = "";

                foreach(int i in Enum.GetValues(typeof(ConsoleColor)))
                {
                    rval += (i.ToString() + '=' + this[(ConsoleColor) i].ToArgb() + ';');
                }

                return rval.Compress();
            }
        }
    }
}
