﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Robbiblubber.Util.Controls
{
    /// <summary>This class provides support for special windows.</summary>
    public sealed class SpecialWindowManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Window.</summary>
        private Form _Window;

        /// <summary>Current sizing direction.</summary>
        private _Direction _Sizing = _Direction.NONE;

        /// <summary>Cursor start point.</summary>
        private Point _CursorStart = new Point(0, 0);

        /// <summary>Close controls.</summary>
        private ObservableCollection<object> _CloseControls = new ObservableCollection<object>();

        /// <summary>Maximize/restore controls.</summary>
        private ObservableCollection<object> _MaxRestoreControls = new ObservableCollection<object>();

        /// <summary>Minimize controls.</summary>
        private ObservableCollection<object> _MinControls = new ObservableCollection<object>();


        /// <summary>Right boundaries.</summary>
        private ObservableCollection<object> _RightBounds = new ObservableCollection<object>();

        /// <summary>Left boundaries.</summary>
        private ObservableCollection<object> _LeftBounds = new ObservableCollection<object>();

        /// <summary>Top boundaries.</summary>
        private ObservableCollection<object> _TopBounds = new ObservableCollection<object>();

        /// <summary>Bottom boundaries.</summary>
        private ObservableCollection<object> _BottomBounds = new ObservableCollection<object>();


        /// <summary>Move handles.</summary>
        private ObservableCollection<object> _MoveHandles = new ObservableCollection<object>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SpecialWindowManager(Form window)
        {
            _Window = window;
            _CloseControls.CollectionChanged += _CtrlBarCollectionChanged;
            _MaxRestoreControls.CollectionChanged += _CtrlBarCollectionChanged;
            _MinControls.CollectionChanged += _CtrlBarCollectionChanged;

            _RightBounds.CollectionChanged += _SizingBoundsCollectionChanged;
            _LeftBounds.CollectionChanged += _SizingBoundsCollectionChanged;
            _TopBounds.CollectionChanged += _SizingBoundsCollectionChanged;
            _BottomBounds.CollectionChanged += _SizingBoundsCollectionChanged;

            _MoveHandles.CollectionChanged += _MoveHandlesCollectionChanged;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the close controls.</summary>
        public IList CloseControls
        {
            get { return _CloseControls; }
        }


        /// <summary>Gets the maximize/restore controls.</summary>
        public IList MaxRestoreControls
        {
            get { return _MaxRestoreControls; }
        }


        /// <summary>Gets the minimize controls.</summary>
        public IList MinimizeControls
        {
            get { return _MinControls; }
        }


        /// <summary>Gets the right bounds controls.</summary>
        public IList RightBounds 
        { 
            get { return _RightBounds; } 
        }


        /// <summary>Gets the left bounds controls.</summary>
        public IList LeftBounds
        {
            get { return _LeftBounds; }
        }


        /// <summary>Gets the right top controls.</summary>
        public IList TopBounds
        {
            get { return _TopBounds; }
        }


        /// <summary>Gets the bottom bounds controls.</summary>
        public IList BottomBounds
        {
            get { return _BottomBounds; }
        }


        /// <summary>Gets the window move handles.</summary>
        public IList MoveHandles
        {
            get { return _MoveHandles; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Enables a control bar using a minimize, maximize/restore, and close control.</summary>
        /// <param name="minCtrl">Minimize control.</param>
        /// <param name="maxRestCtrl">Maximize/Restore control.</param>
        /// <param name="closeCtrl">Close control.</param>
        public void EnableControlBar(object minCtrl, object maxRestCtrl, object closeCtrl)
        {
            MinimizeControls.Add(minCtrl);
            MaxRestoreControls.Add(maxRestCtrl);
            CloseControls.Add(closeCtrl);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets directions for a mouse point.</summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        /// <returns>Directions.</returns>
        private _Direction _GetDirections(object sender, MouseEventArgs e)
        {
            _Direction rval = _Direction.NONE;

            if(_RightBounds.Contains(sender))
            {
                if(e.X >= (int) sender.GetType().GetProperty("Width").GetValue(sender, null) - 4)
                {
                    rval = rval | _Direction.E;
                }
            }

            if(_LeftBounds.Contains(sender))
            {
                if(e.X <= 4)
                {
                    rval = rval | _Direction.W;
                }
            }

            if(_TopBounds.Contains(sender))
            {
                if(e.Y <= 4)
                {
                    rval = rval | _Direction.N;
                }
            }

            if(_BottomBounds.Contains(sender))
            {
                if(e.Y >= (int) sender.GetType().GetProperty("Height").GetValue(sender, null) - 4)
                {
                    rval = rval | _Direction.S;
                }
            }

            return rval;
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Mouse boundary move.</summary>
        private void _SizingMouseMove(object sender, MouseEventArgs e)
        {
            if(_Window.WindowState == FormWindowState.Maximized)
            {
                Cursor.Current = Cursors.Default;
                return;
            }

            if(e.Button != MouseButtons.Left) { _Sizing = _Direction.NONE; }            
            switch((_Sizing == _Direction.NONE) ? _GetDirections(sender, e) : _Sizing)
            {
                case _Direction.NONE:
                    Cursor.Current = Cursors.Default; break;
                case _Direction.N:
                case _Direction.S:
                    Cursor.Current = Cursors.SizeNS; break;
                case _Direction.E:
                case _Direction.W:
                    Cursor.Current = Cursors.SizeWE; break;
                case _Direction.SW:
                case _Direction.NE:
                    Cursor.Current = Cursors.SizeNESW; break;
                case _Direction.NW:
                case _Direction.SE:
                    Cursor.Current = Cursors.SizeNWSE; break;
            }

            if((_Sizing != _Direction.NONE) && (e.Button == MouseButtons.Left))
            {
                int x = e.X, y = e.Y;
                if((_Sizing & _Direction.E) == _Direction.E)
                {
                    _Window.Width = _Window.Width + (e.X - _CursorStart.X);
                }
                if((_Sizing & _Direction.S) == _Direction.S)
                {
                    _Window.Height = _Window.Height + (e.Y - _CursorStart.Y);
                }
                if((_Sizing & _Direction.N)  == _Direction.N)
                {
                    int m = _CursorStart.Y - e.Y;
                    _Window.Top = _Window.Top - m;
                    _Window.Height = _Window.Height + m;
                    y = e.Y + m;
                }
                if((_Sizing & _Direction.W) == _Direction.W)
                {
                    int m = _CursorStart.X - e.X;
                    if(m != 0)
                    {
                        _Window.Left = _Window.Left - m;
                        _Window.Width = _Window.Width + m;
                        x = e.X + m;
                    }
                }
                _CursorStart = new Point(x, y);
            }
        }


        /// <summary>Mouse boundary down.</summary>
        private void _SizingMouseDown(object sender, MouseEventArgs e)
        {
            if((_Window.WindowState != FormWindowState.Maximized) && (e.Button == MouseButtons.Left))
            {
                _Sizing = _GetDirections(sender, e);
                _CursorStart = new Point(e.X, e.Y);
            }
        }


        /// <summary>Mouse boundary move.</summary>
        private void _MovingMouseMove(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                if(_Sizing != _Direction.NONE) return;
                _Window.Location = new Point(_Window.Location.X + (e.X - _CursorStart.X), _Window.Location.Y + (e.Y - _CursorStart.Y));
            }
        }


        /// <summary>Mouse boundary down.</summary>
        private void _MovingMouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                _CursorStart = e.Location;
            }
        }


        /// <summary>Max/restore button click.</summary>
        private void _MaxRestoreButton_Click(object sender, EventArgs e)
        {
            if(_Window.WindowState == FormWindowState.Maximized)
            {
                _Window.WindowState = FormWindowState.Normal;
            }
            else { _Window.WindowState = FormWindowState.Maximized; }
        }


        /// <summary>Close button click.</summary>
        private void _CloseButton_Click(object sender, EventArgs e)
        {
            _Window.Close();
        }


        /// <summary>Minimize button click.</summary>
        private void _MinButton_Click(object sender, EventArgs e)
        {
            _Window.WindowState = FormWindowState.Minimized;
        }


        /// <summary>Collection change event handler.</summary>
        private void _SizingBoundsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach(var i in new (string Name, MouseEventHandler Handler)[] { ("MouseDown", _SizingMouseDown), ("MouseMove", _SizingMouseMove) })
            {
                if(e.OldItems is not null)
                    foreach(object k in e.OldItems)
                    {
                        if(k.GetType().GetEvent(i.Name) is null) continue;
                        k.GetType().GetEvent(i.Name).RemoveEventHandler(k, i.Handler);
                    }

                if(e.NewItems is not null)
                    foreach(object k in e.NewItems)
                    {
                        if(k.GetType().GetEvent(i.Name) is null) continue;
                        k.GetType().GetEvent(i.Name).RemoveEventHandler(k, i.Handler);
                        k.GetType().GetEvent(i.Name).AddEventHandler(k, i.Handler);
                    }
            }
        }


            /// <summary>Collection change event handler.</summary>
            private void _CtrlBarCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            EventHandler h = null;
            
            if(sender == _CloseControls)
            {
                h = _CloseButton_Click;
            }
            else if(sender == _MaxRestoreControls)
            {
                h = _MaxRestoreButton_Click;
            }
            else if(sender == _MinControls)
            {
                h = _MinButton_Click;
            }


            if(e.OldItems is not null)
                foreach(object i in e.OldItems)
                {
                    if(i.GetType().GetEvent("Click") is null) continue;
                    i.GetType().GetEvent("Click").RemoveEventHandler(i, h);
                }

            if(e.NewItems is not null)
                foreach(object i in e.NewItems)
                {
                    if(i.GetType().GetEvent("Click") is null) continue;
                    i.GetType().GetEvent("Click").RemoveEventHandler(i, h);
                    i.GetType().GetEvent("Click").AddEventHandler(i, h);
                }
        }


        /// <summary>Collection change event handler.</summary>
        private void _MoveHandlesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            foreach(var i in new (string Name, MouseEventHandler Handler)[] { ("MouseDown", _MovingMouseDown), ("MouseMove", _MovingMouseMove) })
            {
                if(e.OldItems is not null)
                    foreach(object k in e.OldItems)
                    {
                        if(k.GetType().GetEvent(i.Name) is null) continue;
                        k.GetType().GetEvent(i.Name).RemoveEventHandler(k, i.Handler);
                    }

                if(e.NewItems is not null)
                    foreach(object k in e.NewItems)
                    {
                        if(k.GetType().GetEvent(i.Name) is null) continue;
                        k.GetType().GetEvent(i.Name).RemoveEventHandler(k, i.Handler);
                        k.GetType().GetEvent(i.Name).AddEventHandler(k, i.Handler);
                    }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [enum] _Direction                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This enumeration defines directions.</summary>
        private enum _Direction: int
        {
            NONE = 0,
            N = 1,
            E = 2,
            S = 4,
            W = 8,
            NE = 1|2,
            SE = 4|2,
            SW = 4|8,
            NW = 1|8
        }
    }
}
