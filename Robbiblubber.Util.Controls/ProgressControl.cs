﻿using System;
using System.Drawing;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class implements a sinple progress bar control.</summary>
    public partial class ProgressControl: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Maximum value.</summary>
        protected int _Maximum = 100;

        /// <summary>Current value.</summary>
        protected int _Value = 0;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public ProgressControl()
        {
            InitializeComponent();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the progress bar color.</summary>
        public Color BarColor
        {
            get; set;
        } = SystemColors.Highlight;


        /// <summary>Gets or sets the progress bar maximum value.</summary>
        public int Maximum
        {
            get { return _Maximum; }
            set 
            {
                if(value < 1)
                {
                    _Maximum = 1;
                }
                _Maximum = value;
                Refresh();
            }
        }


        /// <summary>Gets or sets the current progress bar value.</summary>
        public int Value
        {
            get { return _Value; }
            set
            {
                if(value < 0)
                {
                    _Value = 0;
                }
                else if(value > _Maximum)
                {
                    _Value = _Maximum;
                }
                else { _Value = value; }

                Refresh();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Control                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Raises the paint event.</summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            int w = 0;
            try
            {
                w = Convert.ToInt32((Width - 2) * (((double) _Value) / _Maximum));
                if(w > Width - 2) { w = (Width - 2); }
            }
            catch(Exception) {}

            e.Graphics.DrawRectangle(new Pen(ForeColor, 1), 0, 0, Width - 1, Height - 1);
            if(w > 0) { e.Graphics.FillRectangle(new SolidBrush(BarColor), 1, 1, w, Height - 2); }
        }
    }
}
