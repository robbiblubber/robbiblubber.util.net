﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class provides a "recent files" list.</summary>
    public class RecentFileList: IEnumerable<string>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Repository file.</summary>
        private string _RepositoryFile;

        /// <summary>Maximum list length.</summary>
        private int _MaxLength;

        /// <summary>Items.</summary>
        private List<string> _Items;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="repositoryFile">Repository file.</param>
        /// <param name="maxLength">Maximum list length.</param>
        public RecentFileList(string repositoryFile, int maxLength)
        {
            RepositoryFile = repositoryFile;
            _MaxLength = maxLength;
            _Load();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the recent files repository file.</summary>
        public string RepositoryFile
        {
            get { return _RepositoryFile; }
            set
            {
                _RepositoryFile = value;
                _Load();
            }
        }


        /// <summary>Gets or sets the maximum length or the list.</summary>
        public int MaxLength
        {
            get { return _MaxLength; }
            set
            {
                for(int i = _Items.Count - 1; i > (_MaxLength - 2); i--)
                {
                    _Items.RemoveAt(i);
                }
                
                _MaxLength = value;
                _Save();
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds a file name to the list.</summary>
        /// <param name="fileName">File name.</param>
        public void Add(string fileName)
        {
            fileName = fileName.Trim();

            if(File.Exists(fileName))
            {
                for(int i = 0; i < _Items.Count; i++)
                {
                    if(_Items[i].Trim().ToLower() == fileName.ToLower())
                    {
                        _Items.RemoveAt(i);
                        break;
                    }
                }
                
                _Items.Insert(0, fileName);
                MaxLength = _MaxLength;
            }
            _Save();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads the list from repository.</summary>
        private void _Load()
        {
            _Items = new List<string>();

            Ddp cfg = Ddp.Open(_RepositoryFile);
            
            int l = cfg["/list/length"];

            if(l > 0)
            {
                for(int i = 0; i < l; i++)
                {
                    string s = cfg["/files/[" + i.ToString() + "]"];

                    if(string.IsNullOrWhiteSpace(s)) continue;
                    if(File.Exists(s))
                    {
                        _Items.Add(s);
                    }
                }
            }
        }


        /// <summary>Saves the list to repository.</summary>
        private void _Save()
        {
            Ddp cfg = new Ddp();
            int l = 0;

            for(int i = 0; i < _Items.Count; i++)
            {
                if(File.Exists(_Items[i]))
                {
                    cfg.Set("files/[" + (l++).ToString() + "]", _Items[i]);
                }
            }

            cfg.Set("list/length", l);
            cfg.Save(_RepositoryFile);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<string>                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<string> IEnumerable<string>.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }


        /// <summary>Gets an enumerator.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }
    }
}
