﻿using System;
using System.Drawing;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class provides a link control.</summary>
    public partial class LinkControl: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public LinkControl()
        {
            InitializeComponent();

            Text = Name;
            DisabledColor = Color.Gray;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the link text.</summary>
        public string LinkText
        {
            get { return Text; }
            set
            {
                Text = value;
                Refresh();
            }
        }


        /// <summary>Gets or sets the disabled link color.</summary>
        public Color DisabledColor
        {
            get; set;
        }


        /// <summary>Gets or sets if the link should be underlined.</summary>
        public bool Underline
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Control                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Called when the control needs to be painted.</summary>
        /// <param name="e">Event arguments.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if(AutoSize)
            {
                SizeF f = e.Graphics.MeasureString(Text, Font);
                Size = new Size(Convert.ToInt32(f.Width) + 2, Convert.ToInt32(f.Height) + 2);
            }
            
            e.Graphics.DrawString(Text, ((Underline && Enabled) ? new Font(Font, FontStyle.Underline) : Font), new SolidBrush(Enabled ? ForeColor : DisabledColor), new Point(0, 0));
        }
    }
}
