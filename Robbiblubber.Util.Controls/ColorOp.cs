﻿using System;
using System.Drawing;



namespace Robbiblubber.Util.Controls
{
    /// <summary>Color utility class.</summary>
    public static class ColorOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the hexadecimal representation of the color.</summary>
        /// <param name="color">Color.</param>
        /// <param name="prefixSharp">Determines if the color should be preceded by a sharp sign.</param>
        /// <param name="uppercase">Determines if the hexadecimal characters should be upper case.</param>
        /// <returns></returns>
        public static string ToHex(this Color color, bool prefixSharp = false, bool uppercase = false)
        {
            string f = (uppercase ? "X2" : "x2");
            return (prefixSharp ? "#" : "") + color.R.ToString(f) + color.G.ToString(f) + color.B.ToString(f);
        }


        /// <summary>Returns the uppercase hexadecimal representation of the color with leading sharp sign.</summary>
        /// <param name="color">Color.</param>
        public static string ToHTML(this Color color)
        {
            return ToHex(color, true, true);
        }


        /// <summary>Returns a color for a given hexadecimal expression.</summary>
        /// <param name="value">Hexadecimal representation of the color.</param>
        /// <returns>Color.</returns>
        public static Color FromHex(string value)
        {
            int i = (value.StartsWith("#") ? 1 : 0);
            return Color.FromArgb(Convert.ToByte(value.Substring(i, 2), 16), Convert.ToByte(value.Substring(i + 2, 2), 16), Convert.ToByte(value.Substring(i + 4, 2), 16));
        }


        /// <summary>Returns a color for a given hexadecimal (HTML-)expression.</summary>
        /// <param name="value">Hexadecimal representation of the color.</param>
        /// <returns>Color.</returns>
        public static Color FromHTML(this string value)
        {
            return FromHex(value);
        }


        /// <summary>Serializes the color to a hexadecimal string.</summary>
        /// <param name="color">Color.</param>
        /// <returns>Serialized string.</returns>
        public static string Serialize(this Color color)
        {
            return ToHex(color, false, false);
        }


        /// <summary>Deserializes a hexadecimal string to a color object.</summary>
        /// <param name="value">Serialized string.</param>
        /// <returns>Color.</returns>
        public static Color Deserialze(string value)
        {
            return FromHex(value);
        }
    }
}
