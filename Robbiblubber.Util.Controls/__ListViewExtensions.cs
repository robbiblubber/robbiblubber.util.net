﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class provides extensions to the ListView control.</summary>
    internal static class __ListViewExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Selects the previous item.</summary>
        /// <param name="lv">List view.</param>
        internal static void __SelectPrevious(this ListView lv)
        {
            if(lv.SelectedItems.Count == 0) return;

            ListViewItem cur = lv.SelectedItems[0];
            ListViewItem hit = null;

            foreach(ListViewItem i in lv.Items)
            {
                if(i == cur) break;
                hit = i;
            }

            if(hit != null)
            {
                hit.EnsureVisible();
                hit.Selected = true;
            }
        }



        /// <summary>Selects the next item.</summary>
        /// <param name="lv">List view.</param>
        internal static void __SelectNext(this ListView lv)
        {
            if(lv.SelectedItems.Count == 0) return;

            ListViewItem cur = lv.SelectedItems[0];
            ListViewItem hit = null;
            bool found = false;

            foreach(ListViewItem i in lv.Items)
            {
                if(found)
                {
                    hit = i;
                    break;
                }
                if(i == cur) { found = true; }
            }

            if(hit != null)
            {
                hit.EnsureVisible();
                hit.Selected = true;
            }
        }
    }
}
