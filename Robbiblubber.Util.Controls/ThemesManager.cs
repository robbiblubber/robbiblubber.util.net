﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class provides a theme manager.</summary>
    public sealed class ThemesManager
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Availabe themes.</summary>
        private Theme.ThemeList _Themes = null;

        /// <summary>Current theme.</summary>
        private Theme _Current = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public ThemesManager()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public events                                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Raised when the current theme has been changed.</summary>
        public event EventHandler ThemeChanged;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the current theme.</summary>
        public Theme Current
        {
            get { return _Current; }
            set
            {
                _Current = value;
                if(AutoApply) { Apply(); }
                ThemeChanged?.Invoke(this, EventArgs.Empty);
            }
        }


        /// <summary>Gets or sets if changing the current theme will apply it to all elements.</summary>
        public bool AutoApply
        {
            get; set;
        } = true;


        /// <summary>Gets all available themes.</summary>
        public Theme.ThemeList Themes
        {
            get
            {
                if(_Themes == null) { _Themes = new Theme.ThemeList(); }

                return _Themes;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Event handler for menu strip separator paint events.</summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private static void _SeparatorOnPaint(object sender, PaintEventArgs e)
        {
            e.Graphics.FillRectangle(new SolidBrush(((ToolStripItem) sender).BackColor), 0, 0, ((ToolStripItem) sender).Width, ((ToolStripItem) sender).Height);
            e.Graphics.DrawLine(new Pen(((ToolStripItem) sender).ForeColor), 30, ((ToolStripItem) sender).Height / 2, ((ToolStripItem) sender).Width - 4, ((ToolStripItem) sender).Height / 2);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private void _Apply(Theme theme, object item)
        {
            _ApplyToSubitems(theme, item, "Controls", "Items", "DropDownItems");

            if(item is ToolStripSeparator)
            {
                ((ToolStripSeparator) item).Paint -= _SeparatorOnPaint;
                ((ToolStripSeparator) item).Paint += _SeparatorOnPaint;
            }
            else if(item is ToolStrip)
            {
                ((ToolStrip) item).Renderer = new __ThemeToolStripRenderer(theme);
            }

            _ApplyColor(item, "ForeColor", theme.ColorScheme.WindowText);
            _ApplyColor(item, "BackColor", theme.ColorScheme.ButtonFace);
        }


        /// <summary>Applies a theme to all subitems of an item.</summary>
        /// <param name="theme">Theme.</param>
        /// <param name="item">Item.</param>
        /// <param name="subitems">Subitem property names.</param>
        private void _ApplyToSubitems(Theme theme, object item, params string[] subitems)
        {
            foreach(string i in subitems)
            {
                if(item.GetType().GetProperty(i) != null)
                {
                    foreach(object k in (IEnumerable) item.GetType().GetProperty(i).GetValue(item, null)) { _Apply(theme, k); }
                }
            }
        }


        /// <summary>Applies a color to a item property.</summary>
        /// <param name="item">Item.</param>
        /// <param name="property">Property name.</param>
        /// <param name="color">Color.</param>
        private void _ApplyColor(object item, string property, Color color)
        {
            if(item.GetType().GetProperty(property) != null)
            {
                item.GetType().GetProperty(property).SetValue(item, color, null);
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Applys a theme to a form or control.</summary>
        /// <param name="theme">Theme.</param>
        /// <param name="c">Controls.</param>
        public void Apply(Theme theme, params object[] c)
        {
            if(theme == null) { theme = _Current; }
            if(theme == null) return;

            IEnumerable ctrls = c;
            if((c == null) || (c.Length == 0))  
            {  
                ctrls = Application.OpenForms; 
            }

            foreach(Control i in ctrls)
            {
                _Apply(theme, i);
            }
        }


        /// <summary>Applys the current theme to a form or control.</summary>
        /// <param name="c">Control.</param>
        public void Apply(params object[] c)
        {
            Apply(null, c);
        }
    }
}
