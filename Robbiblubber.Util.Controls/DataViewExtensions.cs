﻿using System;
using System.Drawing;
using System.Windows.Forms;

using Robbiblubber.Util.Localization;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class provides data view extension methods.</summary>
    public static class DataViewExtensions
    {
        /// <summary>Finds text in table.</summary>
        /// <param name="gridview">Data grid.</param>
        /// <param name="text">String to find.</param>
        public static void Find(this DataGridView gridview, string text)
        {
            text = text.ToLower();
            Point v, start = gridview.CurrentCellAddress;
            int width = gridview.ColumnCount - 1;

            if(start.X < width)
            {
                v = new Point(start.X + 1, start.Y);
            }
            else
            {
                v = new Point(0, start.Y + 1);
            }
            if(v.Y >= gridview.RowCount) { v = new Point(0, 0); }

            while(v != start)
            {
                if((gridview[v.X, v.Y].Value != null) && gridview[v.X, v.Y].Value.ToString().ToLower().Contains(text))
                {
                    gridview.CurrentCell = gridview[v.X, v.Y];
                    return;
                }

                if(v.X < width)
                {
                    v = new Point(v.X + 1, v.Y);
                }
                else
                {
                    v = new Point(0, v.Y + 1);
                }
                if(v.Y >= gridview.RowCount) { v = new Point(0, 0); }
            }

            string mtext = Locale.Lookup("utctrl::udiag.find.nomatches.text", "No matches");
            string mcap = Locale.Lookup("utctrl::udiag.find.nomatches.caption", "Find");

            MessageBox.Show(mtext, mcap, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
