﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Util.Controls
{
    /// <summary>Key words implement this interface.</summary>
    public interface IAutoCompletionKeyword
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the key word.</summary>
        string Word { get; }


        /// <summary>Gets the image key.</summary>
        string ImageKey { get; }


        /// <summary>Gets if the keyword should be italic.</summary>
        bool Italic { get; }


        /// <summary>Gets if the keyword should be bold.</summary>
        bool Bold { get; }
    }
}
