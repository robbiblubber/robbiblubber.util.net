﻿using System;
using System.Threading;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>This class implements synchronized execution functionality.</summary>
    public class SynchronizedExecution
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SynchronizedExecution()
        {
            ExecutingThread = null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the executing thread.</summary>
        public Thread ExecutingThread { get; protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        /// 
        public static void Run(ThreadStart c)
        {
            new SynchronizedExecution().ExecuteSynchronized(c);
        }



        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        /// <param name="p">Paramter.</param>
        public static void Run(ParameterizedThreadStart c, object p)
        {
            new SynchronizedExecution().ExecuteSynchronized(c, p);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Starts the execution in a separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        public void Start(ThreadStart c)
        {
            ExecutingThread = new Thread(c);
            ExecutingThread.Start();
        }


        /// <summary>Starts the execution in a separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        /// <param name="p">Parameter.</param>
        public void Start(ParameterizedThreadStart c, object p)
        {
            ExecutingThread = new Thread(c);
            ExecutingThread.Start(p);
        }


        /// <summary>Waits for the currently executing thread to terminate.</summary>
        public void Wait()
        {
            while(ExecutingThread.IsAlive)
            {
                Application.DoEvents();
                Thread.Sleep(60);
            }
        }


        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        public void ExecuteSynchronized(ThreadStart c)
        {
            Start(c);
            Wait();
        }

        
        /// <summary>Executes code in separate thread.</summary>
        /// <param name="c">Thread delegate.</param>
        /// <param name="p">Paramter.</param>
        public void ExecuteSynchronized(ParameterizedThreadStart c, object p)
        {
            Start(c, p);
            Wait();
        }
    }
}
