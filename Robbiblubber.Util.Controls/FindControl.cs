﻿using System;
using System.Windows.Forms;



namespace Robbiblubber.Util.Controls
{
    /// <summary>Find control.</summary>
    public partial class FindControl: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Controls pinned flag.</summary>
        internal static bool _ControlsPinned = false;

        /// <summary>Controls search text.</summary>
        internal static string _ControlsSearchText = "";

        /// <summary>Controls replace text.</summary>
        internal static string _ControlsReplaceText = "";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FindControl()
        {
            InitializeComponent();

            PinButton.Checked = _ControlsPinned;
            SearchTextBox.Text = _ControlsSearchText;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the current search text.</summary>
        public static string CurrentSearchText
        {
            get { return _ControlsSearchText; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public events                                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Occurs when search text has changed.</summary>
        public event EventHandler SearchTextChanged;

        /// <summary>Occures when a search is performed.</summary>
        public event EventHandler Search;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the control pin state.</summary>
        public bool Pinned
        {
            get { return PinButton.Checked; }
            set { PinButton.Checked = value; }
        }


        /// <summary>Gets or sets the search text.</summary>
        public string SearchText
        {
            get { return SearchTextBox.Text; }
            set { SearchTextBox.Text = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Moves the current selection in the text box to the Clipboard.</summary>
        public void Cut()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Cut(); }
        }


        /// <summary>Copies the current selection in the text box to the Clipboard.</summary>
        public void Copy()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Copy(); }
        }


        /// <summary>Replaces the current selection in the text box with the contents of the Clipboard.</summary>
        public void Paste()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).Paste(); }
        }


        /// <summary>Selects all text.</summary>
        public void SelectAll()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).SelectAll(); }
        }


        /// <summary>Deselects all text.</summary>
        public void DeselectAll()
        {
            if(ActiveControl is TextBox) { ((TextBox) ActiveControl).DeselectAll(); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Control                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the control text.</summary>
        public override string Text
        {
            get { return SearchTextBox.Text; }
            set { SearchTextBox.Text = value; }
        }


        /// <summary>Shows the control.</summary>
        public new void Show()
        {
            SearchTextBox.Text = _ControlsSearchText;
            Pinned = FindControl._ControlsPinned;
            base.Show();
            Focus();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Search text changed.</summary>
        private void SearchTextBox_TextChanged(object sender, EventArgs e)
        {
            FindButton.Enabled = (SearchTextBox.TextLength > 0);
            _ControlsSearchText = SearchTextBox.Text;

            OnTextChanged(e);
            SearchTextChanged?.Invoke(this, e);
        }


        /// <summary>Pin check changed.</summary>
        private void PinButton_CheckedChanged(object sender, EventArgs e)
        {
            _ControlsPinned = PinButton.Checked;
        }


        /// <summary>Button "Close" click.</summary>
        private void CloseButton_Click(object sender, EventArgs e)
        {
            Visible = false;
        }


        /// <summary>Button "Find" click.</summary>
        private void FindButton_Click(object sender, EventArgs e)
        {
            _ControlsSearchText = SearchTextBox.Text;
            if(!Pinned) Hide();

            Search?.Invoke(this, EventArgs.Empty);
        }


        /// <summary>Control key down.</summary>
        private void FindControl_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                if(FindButton.Enabled) { FindButton_Click(null, null); }
            }
            else if(e.KeyCode == Keys.Escape) { Hide(); }
        }
    }
}
