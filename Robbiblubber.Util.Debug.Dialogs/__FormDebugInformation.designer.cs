﻿namespace Robbiblubber.Util.Debug.Dialogs
{
    partial class __FormDebugInformation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(@__FormDebugInformation));
            this._LabelDebug = new System.Windows.Forms.Label();
            this._TextDebug = new System.Windows.Forms.TextBox();
            this._ButtonClose = new System.Windows.Forms.Button();
            this._LinkCopy = new Robbiblubber.Util.Controls.LinkControl();
            this.SuspendLayout();
            // 
            // _LabelDebug
            // 
            this._LabelDebug.AutoSize = true;
            this._LabelDebug.Location = new System.Drawing.Point(22, 21);
            this._LabelDebug.Name = "_LabelDebug";
            this._LabelDebug.Size = new System.Drawing.Size(148, 17);
            this._LabelDebug.TabIndex = 0;
            this._LabelDebug.Tag = "debugx::udiag.view.text";
            this._LabelDebug.Text = "Debug Information Text:";
            // 
            // _TextDebug
            // 
            this._TextDebug.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this._TextDebug.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._TextDebug.Location = new System.Drawing.Point(25, 43);
            this._TextDebug.Multiline = true;
            this._TextDebug.Name = "_TextDebug";
            this._TextDebug.ReadOnly = true;
            this._TextDebug.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this._TextDebug.Size = new System.Drawing.Size(970, 463);
            this._TextDebug.TabIndex = 1;
            this._TextDebug.WordWrap = false;
            // 
            // _ButtonClose
            // 
            this._ButtonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonClose.Location = new System.Drawing.Point(848, 527);
            this._ButtonClose.Name = "_ButtonClose";
            this._ButtonClose.Size = new System.Drawing.Size(147, 29);
            this._ButtonClose.TabIndex = 4;
            this._ButtonClose.Tag = "debugx::common.button.close";
            this._ButtonClose.Text = "&Close";
            this._ButtonClose.UseVisualStyleBackColor = true;
            // 
            // _LinkCopy
            // 
            this._LinkCopy.AutoSize = true;
            this._LinkCopy.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkCopy.DisabledColor = System.Drawing.Color.Gray;
            this._LinkCopy.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkCopy.LinkText = "Copy to clipboard...";
            this._LinkCopy.Location = new System.Drawing.Point(583, 532);
            this._LinkCopy.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkCopy.Name = "_LinkCopy";
            this._LinkCopy.Size = new System.Drawing.Size(121, 21);
            this._LinkCopy.TabIndex = 5;
            this._LinkCopy.TabStop = false;
            this._LinkCopy.Tag = "debugx::udiag.view.copy";
            this._LinkCopy.Underline = false;
            this._LinkCopy.Click += new System.EventHandler(this._LinkCopy_Click);
            // 
            // __FormDebugInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 578);
            this.Controls.Add(this._LinkCopy);
            this.Controls.Add(this._ButtonClose);
            this.Controls.Add(this._TextDebug);
            this.Controls.Add(this._LabelDebug);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "__FormDebugInformation";
            this.Tag = "debugx::udiag.view.caption";
            this.Text = "Debug Information";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelDebug;
        private System.Windows.Forms.TextBox _TextDebug;
        private System.Windows.Forms.Button _ButtonClose;
        private Controls.LinkControl _LinkCopy;
    }
}