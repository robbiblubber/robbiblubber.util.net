﻿namespace Robbiblubber.Util.Debug.Dialogs
{
    partial class __FormSendDump
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(@__FormSendDump));
            this._LabelInformation = new System.Windows.Forms.Label();
            this._LinkShow = new Robbiblubber.Util.Controls.LinkControl();
            this._LabelDescription = new System.Windows.Forms.Label();
            this._TextDescription = new System.Windows.Forms.TextBox();
            this._ButtonCancel = new System.Windows.Forms.Button();
            this._ButtonSend = new System.Windows.Forms.Button();
            this._PanelSend = new System.Windows.Forms.Panel();
            this._LabelSend = new System.Windows.Forms.Label();
            this._PbarSend = new System.Windows.Forms.ProgressBar();
            this._PanelSend.SuspendLayout();
            this.SuspendLayout();
            // 
            // _LabelInformation
            // 
            this._LabelInformation.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this._LabelInformation.Location = new System.Drawing.Point(31, 27);
            this._LabelInformation.Name = "_LabelInformation";
            this._LabelInformation.Size = new System.Drawing.Size(787, 63);
            this._LabelInformation.TabIndex = 0;
            this._LabelInformation.Text = resources.GetString("_LabelInformation.Text");
            // 
            // _LinkShow
            // 
            this._LinkShow.AutoSize = true;
            this._LinkShow.Cursor = System.Windows.Forms.Cursors.Hand;
            this._LinkShow.DisabledColor = System.Drawing.Color.Gray;
            this._LinkShow.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LinkShow.LinkText = "Show debug information...";
            this._LinkShow.Location = new System.Drawing.Point(34, 94);
            this._LinkShow.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this._LinkShow.Name = "_LinkShow";
            this._LinkShow.Size = new System.Drawing.Size(162, 21);
            this._LinkShow.TabIndex = 0;
            this._LinkShow.TabStop = false;
            this._LinkShow.Tag = "debugx::udiag.send.show";
            this._LinkShow.Underline = false;
            this._LinkShow.Click += new System.EventHandler(this._LinkShow_Click);
            // 
            // _LabelDescription
            // 
            this._LabelDescription.AutoSize = true;
            this._LabelDescription.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelDescription.Location = new System.Drawing.Point(31, 144);
            this._LabelDescription.Name = "_LabelDescription";
            this._LabelDescription.Size = new System.Drawing.Size(113, 13);
            this._LabelDescription.TabIndex = 0;
            this._LabelDescription.Text = "Problem &description:";
            // 
            // _TextDescription
            // 
            this._TextDescription.AcceptsReturn = true;
            this._TextDescription.AcceptsTab = true;
            this._TextDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._TextDescription.Location = new System.Drawing.Point(34, 160);
            this._TextDescription.Multiline = true;
            this._TextDescription.Name = "_TextDescription";
            this._TextDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this._TextDescription.Size = new System.Drawing.Size(784, 232);
            this._TextDescription.TabIndex = 0;
            // 
            // _ButtonCancel
            // 
            this._ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonCancel.Location = new System.Drawing.Point(653, 428);
            this._ButtonCancel.Name = "_ButtonCancel";
            this._ButtonCancel.Size = new System.Drawing.Size(147, 29);
            this._ButtonCancel.TabIndex = 2;
            this._ButtonCancel.Tag = "debugx::common.button.cancel";
            this._ButtonCancel.Text = "&Cancel";
            this._ButtonCancel.UseVisualStyleBackColor = true;
            this._ButtonCancel.Click += new System.EventHandler(this._ButtonCancel_Click);
            // 
            // _ButtonSend
            // 
            this._ButtonSend.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this._ButtonSend.Location = new System.Drawing.Point(497, 428);
            this._ButtonSend.Name = "_ButtonSend";
            this._ButtonSend.Size = new System.Drawing.Size(147, 29);
            this._ButtonSend.TabIndex = 1;
            this._ButtonSend.Tag = "debugx::common.button.send";
            this._ButtonSend.Text = "&Send";
            this._ButtonSend.UseVisualStyleBackColor = true;
            this._ButtonSend.Click += new System.EventHandler(this._ButtonSend_Click);
            // 
            // _PanelSend
            // 
            this._PanelSend.Controls.Add(this._LabelSend);
            this._PanelSend.Controls.Add(this._PbarSend);
            this._PanelSend.Location = new System.Drawing.Point(12, 144);
            this._PanelSend.Name = "_PanelSend";
            this._PanelSend.Size = new System.Drawing.Size(822, 261);
            this._PanelSend.TabIndex = 0;
            this._PanelSend.Visible = false;
            // 
            // _LabelSend
            // 
            this._LabelSend.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this._LabelSend.Location = new System.Drawing.Point(19, 147);
            this._LabelSend.Name = "_LabelSend";
            this._LabelSend.Size = new System.Drawing.Size(769, 18);
            this._LabelSend.TabIndex = 1;
            this._LabelSend.Tag = "debugx::udiag.send.gathering";
            this._LabelSend.Text = "Gathering data...";
            // 
            // _PbarSend
            // 
            this._PbarSend.Location = new System.Drawing.Point(22, 119);
            this._PbarSend.Name = "_PbarSend";
            this._PbarSend.Size = new System.Drawing.Size(766, 23);
            this._PbarSend.Step = 1;
            this._PbarSend.TabIndex = 0;
            // 
            // __FormSendDump
            // 
            this.AcceptButton = this._ButtonSend;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._ButtonCancel;
            this.ClientSize = new System.Drawing.Size(846, 487);
            this.Controls.Add(this._PanelSend);
            this.Controls.Add(this._ButtonSend);
            this.Controls.Add(this._ButtonCancel);
            this.Controls.Add(this._TextDescription);
            this.Controls.Add(this._LabelDescription);
            this.Controls.Add(this._LinkShow);
            this.Controls.Add(this._LabelInformation);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "__FormSendDump";
            this.Tag = "debugx::udiag.send.caption";
            this.Text = "Upload Debug Information";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.@__FormSendDump_FormClosing);
            this._PanelSend.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _LabelInformation;
        private Controls.LinkControl _LinkShow;
        private System.Windows.Forms.Label _LabelDescription;
        private System.Windows.Forms.TextBox _TextDescription;
        private System.Windows.Forms.Button _ButtonCancel;
        private System.Windows.Forms.Button _ButtonSend;
        private System.Windows.Forms.Panel _PanelSend;
        private System.Windows.Forms.Label _LabelSend;
        private System.Windows.Forms.ProgressBar _PbarSend;
    }
}