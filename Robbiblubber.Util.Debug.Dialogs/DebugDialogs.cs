﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Debug.Dialogs
{
    /// <summary>This class provides dialog access.</summary>
    public static class DebugDialogs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Shows a message box.</summary>
        /// <param name="debugKey">Debug key.</param>
        /// <param name="msg">Exception message.</param>
        public static void ShowMessageBox(string debugKey, string msg)
        {
            MessageBox.Show("debugx::udiag.dump.message".Localize("An unexpected error ($(e)) has occured:").Replace("$(e)", debugKey) + "\n" + msg, "debugx::udiag.dump.caption".Localize("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        /// <summary>Shows debug log.</summary>
        /// <param name="text">Log text.</param>
        public static void ShowLog(string text)
        {
            __FormDebugInformation.ShowDialog(text);
        }


        /// <summary>Shows the send dialog.</summary>
        /// <param name="data">Debug data.</param>
        /// <param name="sndMeta">Metadata sending method.</param>
        /// <param name="sndData">Data sending method.</param>
        /// <param name="cleanup">Cleanup method.</param>
        public static void SendData(string data, Action<string> sndMeta, Action sndData, Action cleanup)
        {
            __FormSendDump.ShowDialog(data, sndMeta, sndData, cleanup);
        }
    }
}
