﻿using System;
using System.Windows.Forms;

using Robbiblubber.Util.Localization.Controls;



namespace Robbiblubber.Util.Debug.Dialogs
{
    /// <summary>This class implements the send dump window.</summary>
    internal partial class __FormSendDump: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sending flag.</summary>
        private bool _Sending = false;

        /// <summary>Data.</summary>
        private string _Data;

        /// <summary>Send metadata method.</summary>
        private Action<string> _SendMeta;

        /// <summary>Send data method.</summary>
        private Action _SendData;

        /// <summary>Cleanup method.</summary>
        private Action _Cleanup;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="data">Debug data.</param>
        /// <param name="sndMeta">Metadata sending method.</param>
        /// <param name="sndData">Data sending method.</param>
        /// <param name="cleanup">Cleanup method.</param>
        public __FormSendDump(string data, Action<string> sndMeta, Action sndData, Action cleanup)
        {
            InitializeComponent();

            this.Localize();

            _Data = data;
            _SendMeta = sndMeta;
            _SendData = sndData;
            _Cleanup = cleanup;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Shows the send data dialog.</summary>
        /// <param name="data">Debug data.</param>
        /// <param name="sndMeta">Metadata sending method.</param>
        /// <param name="sndData">Data sending method.</param>
        /// <param name="cleanup">Cleanup method.</param>
        public static void ShowDialog(string data, Action<string> sndMeta, Action sndData, Action cleanup)
        {
            __FormSendDump f = new __FormSendDump(data, sndMeta, sndData, cleanup);
            f.ShowDialog();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Link "Show" clicked.</summary>
        private void _LinkShow_Click(object sender, EventArgs e)
        {
            __FormDebugInformation.ShowDialog(_Data);
        }


        /// <summary>Button "Cancel" clicked.</summary>
        private void _ButtonCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }


        /// <summary>Button "Send" clicked.</summary>
        private void _ButtonSend_Click(object sender, EventArgs e)
        {
            _Sending = true;

            _ButtonCancel.Enabled = _ButtonSend.Enabled = false;
            _LinkShow.Visible = false;

            _PanelSend.Visible = true;

            _LabelInformation.Text = "debugx::udiag.send.sending".Localize("The information you provided is now being sent. Depending on the data size this may take a minute or two. Thank you for the valuabe data you have gathered!");
            Application.DoEvents();
            
            _PbarSend.Maximum = 8;
            _PbarSend.Value = 0;

            _LabelSend.Text = "debugx::udiag.send.senddata".Localize("Sending problem description.");
            _SendMeta(_TextDescription.Text);
            _PbarSend.Value = 2;

            _LabelSend.Text = "debugx::udiag.send.chunk".Localize("Sending chunk $(a) of $(b)..").Replace("$(a)", "1").Replace("$(b)", "1");
            _SendData();
            _PbarSend.Value = 6;

            _LabelSend.Text = "debugx::udiag.send.done".Localize("Done.");
            _Cleanup();
            _PbarSend.Value = 8;
            
            _Sending = false;

            DialogResult = DialogResult.OK;
            Close();
        }


        /// <summary>Form closing.</summary>
        private void __FormSendDump_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = _Sending;
        }
    }
}
