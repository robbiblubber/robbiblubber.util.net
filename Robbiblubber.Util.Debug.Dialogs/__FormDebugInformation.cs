﻿using Robbiblubber.Util.Localization.Controls;
using System;
using System.Diagnostics;
using System.Windows.Forms;



namespace Robbiblubber.Util.Debug.Dialogs
{
    /// <summary>This class implements the debug information window.</summary>
    internal partial class __FormDebugInformation: Form
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public __FormDebugInformation(): this(null)
        {}



        public __FormDebugInformation(string text)
        {
            InitializeComponent();

            this.Localize();

            DebugText = text;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Shows the debug information dialog.</summary>
        /// <param name="text">Debug text.</param>
        /// <returns>Dialog result.</returns>
        public static DialogResult ShowDialog(string text)
        {
            __FormDebugInformation f = new __FormDebugInformation(text);

            return f.ShowDialog();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the debug text.</summary>
        public string DebugText
        {
            get { return _TextDebug.Text; }
            set
            {
                _TextDebug.Text = value;
                _TextDebug.Select(0, 0);
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Link "Copy to clipboard" click.</summary>
        private void _LinkCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(_TextDebug.Text.Replace("\r\n", "\n").Replace("\n", "\r\n"));
        }
    }
}
