﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;



namespace Robbiblubber.Util.ZUnitTests
{
    /// <summary>This class implements tests for the MathOp extension class.</summary>
    [TestFixture]
    internal static class MathOpTests
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // test methods                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Unit test method for MathOp.Power().</summary>
        [Test]
        public static void Test_Power()
        {
            Assert.AreEqual(25L, MathOp.Power(5, 2));
            Assert.AreEqual(25L, MathOp.Power(5L, 2));
        }


        /// <summary>Unit test method for MathOp.GCF().</summary>
        [Test]
        public static void Test_GCF()
        {
            Assert.AreEqual(5, MathOp.GCF(125, 15));
            Assert.AreEqual(5L, MathOp.GCF(125L, 15L));
        }


        /// <summary>Unit test method for MathOp.LCM().</summary>
        [Test]
        public static void Test_LCM()
        {
            Assert.AreEqual(375L, MathOp.LCM(125, 15));
            Assert.AreEqual(375L, MathOp.LCM(125L, 15L));
        }
    }
}
