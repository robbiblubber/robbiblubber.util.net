﻿using System;
using System.Text;

using NUnit.Framework;

using Robbiblubber.Util.Coding;



namespace Robbiblubber.Util.ZUnitTests
{
    /// <summary>This class implements standard coding tests.</summary>
    [TestFixture]
    internal static class EncoderTests
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Default test string.</summary>
        private const string _TEST_STR = "Call me Ishmael. Some years ago — never mind how long precisely — having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world.";

        /// <summary>Default test integer.</summary>
        private const int _TEST_INT = 187325;

        /// <summary>Default test double.</summary>
        private const double _TEST_DBL = 3.141592;

        /// <summary>Test result for Base62.</summary>
        private const string _RESULT_BASE62 = "4zPnMdhZxvZNlkihATjjEXSdmybYAgQgrJ5kFyC2KNsLzdLzcZt4sQ4e06H12HmPVM5qcQS2HFdVrJwlqxsZnMqMGeBOcSLXMSIsthfbCrdAqOleZBS8Uut9oYrnxXzDvdSACqCNSW1Udypeze02xsjFRpwhwPTKQEVAotaypHvhuS5vDrlAHUnRcRbtvRpnSv6D1bMlVVHwGc9jB8zq1u6FxsagT4elPeCDJ5RigApcyp121KfOta7Eu5wqB9iG8EpJdFI3g1OrSfzpWxbvr78Q9duXpTxnVBd2EVIiZKuzipfEzo0idXXa";

        /// <summary>Test result for inverted Base62.</summary>
        private const string _RESULT_INVBASE62 = "4ZpNmDHzXVznLKIHatJJexsDMYByaGqGRj5KfYc2knSlZDlZCzT4Sq4E06h12hMpvm5QCqs2hfDvRjWLQXSzNmQmgEboCslxmsiSTHFBcRDaQoLEzbs8uUT9OyRNXxZdVDsacQcnsw1uDYPEZE02XSJfrPWHWptkqevaOTAYPhVHUs5VdRLahuNrCrBTVrPNsV6d1BmLvvhWgC9Jb8ZQ1U6fXSAGt4ELpEcdj5rIGaPCYP121kFoTA7eU5WQb9Ig8ePjDfi3G1oRsFZPwXBVR78q9DUxPtXNvbD2eviIzkUZIPFeZO0IDxxA";

        /// <summary>Test result for Base64.</summary>
        private const string _RESULT_BASE64 = "Q2FsbCBtZSBJc2htYWVsLiBTb21lIHllYXJzIGFnbyDigJQgbmV2ZXIgbWluZCBob3cgbG9uZyBwcmVjaXNlbHkg4oCUIGhhdmluZyBsaXR0bGUgb3Igbm8gbW9uZXkgaW4gbXkgcHVyc2UsIGFuZCBub3RoaW5nIHBhcnRpY3VsYXIgdG8gaW50ZXJlc3QgbWUgb24gc2hvcmUsIEkgdGhvdWdodCBJIHdvdWxkIHNhaWwgYWJvdXQgYSBsaXR0bGUgYW5kIHNlZSB0aGUgd2F0ZXJ5IHBhcnQgb2YgdGhlIHdvcmxkLg";

        /// <summary>Test result for Hex encoder.</summary>
        private const string _RESULT_HEX = "43616c6c206d65204973686d61656c2e20536f6d652079656172732061676f20e28094206e65766572206d696e6420686f77206c6f6e6720707265636973656c7920e2809420686176696e67206c6974746c65206f72206e6f206d6f6e657920696e206d792070757273652c20616e64206e6f7468696e6720706172746963756c617220746f20696e746572657374206d65206f6e2073686f72652c20492074686f75676874204920776f756c64207361696c2061626f75742061206c6974746c6520616e642073656520746865207761746572792070617274206f662074686520776f726c642e";

        /// <summary>Test result for GZip (Base62).</summary>
        private const string _RESULT_GZIP = "EjA1dQbexlCYzClbs1IdG7Kuk3pohwo7GqoP1UbFIJZprEa43ytmTKRxLVWqcFmWxpGBpuqXMVxvKPHdalWSZRYrgg1uQuMKzYJnbdEX8Iop3FTXWtiizUmZLZwv7Gj3n2IPqAzsmYjqKJg5ax12DrOhsIWL5XuHNTxx0UBnoZlcxrjVX2Ne8NTtik3CkgBMuvyuTAC5C1J4Dyypm6rKUIlbhTqiTniaisIDi0vIf1NC7FI";

        /// <summary>Test result for Funnelcake.</summary>
        private const string _RESULT_FUNNELCAKE = "PRNSOX1cKA90GScJQUVcGk5zPCIOTA8ROkkVcC8hAlK5uf5yKEomIAAJDxhHJwEBKit0IlIXU1YZOTMnIUhZTB5iisrIZUtUChAcEF0IShAmEREPQhhOQDxRASU5SQURGyEETQ0ZSUYRLQNpXSohXGQ3TzkSQ00WRzUyKxk7WzMZJCNQUEdSISlSFBY7IkVbPSxiWFVlUCYZE1xidS0BTTwIRQs8EAp5TUAsNS0gDSw4MUMDURYFNABhIXwqEQEuAEteXEdYATQzXBQvA15DWloKEzoTeRNAOBZlGAV9QS8+YCA5AhEpBw";

        /// <summary>Test result for MD5 (string).</summary>
        private const string _RESULT_STR_MD5 = "015d27bc5f566f6a8345cb740bed8eae";

        /// <summary>Test result for MD5 (int).</summary>
        private const string _RESULT_INT_MD5 = "d381fd18fd1287e8c7219aa7d4627d49";

        /// <summary>Test result for SHA1 (string).</summary>
        private const string _RESULT_STR_SHA1 = "2f62a506c865665e02908f9add501653f609ae10";

        /// <summary>Test result for SHA1 (int).</summary>
        private const string _RESULT_INT_SHA1 = "2131cb1de9749b06b345103946b138fdfc7d88b6";

        /// <summary>Test result for SHA1 (string).</summary>
        private const string _RESULT_STR_SHA256 = "b36430aee8b3ad8f545c336f3b4840fef48bc9dee4ea3433c88cb638192b197f";

        /// <summary>Test result for SHA1 (int).</summary>
        private const string _RESULT_INT_SHA256 = "8648a7006802632cf1f6b9803071d4873fd375cf6864b1641629b7e72fcbc55f";

        /// <summary>Test result for SHA1 (string).</summary>
        private const string _RESULT_STR_SHA512 = "72bd1594de23adbbda8ad46945e4a8e61e1fa3fa8268f2b411df36d8a396a16e9880ed7373e2f8532cb637cd33130d83236e7096c8fd0fa51374e6588c1549ed";

        /// <summary>Test result for SHA1 (int).</summary>
        private const string _RESULT_INT_SHA512 = "3aa0fa43ecaa0e2687809f1b49931ef7be9fa1ec3b525a58df04f60690ebed292b9ae0b1278cb72607dfddc733867bb0eabdd6763b82d126be98a81111d7dc66";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // test methods                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Unit test method for Base62.</summary>
        [Test]
        public static void Test_Base62()
        {
            _TestEncoder(Base62.Instance, _RESULT_BASE62);
        }


        /// <summary>Unit test method for inverted Base62.</summary>
        [Test]
        public static void Test_InvertedBase62()
        {
            _TestEncoder(InvertedBase62.Instance, _RESULT_INVBASE62);
        }


        /// <summary>Unit test method for Base64.</summary>
        [Test]
        public static void Test_Base64()
        {
            _TestEncoder(Base64.Instance, _RESULT_BASE64);
        }


        /// <summary>Unit test method for Hex encoder.</summary>
        [Test]
        public static void Test_Hex()
        {
            _TestEncoder(Hex.Instance, _RESULT_HEX);
        }


        /// <summary>Unit test method for plain text encoder.</summary>
        [Test]
        public static void Test_PlainText()
        {
            _TestEncoder(PlainText.Instance, _TEST_STR);
        }


        /// <summary>Unit test method for GZip encoder (with Base62).</summary>
        [Test]
        public static void Test_GZip()
        {
            _TestEncoder(GZip.BASE62, _RESULT_GZIP);
        }


        /// <summary>Unit test method for Funnelcake encoder (with Base64).</summary>
        [Test]
        public static void Test_FunnelCake()
        {
            _TestEncoder(FunnelCake.BASE64, _RESULT_FUNNELCAKE);
        }


        /// <summary>Unit test method for MD5.</summary>
        [Test]
        public static void Test_MD5()
        {
            Assert.AreEqual(_RESULT_STR_MD5, MD5.HEX.HashString(_TEST_STR));
            Assert.AreEqual(_RESULT_INT_MD5, MD5.HEX.HashInt(_TEST_INT));
        }


        /// <summary>Unit test method for SHA1.</summary>
        [Test]
        public static void Test_SHA1()
        {
            Assert.AreEqual(_RESULT_STR_SHA1, SHA1.HEX.HashString(_TEST_STR));
            Assert.AreEqual(_RESULT_INT_SHA1, SHA1.HEX.HashInt(_TEST_INT));
        }


        /// <summary>Unit test method for SHA1.</summary>
        [Test]
        public static void Test_SHA256()
        {
            Assert.AreEqual(_RESULT_STR_SHA256, SHA256.HEX.HashString(_TEST_STR));
            Assert.AreEqual(_RESULT_INT_SHA256, SHA256.HEX.HashInt(_TEST_INT));
        }


        /// <summary>Unit test method for SHA512.</summary>
        [Test]
        public static void Test_SHA512()
        {
            Assert.AreEqual(_RESULT_STR_SHA512, SHA512.HEX.HashString(_TEST_STR));
            Assert.AreEqual(_RESULT_INT_SHA512, SHA512.HEX.HashInt(_TEST_INT));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Performs a test on an encoder.</summary>
        /// <param name="encoder">Encoder.</param>
        /// <param name="result">Expected result.</param>
        private static void _TestEncoder(IEncoder encoder, string result)
        {
            string resstr = encoder.EncodeString(_TEST_STR);                    // test string encoding
            Assert.AreEqual(result, resstr);
            Assert.AreEqual(_TEST_STR, encoder.DecodeToString(resstr));

            byte[] bytes = Encoding.UTF8.GetBytes(_TEST_STR);                   // test byte encoding
            byte[] resbytes = Encoding.UTF8.GetBytes(result);
            
            Assert.AreEqual(resbytes, encoder.Encode(bytes));
            Assert.AreEqual(result, encoder.EncodeBytes(bytes));
            Assert.AreEqual(bytes, encoder.Decode(resbytes));
                                                                                // test numeric encoding
            Assert.AreEqual(_TEST_INT, encoder.DecodeToInt(encoder.EncodeInt(_TEST_INT)));
            Assert.AreEqual((long) _TEST_INT, encoder.DecodeToLong(encoder.EncodeLong((long) _TEST_INT)));
            Assert.AreEqual(_TEST_DBL, encoder.DecodeToDouble(encoder.EncodeDouble(_TEST_DBL)));
        }
    }
}
