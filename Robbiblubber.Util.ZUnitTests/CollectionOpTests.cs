﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;
using Robbiblubber.Util.Collections;



namespace Robbiblubber.Util.ZUnitTests
{
    /// <summary>This class implements tests for the CollectionOp extension class.</summary>
    [TestFixture]
    internal static class CollectionOpTests
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Test array.</summary>
        private static readonly string[] _ARRAY = new string[] {"a", "b", "c", "d", "e", "f", "g"};

        /// <summary>Test list.</summary>
        private static readonly List<string> _LIST = new List<string>(_ARRAY);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // test methods                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Unit test method for CollectionOp.Contains().</summary>
        [Test]
        public static void Test_Contains()
        {
            Assert.IsTrue(_ARRAY.Contains("c"));
            Assert.IsFalse(_ARRAY.Contains("C"));
        }


        /// <summary>Unit test method for CollectionOp.AddItems().</summary>
        [Test]
        public static void Test_AddItems()
        {
            List<string> t = new List<string>(_ARRAY);
            t.AddItems("x", "y", "z");

            Assert.IsTrue(t.Contains("y"));
        }


        /// <summary>Unit test method for CollectionOp.GetIndex().</summary>
        [Test]
        public static void Test_GetIndex()
        {
            Assert.AreEqual(1, _ARRAY.GetIndex("b"));
        }


        /// <summary>Unit test method for CollectionOp.AsList().</summary>
        [Test]
        public static void Test_AsList()
        {
            ImmutableList<string> iml = new ImmutableList<string>(_ARRAY);
            IList<string> lst = iml.AsList();

            Assert.AreEqual(_ARRAY.Length, lst.Count);

            for(int i = 0; i < _ARRAY.Length; i++)
            {
                Assert.AreEqual(_ARRAY[i], lst[i]);
            }
        }
    }
}
