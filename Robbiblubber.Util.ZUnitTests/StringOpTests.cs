﻿using System;

using NUnit.Framework;



namespace Robbiblubber.Util.ZUnitTests
{
    /// <summary>This class implements tests for the StringOp extension class.</summary>
    [TestFixture]
    internal static class StringOpTests
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // test methods                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Unit test method for StringOp.ToString().</summary>
        [Test]
        public static void Test_ToString()
        {
            Assert.AreEqual("1ho", 1592L.ToString(32));
            Assert.AreEqual("1ho", 1592.ToString(32));
        }


        /// <summary>Unit test method for StringOp.ToInt64().</summary>
        [Test]
        public static void Test_ToInt64()
        {
            Assert.AreEqual(1592L, "1ho".ToInt64(32));
        }


        /// <summary>Unit test method for StringOp.ToInt32().</summary>
        [Test]
        public static void Test_ToInt32()
        {
            Assert.AreEqual(1592, "1ho".ToInt32(32));
        }


        /// <summary>Unit test method for StringOp.ToInt16().</summary>
        [Test]
        public static void Test_ToInt16()
        {
            Assert.AreEqual(1592, "1ho".ToInt16(32));
        }


        /// <summary>Unit test method for StringOp.Before().</summary>
        [Test]
        public static void Test_Before()
        {
            Assert.AreEqual("AAAA", "AAAA/BBBB/CCCC".Before("/"));
        }


        /// <summary>Unit test method for StringOp.After().</summary>
        [Test]
        public static void Test_After()
        {
            Assert.AreEqual("BBBB/CCCC", "AAAA/BBBB/CCCC".After("/"));
        }


        /// <summary>Unit test method for StringOp.MatchesFromStart().</summary>
        [Test]
        public static void Test_MatchesFromStart()
        {
            Assert.AreEqual(6, "GOGOGOLIONFOX".MatchesFromStart("GOGOGOSUMMER"));
            Assert.AreEqual(6, "GOGOGOLIONFOX".MatchesFromStart("gogogoSummer", false));
        }


        /// <summary>Unit test method for StringOp.Matches().</summary>
        [Test]
        public static void Test_Matches()
        {
            Assert.IsTrue("GOGOGOLIONFOX".Matches("GOGO*", "*FOX"));
            Assert.IsFalse("GOGOGOLIONFOX".Matches("gogo", "FOX", "FOX*"));
            Assert.IsTrue("GOGOGOLIONFOX".Matches(false, "*ion*"));
        }


        /// <summary>Unit test method for StringOp.StrLen().</summary>
        [Test]
        public static void Test_StrLen()
        {
            string[] str = new string[] { "22", "333", "4444", "55555" };
            Assert.AreEqual(5, str.StrLen());
        }


        /// <summary>Unit test method for StringOp.ReplaceAll().</summary>
        [Test]
        public static void Test_ReplaceAll()
        {
            Assert.AreEqual("b*b*b*b*b*", "bxbXbxbxbX".ReplaceAll("x", "*", false));
        }


        /// <summary>Unit test method for StringOp.ReplaceFirst().</summary>
        [Test]
        public static void Test_ReplaceFirst()
        {
            Assert.AreEqual("bxb*bxbxbX", "bxbXbxbxbX".ReplaceFirst("X", "*"));
            Assert.AreEqual("b*bXbxbxbX", "bxbXbxbxbX".ReplaceFirst("X", "*", false));
        }


        /// <summary>Unit test method for StringOp.Complies().</summary>
        [Test]
        public static void Test_Complies()
        {
            Assert.IsFalse("A tortoise's hat with a feather.".Complies("Aabcdefghijklmnopqrstuvwxyz "));
            Assert.IsTrue("A tortoise's hat with a feather.".Complies("Aabcdefghijklmnopqrstuvwxyz '."));
        }


        /// <summary>Unit test method for StringOp.GetChars().</summary>
        [Test]
        public static void Test_GetChars()
        {
            Assert.AreEqual(" '.Aaefhiorstw", new string("A tortoise's hat with a feather.".GetChars()));
        }


        /// <summary>Unit test method for StringOp.ReplaceStart().</summary>
        [Test]
        public static void Test_ReplaceStart()
        {
            Assert.AreEqual("PaxPaxdodoRacoon", "DoDododoRacoon".ReplaceStart("Do", "Pax"));
            Assert.AreEqual("PaxPaxPaxPaxRacoon", "DoDododoRacoon".ReplaceStart("Do", "Pax", false));
        }


        /// <summary>Unit test method for StringOp.ReplaceEnd().</summary>
        [Test]
        public static void Test_ReplaceEnd()
        {
            Assert.AreEqual("RacoondodoPaxPax", "RacoondodoDoDo".ReplaceEnd("Do", "Pax"));
            Assert.AreEqual("RacoonPaxPaxPaxPax", "RacoondodoDoDo".ReplaceEnd("Do", "Pax", false));
        }


        /// <summary>Unit test method for StringOp.Snip().</summary>
        [Test]
        public static void Test_Snip()
        {
            Assert.AreEqual(" 3, 4, 5, 6", "1, 2, 3, 4, 5, 6, 7, 8, 9".Snip(2, 6, ','));
            Assert.AreEqual(" 3/ 4, 5/ 6", "1/ 2, 3/ 4, 5/ 6, 7/ 8, 9".Snip(2, 6, ',', '/'));
        }


        /// <summary>Unit test method for StringOp.Chain().</summary>
        [Test]
        public static void Test_Chain()
        {
            string[] test = new string[] { "1", "2", "3", "4", "5" };
            Assert.AreEqual("1, 2, 3, 4, 5", test.Chain(", "));
        }
    }
}
