﻿using System;

using NUnit.Framework;



namespace Robbiblubber.Util.ZUnitTests
{
    /// <summary>This class implements tests for the CalendarOp extension class.</summary>
    [TestFixture]
    internal static class CalenderOpTests
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Test time.</summary>
        private static readonly DateTime _TEST_TIME = new DateTime(2000, 11, 21, 15, 22, 41, DateTimeKind.Utc);

        /// <summary>Unix timestamp result.</summary>
        private const long _RESULT_UNIX = 974820161L;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // test methods                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Unit test method for StringOp.ToTimestamp().</summary>
        [Test]
        public static void Test_ToTimestamp()
        {
            Assert.AreEqual("2000-11-21 15:22:41.617+00:00", new DateTime(2000, 11, 21, 15, 22, 41, 617, DateTimeKind.Utc).ToTimestamp());
            Assert.AreEqual("2000-11-21 15:22:41.005+00:00", new DateTime(2000, 11, 21, 15, 22, 41, 5, DateTimeKind.Utc).ToTimestamp());
            Assert.AreEqual("2000-11-21 15:22:41.000+00:00", new DateTime(2000, 11, 21, 15, 22, 41, DateTimeKind.Utc).ToTimestamp());
        }


        /// <summary>Unit test method for StringOp.ParseTimestamp().</summary>
        [Test]
        public static void Test_ParseTimestamp()
        {
            Assert.AreEqual(new DateTime(2000, 11, 21, 15, 22, 41, 5, DateTimeKind.Utc), "2000-11-21 15:22:41.005+00:00".ParseTimestamp(true));
            Assert.AreEqual(new DateTime(2000, 11, 21, 15, 22, 41, 0, DateTimeKind.Utc).ToLocalTime(), "2000-11-21 15:22:41".ParseTimestamp());
        }


        /// <summary>Unit test method for CalendarOp.ToUnixTimestamp().</summary>
        [Test]
        public static void Test_ToUnixTimestamp()
        {
            long result = CalendarOp.ToUnixTimestamp(_TEST_TIME);

            Assert.AreEqual(_RESULT_UNIX, result);
            Assert.AreEqual(_TEST_TIME, CalendarOp.FromUnixTimestamp(result).ToUniversalTime());
        }


        /// <summary>Unit test method for CalendarOp.ToUnixTimestampMilliseconds().</summary>
        [Test]
        public static void Test_ToUnixTimestampMs()
        {
            DateTime testTime = _TEST_TIME.AddMilliseconds(617);
            long resultUnix = ((_RESULT_UNIX * 1000) + 617);

            long result = CalendarOp.ToUnixTimestampMilliseconds(testTime);

            Assert.AreEqual(resultUnix, result);
            Assert.AreEqual(testTime, CalendarOp.FromUnixTimestampMilliseconds(result).ToUniversalTime());
        }
    }
}
