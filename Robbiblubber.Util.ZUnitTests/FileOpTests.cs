﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;



namespace Robbiblubber.Util.ZUnitTests
{
    /// <summary>This class implements tests for the FileOp extension class.</summary>
    [TestFixture]
    internal static class FileOpTests
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Default test string.</summary>
        private const string _TEST_STR = "Call me Ishmael. Some years ago — never mind how long precisely — having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world.";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Test directory.</summary>
        private static string _TestDir = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // setup methods                                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates the test environment.</summary>
        [OneTimeSetUp]
        public static void SetUp()
        {
            _TestDir = PathOp.TempRoot + @"\" + StringOp.Unique();
            Directory.CreateDirectory(_TestDir);
        }


        /// <summary>Removes the test environment.</summary>
        [OneTimeTearDown]
        public static void TearDown()
        {
            Directory.Delete(_TestDir, true);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // test methods                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Unit test method for FileOp.CopyFile().</summary>
        [Test]
        public static void Test_CopyFile()
        {
            string doomed = "Doomed text.";
            File.WriteAllText(_TestDir + @"\a.txt", doomed);
            File.WriteAllText(_TestDir + @"\b.txt", _TEST_STR);

            FileOp.CopyFile(_TestDir + @"\b.txt", _TestDir + @"\a.txt", true);

            Assert.AreEqual(_TEST_STR, File.ReadAllText(_TestDir + @"\a.txt"));

            File.Delete(_TestDir + @"\a.txt");
            File.Delete(_TestDir + @"\b.txt");
        }


        /// <summary>Unit test method for FileOp.TryCopyFile().</summary>
        [Test]
        public static void Test_TryCopyFile()
        {
            string doomed = "Doomed text.";
            File.WriteAllText(_TestDir + @"\a.txt", doomed);
            File.WriteAllText(_TestDir + @"\b.txt", _TEST_STR);

            Assert.IsFalse(FileOp.TryCopyFile(_TestDir + @"\b.txt", _TestDir + @"\a.txt", false));
            Assert.AreEqual(doomed, File.ReadAllText(_TestDir + @"\a.txt"));

            Assert.IsTrue(FileOp.TryCopyFile(_TestDir + @"\b.txt", _TestDir + @"\a.txt"));
            Assert.AreEqual(_TEST_STR, File.ReadAllText(_TestDir + @"\a.txt"));

            File.Delete(_TestDir + @"\a.txt");
            File.Delete(_TestDir + @"\b.txt");
        }


        /// <summary>Unit test method for FileOp.Delete().</summary>
        [Test]
        public static void Test_Delete()
        {
            Directory.CreateDirectory(_TestDir + @"\del");
            Directory.CreateDirectory(_TestDir + @"\del\sub");

            File.WriteAllText(_TestDir + @"\del\a.txt", "...");
            File.WriteAllText(_TestDir + @"\del\b.txt", "...");
            File.WriteAllText(_TestDir + @"\del\a.keep", "...");
            File.WriteAllText(_TestDir + @"\del\sub\c.txt", "...");
            File.WriteAllText(_TestDir + @"\del\sub\c.keep", "...");

            FileOp.Delete(_TestDir + @"\del\*.txt", true);

            Assert.IsFalse(File.Exists(_TestDir + @"\del\sub\c.txt"));
            Assert.IsTrue(File.Exists(_TestDir + @"\del\sub\c.keep"));
            Assert.IsFalse(File.Exists(_TestDir + @"\del\a.txt"));
            Assert.IsFalse(File.Exists(_TestDir + @"\del\b.txt"));
            Assert.IsTrue(File.Exists(_TestDir + @"\del\a.keep"));

            Directory.Delete(_TestDir + @"\del", true);
        }


        /// <summary>Unit test method for FileOp.CopyDirectory().</summary>
        [Test]
        public static void Test_CopyDirectory()
        {
            Directory.CreateDirectory(_TestDir + @"\del");
            Directory.CreateDirectory(_TestDir + @"\del\sub");
            Directory.CreateDirectory(_TestDir + @"\cpy");
            Directory.CreateDirectory(_TestDir + @"\cpy\old");

            File.WriteAllText(_TestDir + @"\del\a.txt", "...");
            File.WriteAllText(_TestDir + @"\del\b.txt", "...");
            File.WriteAllText(_TestDir + @"\del\a.keep", "...");
            File.WriteAllText(_TestDir + @"\del\sub\c.txt", "...");
            File.WriteAllText(_TestDir + @"\del\sub\c.keep", "...");

            FileOp.CopyDirectory(_TestDir + @"\del", _TestDir + @"\cpy");

            Assert.IsTrue(File.Exists(_TestDir + @"\cpy\sub\c.keep"));
            Assert.IsTrue(Directory.Exists(_TestDir + @"\cpy\old"));

            Directory.Delete(_TestDir + @"\del", true);
            Directory.Delete(_TestDir + @"\cpy", true);
        }


        /// <summary>Unit test method for FileOp.GetRelativePath().</summary>
        [Test]
        public static void Test_GetRelativePath()
        {
            Directory.CreateDirectory(_TestDir + @"\del");
            Directory.CreateDirectory(_TestDir + @"\del\sub");
            Directory.CreateDirectory(_TestDir + @"\cpy");
            Directory.CreateDirectory(_TestDir + @"\cpy\old");

            File.WriteAllText(_TestDir + @"\del\sub\c.txt", "...");
            File.WriteAllText(_TestDir + @"\cpy\old\c.keep", "...");

            FileOp.CopyDirectory(_TestDir + @"\del", _TestDir + @"\cpy");

            string rel = FileOp.GetRelativePath(_TestDir + @"\del\sub", _TestDir + @"\cpy\old\c.keep");
            Assert.AreEqual(@"..\..\cpy\old\c.keep", rel);
            Assert.IsTrue(File.Exists(_TestDir + @"\del\sub\" + rel));

            Directory.Delete(_TestDir + @"\del", true);
            Directory.Delete(_TestDir + @"\cpy", true);
        }
    }
}
