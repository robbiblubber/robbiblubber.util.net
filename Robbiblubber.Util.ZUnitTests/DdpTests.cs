﻿using System;
using System.IO;

using NUnit.Framework;



namespace Robbiblubber.Util.ZUnitTests
{
    /// <summary>This class implements tests for ddp.</summary>
    [TestFixture]
    internal static class DdpTests
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Test directory.</summary>
        private static string _TestDir = null;

        /// <summary>Test data.</summary>
        private static readonly string _TestData = "a = 0; b = top;\r\nsub1 {\r\n_a = 1;\r\nb=sub1;\r\n}\r\n\r\nsub2 { a = 2; b = sub2; }\r\n";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // setup methods                                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates the test environment.</summary>
        [OneTimeSetUp]
        public static void SetUp()
        {
            _TestDir = PathOp.TempRoot + @"\" + StringOp.Unique();
            Directory.CreateDirectory(_TestDir);
        }


        /// <summary>Removes the test environment.</summary>
        [OneTimeTearDown]
        public static void TearDown()
        {
            Directory.Delete(_TestDir, true);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // test methods                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Unit test method for Ddp class.</summary>
        [Test]
        public static void Test_NewDdp()
        {
            Ddp ddp = new Ddp();
            ddp["a"].Value = "alpha";
            ddp["sub/b"].Value = "beta";

            Assert.AreEqual("sub", ddp["sub"].Name);
            Assert.AreEqual("alpha", (string) ddp["a"]);
            Assert.AreEqual("beta", (string) ddp["sub/b"]);
            Assert.AreEqual("beta", (string) ddp["sub"]["b"]);

            ddp.Save(_TestDir + @"\test.ddp");

            ddp = new Ddp(_TestDir + @"\test.ddp");

            Assert.AreEqual("sub", ddp["sub"].Name);
            Assert.AreEqual("alpha", (string) ddp["a"]);
            Assert.AreEqual("beta", (string) ddp["sub/b"]);
            Assert.AreEqual("beta", (string) ddp["sub"]["b"]);

            File.Delete(_TestDir + @"\test.ddp");
        }


        /// <summary>Unit test method for DdpString and DdpFile classes.</summary>
        [Test]
        public static void Test_DdpStringFile()
        {
            Ddp ddp = new Ddp(_TestData);
            Assert.AreEqual(2, (int) ddp["sub2/a"]);
            DateTime dt = new DateTime(2000, 11, 21, 15, 22, 41, 617).ToUniversalTime();

            ddp.Save(_TestDir + @"\test.ddp");
            Ddp dd2 = Ddp.Open(_TestDir + @"\test.ddp");
            Assert.AreEqual("sub1", (string) dd2["sub1/b"]);

            dd2["sub3/test"].Value = new string[] { "x", "y", "z" };
            dd2["dt"].Value = dt;
            dd2.Save();

            Assert.AreEqual("z", ((string[]) dd2["sub3/test"])[2]);
            Assert.AreEqual(dt, ((DateTime) dd2["dt"]).ToUniversalTime());
        }
    }
}
