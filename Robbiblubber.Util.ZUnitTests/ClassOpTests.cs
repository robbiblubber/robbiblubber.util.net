﻿using System;

using NUnit.Framework;



namespace Robbiblubber.Util.ZUnitTests
{
    /// <summary>This class implements tests for the ClassOp extension class.</summary>
    [TestFixture]
    internal static class ClassOpTests
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // test methods                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Unit test method for ClassOp.LoadType().</summary>
        [Test]
        public static void Test_LoadType()
        {
            Assert.AreEqual(typeof(string), ClassOp.LoadType("System.String"));
        }


        /// <summary>Unit test method for ClassOp.Load().</summary>
        [Test]
        public static void Test_Load()
        {
            string test = "TEST";
            string result = (string) ClassOp.Load("System.String", test.ToCharArray());
            
            Assert.AreEqual(test, result);
        }


        /// <summary>Unit test method for ClassOp.GetSubtypesOf().</summary>
        [Test]
        public static void Test_GetSubtypesOf()
        {
            Assert.AreEqual(2, ClassOp.GetSubtypesOf(typeof(DdpElement)).Length);
        }
    }
}
