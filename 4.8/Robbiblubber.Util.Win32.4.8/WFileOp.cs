﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;

using Microsoft.Win32;



namespace Robbiblubber.Util.Win32
{
    /// <summary>This class provides Windows-specific file utility methods.</summary>
    /// <remarks>4.8 version.</remarks>
    public static class WFileOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [import]                                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Imported Win32 API function.</summary>
        /// <param name="wEventId">Event ID.</param>
        /// <param name="uFlags">Flags.</param>
        /// <param name="dwItem1">Item 1.</param>
        /// <param name="dwItem2">Item 2.</param>
        [DllImport("shell32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern void SHChangeNotify(uint wEventId, uint uFlags, IntPtr dwItem1, IntPtr dwItem2);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a file association.</summary>
        /// <param name="extension">File extension.</param>
        /// <param name="application">Application associated with the extension.</param>
        /// <param name="description">File type description.</param>
        /// <param name="icon">Icon path.</param>
        /// <param name="iconIndex">Icon index.</param>
        public static void CreateAssociation(string extension, string application, string description, string icon = null, int iconIndex = 0)
        {
            string keyName = Path.GetFileName(application).Substring(0, Path.GetFileName(application).LastIndexOf('.')) + '_' + extension;

            CreateAssociation(extension, keyName, application, description, icon, iconIndex);
        }



        /// <summary>Creates a file association.</summary>
        /// <param name="extension">File extension.</param>
        /// <param name="keyName">Key name.</param>
        /// <param name="application">Application associated with the extension.</param>
        /// <param name="description">File type description.</param>
        /// <param name="icon">Icon path.</param>
        /// <param name="iconIndex">Icon index.</param>
        public static void CreateAssociation(string extension, string keyName, string application, string description, string icon = null, int iconIndex = 0)
        {
            RegistryKey baseKey;
            RegistryKey openMethod;
            RegistryKey shell;
            RegistryKey currentUser;

            if(icon == null) { icon = application; }

            baseKey = Registry.ClassesRoot.CreateSubKey(extension);
            baseKey.SetValue("", keyName);

            openMethod = Registry.ClassesRoot.CreateSubKey(keyName);
            openMethod.SetValue("", description);
            openMethod.CreateSubKey("DefaultIcon").SetValue("", "\"" + icon + "\"," + iconIndex.ToString());

            shell = openMethod.CreateSubKey("Shell");
            shell.CreateSubKey("edit").CreateSubKey("command").SetValue("", "\"" + application + "\"" + " \"%1\"");
            shell.CreateSubKey("open").CreateSubKey("command").SetValue("", "\"" + application + "\"" + " \"%1\"");

            Disposal.Recycle(baseKey, openMethod, shell);

            currentUser = Registry.CurrentUser.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Explorer\FileExts\" + extension, true);

            if(currentUser != null)
            {
                currentUser.DeleteSubKey("UserChoice", false);
                currentUser.Close();
            }
            SHChangeNotify(0x08000000, 0x0000, IntPtr.Zero, IntPtr.Zero);
        }


        /// <summary>Creates a file link.</summary>
        /// <param name="fileName">File name.</param>
        /// <param name="path">Target path.</param>
        /// <param name="args">Arguments.</param>
        /// <param name="description">Description.</param>
        /// <param name="icon">Icon location.</param>
        /// <param name="iconIndex">Icon index.</param>
        /// <returns></returns>
        public static void CreateLink(string fileName, string path, string args, string description, string icon = null, int iconIndex = 0)
        {
            IWshRuntimeLibrary.WshShell wsh = new IWshRuntimeLibrary.WshShell();

            if(icon == null) { icon = path; }

            if(File.Exists(fileName)) { File.Delete(fileName); }

            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)wsh.CreateShortcut(fileName);
            shortcut.TargetPath = path;
            shortcut.Arguments = args;
            shortcut.Description = description;
            shortcut.WorkingDirectory = Path.GetDirectoryName(path);
            shortcut.IconLocation = icon + "," + iconIndex;
            shortcut.WindowStyle = 1;
            shortcut.Save();
        }
    }
}
