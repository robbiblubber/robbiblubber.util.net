﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;



namespace Robbiblubber.Util.Concurrency
{
    /// <summary>This class represents a compartment.</summary>
    public sealed class Compartment: IDisposable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compartment dictionary by name.</summary>
        private static Dictionary<string, Compartment> _Items = new Dictionary<string, Compartment>();

        /// <summary>Compartment dictionary by thread.</summary>
        private static Dictionary<int, List<Compartment>> _Threads = new Dictionary<int, List<Compartment>>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="name">Compartment name.</param>
        private Compartment(string name)
        {
            Name = name;
            _Items.Add(name, this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a joined compartment for the current thread.</summary>
        /// <remarks>Returns NULL if the current thread has not joined a compartment.</remarks>
        public static Compartment Any
        {
            get
            {
                if(_Threads.ContainsKey(Thread.CurrentThread.ManagedThreadId))
                {
                    if(_Threads[Thread.CurrentThread.ManagedThreadId].Count > 0) { return _Threads[Thread.CurrentThread.ManagedThreadId][0]; }
                }
                return null;
            }
        }


        /// <summary>Gets the current compartment. If the current thread has not joined a compartment, a new compartment will be created.</summary>
        public static Compartment AnyOrNew
        {
            get
            {
                Compartment rval = Any;
                return ((rval == null) ? CreateAndJoin() : rval);
            }
        }


        /// <summary>Gets all compartments for the current thread.</summary>
        public static Compartment[] All
        {
            get
            {
                if(_Threads.ContainsKey(Thread.CurrentThread.ManagedThreadId))
                {
                    return _Threads[Thread.CurrentThread.ManagedThreadId].ToArray();
                }
                return new Compartment[0];
            }
        }


        /// <summary>Gets all compartments for the current thread. If the current thread has not joined a compartment, a new compartment will be created.</summary>
        public static Compartment[] AllOrNew
        {
            get
            {
                Compartment[] rval = All;
                return ((rval.Length == 0) ? new Compartment[] { CreateAndJoin() } : rval);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new compartment.</summary>
        /// <param name="name">Compartment name.</param>
        /// <returns>Compartment.</returns>
        public static Compartment Create(string name = null)
        {
            if(string.IsNullOrEmpty(name))
            {
                string seed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                Random r = new Random();
                name = ":";
                for(int i = 0; i < 6; i++) { name += seed[r.Next(0, 61)]; }
                name += Convert.ToBase64String(BitConverter.GetBytes(DateTime.Now.Ticks)).Trim('=').Replace('+', seed[r.Next(0, 61)]).Replace('/', seed[r.Next(0, 61)]);
            }

            return new Compartment(name);
        }


        /// <summary>Creates a new compartment and joins the current thread.</summary>
        /// <param name="name">Compartment name.</param>
        /// <returns>Compartment.</returns>
        public static Compartment CreateAndJoin(string name = null)
        {
            Compartment rval = Create(name);
            rval.Join();

            return rval;
        }


        /// <summary>Gets a compartment by its name.</summary>
        /// <param name="name">Compartment name.</param>
        /// <returns>Compartment.</returns>
        public Compartment ByName(string name)
        {
            if(_Items.ContainsKey(name)) { return _Items[name]; }
            return null;
        }


        /// <summary>Gets a joined compartment by its name.</summary>
        /// <param name="name">Compartment name.</param>
        /// <returns>Compartment.</returns>
        public Compartment Joined(string name)
        {
            Compartment rval = ByName(name);

            if(rval != null)
            {
                if(All.Contains(rval)) { return rval; }
            }
            return null;
        }


        /// <summary>Cleans up the compartment cache.</summary>
        public void CleanUp()
        {
            List<int> v = new List<int>();

            foreach(int i in _Threads.Keys)
            {
                bool ok = false;
                foreach(Thread j in Process.GetCurrentProcess().Threads)
                {
                    if(j.ManagedThreadId == i) { ok = true; break; }
                }

                if(!ok) { v.Add(i); }
            }

            foreach(int i in v) { _Threads.Remove(i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the apartment name.</summary>
        public string Name { get; private set; }


        /// <summary>Gets or sets compartement data.</summary>
        public object Data { get; set; } = null;


        /// <summary>Gets if the compartment is valid.</summary>
        public bool Valid { get; private set; } = true;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds the current thread to the compartment.</summary>
        public void Join()
        {
            if(!Valid) { throw new ObjectDisposedException(Name); }

            List<Compartment> list;
            if(_Threads.ContainsKey(Thread.CurrentThread.ManagedThreadId))
            {
                list = _Threads[Thread.CurrentThread.ManagedThreadId];
            }
            else
            {
                list = new List<Compartment>();
                _Threads.Add(Thread.CurrentThread.ManagedThreadId, list);
            }

            if(!list.Contains(this)) { list.Add(this); }
        }


        /// <summary>Remove the current thread from the compartment.</summary>
        public void Leave()
        {
            if(!Valid) { throw new ObjectDisposedException(Name); }

            if(_Threads.ContainsKey(Thread.CurrentThread.ManagedThreadId))
            {
                List<Compartment> list = _Threads[Thread.CurrentThread.ManagedThreadId];
                if(list.Contains(this)) { list.Remove(this); }
                if(list.Count == 0)
                {
                    _Threads.Remove(Thread.CurrentThread.ManagedThreadId);
                }
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisposable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Destroys and disposes the compartment.</summary>
        public void Dispose()
        {
            Valid = false;

            if(_Items.ContainsKey(Name)) { _Items.Remove(Name); }

            List<int> v = new List<int>();
            foreach(int i in _Threads.Keys)
            {
                if(_Threads[i].Contains(this)) { _Threads[i].Remove(this); }
            }
        }
    }
}
