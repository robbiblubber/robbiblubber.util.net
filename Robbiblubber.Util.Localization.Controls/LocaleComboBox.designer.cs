﻿namespace Robbiblubber.Util.Localization.Controls
{
    partial class LocaleComboBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LocaleComboBox));
            this._IlistLocales = new System.Windows.Forms.ImageList(this.components);
            this._CboLocales = new Robbiblubber.Util.Controls.RichComboBox();
            this.SuspendLayout();
            // 
            // _IlistLocales
            // 
            this._IlistLocales.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_IlistLocales.ImageStream")));
            this._IlistLocales.TransparentColor = System.Drawing.Color.Transparent;
            this._IlistLocales.Images.SetKeyName(0, "****");
            // 
            // _CboLocales
            // 
            this._CboLocales.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this._CboLocales.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._CboLocales.ImageList = this._IlistLocales;
            this._CboLocales.Location = new System.Drawing.Point(0, 0);
            this._CboLocales.Name = "_CboLocales";
            this._CboLocales.Size = new System.Drawing.Size(265, 26);
            this._CboLocales.TabIndex = 0;
            this._CboLocales.SelectedValueChanged += new System.EventHandler(this._CboLocales_SelectedValueChanged);
            // 
            // LocaleComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._CboLocales);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "LocaleComboBox";
            this.Size = new System.Drawing.Size(267, 27);
            this.Resize += new System.EventHandler(this.LocaleDropDownList_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private Robbiblubber.Util.Controls.RichComboBox _CboLocales;
        private System.Windows.Forms.ImageList _IlistLocales;
    }
}
