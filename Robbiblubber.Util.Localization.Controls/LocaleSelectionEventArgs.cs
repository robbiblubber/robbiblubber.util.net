﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Util.Localization.Controls
{
    /// <summary>This class contains locale selection event data.</summary>
    public class LocaleSelectionEventArgs: EventArgs
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="selectedLocale">Selected locale.</param>
        public LocaleSelectionEventArgs(Locale selectedLocale)
        {
            SelectedLocale = selectedLocale;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the selected locale.</summary>
        public Locale SelectedLocale
        {
            get; protected set;
        }
    }
}
