﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using Robbiblubber.Util.Controls;



namespace Robbiblubber.Util.Localization.Controls
{
    /// <summary>This is a locale selection dropdown list control.</summary>
    public partial class LocaleComboBox: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public events                                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Event is raised when a locale is selected.</summary>
        public event LocaleSelectedEventHandler LocaleSelected;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public LocaleComboBox()
        {
            InitializeComponent();
            
            if(Process.GetCurrentProcess().ProcessName.ToLower() == "devenv")
            {
                Label l = new Label();
                Controls.Add(l);

                l.AutoSize = true;
                l.Text = "English (United States)";
                l.BackColor = SystemColors.Window;
                l.Visible = true;
                l.Location = new Point(26, 4);
                l.BringToFront();

                l = new Label();
                Controls.Add(l);

                l.Text = "";
                l.BackColor = SystemColors.Window;
                l.Image = _IlistLocales.Images["****"];
                l.Location = new Point(6, 4);
                l.Visible = true;
                l.Size = new Size(18, 18);
                l.BringToFront();
            }
            else
            {
                foreach(Locale i in Locale.Locales)
                {
                    try
                    {
                        _IlistLocales.Images.Add(i.Key, (Image) i.SmallIcon);
                        _CboLocales.Items.Add(new RichComboItem(i.Name, i.Key, i));
                    }
                    catch(Exception) {}
                }

                if(Locale.Selected != null)
                {
                    RichComboItem m = _CboLocales.GetItemByTag(Locale.Selected);

                    if(m == null)
                    {
                        if(_CboLocales.Items.Count > 0) { _CboLocales.SelectedIndex = 0; }
                    }
                    else { _CboLocales.SelectedItem = m; }
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the value for this control.</summary>
        public Locale Value
        {
            get
            {
                if(_CboLocales.SelectedItem is RichComboItem) { return ((Locale) ((RichComboItem) _CboLocales.SelectedItem).Tag); }
                return null;
            }
            set
            {
                RichComboItem m = _CboLocales.GetItemByTag(Locale.Selected);

                if(m != null){ _CboLocales.SelectedItem = m; }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Control resize.</summary>
        private void LocaleDropDownList_Resize(object sender, EventArgs e)
        {
            _CboLocales.Width = Width;
        }


        /// <summary>Selected value changed.</summary>
        private void _CboLocales_SelectedValueChanged(object sender, EventArgs e)
        {
            LocaleSelected?.Invoke(this, new LocaleSelectionEventArgs((Locale) ((RichComboItem) _CboLocales.SelectedItem).Tag));
        }
    }
}
