﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;



namespace Robbiblubber.Util.Localization.Controls
{
    /// <summary>This class defines localization extensions.</summary>
    public static class LocaleExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets a control text to a localized name for a key.</summary>
        /// <param name="c">Control.</param>
        /// <param name="t">Tool tip.</param>
        public static void Localize(this Control c, ToolTip t = null)
        {
            try
            {
                LTag tag = ParseTag(c.Tag, c);

                if(!string.IsNullOrEmpty(tag.TextKey))
                {
                    c.Text = Locale.Lookup(tag.TextKey, c.Text);
                }

                if(!string.IsNullOrEmpty(tag.ToolTipKey))
                {
                    if(t == null) { t = GetToolTip(c); }
                    if(t != null) { t.SetToolTip(c, Locale.Lookup(tag.ToolTipKey, t.GetToolTip(c))); }
                }
            }
            catch(Exception) {}

            foreach(Control i in c.Controls)
            {
                Localize(i, t);
            }

            if(c is ToolStrip)
            {
                foreach(ToolStripItem i in ((ToolStrip) c).Items) { Localize(i); }
            }

            if(c is TreeView)
            {
                foreach(TreeNode i in ((TreeView) c).Nodes) { Localize(i); }
            }

            if(c is ListView)
            {
                foreach(ColumnHeader i in ((ListView) c).Columns) { Localize(i); }
            }
        }


        /// <summary>Sets a tool strip item text to a localized name for a key.</summary>
        /// <param name="c">Component.</param>
        public static void Localize(this ToolStripItem c)
        {
            try
            {
                LTag tag = ParseTag(c.Tag, c);

                if(!string.IsNullOrEmpty(tag.TextKey))
                {
                    c.Text = Locale.Lookup(tag.TextKey, c.Text);
                    if(!string.IsNullOrWhiteSpace(tag.ToolTipKey)) { c.ToolTipText = Locale.Lookup(tag.ToolTipKey, c.ToolTipText); }
                }
            }
            catch(Exception) {}

            if(c is ToolStripMenuItem)
            {
                foreach(ToolStripItem i in ((ToolStripMenuItem) c).DropDownItems) { Localize(i); }
            }
        }


        /// <summary>Sets a tree node item text to a localized name for a key.</summary>
        /// <param name="c">Component.</param>
        public static void Localize(this TreeNode c)
        {
            try
            {
                LTag tag = ParseTag(c.Tag, c);

                if(!string.IsNullOrEmpty(tag.TextKey))
                {
                    c.Text = Locale.Lookup(tag.TextKey, c.Text);
                    if(!string.IsNullOrWhiteSpace(tag.ToolTipKey)) { c.ToolTipText = Locale.Lookup(tag.ToolTipKey, c.ToolTipText); }
                }
            }
            catch(Exception) {}

            foreach(TreeNode i in ((TreeNode) c).Nodes) { Localize(i); }
        }


        /// <summary>Sets a tree node item text to a localized name for a key.</summary>
        /// <param name="c">Component.</param>
        public static void Localize(this ColumnHeader c)
        {
            try
            {
                LTag tag = ParseTag(c.Tag, c);

                if(!string.IsNullOrEmpty(tag.TextKey))
                {
                    c.Text = Locale.Lookup(tag.TextKey, c.Text);
                }
            }
            catch(Exception) {}
        }


        /// <summary>Localizes a string.</summary>
        /// <param name="key">Language key.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Localized string.</returns>
        public static string Localize(this string key, string defaultValue = null)
        {
            return Locale.Lookup(key, defaultValue);
        }


        /// <summary>Converts a string to a tag.</summary>
        /// <param name="s">String.</param>
        /// <param name="owner">Owner.</param>
        /// <param name="data">Additional data.</param>
        /// <returns>Tag.</returns>
        public static LTag ToTag(this string s, object owner = null, object data = null)
        {
            if(s.Contains("||"))
            {
                return new LTag(s.Substring(0, s.IndexOf("||")), s.Substring(s.IndexOf("||") + 2), owner, data);
            }
            else { return new LTag(s, null, owner, data); }
        }


        /// <summary>Gets the tag for this object.</summary>
        /// <param name="c">Control.</param>
        /// <returns>Tag.</returns>
        public static LTag GetTag(this Control c)
        {
            return ParseTag(c.Tag, c);
        }


        /// <summary>Gets the tag for this object.</summary>
        /// <param name="c">Component.</param>
        /// <returns>Tag.</returns>
        public static LTag GetTag(this ToolStripItem c)
        {
            return ParseTag(c.Tag, c);
        }


        /// <summary>Gets the tag for this object.</summary>
        /// <param name="c">Component.</param>
        /// <returns>Tag.</returns>
        public static LTag GetTag(this TreeNode c)
        {
            return ParseTag(c.Tag, c);
        }


        /// <summary>Parses a tag object to an LTag instance.</summary>
        /// <param name="tag">Tag object.</param>
        /// <param name="owner">Owner.</param>
        /// <returns>Tag.</returns>
        public static LTag ParseTag(object tag, object owner = null)
        {
            if(tag is LTag) { return (LTag) tag; }
            if(tag is string) { return ToTag((string) tag, owner, null); }

            return new LTag(null, null, owner, tag);
        }


        /// <summary>Gets a tooltip for this control if one exits.</summary>
        /// <param name="c">Control.</param>
        /// <returns>Tooltip.</returns>
        public static ToolTip GetToolTip(this Control c)
        {
            try
            {
                FieldInfo f = c.GetType().GetField("components", BindingFlags.Instance | BindingFlags.NonPublic);

                if(f != null)
                {
                    IContainer cnt = (IContainer) f.GetValue(c);

                    if(cnt != null)
                    {
                        foreach(ToolTip i in cnt.Components.OfType<ToolTip>()) { return i; }
                    }
                }
            }
            catch(Exception) {}

            if(c.Parent != null) { return GetToolTip(c.Parent); }
            return null;
        }
    }
}
