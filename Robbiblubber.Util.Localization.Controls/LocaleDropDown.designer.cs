﻿namespace Robbiblubber.Util.Localization.Controls
{
    partial class LocaleDropDown
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LocaleDropDown));
            this._LabelSelectLocale = new System.Windows.Forms.Label();
            this._LabelLocale = new System.Windows.Forms.Label();
            this._CmenuDropDown = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SuspendLayout();
            // 
            // _LabelSelectLocale
            // 
            this._LabelSelectLocale.Image = ((System.Drawing.Image)(resources.GetObject("_LabelSelectLocale.Image")));
            this._LabelSelectLocale.Location = new System.Drawing.Point(26, 0);
            this._LabelSelectLocale.Name = "_LabelSelectLocale";
            this._LabelSelectLocale.Size = new System.Drawing.Size(12, 28);
            this._LabelSelectLocale.TabIndex = 5;
            this._LabelSelectLocale.Click += new System.EventHandler(this._Click);
            // 
            // _LabelLocale
            // 
            this._LabelLocale.Image = ((System.Drawing.Image)(resources.GetObject("_LabelLocale.Image")));
            this._LabelLocale.Location = new System.Drawing.Point(0, 0);
            this._LabelLocale.Name = "_LabelLocale";
            this._LabelLocale.Size = new System.Drawing.Size(28, 28);
            this._LabelLocale.TabIndex = 4;
            this._LabelLocale.Click += new System.EventHandler(this._Click);
            // 
            // _CmenuDropDown
            // 
            this._CmenuDropDown.Name = "_CmenuDropDown";
            this._CmenuDropDown.Size = new System.Drawing.Size(61, 4);
            // 
            // LocaleDropDown
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._LabelSelectLocale);
            this.Controls.Add(this._LabelLocale);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "LocaleDropDown";
            this.Size = new System.Drawing.Size(39, 27);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label _LabelSelectLocale;
        private System.Windows.Forms.Label _LabelLocale;
        private System.Windows.Forms.ContextMenuStrip _CmenuDropDown;
    }
}
