﻿using System;
using System.Drawing;
using System.Windows.Forms;



namespace Robbiblubber.Util.Localization.Controls
{
    /// <summary>This is a compact locale selection control.</summary>
    public partial class LocaleDropDown: UserControl
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public events                                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Event is raised when a locale is selected.</summary>
        public event LocaleSelectedEventHandler LocaleSelected;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public LocaleDropDown()
        {
            InitializeComponent();

            foreach(Locale i in Locale.Locales)
            {
                _CmenuDropDown.Items.Add(i.Name, (Image) i.SmallIcon, new EventHandler(_CmenuDropDown_ItemClick)).Tag = i;
            }
            if(Locale.Selected != null) { _LabelLocale.Image = (Image) Locale.Selected.Icon; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Context menu item clicked.</summary>
        private void _CmenuDropDown_ItemClick(object sender, EventArgs e)
        {
            try
            {
                _LabelLocale.Image = (Image) ((Locale) ((ToolStripItem) sender).Tag).Icon;
            }
            catch(Exception) {}

            LocaleSelected?.Invoke(this, new LocaleSelectionEventArgs((Locale) ((ToolStripItem) sender).Tag));
        }


        /// <summary>Control clicked.</summary>
        private void _Click(object sender, EventArgs e)
        {
            if(_CmenuDropDown.Items.Count > 0)
            {
                _CmenuDropDown.Show(_LabelSelectLocale, new Point(14, 14));
            }
        }
    }
}
