﻿using System;



namespace Robbiblubber.Util.Localization.Controls
{
    /// <summary>This delegate represents a method that will handle a locale selection event.</summary>
    public delegate void LocaleSelectedEventHandler(object sender, LocaleSelectionEventArgs e);
}
