﻿using System;
using System.Drawing;
using System.IO;



namespace Robbiblubber.Util.Localization.Controls.Internal
{
    /// <summary>This class process locale images.</summary>
    public static class __LocaleImages
    {
        /// <summary>Returns the images for a locale.</summary>        /// <param name="iconFile">Icon file name.</param>
        /// <param name="smallIconFile">Small icon file name.</param>
        /// <returns>Array of images.</returns>
        public static object[] GetImages(string iconFile, string smallIconFile)
        {
            object[] rval = new object[2];
            
            if(File.Exists(iconFile))
            {
                Image img = Image.FromFile(iconFile);
                if(!((img.Width == 24) && (img.Height == 24))) { img = new Bitmap(img, new Size(24, 24)); }

                rval[0] = img;
            }
            
            if(File.Exists(smallIconFile))
            {
                Image img = Image.FromFile(smallIconFile);
                if(!((img.Width == 16) && (img.Height == 16))) { img = new Bitmap(img, new Size(16, 16)); }

                rval[1] = img;
            }
            else if(rval[0] != null)
            {
                rval[1] = new Bitmap((Image) rval[0], new Size(16, 16));
            }

            if((rval[0] == null) && (rval[1] != null))
            {
                rval[0] = new Bitmap((Image) rval[1], new Size(24, 24));
            }

            return rval;
        }
    }
}
