﻿using System;



namespace Robbiblubber.Util.Localization.Controls
{
    /// <summary>This class represents a tag.</summary>
    public class LTag
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Text key.</summary>
        protected string _TextKey;

        /// <summary>Tool tip key.</summary>
        protected string _ToolTipKey;

        /// <summary>Owner.</summary>
        protected object _Owner;

        /// <summary>Additional data.</summary>
        protected object _Data;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        protected LTag()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="textKey">Text key.</param>
        /// <param name="toolTipKey">Tool tip key.</param>
        /// <param name="data">Additional data.</param>
        /// <param name="owner">Owner.</param>
        public LTag(string textKey, string toolTipKey, object owner = null, object data = null)
        {
            _TextKey = textKey;
            _ToolTipKey = toolTipKey;
            _Owner = owner;
            _Data = data;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the text key.</summary>
        public string TextKey
        {
            get { return _TextKey; }
        }


        /// <summary>Gets the tool tip key.</summary>
        public string ToolTipKey
        {
            get { return _ToolTipKey; }
        }


        /// <summary>Gets the owner.</summary>
        public object Owner
        {
            get { return _Owner; }
        }


        /// <summary>Gets additional data.</summary>
        public object Data
        {
            get { return _Data; }
        }
    }
}
