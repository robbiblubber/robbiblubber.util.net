﻿using System;
using System.Text;
using System.Runtime.InteropServices;



namespace Robbiblubber.Util.Win32
{
    /// <summary>This class allows access to .ini-type configuration files.</summary>
    public sealed class IniFile
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // imports                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileIntA", CharSet = CharSet.Ansi)]
        private static extern int GetPrivateProfileInt(string lpApplicationName, string lpKeyName, int nDefault, string lpFileName);

        [DllImport("kernel32.dll", EntryPoint = "WritePrivateProfileStringA", CharSet = CharSet.Ansi)]

        private static extern int WritePrivateProfileString(string lpApplicationName, string lpKeyName, string lpString, string lpFileName);
        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileStringA", CharSet = CharSet.Ansi)]

        private static extern int GetPrivateProfileString(string lpApplicationName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);
        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileSectionNamesA", CharSet = CharSet.Ansi)]

        private static extern int GetPrivateProfileSectionNames(byte[] lpszReturnBuffer, int nSize, string lpFileName);

        [DllImport("kernel32.dll", EntryPoint = "WritePrivateProfileSectionA", CharSet = CharSet.Ansi)]
        private static extern int WritePrivateProfileSection(string lpAppName, string lpString, string lpFileName);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Maximum entry count.</summary>
        private const int MAX_ENTRY = 32768;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>File name.</summary>
        private string _FileName;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of the class.</summary>
        /// <param name="fileName">Configuration file name.</param>
        public IniFile(string fileName)
        {
            _FileName = fileName;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the configuration file name.</summary>
        public string Filename
        {
            get { return _FileName; }
            set
            {
                _FileName = value;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reads an integer value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="defaultValue">Default value.</param>
        public int GetInt32(string section, string key, int defaultValue)
        {
            return GetPrivateProfileInt(section, key, defaultValue, Filename);
        }


        /// <summary>Reads an integer value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        public int GetInt32(string section, string key)
        {
            return GetInt32(section, key, 0);
        }


        /// <summary>Reads a string value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="defaultValue">Default value.</param>
        public string GetString(string section, string key, string defaultValue)
        {
            StringBuilder s = new StringBuilder(MAX_ENTRY);
            int Ret = GetPrivateProfileString(section, key, defaultValue, s, MAX_ENTRY, Filename);
            return s.ToString();
        }


        /// <summary>Reads a string value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        public string GetString(string section, string key)
        {
            return GetString(section, key, "");
        }


        /// <summary>Reads a long integer value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="defaultValue">Default value.</param>
        public long GetInt64(string section, string key, long defaultValue)
        {
            return long.Parse(GetString(section, key, defaultValue.ToString()));
        }


        /// <summary>Reads a long integer value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        public long GetInt64(string section, string key)
        {
            return GetInt64(section, key, 0);
        }


        /// <summary>Reads a short integer value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="defaultValue">Default value.</param>
        public short GetInt16(string section, string key, short defaultValue)
        {
            return (short) GetInt32(section, key, (int) defaultValue);
        }


        /// <summary>Reads a binary value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        public byte[] GetBinary(string section, string key)
        {
            try
            {
                return Convert.FromBase64String(GetString(section, key));
            }
            catch {}

            return null;
        }


        /// <summary>Reads a boolean value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="defaultValue">Default value.</param>
        public bool GetBoolean(string section, string key, bool defaultValue)
        {
            string v = GetString(section, key, (defaultValue ? "1" : "0")).ToLower();
            int i;

            if(int.TryParse(v, out i))
            {
                return (i != 0);
            }
            else
            {
                return (v.StartsWith("t") || v.StartsWith("y"));
            }
        }


        /// <summary>Reads a boolean value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        public bool GetBoolean(string section, string key)
        {
            return GetBoolean(section, key, false);
        }


        /// <summary>Reads a boolean value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="defaultValue">Default value.</param>
        public bool GetIntBoolean(string section, string key, bool defaultValue)
        {
            return (GetInt32(section, key, (defaultValue ? 1 : 0)) != 0);
        }


        /// <summary>Reads a boolean value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        public bool GetIntBoolean(string section, string key)
        {
            return GetIntBoolean(section, key, false);
        }


        /// <summary>Writes an integer value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        public bool SetValue(string section, string key, int value)
        {
            return SetValue(section, key, value.ToString());
        }


        /// <summary>Writes a string value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        public bool SetValue(string section, string key, string value)
        {
            return (WritePrivateProfileString(section, key, value, Filename) != 0);
        }


        /// <summary>Writes a long integer value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        public bool SetValue(string section, string key, long value)
        {
            return SetValue(section, key, value.ToString());
        }


        /// <summary>Writes a binary value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        public bool SetValue(string section, string key, byte[] value)
        {
            if(value == null)
                return SetValue(section, key, (string) null);
            else
                return SetValue(section, key, value, 0, value.Length);
        }


        /// <summary>Writes a binary value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        /// <param name="offset">Offset to start from.</param>
        /// <param name="length">Offset to end with.</param>
        public bool SetValue(string section, string key, byte[] value, int offset, int length)
        {
            if(value == null)
                return SetValue(section, key, (string) null);
            else
                return SetValue(section, key, Convert.ToBase64String(value, offset, length));
        }


        /// <summary>Writes a boolean value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        /// <remarks>SetValue will write "true" or "false". To store a Boolean value as "1" or "0" use SetIntBoolean().</remarks>
        public bool SetValue(string section, string key, bool value)
        {
            return SetValue(section, key, value ? "true" : "false");
        }


        /// <summary>Writes an numeric equivalent of a boolean value.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        public bool SetIntBoolean(string section, string key, bool value)
        {
            return SetValue(section, key, value ? 1 : 0);
        }


        /// <summary>Deletes a key.</summary>
        /// <param name="section">Section.</param>
        /// <param name="key">Key.</param>
        public bool DeleteKey(string section, string key)
        {
            return (WritePrivateProfileString(section, key, null, Filename) != 0);
        }


        /// <summary>Deletes a section.</summary>
        /// <param name="section">Section.</param>
        public bool DeleteSection(string section)
        {
            return WritePrivateProfileSection(section, null, Filename) != 0;
        }


        /// <summary>Returns an array that contains all sections of the configuration file.</summary>
        public string[] GetSections()
        {
            try
            {
                byte[] buffer = new byte[MAX_ENTRY];
                GetPrivateProfileSectionNames(buffer, MAX_ENTRY, Filename);
                return Encoding.UTF8.GetString(buffer).Trim('\0').Split('\0');
            }
            catch {}

            return null;
        }
    }
}