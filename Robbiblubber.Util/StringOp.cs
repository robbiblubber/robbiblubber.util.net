﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Robbiblubber.Util.Coding;
using Robbiblubber.Util.Collections;



namespace Robbiblubber.Util
{
    /// <summary>This class provides string operation methods.</summary>
    public static class StringOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Random object.</summary>
        private static readonly Random _RAND = new System.Random();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a random alphanumeric string of a given length from an alphabet.</summary>
        /// <param name="alphabet">Alphabet.</param>
        /// <returns>Random string.</returns>
        public static string Random(string alphabet)
        {
            return Random(24, alphabet);
        }


        /// <summary>Returns a random alphanumeric string of a given length from an alphabet.</summary>
        /// <param name="length">Random string length.</param>
        /// <param name="alphabet">Alphabet.</param>
        /// <returns>Random string.</returns>
        public static string Random(int length = 24, string alphabet = InvertedBase62.ALPHABET)
        {
            StringBuilder rval = new StringBuilder();
            int l = alphabet.Length - 1;

            for(int i = 0; i < length; i++)
            {
                rval.Append(alphabet[_RAND.Next(0, l)]);
            }

            return rval.ToString();
        }


        /// <summary>Returns a unique random alphanumeric string of a given length.</summary>
        /// <param name="length">String length.</param>
        /// <returns>Unique string.</returns>
        public static string Unique(int length = 24)
        {
            return (Random(3) + ToAlpha(DateTime.Now.Ticks) + Random(length)).Substring(0, length);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Converts an integer to a signed string of a given base.</summary>
        /// <param name="value">Integer value.</param>
        /// <param name="toBase">The base of the return value.</param>
        /// <param name="alphabet">Encoding alphabet.</param>
        /// <returns>String.</returns>
        public static string ToString(this long value, int toBase, string alphabet = InvertedBase62.ALPHABET)
        {
            bool neg = false;
            StringBuilder rval = new StringBuilder();

            if((toBase < 2) || (toBase > alphabet.Length)) throw new ArgumentException("Invalid base.");

            if(value < 0)
            {
                neg = true;
                value = -value;
            }

            while(value > 0)
            {
                rval.Insert(0, (alphabet[(int) (value % toBase)]));
                value /= toBase;
            }

            if(neg) { rval.Insert(0, '-'); }

            return rval.ToString();
        }


        /// <summary>Converts an integer to a signed string of a given base.</summary>
        /// <param name="value">Integer value.</param>
        /// <param name="toBase">The base of the return value.</param>
        /// <param name="alphabet">Encoding alphabet.</param>
        /// <returns>String.</returns>
        public static string ToString(this int value, int toBase, string alphabet = InvertedBase62.ALPHABET)
        {
            return ToString((long) value, toBase, alphabet);
        }


        /// <summary>Converts an integer to a signed string of a given base.</summary>
        /// <param name="value">Integer value.</param>
        /// <param name="toBase">The base of the return value.</param>
        /// <param name="alphabet">Encoding alphabet.</param>
        /// <returns>String.</returns>
        public static string ToString(this short value, int toBase, string alphabet = InvertedBase62.ALPHABET)
        {
            return ToString((long) value, toBase, alphabet);
        }


        /// <summary>Converts a string of a given base to an integer.</summary>
        /// <param name="value">String.</param>
        /// <param name="toBase">The base of the return value.</param>
        /// <param name="alphabet">Encoding alphabet.</param>
        /// <returns>Integer.</returns>
        public static long ToInt64(this string value, int toBase = 10, string alphabet = InvertedBase62.ALPHABET)
        {
            bool neg = false;
            long rval = 0;
            
            if((toBase < 2) || (toBase > alphabet.Length)) throw new ArgumentException("Invalid base.");

            value = value.Trim();
            if(value.StartsWith("-"))
            {
                neg = true;
                value = value.Substring(1);
            }

            int l = (value.Length - 1);
            for(int i = 0; i < value.Length; i++)
            {
                rval += MathOp.Power(toBase, i) * alphabet.IndexOf(value[l - i]);
            }

            return (neg ? -rval : rval);
        }


        /// <summary>Converts a string of a given base to an integer.</summary>
        /// <param name="value">Integer value.</param>
        /// <param name="toBase">The base of the return value.</param>
        /// <param name="alphabet">Encoding alphabet.</param>
        /// <returns>Integer.</returns>
        public static int ToInt32(this string value, int toBase = 10, string alphabet = InvertedBase62.ALPHABET)
        {
            return Convert.ToInt32(value.ToInt64(toBase, alphabet));
        }


        /// <summary>Converts a string of a given base to an integer.</summary>
        /// <param name="value">Integer value.</param>
        /// <param name="toBase">The base of the return value.</param>
        /// <param name="alphabet">Encoding alphabet.</param>
        /// <returns>Integer.</returns>
        public static int ToInt16(this string value, int toBase = 10, string alphabet = InvertedBase62.ALPHABET)
        {
            return Convert.ToInt16(value.ToInt64(toBase, alphabet));
        }


        /// <summary>Returns the string part before the separator.</summary>
        /// <param name="s">String.</param>
        /// <param name="separator">Separator.</param>
        /// <returns>String part preceding the separator expression.</returns>
        public static string Before(this string s, string separator)
        {
            int v = s.IndexOf(separator);

            return ((v < 0) ? s : s.Substring(0, v));
        }


        /// <summary>Returns the string part after the separator.</summary>
        /// <param name="s">String.</param>
        /// <param name="separator">Separator.</param>
        /// <returns>String part succeding the separator expression.</returns>
        public static string After(this string s, string separator)
        {
            int v = s.IndexOf(separator);

            return ((v < 0) ? "" : s.Substring(v + separator.Length));
        }


        /// <summary>Gets the number of identical characters at the start of two strings.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="pattern">String pattern.</param>
        /// <param name="caseSensitive">Determines if the match is case-sensitive.</param>
        /// <returns>Returns the number of matching characters.</returns>
        public static int MatchesFromStart(this string expression, string pattern, bool caseSensitive = true)
        {
            if(!caseSensitive)
            {
                expression = expression.ToLower();
                pattern = pattern.ToLower();
            }

            int i;
            for(i = 0; i < ((expression.Length < pattern.Length) ? expression.Length : pattern.Length); i++)
            {
                if(expression[i] != pattern[i]) return i;
            }

            return i;
        }


        /// <summary>Checks if an expression matches a pattern.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="pattern">Pattern.</param>
        /// <param name="caseSensitive">Determines if the match is case-sensitive.</param>
        /// <returns>Returns TRUE if the expression matches, otherwise returns FALSE.</returns>
        public static bool Matches(this string expression, bool caseSensitive, string pattern)
        {
            if(pattern == null) { return true; }
            pattern = "^" + Regex.Escape(pattern).Replace(@"\*", ".*").Replace(@"\?", ".") + "$";

            return new Regex(pattern, caseSensitive ? RegexOptions.None : RegexOptions.IgnoreCase).IsMatch(expression);
        }


        /// <summary>Checks if an expression matches a pattern.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="pattern">Pattern.</param>
        /// <param name="caseSensitive">Determines if the match is case-sensitive.</param>
        /// <returns>Returns TRUE if the expression matches, otherwise returns FALSE.</returns>
        public static bool Matches(this string expression, bool caseSensitive, IEnumerable<string> pattern)
        {
            if(pattern == null) { return true; }
            if(pattern.Count() == 0) { return true; }

            foreach(string i in pattern)
            {
                if(Matches(expression, caseSensitive, i)) return true;
            }

            return false;
        }


        /// <summary>Checks if an expression matches a pattern.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="pattern">Pattern.</param>
        /// <returns>Returns TRUE if the expression matches, otherwise returns FALSE.</returns>
        public static bool Matches(this string expression, IEnumerable<string> pattern)
        {
            return Matches(expression, true, pattern);
        }


        /// <summary>Checks if an expression matches a pattern.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="pattern">Pattern.</param>
        /// <param name="caseSensitive">Determines if the match is case-sensitive.</param>
        /// <returns>Returns TRUE if the expression matches, otherwise returns FALSE.</returns>
        public static bool Matches(this string expression, bool caseSensitive, params string[] pattern)
        {
            return Matches(expression, caseSensitive, (IEnumerable<string>)pattern);
        }


        /// <summary>Checks if an expression matches a pattern.</summary>
        /// <param name="expression">Expression.</param>
        /// <param name="pattern">Pattern.</param>
        /// <returns>Returns TRUE if the expression matches, otherwise returns FALSE.</returns>
        public static bool Matches(this string expression, params string[] pattern)
        {
            return Matches(expression, true, (IEnumerable<string>)pattern);
        }


        /// <summary>Gets the maximum length of the strings in an array.</summary>
        /// <param name="array">String array.</param>
        /// <returns>Length of the longest string in the array.</returns>
        public static int StrLen(this IEnumerable<string> array)
        {
            int rval = 0;

            foreach(string i in array)
            {
                if(i.Length > rval) { rval = i.Length; }
            }

            return rval;
        }


        /// <summary>Converts a long integer to an alphanumeric string.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>String.</returns>
        public static string ToAlpha(this long value)
        {
            return ToString(value, 62);
        }


        /// <summary>Converts an integer to an alphanumeric string.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>String.</returns>
        public static string ToAlpha(this int value)
        {
            return ToAlpha((long) value);
        }


        /// <summary>Converts an integer to an alphanumeric string.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>String</returns>
        public static string ToAlpha(this short value)
        {
            return ToAlpha((long) value);
        }


        /// <summary>Returns if any of the given objects is null.</summary>
        /// <param name="values">Objects.</param>
        /// <returns>Returns TRUE if one of the object is null.</returns>
        public static bool AnyNull(params object[] values)
        {
            foreach(object i in values) { if(i == null) return true; }
            return false;
        }


        /// <summary>Returns a string in which all occurances of a character sequence are replaced.</summary>
        /// <param name="s">String.</param>
        /// <param name="oldv">Old value.</param>
        /// <param name="newv">New value.</param>
        /// <param name="caseSensitive">Determines if the replace operation should be case-sensitive.</param>
        /// <returns>String with replacements.</returns>
        public static string ReplaceAll(this string s, string oldv, string newv, bool caseSensitive = true)
        {
            if(AnyNull(s, oldv, newv)) { return s; }

            if(caseSensitive)
            {
                return s.Replace(oldv, newv);
            }
            
            int v;
            while(true)
            {
                if((v = s.ToLower().IndexOf(oldv.ToLower())) < 0) { return s; }

                s = s.Substring(0, v) + newv + s.Substring(v + oldv.Length);
            }
        }


        /// <summary>Returns a string in which the first occurance of a character sequence is replaced.</summary>
        /// <param name="s">String.</param>
        /// <param name="oldv">Old value.</param>
        /// <param name="newv">New value.</param>
        /// <param name="caseSensitive">Determines if the replace operation should be case sensitive.</param>
        public static string ReplaceFirst(this string s, string oldv, string newv, bool caseSensitive = true)
        {
            if(AnyNull(s, oldv, newv)) { return s; }

            int v;
            if(caseSensitive)
            {
                v = s.IndexOf(oldv);
            }
            else { v = s.ToLower().IndexOf(oldv.ToLower()); }

            if(v < 0) return s;

            return s.Substring(0, v) + newv + s.Substring(v + oldv.Length);
        }


        /// <summary>Gets the part of the string before the last occurance of a character.</summary>
        /// <param name="s">String.</param>
        /// <param name="c">Character.</param>
        /// <returns>String part.</returns>
        public static string BeforeLast(this string s, char c)
        {
            int i = s.LastIndexOf(c);

            return ((i < 0) ? s : s.Substring(0, i));
        }


        /// <summary>Gets the part of the string before the last occurance of a character sequence.</summary>
        /// <param name="s">String.</param>
        /// <param name="c">Sequence.</param>
        /// <returns>String part.</returns>
        public static string BeforeLast(this string s, string c)
        {
            int i = s.LastIndexOf(c);

            return ((i < 0) ? s : s.Substring(0, i));
        }


        /// <summary>Gets the part of the string before the first occurance of a character.</summary>
        /// <param name="s">String.</param>
        /// <param name="c">Character.</param>
        /// <returns>String part.</returns>
        public static string BeforeFirst(this string s, char c)
        {
            int i = s.IndexOf(c);

            return ((i < 0) ? s : s.Substring(0, i));
        }


        /// <summary>Gets the part of the string before the first occurance of a character sequence.</summary>
        /// <param name="s">String.</param>
        /// <param name="c">Sequence.</param>
        /// <returns>String part.</returns>
        public static string BeforeFirst(this string s, string c)
        {
            int i = s.IndexOf(c);

            return ((i < 0) ? s : s.Substring(0, i));
        }


        /// <summary>Gets the part of the string behind the last occurance of a character.</summary>
        /// <param name="s">String.</param>
        /// <param name="c">Character.</param>
        /// <returns>String part.</returns>
        public static string AfterLast(this string s, char c)
        {
            int i = s.LastIndexOf(c);

            return ((i < 0) ? s : s.Substring(i + 1));
        }


        /// <summary>Gets the part of the string behind the last occurance of a character sequence.</summary>
        /// <param name="s">String.</param>
        /// <param name="c">Sequence.</param>
        /// <returns>String part.</returns>
        public static string AfterLast(this string s, string c)
        {
            int i = s.LastIndexOf(c);

            return ((i < 0) ? s : s.Substring(i + c.Length));
        }


        /// <summary>Gets the part of the string behind the first occurance of a character.</summary>
        /// <param name="s">String.</param>
        /// <param name="c">Character.</param>
        /// <returns>String part.</returns>
        public static string AfterFirst(this string s, char c)
        {
            int i = s.IndexOf(c);

            return ((i < 0) ? s : s.Substring(i + 1));
        }


        /// <summary>Gets the part of the string behind the first occurance of a character sequence.</summary>
        /// <param name="s">String.</param>
        /// <param name="c">Sequence.</param>
        /// <returns>String part.</returns>
        public static string AfterFirst(this string s, string c)
        {
            int i = s.IndexOf(c);

            return ((i < 0) ? s : s.Substring(i + c.Length));
        }


        /// <summary>Returns if a string complies to a given pattern.</summary>
        /// <param name="s">String.</param>
        /// <param name="pattern">Pattern.</param>
        /// <returns>Returns TRUE if the string complies, otherwise returns FALSE.</returns>
        public static bool Complies(this string s, string pattern = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
        {
            foreach(char i in s)
            {
                if(!pattern.Contains(i)) return false;
            }

            return true;
        }


        /// <summary>Gets all characters contained in a string.</summary>
        /// <param name="s">String.</param>
        /// <returns>An array containing each character used in the string once.</returns>
        public static char[] GetChars(this string s)
        {
            List<char> rval = new List<char>();

            foreach(char i in s)
            {
                if(!rval.Contains(i)) { rval.Add(i); }
            }
            rval.Sort();

            return rval.ToArray();
        }


        /// <summary>Returns an integer representation of the string value.</summary>
        /// <param name="s">String.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Integer.</returns>
        public static int ToInteger(this string s, int defaultValue = 0)
        {
            int n = 0;
            if(Int32.TryParse(s, out n)) { return n; }

            return defaultValue;
        }


        /// <summary>Returns a boolean representation of the string value.</summary>
        /// <param name="s">String.</param>
        /// <returns>Boolean.</returns>
        public static bool ToBoolean(this string s)
        {
            if(s == null) return false;
            if(long.TryParse(s, out long v)) { return (v != 0); }

            return (s.ToLower().StartsWith("t") || s.ToLower().StartsWith("y"));
        }


        /// <summary>Returns a string representation of the Boolean value.</summary>
        /// <param name="b">Boolean.</param>
        /// <returns>String.</returns>
        public static string ToBooleanString(this bool b)
        {
            return (b ? "true" : "false");
        }


        /// <summary>Returns a string or default value if string is null.</summary>
        /// <param name="value">Value.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>String</returns>
        public static string Nvl(string value, string defaultValue = "")
        {
            return ((value == null) ? defaultValue : value);
        }


        /// <summary>Replaces a string part at the beginning of a string.</summary>
        /// <param name="str">String.</param>
        /// <param name="find">String to replace.</param>
        /// <param name="replace">String to replace with.</param>
        /// <param name="caseSensitive">Determines if the replace operation should be case-sensitive.</param>
        /// <returns>String.</returns>
        public static string ReplaceStart(this string str, string find, string replace, bool caseSensitive = true)
        {
            int n = 0;
            while(str.StartsWith(find) || ((!caseSensitive) && (str.ToLower().StartsWith(find.ToLower()))))
            {
                n++;
                str = str.Substring(find.Length);
            }
            for(int i = 0; i < n; i++) { str = (replace + str); }

            return str;
        }


        /// <summary>Replaces a string part at the end of a string.</summary>
        /// <param name="str">String.</param>
        /// <param name="find">String to replace.</param>
        /// <param name="replace">String to replace with.</param>
        /// <param name="caseSensitive">Determines if the replace operation should be case-sensitive.</param>
        /// <returns>String.</returns>
        public static string ReplaceEnd(this string str, string find, string replace, bool caseSensitive = true)
        {
            int n = 0;
            while(str.EndsWith(find) || ((!caseSensitive) && (str.ToLower().EndsWith(find.ToLower()))))
            {
                n++;
                str = str.Substring(0, str.Length - find.Length);
            }
            for(int i = 0; i < n; i++) { str += replace; }

            return str;
        }


        /// <summary>Returns a part of the string between the m-th occurence of a character (from) and the n-th occurrence of a string (to)
        ///          or the end of the string (to is omitted or 0).</summary>
        /// <remarks>Default delimiter is a blank space.</remarks>
        /// <param name="s">String.</param>
        /// <param name="from">Start occurrence.</param>
        /// <param name="to">End occurrence.</param>
        /// <returns>String sniplet.</returns>
        public static string Snip(this string s, int from = 1, int to = 0)
        {
            return Snip(s, from, to, ' ');
        }


        /// <summary>Returns a part of the string between the m-th occurence of a character (from) and the n-th occurrence of a string (to)
        ///          or the end of the string (to is omitted or 0).</summary>
        /// <param name="s">String.</param>
        /// <param name="delimiters">Delimiter characters.</param>
        /// <returns>String sniplet.</returns>
        public static string Snip(this string s, params char[] delimiters)
        {
            return Snip(s, 1, 0, delimiters);
        }


        /// <summary>Returns a part of the string between the m-th occurence of a character (from) and the n-th occurrence of a string (to)
        ///          or the end of the string (to is omitted or 0).</summary>
        /// <param name="s">String.</param>
        /// <param name="from">Start occurrence.</param>
        /// <param name="delimiters">Delimiter characters.</param>
        /// <returns>String sniplet.</returns>
        public static string Snip(this string s, int from, params char[] delimiters)
        {
            return Snip(s, from, 0, delimiters);
        }


        /// <summary>Returns a part of the string between the m-th occurence of a character (from) and the n-th occurrence of a string (to)
        ///          or the end of the string (to is omitted or 0).</summary>
        /// <param name="s">String.</param>
        /// <param name="from">Start occurrence.</param>
        /// <param name="to">End occurrence.</param>
        /// <param name="delimiters">Delimiter characters.</param>
        /// <returns>String sniplet.</returns>
        public static string Snip(this string s, int from, int to, params char[] delimiters)
        {
            int start = -1, end = -1;

            if(to == 0) { to = -2; }
            for(int i = 0; i < from; i++)
            {
                start = s.IndexOfAny(delimiters, start + 1);
                if(start < 0) { return ""; }
            }

            if(to < from) { return s.Substring(start + 1); }
            if(to == from) { return ""; }

            for(int i = 0; i < to; i++)
            {
                end = s.IndexOfAny(delimiters, end + 1);
                if(end < 1) { return s.Substring(start + 1); }
            }

            return s.Substring(start + 1, end - start - 1);
        }


        /// <summary>Returns a list of strings as a chained string.</summary>
        /// <param name="l">Enumerable list of strings.</param>
        /// <param name="separator">Separator string.</param>
        /// <remarks>Chained string.</remarks>
        public static string Chain(this IEnumerable<string> l, string separator = ",")
        {
            if(l == null) { return ""; }

            StringBuilder rval = new StringBuilder();
            bool first = true;

            foreach(string i in l)
            {
                if(first) { first = false; } else { rval.Append(separator); }
                rval.Append(i);
            }

            return rval.ToString();
        }


        /// <summary>Returns an EnumerableStringCollection that contains this StringCollection.</summary>
        /// <param name="s">StringCollection.</param>
        /// <returns>EnumerableStringCollection.</returns>
        public static EnumerableStringCollection AsEnumerable(this StringCollection s)
        {
            return new EnumerableStringCollection(s);
        }


        /// <summary>Returns an array that contains the elements of this collection.</summary>
        /// <param name="s">StringCollection.</param>
        /// <returns>Array.</returns>
        public static string[] ToArray(this StringCollection s)
        {
            string[] rval = new string[s.Count];
            s.CopyTo(rval, 0);

            return rval;
        }


        /// <summary>Returns if a string is numeric.</summary>
        /// <param name="s">String.</param>
        /// <returns>Returns TRUE if the string is numeric, otherwise returns FALSE.</returns>
        public static bool IsNumeric(this string s)
        {
            return decimal.TryParse(s, out _);
        }


        /// <summary>Returns if a string is a numeric intger.</summary>
        /// <param name="s">String.</param>
        /// <returns>Returns TRUE if the string is numeric, otherwise returns FALSE.</returns>
        public static bool IsNumericInteger(this string s)
        {
            return long.TryParse(s, out _);
        }
    }
}
