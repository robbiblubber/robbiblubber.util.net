﻿using System;



namespace Robbiblubber.Util
{
    /// <summary>This class provides mathematical methods.</summary>
    public static class MathOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an integer raised to a specific power.</summary>
        /// <param name="n">Integer.</param>
        /// <param name="power">Power.</param>
        /// <returns>Result.</returns>
        public static long Power(long n, ushort power)
        {
            if(power == 0) return 1;

            long rval = n;

            for(ushort i = 1; i < power; i++)
            {
                rval *= n;
            }

            return rval;
        }


        /// <summary>Returns an integer raised to a specific power.</summary>
        /// <param name="n">Integer.</param>
        /// <param name="power">Power.</param>
        /// <returns>Result.</returns>
        public static long Power(long n, int power)
        {
            return Power(n, (ushort) power);
        }


        /// <summary>Returns an integer raised to a specific power.</summary>
        /// <param name="n">Integer.</param>
        /// <param name="power">Power.</param>
        /// <returns>Result.</returns>
        public static long Power(int n, ushort power)
        {
            return Power((long) n, power);
        }


        /// <summary>Returns an integer raised to a specific power.</summary>
        /// <param name="n">Integer.</param>
        /// <param name="power">Power.</param>
        /// <returns>Result.</returns>
        public static long Power(int n, int power)
        {
            return Power((long) n, (ushort) power);
        }


        /// <summary>Returns an integer raised to a specific power.</summary>
        /// <param name="n">Integer.</param>
        /// <param name="power">Power.</param>
        /// <returns>Result.</returns>
        public static long Power(short n, ushort power)
        {
            return Power((long) n, power);
        }


        /// <summary>Returns the greatest common factor for two integers.</summary>
        /// <param name="a">An integer.</param>
        /// <param name="b">Another integer.</param>
        /// <returns>The greatest common factor of a and b.</returns>
        public static long GCF(long a, long b)
        {
            while(b != 0)
            {
                long t = b;
                b = a % b;
                a = t;
            }

            return a;
        }



        /// <summary>Returns the greatest common factor for two integers.</summary>
        /// <param name="a">An integer.</param>
        /// <param name="b">Another integer.</param>
        /// <returns>The greatest common factor of a and b.</returns>
        public static int GCF(int a, int b)
        {
            return (int) GCF(a, (long) b);
        }


        /// <summary>Returns the least common multiple for two integers.</summary>
        /// <param name="a">An integer.</param>
        /// <param name="b">Another integer.</param>
        /// <returns>The least common multiple of a and b.</returns>
        public static long LCM(long a, long b)
        {
            return (a / GCF(a, b)) * b;
        }


        /// <summary>Returns the least common multiple for two integers.</summary>
        /// <param name="a">An integer.</param>
        /// <param name="b">Another integer.</param>
        /// <returns>The least common multiple of a and b.</returns>
        public static long LCM(int a, int b)
        {
            return LCM((long) a, (long) b);

        }
    }
}
