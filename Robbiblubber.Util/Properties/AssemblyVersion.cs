﻿using System.Reflection;



[assembly: AssemblyCompany("robbiblubber.org")]
[assembly: AssemblyProduct("robbiblubber.org Utility Library")]
[assembly: AssemblyCopyright("© 2022 robbiblubber.org")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: AssemblyVersion("3.2.0")]
[assembly: AssemblyFileVersion("3.2.0")]
