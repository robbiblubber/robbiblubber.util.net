﻿using System;



namespace Robbiblubber.Util
{
    /// <summary>This class represents a read line.</summary>
    internal sealed class __DdpLine
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>End of file.</summary>
        public const int EOF = 0;

        /// <summary>Semicolon as termination sign.</summary>
        public const int SEMICOLON = 1;

        /// <summary>Opening bracket.</summary>
        public const int BRACKET_OPEN = 2;

        /// <summary>Closing bracket.</summary>
        public const int BRACKET_CLOSE = 4;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public members                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Line text.</summary>
        public string Text;

        /// <summary>Line termination sign.</summary>
        public int Termination;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="text">Read line text.</param>
        /// <param name="termination">Line termination sign.</param>
        internal __DdpLine(string text, int termination)
        {
            Text = text;
            Termination = termination;
        }
    }
}
