﻿using System;



namespace Robbiblubber.Util
{
    /// <summary>This class provides Date/Time extension methods.</summary>
    public static class CalendarOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Unix era start time.</summary>
        private static readonly DateTime _UnixEra = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parses a date/time value to a timestamp string.</summary>
        /// <param name="value">Value.</param>
        /// <returns>Timestamp string.</returns>
        public static string ToTimestamp(this DateTime value)
        {
            string offset = "+00:00";
            if(value.Kind != DateTimeKind.Utc)
            {
                TimeSpan off = TimeZoneInfo.Local.GetUtcOffset(value);

                if(off.TotalMinutes < 0)
                {
                    offset = "-";
                    off = off.Negate();
                }
                else { offset = "+"; }

                offset += off.Hours.ToString().PadLeft(2, '0') + ':' + off.Minutes.ToString().PadLeft(2, '0');
            }

            return value.Year.ToString().PadLeft(4, '0') + '-' + value.Month.ToString().PadLeft(2, '0') + '-' + value.Day.ToString().PadLeft(2, '0') + ' ' +
                   value.Hour.ToString().PadLeft(2, '0') + ':' + value.Minute.ToString().PadLeft(2, '0') + ':' + value.Second.ToString().PadLeft(2, '0') + '.' + value.Millisecond.ToString().PadLeft(3, '0') + offset;
        }


        /// <summary>Parses a timestamp string to date/time value.</summary>
        /// <param name="value">Value.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <param name="utc">Determines if the DateTime object should be UTC.</param>
        /// <returns>DateTime.</returns>
        public static DateTime ParseTimestamp(this string value, DateTime defaultValue, bool utc = false)
        {
            DateTime rval = defaultValue;
            if(rval.Kind != DateTimeKind.Utc) { rval = TimeZoneInfo.ConvertTimeToUtc(rval); }

            try
            {
                int sparse = value.LastIndexOf('-');
                int offset = 0;
                string offstr = null;

                if(value.Contains("+"))
                {
                    offstr = value.Substring(value.IndexOf('+'));
                    value = value.Substring(0, value.IndexOf('+'));
                }
                else if((sparse = value.IndexOf('-', sparse)) >= 8)
                {
                    offstr = value.Substring(sparse);
                    value = value.Substring(0, sparse);
                }

                if(offstr != null)
                {
                    offset = ((int.Parse(offstr.Substring(1, 2)) * 60) + int.Parse(offstr.Substring(4, 2)));

                    if(!offstr.StartsWith("-")) { offset = -offset; }
                }

                int year = int.Parse(value.Substring(0, 4));
                int month = int.Parse(value.Substring(5, 2));
                int day = int.Parse(value.Substring(8, 2));

                int hour = int.Parse(value.Substring(11, 2));
                int minute = int.Parse(value.Substring(14, 2));
                int second = int.Parse(value.Substring(17, 2));

                int millisecond = 0;
                if(value.Length > 20)
                {
                    millisecond = int.Parse(value.Substring(20).PadRight(3, '0'));
                }

                rval = new DateTime(year, month, day, hour, minute, second, millisecond, DateTimeKind.Utc);
                if(offset != 0) { rval = rval.AddMinutes(offset); }
            }
            catch(Exception) { }

            if(!utc)
            {
                return rval.ToLocalTime();
            }
            return rval;
        }


        /// <summary>Parses a timestamp string to d date/time value.</summary>
        /// <param name="value">Value.</param>
        /// <param name="utc">Determines if the DateTime object should be UTC.</param>
        /// <returns>DateTime.</returns>
        public static DateTime ParseTimestamp(this string value, bool utc = false)
        {
            return ParseTimestamp(value, new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc), utc);
        }


        /// <summary>Converts a Unix timestamp to a DateTime value.</summary>
        /// <param name="u">Unix timestamp.</param>
        /// <returns>DateTime.</returns>
        public static DateTime FromUnixTimestamp(this long u)
        {
            return _UnixEra.AddSeconds(u).ToLocalTime();
        }


        /// <summary>Converts a Unix timestamp to a DateTime value.</summary>
        /// <param name="u">Unix timestamp.</param>
        /// <returns>DateTime.</returns>
        public static DateTime FromUnixTimestamp(this int u)
        {
            return FromUnixTimestamp((long) u);
        }


        /// <summary>Converts a milliseconds Unix timestamp to a DateTime value.</summary>
        /// <param name="u">Unix timestamp.</param>
        /// <returns>DateTime.</returns>
        public static DateTime FromUnixTimestampMilliseconds(this long u)
        {
            return _UnixEra.AddMilliseconds(u).ToLocalTime();
        }


        /// <summary>Converts a DateTime value to a Unix timestamp.</summary>
        /// <param name="d">DateTime.</param>
        /// <returns>Unix timestamp.</returns>
        public static int ToUnixTimestamp(this DateTime d)
        {
            return Convert.ToInt32((d.ToUniversalTime() - _UnixEra).TotalSeconds);
        }


        /// <summary>Converts a DateTime value to a milliseconds Unix timestamp.</summary>
        /// <param name="d">DateTime.</param>
        /// <returns>Unix timestamp.</returns>
        public static long ToUnixTimestampMilliseconds(this DateTime d)
        {
            return Convert.ToInt64((d.ToUniversalTime() - _UnixEra).TotalMilliseconds);
        }
    }
}
