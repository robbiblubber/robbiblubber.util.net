﻿using System;



namespace Robbiblubber.Util
{
    /// <summary>This class implements a string reader.</summary>
    public class StringRdr
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>String value.</summary>
        protected string _String;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="str">Value.</param>
        /// <param name="position">Position.</param>
        public StringRdr(string str = null, int position = 0)
        {
            String = str;
            Position = position;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the underlying string.</summary>
        public string String
        {
            get { return _String; }
            set
            {
                _String = value;
                Length = ((value == null) ? 0 : value.Length);
            }
        }


        /// <summary>Gets the current character.</summary>
        public virtual char Current
        {
            get
            {
                return _String[Position - 1];
            }
        }


        /// <summary>Gets or sets the read position.</summary>
        public virtual int Position
        {
            get; set;
        }


        /// <summary>Gets the string length.</summary>
        public virtual int Length
        {
            get; protected set;
        }


        /// <summary>Gets if more characters can be read from the current position.</summary>
        public virtual bool More
        {
            get
            {
                return (Length > Position);
            }
        }


        /// <summary>Gets the preceding character.</summary>
        public virtual char Preceding
        {
            get
            {
                if(Position < 2) { return '\0'; }
                return String[Position - 2];
            }
        }


        /// <summary>Gets the preceding character.</summary>
        public virtual char Next
        {
            get
            {
                if(Position >= Length) { return '\0'; }
                return String[Position];
            }
        }


        /// <summary>Gets the part of the string from the current position.</summary>
        public virtual string Tail
        {
            get { return String.Substring(Position); }
        }


        /// <summary>Gets the part of the string from the beginning to the current position.</summary>
        public virtual string Lead
        {
            get { return String.Substring(0, Position); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Resets the reader.</summary>
        public virtual void Reset()
        {
            Position = 0;
        }


        /// <summary>Reads the next character.</summary>
        /// <returns>Returns TRUE if a character has been read, otherwise returns FALSE.</returns>
        public virtual bool Read()
        {
            if(More)
            {
                Position++;
                return true;
            }

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // operators                                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Casts a string to a string reader.</summary>
        /// <param name="str">String.</param>
        public static explicit operator StringRdr(string str)
        {
            return new StringRdr(str);
        }


        /// <summary>Casts a string reader to a string.</summary>
        /// <param name="p">String reader.</param>
        public static explicit operator string(StringRdr p)
        {
            return p.String;
        }
    }
}
