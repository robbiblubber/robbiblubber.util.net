﻿using System;
using System.IO;



namespace Robbiblubber.Util
{
    /// <summary>This class provides utility methods for handling streams.</summary>
    public static class StreamOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Reads all bytes from a stream.</summary>
        /// <param name="s">Stream.</param>
        /// <returns>Array of bytes.</returns>
        public static byte[] ReadAllBytes(this Stream s)
        {
            using(MemoryStream m = new MemoryStream())
            {
                s.CopyTo(m);
                return m.ToArray();
            }
        }
    }
}
