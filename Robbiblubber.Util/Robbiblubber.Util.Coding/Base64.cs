﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class provides Base64 support.</summary>
    public sealed class Base64: __Encoder, IEncoder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The default Base64 alphabet.</summary>
        public const string ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Singleton instance.</summary>
        private static Base64 _Instance = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Base64(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoding">Encoding.</param>
        public Base64(Encoding encoding): base(encoding)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an instance for this algorithm.</summary>
        public static Base64 Instance
        {
            get
            {
                if(_Instance == null) { _Instance = new Base64(); }

                return _Instance;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Converts bytes to a Base64-encoded string.</summary>
        /// <param name="value">Byte list.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Encoded string.</returns>
        public static string FromBytes(IEnumerable<byte> value, bool pad = false)
        {
            return Instance.EncodeBytes(value, pad);
        }


        /// <summary>Converts characters to a Base64-encoded string.</summary>
        /// <param name="value">Character list.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Encoded string.</returns>
        public static string FromChars(IEnumerable<char> value, bool pad = false)
        {
            return Instance.EncodeChars(value, pad);
        }


        /// <summary>Converts a string to a Base64-encoded string.</summary>
        /// <param name="value">String.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Encoded string.</returns>
        public static string FromString(string value, bool pad = false)
        {
            return Instance.EncodeString(value, pad);
        }


        /// <summary>Converts an integer to a Base64-encoded string.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Encoded string.</returns>
        public static string FromInt(int value)
        {
            return Instance.EncodeInt(value);
        }


        /// <summary>Converts a long integer to a Base64-encoded string.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Encoded string.</returns>
        public static string FromLong(long value)
        {
            return Instance.EncodeLong(value);
        }


        /// <summary>Converts a double to a Base64-encoded string.</summary>
        /// <param name="value">Double.</param>
        /// <returns>Encoded string.</returns>
        public static string FromDouble(double value)
        {
            return Instance.EncodeDouble(value);
        }


        /// <summary>Reads a file to a Base64-encoded string.</summary>
        /// <param name="value">String.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Encoded string.</returns>
        public static string FromFile(string value, bool pad = false)
        {
            return Instance.EncodeFile(value, pad);
        }


        /// <summary>Decodes a Base64-encoded string to a byte array.</summary>
        /// <param name="s">String.</param>
        /// <returns>Byte array.</returns>
        public static byte[] ToBytes(string s)
        {
            return Instance.DecodeToBytes(s);
        }


        /// <summary>Decodes a Base64-encoded string to a char array.</summary>
        /// <param name="s">String.</param>
        /// <returns>Char array.</returns>
        public static char[] ToChars(string s)
        {
            return Instance.DecodeToChars(s);
        }


        /// <summary>Decodes a Base64-encoded string to a string.</summary>
        /// <param name="s">String.</param>
        /// <returns>String.</returns>
        public static string ToString(string s)
        {
            return Instance.DecodeToString(s);
        }


        /// <summary>Decodes a Base64-encoded string to an integer.</summary>
        /// <param name="s">String.</param>
        /// <returns>Integer.</returns>
        public static int ToInt(string s)
        {
            return Instance.DecodeToInt(s);
        }


        /// <summary>Decodes a Base64-encoded string to a long integer.</summary>
        /// <param name="s">String.</param>
        /// <returns>Long integer.</returns>
        public static long ToLong(string s)
        {
            return Instance.DecodeToLong(s);
        }


        /// <summary>Decodes a Base64-encoded string to a double.</summary>
        /// <param name="s">String.</param>
        /// <returns>Double.</returns>
        public static double ToDouble(string s)
        {
            return Instance.DecodeToDouble(s);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Converts bytes to an encoded string.</summary>
        /// <param name="value">Byte list.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Encoded string.</returns>
        public string EncodeBytes(IEnumerable<byte> value, bool pad)
        {
            string rval = Convert.ToBase64String(value.ToArray());
            return (pad ? rval : rval.TrimEnd('='));
        }


        /// <summary>Converts characters to an encoded string.</summary>
        /// <param name="value">Character list.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Encoded string.</returns>
        public string EncodeChars(IEnumerable<char> value, bool pad)
        {
            return EncodeBytes(Encoding.GetBytes(value.ToArray()), pad);
        }


        /// <summary>Converts a string to an encoded string.</summary>
        /// <param name="value">String.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Encoded string.</returns>
        public string EncodeString(string value, bool pad)
        {
            return EncodeBytes(Encoding.GetBytes(value));
        }


        /// <summary>Reads a (binary) file into a Base64-encode string.</summary>
        /// <param name="fileName">File name.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Base64-encoded string.</returns>
        public string EncodeFile(string fileName, bool pad)
        {
            return EncodeBytes(File.ReadAllBytes(fileName), pad);
        }


        /// <summary>Encodes a byte array.</summary>
        /// <param name="data">Source data.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Encoded data.</returns>
        public byte[] Encode(byte[] data, bool pad)
        {
            return Encoding.GetBytes(EncodeBytes(data, pad));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __Encoder                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Converts bytes to an encoded string.</summary>
        /// <param name="value">Byte list.</param>
        /// <returns>Encoded string.</returns>
        public override string EncodeBytes(IEnumerable<byte> value)
        {
            return EncodeBytes(value, false);
        }


        /// <summary>Converts characters to an encoded string.</summary>
        /// <param name="value">Character list.</param>
        /// <returns>Encoded string.</returns>
        public override string EncodeChars(IEnumerable<char> value)
        {
            return EncodeChars(value, false);
        }


        /// <summary>Converts a string to an encoded string.</summary>
        /// <param name="value">String.</param>
        /// <returns>Encoded string.</returns>
        public override string EncodeString(string value)
        {
            return EncodeString(value, false);
        }


        /// <summary>Converts an encoded string to a byte array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded byte array.</returns>
        public override byte[] DecodeToBytes(string s)
        {
            while(s.Length % 4 != 0) { s += '='; }

            return Convert.FromBase64String(s);
        }


        /// <summary>Encodes a byte array.</summary>
        /// <param name="data">Source data.</param>
        /// <returns>Encoded data.</returns>
        public override byte[] Encode(byte[] data)
        {
            return Encode(data, false);
        }


        /// <summary>Decodes a byte array.</summary>
        /// <param name="data">Encoded source data.</param>
        /// <returns>Decoded data.</returns>
        public override byte[] Decode(byte[] data)
        {
            return DecodeToBytes(Encoding.GetString(data));
        }
    }
}
