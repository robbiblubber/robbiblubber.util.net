﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class implements the FunnelCake encryption algorithm.</summary>
    public sealed class FunnelCake: __Encoder, IEncrypter, IEncoder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Seed string.</summary>
        private const string _SEED = "ZDQwZGI1YmFhNDVlNTM5ZTRmM2JiYjU4ZGQ1OGNjZDJkZDNiMjJiNThlYTBmYjE4MjdlOWNkNGFhM2U3Zjk5NQ";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Instance using Base64 encoding.</summary>
        public static readonly FunnelCake BASE64 = new FunnelCake(Base64.Instance);

        /// <summary>Instance using Base62 encoding.</summary>
        public static readonly FunnelCake BASE62 = new FunnelCake(Base62.Instance);

        /// <summary>Instance using inverted Base62 encoding.</summary>
        public static readonly FunnelCake INVERTED_BASE62 = new FunnelCake(InvertedBase62.Instance);

        /// <summary>Instance using hexadecimal encoding.</summary>
        public static readonly FunnelCake HEX = new FunnelCake(Hex.Instance);

        /// <summary>Instance using hexadecimal encoding.</summary>
        public static readonly FunnelCake PLAINTEXT = new FunnelCake(PlainText.Instance);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Secret.</summary>
        private string _Secret = "hUZ6wwgC20Qy";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public FunnelCake(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        public FunnelCake(IEncoder encoder): base()
        {
            Encoder = encoder;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="secret">Secret.</param>
        public FunnelCake(string secret)
        {
            _Secret = secret;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        /// <param name="secret">Secret.</param>
        public FunnelCake(IEncoder encoder, string secret)
        {
            Encoder = encoder;
            _Secret = secret;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Encrypts a byte array.</summary>
        /// <param name="data">Data.</param>
        /// <param name="secret">Secret.</param>
        /// <returns>Encrypted data.</returns>
        public static byte[] Encrypt(byte[] data, string secret)
        {
            string key = "";
            string seed = _SEED;
            SHA256 sha256 = new SHA256(new BaseN(92));

            while(key.Length < data.Length)
            {
                seed = sha256.HashString(secret + seed);
                key += seed;
            }

            byte[] k = Encoding.ASCII.GetBytes(key);
            byte[] rval = new byte[data.Length];

            for(int i = 0; i < data.Length; i++)
            {
                rval[i] = (byte) (data[i] ^ k[i]);
            }

            return rval;
        }


        /// <summary>Encrypts a string.</summary>
        /// <param name="s">String.</param>
        /// <param name="secret">Secret.</param>
        /// <returns>Encrypted string.</returns>
        public static string Encrypt(string s, string secret)
        {
            return Encrypt(s, secret, PlainText.Instance);
        }


        /// <summary>Encrypts a string.</summary>
        /// <param name="s">String.</param>
        /// <param name="secret">Secret.</param>
        /// <param name="encoder">Encoder.</param>
        /// <returns>Encrypted string.</returns>
        public static string Encrypt(string s, string secret, IEncoder encoder)
        {
            return encoder.EncodeBytes(Encrypt(encoder.Encoding.GetBytes(s), secret));
        }


        /// <summary>Decrypts a byte array.</summary>
        /// <param name="data">Data.</param>
        /// <param name="secret">Secret.</param>
        /// <returns>Decrypted data.</returns>
        public static byte[] Decrypt(byte[] data, string secret)
        {
            return Encrypt(data, secret);
        }


        /// <summary>Decrypts a string.</summary>
        /// <param name="s">String.</param>
        /// <param name="secret">Secret.</param>
        /// <param name="encoder">Encoder.</param>
        /// <returns>Decrypted string.</returns>
        public static string Decrypt(string s, string secret, IEncoder encoder)
        {
            return encoder.Encoding.GetString(Decrypt(encoder.DecodeToBytes(s), secret)); ;
        }


        /// <summary>Decrypts a string.</summary>
        /// <param name="s">String.</param>
        /// <param name="secret">Secret.</param>
        /// <returns>Decrypted string.</returns>
        public static string Decrypt(string s, string secret)
        {
            return Decrypt(s, secret, PlainText.Instance);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __Encoder                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the encoding for this instance.</summary>
        public override Encoding Encoding
        {
            get { return Encoder.Encoding; }
        }


        /// <summary>Encodes a byte array.</summary>
        /// <param name="data">Source data.</param>
        /// <returns>Encoded data.</returns>
        public override byte[] Encode(byte[] data)
        {
            return Encoder.Encode(Encrypt(data, _Secret));
        }


        /// <summary>Decodes a byte array.</summary>
        /// <param name="data">Encoded source data.</param>
        /// <returns>Decoded data.</returns>
        public override byte[] Decode(byte[] data)
        {
            return Decrypt(Encoder.Decode(data), _Secret);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEncrypter                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the encoder for this instance.</summary>
        public IEncoder Encoder
        {
            get; private set;
        } = PlainText.Instance;


        /// <summary>Sets the secret for this instance.</summary>
        /// <param name="secret">Secret.</param>
        public void SetSecret(string secret)
        {
            _Secret = secret;
        }
    }
}
