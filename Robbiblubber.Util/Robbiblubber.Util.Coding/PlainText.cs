﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class provides a plain text encoder.</summary>
    public sealed class PlainText: IEncoder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Singleton instance.</summary>
        private static PlainText _Instance = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public PlainText()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoding">Encoding.</param>
        public PlainText(Encoding encoding)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an instance for this algorithm.</summary>
        public static PlainText Instance
        {
            get
            {
                if(_Instance == null) { _Instance = new PlainText(); }

                return _Instance;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEncoder                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the base encoding for this instance.</summary>
        public Encoding Encoding
        {
            get; private set;
        } = Encoding.UTF8;


        /// <summary>Converts bytes to an encoded string.</summary>
        /// <param name="value">Byte list.</param>
        /// <returns>Encoded string.</returns>
        public string EncodeBytes(IEnumerable<byte> value)
        {
            return Encoding.GetString(value.ToArray());
        }


        /// <summary>Converts characters to an encoded string.</summary>
        /// <param name="value">Character list.</param>
        /// <returns>Encoded string.</returns>
        public string EncodeChars(IEnumerable<char> value)
        {
            return new string(value.ToArray());
        }


        /// <summary>Converts a string to an encoded string.</summary>
        /// <param name="value">String.</param>
        /// <returns>Encoded string.</returns>
        public string EncodeString(string value)
        {
            return value;
        }


        /// <summary>Converts an integer to an encoded string.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Encoded string.</returns>
        public string EncodeInt(int value)
        {
            return value.ToString();
        }


        /// <summary>Converts a long integer to an encoded string.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Encoded string.</returns>
        public string EncodeLong(long value)
        {
            return value.ToString();
        }


        /// <summary>Converts a double to an encoded string.</summary>
        /// <param name="value">Double.</param>
        /// <returns>Encoded string.</returns>
        public string EncodeDouble(double value)
        {
            return value.ToString().Replace(NumberFormatInfo.CurrentInfo.NumberDecimalSeparator, ".");
        }


        /// <summary>Converts an encoded string to a byte array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <param name="len">Array length.</param>
        /// <returns>Decoded byte array.</returns>
        public byte[] DecodeToBytes(string s, int len)
        {
            byte[] rval = new byte[len];
            byte[] data = DecodeToBytes(s);

            Array.Copy(data, 0, rval, rval.Length - data.Length, data.Length);
            return rval;
        }


        /// <summary>Converts an encoded string to a byte array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded byte array.</returns>
        public byte[] DecodeToBytes(string s)
        {
            return Encoding.GetBytes(s);
        }


        /// <summary>Converts an encoded string to a char array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded char array.</returns>
        public char[] DecodeToChars(string s)
        {
            return s.ToCharArray();
        }


        /// <summary>Converts an encoded string to a string.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded string.</returns>
        public string DecodeToString(string s)
        {
            return s;
        }


        /// <summary>Converts an encoded string to an integer.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded integer.</returns>
        public int DecodeToInt(string s)
        {
            return int.Parse(s);
        }


        /// <summary>Converts an encoded string to a long integer.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded long integer.</returns>
        public long DecodeToLong(string s)
        {
            return long.Parse(s);
        }


        /// <summary>Converts an encoded string to a double.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded double.</returns>
        public double DecodeToDouble(string s)
        {
            return double.Parse(s.Replace(".", NumberFormatInfo.CurrentInfo.NumberDecimalSeparator));
        }


        /// <summary>Encodes a byte array.</summary>
        /// <param name="data">Source data.</param>
        /// <returns>Encoded data.</returns>
        public byte[] Encode(byte[] data)
        {
            return data;
        }


        /// <summary>Decodes a byte array.</summary>
        /// <param name="data">Encoded source data.</param>
        /// <returns>Decoded data.</returns>
        public byte[] Decode(byte[] data)
        {
            return data;
        }
    }
}
