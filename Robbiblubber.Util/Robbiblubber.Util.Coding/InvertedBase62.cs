﻿using System;
using System.Collections.Generic;
using System.Text;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class provides Base62 support with inverted alphabet.</summary>
    public sealed class InvertedBase62: BaseN, IEncoder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Inverted Base62 alphabet.</summary>
        public const string ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Singleton instance.</summary>
        private static InvertedBase62 _Instance = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public InvertedBase62(): base(62, ALPHABET)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        public InvertedBase62(Encoding encoding): base(62, ALPHABET, encoding)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an instance for this algorithm.</summary>
        public static InvertedBase62 Instance
        {
            get
            {
                if(_Instance == null) { _Instance = new InvertedBase62(); }

                return _Instance;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Converts bytes to a Base62-encoded string.</summary>
        /// <param name="value">Byte list.</param>
        /// <returns>Encoded string.</returns>
        public static string FromBytes(IEnumerable<byte> value)
        {
            return Instance.EncodeBytes(value);
        }


        /// <summary>Converts characters to a Base62-encoded string.</summary>
        /// <param name="value">Character list.</param>
        /// <returns>Encoded string.</returns>
        public static string FromChars(IEnumerable<char> value)
        {
            return Instance.EncodeChars(value);
        }


        /// <summary>Converts a string to a Base62-encoded string.</summary>
        /// <param name="value">String.</param>
        /// <returns>Encoded string.</returns>
        public static string FromString(string value)
        {
            return Instance.EncodeString(value);
        }


        /// <summary>Converts an integer to a Base62-encoded string.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Encoded string.</returns>
        public static string FromInt(int value)
        {
            return Instance.EncodeInt(value);
        }


        /// <summary>Converts a long integer to a Base62-encoded string.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Encoded string.</returns>
        public static string FromLong(long value)
        {
            return Instance.EncodeLong(value);
        }


        /// <summary>Converts a double to a Base62-encoded string.</summary>
        /// <param name="value">Double.</param>
        /// <returns>Encoded string.</returns>
        public static string FromDouble(double value)
        {
            return Instance.EncodeDouble(value);
        }


        /// <summary>Reads a file to a Base62-encoded string.</summary>
        /// <param name="value">String.</param>
        /// <returns>Encoded string.</returns>
        public static string FromFile(string value)
        {
            return Instance.EncodeFile(value);
        }


        /// <summary>Decodes a Base62-encoded string to a byte array.</summary>
        /// <param name="s">String.</param>
        /// <returns>Byte array.</returns>
        public static byte[] ToBytes(string s)
        {
            return Instance.DecodeToBytes(s);
        }


        /// <summary>Decodes a Base62-encoded string to a char array.</summary>
        /// <param name="s">String.</param>
        /// <returns>Char array.</returns>
        public static char[] ToChars(string s)
        {
            return Instance.DecodeToChars(s);
        }


        /// <summary>Decodes a Base62-encoded string to a string.</summary>
        /// <param name="s">String.</param>
        /// <returns>String.</returns>
        public static string ToString(string s)
        {
            return Instance.DecodeToString(s);
        }


        /// <summary>Decodes a Base62-encoded string to an integer.</summary>
        /// <param name="s">String.</param>
        /// <returns>Integer.</returns>
        public static int ToInt(string s)
        {
            return Instance.DecodeToInt(s);
        }


        /// <summary>Decodes a Base62-encoded string to a long integer.</summary>
        /// <param name="s">String.</param>
        /// <returns>Long integer.</returns>
        public static long ToLong(string s)
        {
            return Instance.DecodeToLong(s);
        }


        /// <summary>Decodes a Base62-encoded string to a double.</summary>
        /// <param name="s">String.</param>
        /// <returns>Double.</returns>
        public static double ToDouble(string s)
        {
            return Instance.DecodeToDouble(s);
        }
    }
}
