﻿using System;
using System.Text;
using System.Security.Cryptography;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class implements the SHA1 algorithm.</summary>
    public sealed class SHA1: __Hash, IHash
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Instance using Base64 encoding.</summary>
        public static readonly SHA1 BASE64 = new SHA1(Base64.Instance);

        /// <summary>Instance using Base62 encoding.</summary>
        public static readonly SHA1 BASE62 = new SHA1(Base62.Instance);

        /// <summary>Instance using inverted Base62 encoding.</summary>
        public static readonly SHA1 INVERTED_BASE62 = new SHA1(InvertedBase62.Instance);

        /// <summary>Instance using hexadecimal encoding.</summary>
        public static readonly SHA1 HEX = new SHA1(Hex.Instance);

        /// <summary>Instance using hexadecimal encoding.</summary>
        public static readonly SHA1 PLAINTEXT = new SHA1(PlainText.Instance);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        public SHA1(IEncoder encoder = null): base(encoder)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a hash value.</summary>
        /// <param name="value">Input expression.</param>
        /// <param name="encoder">Output encoder.</param>
        /// <returns>Hash value.</returns>
        public static string GetHash(string value, IEncoder encoder = null)
        {
            return new SHA1(encoder).HashString(value);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __Hash                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Hashes a byte array.</summary>
        /// <param name="data">Byte array.</param>
        /// <returns>Hash.</returns>
        public override byte[] Hash(byte[] data)
        {
            using(SHA1Managed sha1 = new SHA1Managed())
            {
                return sha1.ComputeHash(data);
            }
        }
    }
}
