﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Util.Coding
{
    /// <summary>Classes that provide hash algorithms implement this interface.</summary>
    public interface IHash
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the encoder for this instance.</summary>
        IEncoder Encoder { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Hashes a byte array.</summary>
        /// <param name="value">Byte array.</param>
        /// <returns>Hash.</returns>
        string HashBytes(IEnumerable<byte> value);


        /// <summary>Hashes a character array.</summary>
        /// <param name="value">Character array.</param>
        /// <returns>Hash.</returns>
        string HashChars(IEnumerable<char> value);


        /// <summary>Hashes a string value.</summary>
        /// <param name="value">String.</param>
        /// <returns>Hash.</returns>
        string HashString(string value);


        /// <summary>Hashes an integer value.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Hash.</returns>
        string HashInt(int value);


        /// <summary>Hashes a long integer value.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Hash.</returns>
        string HashLong(int value);


        /// <summary>Hashes a double value.</summary>
        /// <param name="value">Double.</param>
        /// <returns>Hash.</returns>
        string HashDouble(int value);


        /// <summary>Hashes a byte array.</summary>
        /// <param name="data">Byte array.</param>
        /// <returns>Hash.</returns>
        byte[] Hash(byte[] data);
    }
}
