﻿using System;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class provides extension methods for encoding and decoding.</summary>
    public static class CodingExtensions
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Returns a Base62-encoded representation of this string.</summary>
        /// <param name="value">String.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToBase62(this string value)
        {
            return Base62.Instance.EncodeString(value);
        }


        /// <summary>Returns a Base62-encoded representation of this byte array.</summary>
        /// <param name="value">Byte array.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToBase62(this byte[] value)
        {
            return Base62.Instance.EncodeBytes(value);
        }


        /// <summary>Returns a Base62-encoded representation of this char array.</summary>
        /// <param name="value">Char array.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToBase62(this char[] value)
        {
            return Base62.Instance.EncodeChars(value);
        }


        /// <summary>Returns a Base62-encoded representation of this integer.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToBase62(this int value)
        {
            return Base62.Instance.EncodeInt(value);
        }


        /// <summary>Returns a Base62-encoded representation of this long integer.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToBase62(this long value)
        {
            return Base62.Instance.EncodeLong(value);
        }


        /// <summary>Returns a Base62-encoded representation of this double.</summary>
        /// <param name="value">Double.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToBase62(this double value)
        {
            return Base62.Instance.EncodeDouble(value);
        }


        /// <summary>Returns a string for a Base62-encoded string.</summary>
        /// <param name="value">String.</param>
        /// <returns>String.</returns>
        public static string FromBase62(this string value)
        {
            return Base62.Instance.DecodeToString(value);
        }


        /// <summary>Returns a value for a Base62-encoded string.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="value">String.</param>
        /// <returns>Value.</returns>
        public static T FromBase62<T>(this string value)
        {
            if(typeof(T) == typeof(string)) { return (T)(object) Base62.Instance.DecodeToString(value); }
            if(typeof(T).Name.ToLower() == "byte[]") { return (T)(object) Base62.Instance.DecodeToBytes(value); }
            if(typeof(T).Name.ToLower() == "char[]") { return (T)(object) Base62.Instance.DecodeToChars(value); }
            if(typeof(T) == typeof(short)) { return (T)(object) Base62.Instance.DecodeToInt(value); }
            if(typeof(T) == typeof(int)) { return (T)(object) Base62.Instance.DecodeToInt(value); }
            if(typeof(T) == typeof(long)) { return (T)(object) Base62.Instance.DecodeToLong(value); }
            if(typeof(T) == typeof(double)) { return (T)(object) Base62.Instance.DecodeToDouble(value); }
            if(typeof(T) == typeof(float)) { return (T)(object) Convert.ToSingle(Base62.Instance.DecodeToDouble(value)); }

            throw new ArgumentException("Cannot decode Base62 string to type " + typeof(T).Name + ".");
        }


        /// <summary>Returns an inverted Base62-encoded representation of this string.</summary>
        /// <param name="value">String.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToInvertedBase62(this string value)
        {
            return InvertedBase62.Instance.EncodeString(value);
        }


        /// <summary>Returns an inverted Base62-encoded representation of this byte array.</summary>
        /// <param name="value">Byte array.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToInvertedBase62(this byte[] value)
        {
            return InvertedBase62.Instance.EncodeBytes(value);
        }


        /// <summary>Returns an inverted Base62-encoded representation of this char array.</summary>
        /// <param name="value">Char array.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToInvertedBase62(this char[] value)
        {
            return InvertedBase62.Instance.EncodeChars(value);
        }


        /// <summary>Returns an inverted Base62-encoded representation of this integer.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToInvertedBase62(this int value)
        {
            return InvertedBase62.Instance.EncodeInt(value);
        }


        /// <summary>Returns a Base62-encoded representation of this long integer.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToInvertedBase62(this long value)
        {
            return InvertedBase62.Instance.EncodeLong(value);
        }


        /// <summary>Returns an inverted Base62-encoded representation of this double.</summary>
        /// <param name="value">Double.</param>
        /// <returns>Base62-encoded string.</returns>
        public static string ToInvertedBase62(this double value)
        {
            return InvertedBase62.Instance.EncodeDouble(value);
        }


        /// <summary>Returns a string for an inverted Base62-encoded string.</summary>
        /// <param name="value">String.</param>
        /// <returns>String.</returns>
        public static string FromInvertedBase62(this string value)
        {
            return InvertedBase62.Instance.DecodeToString(value);
        }


        /// <summary>Returns a value for an inverted Base62-encoded string.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="value">String.</param>
        /// <returns>Value.</returns>
        public static T FromInvertedBase62<T>(this string value)
        {
            if(typeof(T) == typeof(string)) { return (T)(object) InvertedBase62.Instance.DecodeToString(value); }
            if(typeof(T).Name.ToLower() == "byte[]") { return (T)(object) InvertedBase62.Instance.DecodeToBytes(value); }
            if(typeof(T).Name.ToLower() == "char[]") { return (T)(object) InvertedBase62.Instance.DecodeToChars(value); }
            if(typeof(T) == typeof(short)) { return (T)(object) InvertedBase62.Instance.DecodeToInt(value); }
            if(typeof(T) == typeof(int)) { return (T)(object) InvertedBase62.Instance.DecodeToInt(value); }
            if(typeof(T) == typeof(long)) { return (T)(object) InvertedBase62.Instance.DecodeToLong(value); }
            if(typeof(T) == typeof(double)) { return (T)(object) InvertedBase62.Instance.DecodeToDouble(value); }
            if(typeof(T) == typeof(float)) { return (T)(object) Convert.ToSingle(InvertedBase62.Instance.DecodeToDouble(value)); }

            throw new ArgumentException("Cannot decode inverted Base62 string to type " + typeof(T).Name + ".");
        }


        /// <summary>Returns a Base64-encoded representation of this string.</summary>
        /// <param name="value">String.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Base64-encoded string.</returns>
        public static string ToBase64(this string value, bool pad = false)
        {
            return Base64.Instance.EncodeString(value, pad);
        }


        /// <summary>Returns a Base64-encoded representation of this byte array.</summary>
        /// <param name="value">Byte array.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Base64-encoded string.</returns>
        public static string ToBase64(this byte[] value, bool pad = false)
        {
            return Base64.Instance.EncodeBytes(value, pad);
        }


        /// <summary>Returns a Base64-encoded representation of this char array.</summary>
        /// <param name="value">Char array.</param>
        /// <param name="pad">Add padding characters.</param>
        /// <returns>Base64-encoded string.</returns>
        public static string ToBase64(this char[] value, bool pad = false)
        {
            return Base64.Instance.EncodeChars(value, pad);
        }


        /// <summary>Returns a Base64-encoded representation of this integer.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Base64-encoded string.</returns>
        public static string ToBase64(this int value)
        {
            return Base64.Instance.EncodeInt(value);
        }


        /// <summary>Returns a Base64-encoded representation of this long integer.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Base64-encoded string.</returns>
        public static string ToBase64(this long value)
        {
            return Base64.Instance.EncodeLong(value);
        }


        /// <summary>Returns a Base64-encoded representation of this double.</summary>
        /// <param name="value">Double.</param>
        /// <returns>Base64-encoded string.</returns>
        public static string ToBase64(this double value)
        {
            return Base64.Instance.EncodeDouble(value);
        }


        /// <summary>Returns a string for a Base64-encoded string.</summary>
        /// <param name="value">String.</param>
        /// <returns>String.</returns>
        public static string FromBase64(this string value)
        {
            return Base64.Instance.DecodeToString(value);
        }


        /// <summary>Returns a value for a Base64-encoded string.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="value">String.</param>
        /// <returns>Value.</returns>
        public static T FromBase64<T>(this string value)
        {
            if(typeof(T) == typeof(string)) { return (T)(object) Base64.Instance.DecodeToString(value); }
            if(typeof(T).Name.ToLower() == "byte[]") { return (T)(object) Base64.Instance.DecodeToBytes(value); }
            if(typeof(T).Name.ToLower() == "char[]") { return (T)(object) Base64.Instance.DecodeToChars(value); }
            if(typeof(T) == typeof(short)) { return (T)(object) Base64.Instance.DecodeToInt(value); }
            if(typeof(T) == typeof(int)) { return (T)(object) Base64.Instance.DecodeToInt(value); }
            if(typeof(T) == typeof(long)) { return (T)(object) Base64.Instance.DecodeToLong(value); }
            if(typeof(T) == typeof(double)) { return (T)(object) Base64.Instance.DecodeToDouble(value); }
            if(typeof(T) == typeof(float)) { return (T)(object) Convert.ToSingle(Base64.Instance.DecodeToDouble(value)); }

            throw new ArgumentException("Cannot decode Base64 string to type " + typeof(T).Name + ".");
        }


        /// <summary>Returns a hexadecimal representation of this string.</summary>
        /// <param name="value">String.</param>
        /// <returns>Hexadecimal string.</returns>
        public static string ToHex(this string value)
        {
            return Hex.Instance.EncodeString(value);
        }


        /// <summary>Returns a hexadecimal representation of this byte array.</summary>
        /// <param name="value">Byte array.</param>
        /// <returns>Hexadecimal string.</returns>
        public static string ToHex(this byte[] value)
        {
            return Hex.Instance.EncodeBytes(value);
        }


        /// <summary>Returns a hexadecimal representation of this char array.</summary>
        /// <param name="value">Char array.</param>
        /// <returns>Hexadecimal string.</returns>
        public static string ToHex(this char[] value)
        {
            return Hex.Instance.EncodeChars(value);
        }


        /// <summary>Returns a hexadecimal representation of this integer.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Hexadecimal string.</returns>
        public static string ToHex(this int value)
        {
            return Hex.Instance.EncodeInt(value);
        }


        /// <summary>Returns a hexadecimal representation of this long integer.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Hexadecimal string.</returns>
        public static string ToHex(this long value)
        {
            return Hex.Instance.EncodeLong(value);
        }


        /// <summary>Returns a hexadecimal representation of this double.</summary>
        /// <param name="value">Double.</param>
        /// <returns>hexadecimal Hstring.</returns>
        public static string ToHex(this double value)
        {
            return Hex.Instance.EncodeDouble(value);
        }


        /// <summary>Returns a string for a hexadecimal string.</summary>
        /// <param name="value">String.</param>
        /// <returns>String.</returns>
        public static string FromHex(this string value)
        {
            return Hex.Instance.DecodeToString(value);
        }


        /// <summary>Returns a value for a hexadecimal string.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="value">String.</param>
        /// <returns>Value.</returns>
        public static T FromHex<T>(this string value)
        {
            if(typeof(T) == typeof(string)) { return (T)(object) Base64.Instance.DecodeToString(value); }
            if(typeof(T).Name.ToLower() == "byte[]") { return (T)(object) Base64.Instance.DecodeToBytes(value); }
            if(typeof(T).Name.ToLower() == "char[]") { return (T)(object) Base64.Instance.DecodeToChars(value); }
            if(typeof(T) == typeof(short)) { return (T)(object) Base64.Instance.DecodeToInt(value); }
            if(typeof(T) == typeof(int)) { return (T)(object) Base64.Instance.DecodeToInt(value); }
            if(typeof(T) == typeof(long)) { return (T)(object) Base64.Instance.DecodeToLong(value); }
            if(typeof(T) == typeof(double)) { return (T)(object) Base64.Instance.DecodeToDouble(value); }
            if(typeof(T) == typeof(float)) { return (T)(object) Convert.ToSingle(Base64.Instance.DecodeToDouble(value)); }

            throw new ArgumentException("Cannot decode hexadecimal string to type " + typeof(T).Name + ".");
        }


        /// <summary>Compresses a string.</summary>
        /// <param name="text">Input text.</param>
        /// <returns>Compressed string.</returns>
        public static string Compress(this string text)
        {
            return GZip.Compress(text, InvertedBase62.Instance);
        }


        /// <summary>Decompresses a string.</summary>
        /// <param name="data">Compressed data.</param>
        /// <returns>String.</returns>
        public static string Decompress(this string data)
        {
            return GZip.Decompress(data, InvertedBase62.Instance);
        }
    }
}
