﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class provides an generic implementation of a BaseN algorithm.</summary>
    /// <remarks>Part of the code is based on the work of Daniel Destouche (https://github.com/ghost1face/base62)
    ///          published under MIT License (see https://github.com/ghost1face/base62/blob/master/LICENSE/). </remarks>
    public class BaseN: __Encoder, IEncoder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Base.</summary>
        protected int _Base = 92;
        
        /// <summary>Alphabet.</summary>
        protected string _Alphabet = @" !#$%&()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_abcdefghijklmnopqrstuvwxyz{|}~";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="base">Base.</param>
        public BaseN(int @base): base()
        {
            _Base = @base;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="base">Base.</param>
        /// <param name="alphabet">Alphabet.</param>
        public BaseN(int @base, string alphabet): base()
        {
            _Base = @base;
            if(alphabet != null) { _Alphabet = alphabet; }
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="base">Base.</param>
        /// <param name="alphabet">Alphabet.</param>
        /// <param name="encoding">Encoding.</param>
        public BaseN(int @base, string alphabet, Encoding encoding): base(encoding)
        {
            _Base = @base;
            if(alphabet != null) { _Alphabet = alphabet; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static methods                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Encodes or decodes data.</summary>
        /// <param name="data">Data.</param>
        /// <param name="inBase">Input base.</param>
        /// <param name="outBase">Output base.</param>
        /// <returns>Converted byte array.</returns>
        protected static byte[] _Convert(byte[] data, int inBase, int outBase)
        {
            List<byte> rval = new List<byte>();
            int count;

            while((count = data.Length) > 0)
            {
                List<byte> q = new List<byte>();
                byte rem = 0;

                for(int i = 0; i < count; i++)
                {
                    int acc = data[i] + (rem * inBase);
                    byte d = (byte) ((acc - (acc % outBase)) / outBase);
                    rem = (byte) (acc % outBase);

                    if((q.Count > 0) || (d != 0)) { q.Add(d); }
                }

                rval.Insert(0, rem);
                data = q.ToArray();
            }

            return rval.ToArray();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __Encoder                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Converts bytes to an encoded string.</summary>
        /// <param name="value">Byte list.</param>
        /// <returns>Encoded string.</returns>
        public override string EncodeBytes(IEnumerable<byte> value)
        {
            StringBuilder rval = new StringBuilder();

            foreach(byte i in _Convert(value.ToArray(), 256, _Base)) { rval.Append(_Alphabet[i]); }
            return rval.ToString();
        }


        /// <summary>Converts an encoded string to a byte array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded byte array.</returns>
        public override byte[] DecodeToBytes(string s)
        {
            List<byte> rval = new List<byte>();

            foreach(char i in s)
            {
                rval.Add((byte) _Alphabet.IndexOf(i));
            }

            return _Convert(rval.ToArray(), _Base, 256);
        }


        /// <summary>Encodes a byte array.</summary>
        /// <param name="data">Source data.</param>
        /// <returns>Encoded data.</returns>
        public override byte[] Encode(byte[] data)
        {
            return Encoding.GetBytes(EncodeBytes(data));
        }


        /// <summary>Decodes a byte array.</summary>
        /// <param name="data">Encoded source data.</param>
        /// <returns>Decoded data.</returns>
        public override byte[] Decode(byte[] data)
        {
            return DecodeToBytes(Encoding.GetString(data));
        }
    }
}
