﻿using System;
using System.Collections.Generic;
using System.Text;



namespace Robbiblubber.Util.Coding
{
    /// <summary>Classes that provide encoding and decoding implement this interface.</summary>
    public interface IEncoder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the encoding for this instance.</summary>
        Encoding Encoding
        {
            get;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Converts bytes to an encoded string.</summary>
        /// <param name="value">Byte list.</param>
        /// <returns>Encoded string.</returns>
        string EncodeBytes(IEnumerable<byte> value);


        /// <summary>Converts characters to an encoded string.</summary>
        /// <param name="value">Character list.</param>
        /// <returns>Encoded string.</returns>
        string EncodeChars(IEnumerable<char> value);


        /// <summary>Converts a string to an encoded string.</summary>
        /// <param name="value">String.</param>
        /// <returns>Encoded string.</returns>
        string EncodeString(string value);


        /// <summary>Converts an integer to an encoded string.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Encoded string.</returns>
        string EncodeInt(int value);


        /// <summary>Converts a long integer to an encoded string.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Encoded string.</returns>
        string EncodeLong(long value);


        /// <summary>Converts a double to an encoded string.</summary>
        /// <param name="value">Double.</param>
        /// <returns>Encoded string.</returns>
        string EncodeDouble(double value);


        /// <summary>Converts an encoded string to a byte array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <param name="len">Array length.</param>
        /// <returns>Decoded byte array.</returns>
        byte[] DecodeToBytes(string s, int len);


        /// <summary>Converts an encoded string to a byte array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded byte array.</returns>
        byte[] DecodeToBytes(string s);


        /// <summary>Converts an encoded string to a char array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded char array.</returns>
        char[] DecodeToChars(string s);


        /// <summary>Converts an encoded string to a string.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded string.</returns>
        string DecodeToString(string s);


        /// <summary>Converts an encoded string to an integer.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded integer.</returns>
        int DecodeToInt(string s);


        /// <summary>Converts an encoded string to a long integer.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded long integer.</returns>
        long DecodeToLong(string s);


        /// <summary>Converts an encoded string to a double.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded double.</returns>
        double DecodeToDouble(string s);


        /// <summary>Encodes a byte array.</summary>
        /// <param name="data">Source data.</param>
        /// <returns>Encoded data.</returns>
        byte[] Encode(byte[] data);


        /// <summary>Decodes a byte array.</summary>
        /// <param name="data">Encoded source data.</param>
        /// <returns>Decoded data.</returns>
        byte[] Decode(byte[] data);
    }
}
