﻿using System;



namespace Robbiblubber.Util.Coding
{
    /// <summary>Classes that provide encryption implement this interface.</summary>
    public interface IEncrypter: IEncoder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the encoder for this instance.</summary>
        IEncoder Encoder { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets the secret for this instance.</summary>
        /// <param name="secret">Secret.</param>
        void SetSecret(string secret);
    }
}
