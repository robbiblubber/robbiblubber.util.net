﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class provides a base implementation for encoders.</summary>
    public abstract class __Encoder: IEncoder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        protected __Encoder()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoding">Encoding.</param>
        protected __Encoder(Encoding encoding)
        {
            if(encoding != null) { Encoding = encoding; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reads a (binary) file into an encoded string.</summary>
        /// <param name="fileName">File name.</param>
        /// <returns>Encoded string.</returns>
        public string EncodeFile(string fileName)
        {
            return EncodeBytes(File.ReadAllBytes(fileName));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEncoder                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the base encoding for this instance.</summary>
        public virtual Encoding Encoding
        {
            get; protected set;
        } = Encoding.UTF8;


        /// <summary>Converts bytes to an encoded string.</summary>
        /// <param name="value">Byte list.</param>
        /// <returns>Encoded string.</returns>
        public virtual string EncodeBytes(IEnumerable<byte> value)
        {
            return Encoding.GetString(Encode(value.ToArray()));
        }


        /// <summary>Converts characters to an encoded string.</summary>
        /// <param name="value">Character list.</param>
        /// <returns>Encoded string.</returns>
        public virtual string EncodeChars(IEnumerable<char> value)
        {
            return EncodeBytes(Encoding.GetBytes(value.ToArray()));
        }


        /// <summary>Converts a string to an encoded string.</summary>
        /// <param name="value">String.</param>
        /// <returns>Encoded string.</returns>
        public virtual string EncodeString(string value)
        {
            return EncodeBytes(Encoding.GetBytes(value));
        }


        /// <summary>Converts an integer to an encoded string.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Encoded string.</returns>
        public virtual string EncodeInt(int value)
        {
            return EncodeBytes(BitConverter.GetBytes(value));
        }


        /// <summary>Converts a long integer to an encoded string.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Encoded string.</returns>
        public virtual string EncodeLong(long value)
        {
            return EncodeBytes(BitConverter.GetBytes(value));
        }


        /// <summary>Converts a double to an encoded string.</summary>
        /// <param name="value">Double.</param>
        /// <returns>Encoded string.</returns>
        public virtual string EncodeDouble(double value)
        {
            return EncodeBytes(BitConverter.GetBytes(value));
        }


        /// <summary>Converts an encoded string to a byte array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <param name="len">Array length.</param>
        /// <returns>Decoded byte array.</returns>
        public virtual byte[] DecodeToBytes(string s, int len)
        {
            byte[] rval = new byte[len];
            byte[] data = DecodeToBytes(s);

            Array.Copy(data, 0, rval, rval.Length - data.Length, data.Length);
            return rval;
        }


        /// <summary>Converts an encoded string to a byte array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded byte array.</returns>
        public virtual byte[] DecodeToBytes(string s)
        {
            return Decode(Encoding.GetBytes(s));
        }


        /// <summary>Converts an encoded string to a char array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded char array.</returns>
        public virtual char[] DecodeToChars(string s)
        {
            return DecodeToString(s).ToArray();
        }


        /// <summary>Converts an encoded string to a string.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded string.</returns>
        public virtual string DecodeToString(string s)
        {
            return Encoding.GetString(DecodeToBytes(s));
        }


        /// <summary>Converts an encoded string to an integer.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded integer.</returns>
        public virtual int DecodeToInt(string s)
        {
            return BitConverter.ToInt32(DecodeToBytes(s, 4), 0);
        }


        /// <summary>Converts an encoded string to a long integer.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded long integer.</returns>
        public virtual long DecodeToLong(string s)
        {
            return BitConverter.ToInt64(DecodeToBytes(s, 8), 0);
        }


        /// <summary>Converts an encoded string to a double.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded double.</returns>
        public virtual double DecodeToDouble(string s)
        {
            return BitConverter.ToDouble(DecodeToBytes(s, 8), 0);
        }


        /// <summary>Encodes a byte array.</summary>
        /// <param name="data">Source data.</param>
        /// <returns>Encoded data.</returns>
        public abstract byte[] Encode(byte[] data);


        /// <summary>Decodes a byte array.</summary>
        /// <param name="data">Encoded source data.</param>
        /// <returns>Decoded data.</returns>
        public abstract byte[] Decode(byte[] data);
    }
}
