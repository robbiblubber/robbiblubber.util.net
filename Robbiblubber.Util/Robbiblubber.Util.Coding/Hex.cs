﻿using System;
using System.Collections.Generic;
using System.Text;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class provides hexadecimal encoding methods.</summary>
    public sealed class Hex: __Encoder, IEncoder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>The hexadecimal alphabet.</summary>
        public const string ALPHABET = "0123456789abcdef";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Singleton instance.</summary>
        private static Hex _Instance = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public Hex(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoding">Encoding.</param>
        public Hex(Encoding encoding): base(encoding)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an instance for this algorithm.</summary>
        public static Hex Instance
        {
            get
            {
                if(_Instance == null) { _Instance = new Hex(); }

                return _Instance;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Converts bytes to a hexadecimal string.</summary>
        /// <param name="value">Byte list.</param>
        /// <returns>Encoded string.</returns>
        public static string FromBytes(IEnumerable<byte> value)
        {
            return Instance.EncodeBytes(value);
        }


        /// <summary>Converts characters to a hexadecimal string.</summary>
        /// <param name="value">Character list.</param>
        /// <returns>Encoded string.</returns>
        public static string FromChars(IEnumerable<char> value)
        {
            return Instance.EncodeChars(value);
        }


        /// <summary>Converts a string to a hexadecimal string.</summary>
        /// <param name="value">String.</param>
        /// <returns>Encoded string.</returns>
        public static string FromString(string value)
        {
            return Instance.EncodeString(value);
        }


        /// <summary>Converts an integer to a hexadecimal string.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Encoded string.</returns>
        public static string FromInt(int value)
        {
            return Instance.EncodeInt(value);
        }


        /// <summary>Converts a long integer to a hexadecimal string.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Encoded string.</returns>
        public static string FromLong(long value)
        {
            return Instance.EncodeLong(value);
        }


        /// <summary>Converts a double to a hexadecimal string.</summary>
        /// <param name="value">Double.</param>
        /// <returns>Encoded string.</returns>
        public static string FromDouble(double value)
        {
            return Instance.EncodeDouble(value);
        }


        /// <summary>Decodes a hexadecimal string to a byte array.</summary>
        /// <param name="s">String.</param>
        /// <returns>Byte array.</returns>
        public static byte[] ToBytes(string s)
        {
            return Instance.DecodeToBytes(s);
        }


        /// <summary>Decodes a hexadecimal string to a char array.</summary>
        /// <param name="s">String.</param>
        /// <returns>Char array.</returns>
        public static char[] ToChars(string s)
        {
            return Instance.DecodeToChars(s);
        }


        /// <summary>Decodes a hexadecimal string to a string.</summary>
        /// <param name="s">String.</param>
        /// <returns>String.</returns>
        public static string ToString(string s)
        {
            return Instance.DecodeToString(s);
        }


        /// <summary>Decodes a hexadecimal string to an integer.</summary>
        /// <param name="s">String.</param>
        /// <returns>Integer.</returns>
        public static int ToInt(string s)
        {
            return Instance.DecodeToInt(s);
        }


        /// <summary>Decodes a hexadecimal string to a long integer.</summary>
        /// <param name="s">String.</param>
        /// <returns>Long integer.</returns>
        public static long ToLong(string s)
        {
            return Instance.DecodeToLong(s);
        }


        /// <summary>Decodes a hexadecimals string to a double.</summary>
        /// <param name="s">String.</param>
        /// <returns>Double.</returns>
        public static double ToDouble(string s)
        {
            return Instance.DecodeToDouble(s);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a hexadecimal representation of a character.</summary>
        /// <param name="value">Character.</param>
        /// <param name="pad">Add padding character.</param>
        /// <returns>Hexadecimal representation.</returns>
        public string EncodeChar(char value, bool pad = true)
        {
            string rval = Convert.ToString((byte) value, 16);

            return (pad ? rval.PadLeft(2, '0') : rval);
        }


        /// <summary>Returns a hexadecimal representation of a byte.</summary>
        /// <param name="value">Byte.</param>
        /// <param name="pad">Add padding character.</param>
        /// <returns>Hexadecimal representation.</returns>
        public string EncodeByte(byte value, bool pad = true)
        {
            string rval = Convert.ToString(value, 16);

            return (pad ? rval.PadLeft(2, '0') : rval);
        }


        /// <summary>Returns a byte for a hexadecimal string.</summary>
        /// <param name="s">Hexadecimal string.</param>
        /// <returns>Byte.</returns>
        public byte DecodeToByte(string s)
        {
            return Convert.ToByte(s, 16);
        }


        /// <summary>Returns a character for a hexadecimal string.</summary>
        /// <param name="s">Hexadecimal string.</param>
        /// <returns>Character.</returns>
        public char DecodeToChar(string s)
        {
            return Convert.ToChar(Convert.ToByte(s, 16));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __Encoder                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Converts bytes to an encoded string.</summary>
        /// <param name="value">Byte list.</param>
        /// <returns>Encoded string.</returns>
        public override string EncodeBytes(IEnumerable<byte> value)
        {
            StringBuilder rval = new StringBuilder();

            foreach(byte i in value) { rval.Append(EncodeByte(i)); }
            return rval.ToString();
        }


        /// <summary>Converts an encoded string to a byte array.</summary>
        /// <param name="s">Encoded string.</param>
        /// <returns>Decoded byte array.</returns>
        public override byte[] DecodeToBytes(string s)
        {
            byte[] rval = new byte[s.Length / 2];

            for(int i = 0; i < s.Length; i += 2)
            {
                rval[i / 2] = DecodeToByte(s.Substring(i, 2));
            }

            return rval;
        }


        /// <summary>Encodes a byte array.</summary>
        /// <param name="data">Source data.</param>
        /// <returns>Encoded data.</returns>
        public override byte[] Encode(byte[] data)
        {
            return Encoding.GetBytes(EncodeBytes(data));
        }


        /// <summary>Decodes a byte array.</summary>
        /// <param name="data">Encoded source data.</param>
        /// <returns>Decoded data.</returns>
        public override byte[] Decode(byte[] data)
        {
            return DecodeToBytes(Encoding.GetString(data));
        }
    }
}
