﻿using System;
using System.Collections.Generic;
using System.Linq;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class provides a base implementation of the IHash interface.</summary>
    public abstract class __Hash: IHash
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        protected __Hash(IEncoder encoder)
        {
            Encoder = ((encoder == null) ? Hex.Instance : encoder);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IHash                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the encoder for this instance.</summary>
        public virtual IEncoder Encoder 
        { 
            get; protected set;
        }


        /// <summary>Hashes a byte array.</summary>
        /// <param name="value">Byte array.</param>
        /// <returns>Hash.</returns>
        public virtual string HashBytes(IEnumerable<byte> value)
        {
            return Encoder.EncodeBytes(Hash(value.ToArray()));
        }


        /// <summary>Hashes a character array.</summary>
        /// <param name="value">Character array.</param>
        /// <returns>Hash.</returns>
        public virtual string HashChars(IEnumerable<char> value)
        {
            return HashString(new string(value.ToArray()));
        }


        /// <summary>Hashes a string value.</summary>
        /// <param name="value">String.</param>
        /// <returns>Hash.</returns>
        public virtual string HashString(string value)
        {
            return Encoder.EncodeBytes(Hash(Encoder.Encoding.GetBytes(value)));
        }


        /// <summary>Hashes an integer value.</summary>
        /// <param name="value">Integer.</param>
        /// <returns>Hash.</returns>
        public virtual string HashInt(int value)
        {
            return Encoder.EncodeBytes(Hash(BitConverter.GetBytes(value)));
        }


        /// <summary>Hashes a long integer value.</summary>
        /// <param name="value">Long integer.</param>
        /// <returns>Hash.</returns>
        public virtual string HashLong(int value)
        {
            return Encoder.EncodeBytes(Hash(BitConverter.GetBytes(value)));
        }


        /// <summary>Hashes a double value.</summary>
        /// <param name="value">Double.</param>
        /// <returns>Hash.</returns>
        public virtual string HashDouble(int value)
        {
            return Encoder.EncodeBytes(Hash(BitConverter.GetBytes(value)));
        }


        /// <summary>Hashes a byte array.</summary>
        /// <param name="data">Byte array.</param>
        /// <returns>Hash.</returns>
        public abstract byte[] Hash(byte[] data);
    }
}
