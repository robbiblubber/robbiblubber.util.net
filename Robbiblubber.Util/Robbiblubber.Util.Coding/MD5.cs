﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class implements the MD5 algorithm.</summary>
    public sealed class MD5: __Hash
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Instance using Base64 encoding.</summary>
        public static readonly MD5 BASE64 = new MD5(Base64.Instance);

        /// <summary>Instance using Base62 encoding.</summary>
        public static readonly MD5 BASE62 = new MD5(Base62.Instance);

        /// <summary>Instance using inverted Base62 encoding.</summary>
        public static readonly MD5 INVERTED_BASE62 = new MD5(InvertedBase62.Instance);

        /// <summary>Instance using hexadecimal encoding.</summary>
        public static readonly MD5 HEX = new MD5(Hex.Instance);

        /// <summary>Instance using plain text encoding.</summary>
        public static readonly MD5 PLAINTEXT = new MD5(PlainText.Instance);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        public MD5(IEncoder encoder = null): base(encoder)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a hash value.</summary>
        /// <param name="value">Input expression.</param>
        /// <param name="encoder">Output encoder.</param>
        /// <returns>Hash value.</returns>
        public static string GetHash(string value, IEncoder encoder = null)
        {
            return new MD5(encoder).HashString(value);
        }


        /// <summary>Returns a hash value for a file.</summary>
        /// <param name="fileName">File name.</param>
        /// <param name="withUnixBreaks">Determies if unix line breaks will be enforced in file.</param>
        /// <returns>Hash value.</returns>
        public static string GetFileHash(string fileName, bool withUnixBreaks = false)
        {
            string content = File.ReadAllText(fileName, Encoding.UTF8);            
            if(withUnixBreaks) { content = content.Replace("\r\n", "\n"); }

            return GetHash(content);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __Hash                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Hashes a byte array.</summary>
        /// <param name="data">Byte array.</param>
        /// <returns>Hash.</returns>
        public override byte[] Hash(byte[] data)
        {
            using(System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                return md5.ComputeHash(data);
            }
        }
    }
}
