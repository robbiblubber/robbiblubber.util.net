﻿using System;
using System.Text;
using System.Security.Cryptography;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class implements the SHA256 algorithm.</summary>
    public sealed class SHA256: __Hash, IHash
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Instance using Base64 encoding.</summary>
        public static readonly SHA256 BASE64 = new SHA256(Base64.Instance);

        /// <summary>Instance using Base62 encoding.</summary>
        public static readonly SHA256 BASE62 = new SHA256(Base62.Instance);

        /// <summary>Instance using inverted Base62 encoding.</summary>
        public static readonly SHA256 INVERTED_BASE62 = new SHA256(InvertedBase62.Instance);

        /// <summary>Instance using hexadecimal encoding.</summary>
        public static readonly SHA256 HEX = new SHA256(Hex.Instance);

        /// <summary>Instance using hexadecimal encoding.</summary>
        public static readonly SHA256 PLAINTEXT = new SHA256(PlainText.Instance);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        public SHA256(IEncoder encoder = null): base(encoder)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a hash value.</summary>
        /// <param name="value">Input expression.</param>
        /// <param name="encoder">Output encoder.</param>
        /// <returns>Hash value.</returns>
        public static string GetHash(string value, IEncoder encoder = null)
        {
            return new SHA256(encoder).HashString(value);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __Hash                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Hashes a byte array.</summary>
        /// <param name="data">Byte array.</param>
        /// <returns>Hash.</returns>
        public override byte[] Hash(byte[] data)
        {
            using(SHA256Managed sha256 = new SHA256Managed())
            {
                return sha256.ComputeHash(data);
            }
        }
    }
}
