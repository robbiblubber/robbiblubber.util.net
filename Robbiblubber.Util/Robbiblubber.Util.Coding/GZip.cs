﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;



namespace Robbiblubber.Util.Coding
{
    /// <summary>This class provides gzip support.</summary>
    public sealed class GZip: __Encoder, IEncoder
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Instance using Base64 encoding.</summary>
        public static readonly GZip BASE64 = new GZip(Base64.Instance);
        
        /// <summary>Instance using Base62 encoding.</summary>
        public static readonly GZip BASE62 = new GZip(Base62.Instance);
        
        /// <summary>Instance using inverted Base62 encoding.</summary>
        public static readonly GZip INVERTED_BASE62 = new GZip(InvertedBase62.Instance);

        /// <summary>Instance using hexadecimal encoding.</summary>
        public static readonly GZip HEX = new GZip(Hex.Instance);

        /// <summary>Instance using hexadecimal encoding.</summary>
        public static readonly GZip PLAINTEXT = new GZip(PlainText.Instance);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public GZip()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        public GZip(IEncoder encoder)
        {
            Encoder = encoder;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Compresses a byte array.</summary>
        /// <param name="data">Source data.</param>
        /// <returns>Compresses data.</returns>
        public static byte[] Compress(byte[] data)
        {
            using(var outStream = new MemoryStream())
            {
                using(var gzStream = new GZipStream(outStream, CompressionMode.Compress))
                using(var inStream = new MemoryStream(data))
                {
                    inStream.CopyTo(gzStream);
                }

                return outStream.ToArray();
            }
        }


        /// <summary>Decompresses a byte array.</summary>
        /// <param name="data">Encoded source data.</param>
        /// <returns>Decompressed data.</returns>
        public static byte[] Decompress(byte[] data)
        {
            using(MemoryStream outStream = new MemoryStream())
            using(MemoryStream inStream = new MemoryStream(data))
            using(Stream gzStream = new GZipStream(inStream, CompressionMode.Decompress))
            {
                gzStream.CopyTo(outStream);
                return outStream.ToArray();
            }
        }


        /// <summary>Encodes a byte array.</summary>
        /// <param name="data">Source data.</param>
        /// <param name="encoder">Encoder.</param>
        /// <returns>Encoded data.</returns>
        public static byte[] Compress(byte[] data, IEncoder encoder)
        {
            return new GZip(encoder).Encode(data);
        }


        /// <summary>Decodes a byte array.</summary>
        /// <param name="data">Encoded source data.</param>
        /// <param name="encoder">Encoder.</param>
        /// <returns>Decoded data.</returns>
        public static byte[] Decompress(byte[] data, IEncoder encoder)
        {
            return new GZip(encoder).Decode(data);
        }


        /// <summary>Encodes a byte array.</summary>
        /// <param name="s">Source data.</param>
        /// <param name="encoder">Encoder.</param>
        /// <returns>Encoded data.</returns>
        public static string Compress(string s, IEncoder encoder = null)
        {
            if(encoder == null) { encoder = PlainText.Instance; }

            return new GZip(encoder).EncodeString(s);
        }


        /// <summary>Decodes a byte array.</summary>
        /// <param name="s">Encoded source data.</param>
        /// <param name="encoder">Encoder.</param>
        /// <returns>Decoded data.</returns>
        public static string Decompress(string s, IEncoder encoder = null)
        {
            if(encoder == null) { encoder = PlainText.Instance; }

            return new GZip(encoder).DecodeToString(s);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the base encoder for this class.</summary>
        public IEncoder Encoder
        {
            get; private set;
        } = PlainText.Instance;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __Encoder                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the base encoding for this instance.</summary>
        public override Encoding Encoding
        {
            get { return Encoder.Encoding; }
        }
        
        
        /// <summary>Encodes a byte array.</summary>
        /// <param name="data">Source data.</param>
        /// <returns>Encoded data.</returns>
        public override byte[] Encode(byte[] data)
        {
            return Encoder.Encode(Compress(data));
        }


        /// <summary>Decodes a byte array.</summary>
        /// <param name="data">Encoded source data.</param>
        /// <returns>Decoded data.</returns>
        public override byte[] Decode(byte[] data)
        {
            return Decompress(Encoder.Decode(data));
        }
    }
}
