﻿using System;
using System.IO;
using System.Reflection;



namespace Robbiblubber.Util
{
    /// <summary>This class defines configuration paths.</summary>
    public static class PathOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Local configuration path.</summary>
        private static string _LocalConfigurationPath = SystemLocalConfigurationPath;

        /// <summary>User configuration path.</summary>
        private static string _UserConfigurationPath = SystemUserConfigurationPath;

        /// <summary>Application part.</summary>
        private static string _ApplicationPart = null;

        /// <summary>Root temporary directory.</summary>
        private static string _TempRoot = SystemLocalConfigurationPath + @"\robbiblubber.org\temp";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the system local configuration path.</summary>
        public static string SystemLocalConfigurationPath
        {
            get { return Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData); }
        }



        /// <summary>Gets the system user configuration path.</summary>
        public static string SystemUserConfigurationPath
        {
            get { return Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData); }
        }


        /// <summary>Gets the application-specific local configuration path.</summary>
        public static string LocalConfigurationPath
        {
            get { return _LocalConfigurationPath; }
        }


        /// <summary>Gets the application-specific user configuration path.</summary>
        public static string UserConfigurationPath
        {
            get { return _UserConfigurationPath; }
        }


        /// <summary>Gets or sets the application part of configuration PathOp.</summary>
        public static string ApplicationPart
        {
            get { return _ApplicationPart; }
            set
            {
                _ApplicationPart = value;

                if(value == null) { value = ""; }
                _LocalConfigurationPath = SystemLocalConfigurationPath + '\\' + value.Replace('/', '\\').Trim('\\');
                _UserConfigurationPath = SystemUserConfigurationPath + '\\' + value.Replace('/', '\\').Trim('\\');

                if(!Directory.Exists(_LocalConfigurationPath)) { Directory.CreateDirectory(_LocalConfigurationPath); }
                if(!Directory.Exists(_UserConfigurationPath)) { Directory.CreateDirectory(_UserConfigurationPath); }
            }
        }


        /// <summary>Gets the application directory.</summary>
        public static string ApplicationDirectory
        {
            get
            {
                Assembly a = Assembly.GetEntryAssembly();
                if(a == null) { a = Assembly.GetCallingAssembly(); }

                return Path.GetDirectoryName(a.Location).TrimEnd('\\');
            }
        }


        /// <summary>Gets the library codebase directory.</summary>
        public static string LibraryDirectory
        {
            get
            {
                #pragma warning disable SYSLIB0012 // Type or member is obsolete
                var codeBase = Assembly.GetExecutingAssembly().CodeBase;
                #pragma warning restore SYSLIB0012 // Type or member is obsolete
                if(codeBase != null && codeBase.StartsWith("file:///"))
                {
                    codeBase = codeBase.Substring(8);
                    if(Path.DirectorySeparatorChar != '/') codeBase = codeBase.Replace('/', Path.DirectorySeparatorChar);
                    return Path.GetDirectoryName(codeBase);
                }

                return Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            }
        }


        /// <summary>Gets the user settings file.</summary>
        public static string UserSettingsFile
        {
            get { return UserConfigurationPath + @"\application.settings"; }
        }


        /// <summary>Gets or sets the temporary files root directory.</summary>
        public static string TempRoot
        {
            get
            {
                if(!Directory.Exists(_TempRoot)) { Directory.CreateDirectory(_TempRoot); }
                return _TempRoot;
            }
            set { _TempRoot = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Initializes the configuration PathOp.</summary>
        /// <param name="applicationPart">Application part of the configuration path.</param>
        public static void Initialize(string applicationPart)
        {
            ApplicationPart = applicationPart;
        }


        /// <summary>Gets a temporary file name.</summary>
        /// <param name="extension">Desired file extension.</param>
        /// <returns>Temporary file name.</returns>
        public static string GetTempFile(string extension = null)
        {
            return (TempRoot + @"\" + StringOp.Unique() + ((extension == null) ? ".tmp" : ("." + extension.Trim().TrimStart('.'))));
        }
    }
}
