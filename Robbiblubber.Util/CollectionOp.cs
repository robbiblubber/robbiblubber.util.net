﻿using System;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Util.Collections;



namespace Robbiblubber.Util
{
    /// <summary>This class implements extension methods for collections.</summary>
    public static class CollectionOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Random object.</summary>
        private static Random _Rnd = new Random();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a random item from the collection.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="c">Collection.</param>
        /// <returns>Random item.</returns>
        public static T Random<T>(this IEnumerable<T> c)
        {
            try
            {
                return c.Random(1).Single();
            }
            catch(Exception) { return default(T); }
        }


        /// <summary>Returns a random collection of items from the collection.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="c">Collection.</param>
        /// <param name="count">Item count.</param>
        /// <returns>Random item collection.</returns>
        public static IEnumerable<T> Random<T>(this IEnumerable<T> c, int count)
        {
            return c.Shuffled().Take(count);
        }


        /// <summary>Returns a shuffled collection.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="source">Collection.</param>
        /// <returns>Shuffled collection.</returns>
        public static IEnumerable<T> Shuffled<T>(this IEnumerable<T> source)
        {
            return source.OrderBy(x => _Rnd.Next());
        }


        /// <summary>Shuffles a list.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="list">List.</param>
        /// <returns>Returns the shuffled list.</returns>
        public static IList<T> Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            T keep; int k;
            
            while(n > 1)
            {
                k = _Rnd.Next((--n) + 1);
                keep = list[k];
                list[k] = list[n];
                list[n] = keep;
            }

            return list;
        }


        /// <summary>Shuffles an array.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="list">List.</param>
        /// <returns>Returns the shuffled array.</returns>
        public static T[] Shuffle<T>(this T[] list)
        {
            int n = list.Length;
            T keep; int k;

            while(n > 1)
            {
                k = _Rnd.Next((--n) + 1);
                keep = list[k];
                list[k] = list[n];
                list[n] = keep;
            }

            return list;
        }


        /// <summary>Returns if an array contains an element.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="array">Array.</param>
        /// <param name="element">Element.</param>
        /// <returns>Returns TRUE if the array contains the element, otherwise returns FALSE.</returns>
        public static bool Contains<T>(this T[] array, T element)
        {
            foreach(T i in array)
            {
                if(i == null)
                {
                    if(element == null) { return true; }
                }
                else
                {
                    if(i.Equals(element)) { return true; }
                }
            }

            return false;
        }


        /// <summary>Adds items to a collection.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="collection">Collection.</param>
        /// <param name="items">Items.</param>
        public static void AddItems<T>(this ICollection<T> collection, params T[] items)
        {
            foreach(T i in items) { collection.Add(i); }
        }


        /// <summary>Gets the index of an item.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="c">Enumerable.</param>
        /// <param name="item">Item.</param>
        /// <returns>Index. Returns -1 if the item was not found.</returns>
        public static int GetIndex<T>(this IEnumerable<T> c, T item)
        {
            int rval = 0;
            
            foreach(T i in c)
            {
                if((i != null) && (i.Equals(item))) return rval;
                rval++;
            }

            return -1;
        }


        /// <summary>Gets an IList interface for an IImmutableList.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="list">List.</param>
        /// <returns>IList.</returns>
        public static IList<T> AsList<T>(this IImmutableList<T> list)
        {
            if(list is ImmutableList<T>)
            {
                if(((ImmutableList<T>) list)._Items is IList<T>) { return (IList<T>) ((ImmutableList<T>) list)._Items; }
            }

            return new __List<T>(list);
        }
    }
}
