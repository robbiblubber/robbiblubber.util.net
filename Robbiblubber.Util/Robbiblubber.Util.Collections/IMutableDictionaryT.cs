﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Util.Collections
{
    /// <summary>Mutable dictionaries implement this interface.</summary>
    /// <typeparam name="TKey">Key type.</typeparam>
    /// <typeparam name="TValue">Value type.</typeparam>
    public interface IMutableDictionary<TKey, TValue>: IExtensibleDictionary<TKey, TValue>, IImmutableDictionary<TKey, TValue>, IImmutableList<TValue>, IDictionary<TKey, TValue>, IEnumerable<TValue>
    {}
}
