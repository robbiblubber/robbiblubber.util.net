﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Util.Collections
{
    /// <summary>This class provides an IMutableList container for collections.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public class MutableList<T>: ExtensibleList<T>, IMutableList<T>, IExtensibleList<T>, IImmutableList<T>, ICollection<T>, ICollection, IList<T>, IList, IEnumerable<T>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MutableList(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="elements">Elements.</param>
        public MutableList(IEnumerable<T> elements): base(elements)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMutableList<T>                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets an element by its index.</summary>
        /// <param name="i">Index.</param>
        public virtual new T this[int i] 
        { 
            get { return ((IList<T>) _Items)[i]; }
            set { ((IList<T>) _Items)[i] = value; }
        }


        /// <summary>Clears the list.</summary>
        public virtual void Clear()
        {
            ((IList<T>) _Items).Clear();
        }


        /// <summary>Removes an item.</summary>
        /// <param name="item">Item.</param>
        public virtual void Remove(T item)
        {
            ((IList<T>) _Items).Remove(item);
        }


        /// <summary>Removes the item with the given index.</summary>
        /// <param name="i">Index.</param>
        public virtual void RemoveAt(int i)
        {
            ((IList<T>) _Items).RemoveAt(i);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICollection<T>                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Copies the elements of the list to an array.</summary>
        /// <param name="array">Array.</param>
        /// <param name="arrayIndex">Index where copying begins.</param>
        public virtual void CopyTo(T[] array, int arrayIndex)
        {
            ((IList<T>) _Items).CopyTo(array, arrayIndex);
        }


        /// <summary>Removes an item from the list.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the copy operation was successful, otherwise returns FALSE.</returns>
        bool ICollection<T>.Remove(T item)
        {
            int c = Count;
            Remove(item);
            return (c > Count);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICollection                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an object that can be used to synchrone list access.</summary>
        object ICollection.SyncRoot
        {
            get { return ((ICollection) _Items).SyncRoot; }
        }


        /// <summary>Gets if the list is synchronized.</summary>
        bool ICollection.IsSynchronized
        {
            get { return ((ICollection) _Items).IsSynchronized; }
        }


        /// <summary>Copies the elements of the list to an array.</summary>
        /// <param name="array">Array.</param>
        /// <param name="index">Index where copying begins.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            foreach(T i in _Items)
            {
                array.SetValue(i, index++);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICollection<T>                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets if the list is read-only.</summary>
        bool ICollection<T>.IsReadOnly
        {
            get { return false; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IList                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the list is read-only.</summary>
        bool IList.IsReadOnly
        {
            get { return false; }
        }


        /// <summary>Gets if the list has a fixed size.</summary>
        bool IList.IsFixedSize
        {
            get { return false; }
        }


        /// <summary>Gets an element by its index.</summary>
        /// <param name="index">Index.</param>
        /// <returns>Element.</returns>
        object IList.this[int index]
        {
            get { return this[index]; }
            set { this[index] = (T) value; }
        }

        /// <summary>Adds an item to the list.</summary>
        /// <param name="value">Value.</param>
        /// <returns>Returns the position where the new item was inserted.</returns>
        int IList.Add(object value)
        {
            Add((T) value);
            return (Count - 1);
        }


        /// <summary>Returns if the list contains an object.</summary>
        /// <param name="value">Value.</param>
        /// <returns>Returns TRUE if the list contains the object, otherwise returns FALSE.</returns>
        bool IList.Contains(object value)
        {
            if(!(value is T)) return false;
            return Contains((T) value);
        }


        /// <summary>Returns the index for the specified object.</summary>
        /// <param name="value">Value.</param>
        /// <returns>The index of the value if found in the list, otherwise -1.</returns>
        int IList.IndexOf(object value)
        {
            return IndexOf((T) value);
        }


        /// <summary>Inserts an object into the list.</summary>
        /// <param name="index">Index.</param>
        /// <param name="value">Value.</param>
        void IList.Insert(int index, object value)
        {
            Insert(index, (T) value);
        }


        /// <summary>Removes an object from the list.</summary>
        /// <param name="value">Value.</param>
        void IList.Remove(object value)
        {
            Remove((T) value);
        }
    }
}
