﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Util.Collections
{
    /// <summary>This class provides an IList wrapper for IImmutableList.</summary>
    /// <typeparam name="T">Type.</typeparam>
    internal sealed class __List<T>: IList<T>, IList, ICollection<T>, ICollection, IEnumerable<T>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent object.</summary>
        private IImmutableList<T> _Parent;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constrcutors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent object.</param>
        internal __List(IImmutableList<T> parent)
        {
            _Parent = parent;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICollection                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the number of items in this list.</summary>
        public int Count
        {
            get { return _Parent.Count; }
        }


        /// <summary>Gets an object that can be used to synchrone list access.</summary>
        public object SyncRoot
        {
            get { return _Parent; }
        }


        /// <summary>Gets if the list is synchronized.</summary>
        public bool IsSynchronized
        {
            get { return false; }
        }


        /// <summary>Copies the elements of the list to an array.</summary>
        /// <param name="array">Array.</param>
        /// <param name="index">Index where copying begins.</param>
        public void CopyTo(Array array, int index)
        {
            foreach(T i in _Parent)
            {
                array.SetValue(i, index++);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICollection<T>                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the list is read-only.</summary>
        public bool IsReadOnly
        {
            get { return (!(_Parent is IExtensibleList<T>)); }
        }


        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        public void Add(T item)
        {
            ((IExtensibleList<T>) _Parent).Add(item);
        }


        /// <summary>Clears the list.</summary>
        public void Clear()
        {
            ((IMutableList<T>) _Parent).Clear();
        }


        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        public bool Contains(T item)
        {
            return _Parent.Contains(item);
        }


        /// <summary>Copies the elements of the list to an array.</summary>
        /// <param name="array">Array.</param>
        /// <param name="arrayIndex">Index where copying begins.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            CopyTo((Array) array, arrayIndex);
        }


        /// <summary>Removes an item.</summary>
        /// <param name="item">Item.</param>
        public bool Remove(T item)
        {
            int c = _Parent.Count;
            ((IMutableList<T>) _Parent).Remove(item);

            return (c > _Parent.Count);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IList<T>                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets an element by its index.</summary>
        /// <param name="index">Index.</param>
        /// <returns>Element.</returns>
        public T this[int index]
        {
            get { return _Parent[index]; }
            set { ((IMutableList<T>) _Parent)[index] = value; }
        }


        /// <summary>Gets the index of an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Index.</returns>
        public int IndexOf(T item)
        {
            return _Parent.IndexOf(item);
        }


        /// <summary>Inserts an item at a given position.</summary>
        /// <param name="index">Index.</param>
        /// <param name="item">Item.</param>
        public void Insert(int index, T item)
        {
            ((IExtensibleList<T>) _Parent).Insert(index, item);
        }


        /// <summary>Removes the item with the given index.</summary>
        /// <param name="index">Index.</param>
        public void RemoveAt(int index)
        {
            ((IMutableList<T>) _Parent).RemoveAt(index);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IList                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the list has a fixed size.</summary>
        public bool IsFixedSize
        {
            get { return (!(_Parent is IExtensibleList<T>)); }
        }


        /// <summary>Gets an element by its index.</summary>
        /// <param name="index">Index.</param>
        /// <returns>Element.</returns>
        object IList.this[int index]
        {
            get { return _Parent[index]; }
            set { ((IMutableList<T>) _Parent)[index] = (T) value; }
        }


        /// <summary>Adds an item to the list.</summary>
        /// <param name="value">Value.</param>
        /// <returns>Returns the position where the new item was inserted.</returns>
        int IList.Add(object value)
        {
            ((IExtensibleList<T>) _Parent).Add((T) value);
            return _Parent.IndexOf((T) value);
        }


        /// <summary>Returns if the list contains an object.</summary>
        /// <param name="value">Value.</param>
        /// <returns>Returns TRUE if the list contains the object, otherwise returns FALSE.</returns>
        bool IList.Contains(object value)
        {
            return _Parent.Contains((T) value);
        }


        /// <summary>Returns the index for the specified object.</summary>
        /// <param name="value">Value.</param>
        /// <returns>The index of the value if found in the list, otherwise -1.</returns>
        int IList.IndexOf(object value)
        {
            return _Parent.IndexOf((T) value);
        }


        /// <summary>Inserts an object into the list.</summary>
        /// <param name="index">Index.</param>
        /// <param name="value">Value.</param>
        void IList.Insert(int index, object value)
        {
            ((IExtensibleList<T>) _Parent).Insert(index, (T) value);
        }


        /// <summary>Removes an object from the list.</summary>
        /// <param name="value">Value.</param>
        void IList.Remove(object value)
        {
            ((IMutableList<T>) _Parent).Remove((T) value);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) _Parent).GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<T>                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this collection.</summary>
        /// <returns>Enumerator.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            return _Parent.GetEnumerator();
        }
    }
}
