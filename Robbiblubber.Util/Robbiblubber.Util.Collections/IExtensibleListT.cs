﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Util.Collections
{
    /// <summary>Extensible lists implement this interface.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public interface IExtensibleList<T>: IImmutableList<T>, IReadOnlyList<T>, IReadOnlyCollection<T>, IEnumerable<T>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        void Add(T item);


        /// <summary>Adds a range of items to the list.</summary>
        /// <param name="items">Items.</param>
        void AddRange(IEnumerable<T> items);


        /// <summary>Inserts an item at a given position.</summary>
        /// <param name="i">Index.</param>
        /// <param name="item">Item.</param>
        void Insert(int i, T item);
    }
}
