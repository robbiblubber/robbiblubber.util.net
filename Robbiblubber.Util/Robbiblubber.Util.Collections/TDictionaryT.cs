﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;



namespace Robbiblubber.Util.Collections
{
    /// <summary>This class provides an implementation of Dictionary that implements the IMutableDictionary interface.</summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class TDictionary<TKey, TValue>: Dictionary<TKey, TValue>, IMutableDictionary<TKey, TValue>, IExtensibleDictionary<TKey, TValue>, IImmutableDictionary<TKey, TValue>, IImmutableList<TValue>, IDictionary<TKey, TValue>, IEnumerable<TValue>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public TDictionary(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="dictionary">Dictionary.</param>
        public TDictionary(IDictionary<TKey, TValue> dictionary): base(dictionary)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="dictionary">Dictionary.</param>
        /// <param name="comparer">Comparer.</param>
        public TDictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer): base(dictionary, comparer)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="values">Values.</param>
        public TDictionary(IEnumerable<KeyValuePair<TKey, TValue>> values): base()
        {
            foreach(KeyValuePair<TKey, TValue> i in values) { Add(i.Key, i.Value);  }
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="values">Values.</param>
        /// <param name="comparer">Comparer.</param>
        public TDictionary(IEnumerable<KeyValuePair<TKey, TValue>> values, IEqualityComparer<TKey> comparer): base(comparer)
        {
            foreach(KeyValuePair<TKey, TValue> i in values) { Add(i.Key, i.Value); }
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="capacity">Capacity.</param>
        public TDictionary(int capacity): base(capacity)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="capacity">Capacity.</param>
        /// <param name="comparer">Comparer.</param>
        public TDictionary(int capacity, IEqualityComparer<TKey> comparer): base(capacity, comparer)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="comparer">Comparer.</param>
        public TDictionary(IEqualityComparer<TKey> comparer): base(comparer)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="info">Serialization information.</param>
        /// <param name="context">Streaming context.</param>
        public TDictionary(SerializationInfo info, StreamingContext context): base(info, context)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] Dictionary<TKey, TValue>                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets all keys for this dictionary.</summary>
        public virtual new IImmutableList<TKey> Keys
        {
            get { return new ImmutableList<TKey>(base.Keys); }
        }


        /// <summary>Gets all values for this dictionary.</summary>
        public virtual new IImmutableList<TValue> Values
        { 
            get { return new ImmutableList<TValue>(base.Values); }
        }


        /// <summary>Returns an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        public virtual new IEnumerator<TValue> GetEnumerator()
        {
            return Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IImmutableDictionary<TKey, TValue>                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the key for an object.</summary>
        /// <param name="item">Object.</param>
        /// <returns>Key.</returns>
        public virtual TKey KeyOf(TValue item)
        {
            foreach(KeyValuePair<TKey, TValue> i in ((IEnumerable<KeyValuePair<TKey, TValue>>) this))
            {
                if(item.Equals(i.Value)) return i.Key;
            }
            return default;
        }


        /// <summary>Adds a range of items to the list.</summary>
        /// <param name="items">Items.</param>
        public virtual void AddRange(IEnumerable<KeyValuePair<TKey, TValue>> items)
        {
            foreach(KeyValuePair<TKey, TValue> i in items) { Add(i.Key, i.Value); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IImmutableList<TValue>                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        public virtual bool Contains(TValue item)
        {
            return base.Values.Contains(item);
        }


        /// <summary>Gets the index of an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Index.</returns>
        public int IndexOf(TValue item)
        {
            return base.Values.GetIndex(item);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IReadonlyList<TValue>                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the element with a given index.</summary>
        /// <param name="index">Index.</param>
        /// <returns>Element.</returns>
        TValue IReadOnlyList<TValue>.this[int index]
        {
            get { return Values.ElementAt(index); }
        }
    }
}
