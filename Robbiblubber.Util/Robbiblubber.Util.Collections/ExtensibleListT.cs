﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Util.Collections
{
    /// <summary>This class provides an IExtensibleList container for collections.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public class ExtensibleList<T>: ImmutableList<T>, IExtensibleList<T>, IImmutableList<T>, IReadOnlyList<T>, IReadOnlyCollection<T>, IEnumerable<T>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instnace of this class.</summary>
        public ExtensibleList(): base(null)
        {
            _Items = new List<T>();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="elements">Elements.</param>
        public ExtensibleList(IEnumerable<T> elements): base(elements)
        {
            if(!(elements is IList<T>))
            {
                _Items = new List<T>();
                ((List<T>) _Items).AddRange(elements);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IExtensibleList<T>                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        public virtual void Add(T item)
        {
            ((IList<T>) _Items).Add(item);
        }


        /// <summary>Adds a range of items to the list.</summary>
        /// <param name="items">Items.</param>
        public virtual void AddRange(IEnumerable<T> items)
        {
            if(_Items is List<T>)
            {
                ((List<T>) _Items).AddRange(items);
            }
            else
            {
                foreach(T i in items) { ((IList<T>) _Items).Add(i); }
            }
        }


        /// <summary>Inserts an item at a given position.</summary>
        /// <param name="i">Index.</param>
        /// <param name="item">Item.</param>
        public virtual void Insert(int i, T item)
        {
            ((IList<T>) _Items).Insert(i, item);
        }
    }
}
