﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Util.Collections
{
    /// <summary>Immutable dictionaries implement this interface.</summary>
    /// <typeparam name="TKey">Key type.</typeparam>
    /// <typeparam name="TValue">Value type.</typeparam>
    public interface IImmutableDictionary<TKey, TValue>: IImmutableList<TValue>, IReadOnlyDictionary<TKey, TValue>, IEnumerable<TValue>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets all keys for this dictionary.</summary>
        new IImmutableList<TKey> Keys { get; }


        /// <summary>Gets all values for this dictionary.</summary>
        new IImmutableList<TValue> Values { get; }


        /// <summary>Gets the key for an object.</summary>
        /// <param name="item">Object.</param>
        /// <returns>Key.</returns>
        TKey KeyOf(TValue item);
    }
}
