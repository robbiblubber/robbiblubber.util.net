﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Util.Collections
{
    /// <summary>Mutable collections implement this interface.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public interface IMutableList<T>: IExtensibleList<T>, IImmutableList<T>, IList<T>, IEnumerable<T>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets an element by its index.</summary>
        /// <param name="i">Index.</param>
        new T this[int i] { get; set; }
    }
}
