﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;



namespace Robbiblubber.Util.Collections
{
    /// <summary>This class provides an IImmutableDictionary container for collections.</summary>
    /// <typeparam name="TKey">Key type.</typeparam>
    /// <typeparam name="TValue">Value type.</typeparam>
    public class ImmutableDictionary<TKey, TValue>: IImmutableDictionary<TKey, TValue>, IImmutableList<TValue>, IReadOnlyDictionary<TKey, TValue>, IEnumerable<TValue>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Items.</summary>
        protected internal IDictionary<TKey, TValue> _Items;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        protected ImmutableDictionary()
        {
            _Items = new Dictionary<TKey, TValue>();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="elements">Elements.</param>
        public ImmutableDictionary(IEnumerable<KeyValuePair<TKey, TValue>> elements)
        {
            if(elements is IDictionary<TKey, TValue>) { _Items = (IDictionary<TKey, TValue>) elements; return; }

            _Items = new Dictionary<TKey, TValue>();
            foreach(KeyValuePair<TKey, TValue> i in elements) { _Items.Add(i); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IImmutableDictionary<TKey, TValue>                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets all keys for this dictionary.</summary>
        public virtual IImmutableList<TKey> Keys 
        { 
            get { return new ImmutableList<TKey>(_Items.Keys); }
        }


        /// <summary>Gets all values for this dictionary.</summary>
        public virtual IImmutableList<TValue> Values 
        { 
            get { return new ImmutableList<TValue>(_Items.Values); }
        }


        /// <summary>Gets the key for an object.</summary>
        /// <param name="item">Object.</param>
        /// <returns>Key.</returns>
        public virtual TKey KeyOf(TValue item)
        {
            foreach(KeyValuePair<TKey, TValue> i in _Items)
            {
                if(item.Equals(i)) { return i.Key; }
            }
            return default;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IImmutableList<TValue>                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if the list contains an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        public virtual bool Contains(TValue item)
        {
            return _Items.Values.Contains(item);
        }


        /// <summary>Gets the index of an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Index.</returns>
        public int IndexOf(TValue item)
        {
            return _Items.Values.GetIndex(item);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IReadOnlyCollection<T>                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the number of items in this collection.</summary>
        public virtual int Count
        {
            get { return _Items.Count; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IReadOnlyList<T>                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an element by its index.</summary>
        /// <param name="i">Index.</param>
        /// <returns>Element.</returns>
        public virtual TValue this[int i]
        {
            get { return _Items.ElementAt(i).Value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IReadOnlyDictionary<TKey, TValue>                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the element with a given key.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Element.</returns>
        public TValue this[TKey key] 
        { 
            get { return _Items[key]; }
        }


        /// <summary>Gets all keys for this dictionary.</summary>
        IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys 
        { 
            get { return Keys; }
        }


        /// <summary>Gets all values for this dictionary.</summary>
        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values 
        { 
            get { return Values; }
        }


        /// <summary>Returns if the list contains a key.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Returns TRUE if the dictionary contains the key, otherwise returns FALSE.</returns>
        public virtual bool ContainsKey(TKey key)
        {
            return _Items.ContainsKey(key);
        }
        
        
        /// <summary>Gets the item for a key.</summary>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        /// <returns>Returns TRUE if the item is returned, otherwise returns FALSE.</returns>
        bool IReadOnlyDictionary<TKey, TValue>.TryGetValue(TKey key, out TValue value)
        {
            return _Items.TryGetValue(key, out value);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<T>                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this collection.</summary>
        /// <returns>Enumerator.</returns>
        public IEnumerator<TValue> GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<KeyValuePair<TKey, TValue>>                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }
    }
}
