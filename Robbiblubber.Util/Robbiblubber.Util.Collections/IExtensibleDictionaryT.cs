﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Util.Collections
{
    /// <summary>Extensible dictionaries implement this interface.</summary>
    /// <typeparam name="TKey">Key type.</typeparam>
    /// <typeparam name="TValue">Value type.</typeparam>
    public interface IExtensibleDictionary<TKey, TValue>: IImmutableDictionary<TKey, TValue>, IImmutableList<TValue>, IReadOnlyDictionary<TKey, TValue>, IEnumerable<TValue>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="key">Item key.</param>
        /// <param name="value">Item value.</param>
        void Add(TKey key, TValue value);


        /// <summary>Adds a range of items to the list.</summary>
        /// <param name="items">Items.</param>
        void AddRange(IEnumerable<KeyValuePair<TKey, TValue>> items);
    }
}
