﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;



namespace Robbiblubber.Util.Collections
{
    /// <summary>This class provides a wrapper for StringCollection that implements IEnumerable for strings.</summary>
    public class EnumerableStringCollection: IList<string>, IList, ICollection, ICollection<string>, IEnumerable<string>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        public EnumerableStringCollection()
        {
            UnderlyingCollection = new StringCollection();
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="underlyingCollection">Underlying collection.</param>
        public EnumerableStringCollection(StringCollection underlyingCollection)
        {
            UnderlyingCollection = underlyingCollection;
        }

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the underlying collection.</summary>
        public virtual StringCollection UnderlyingCollection
        {
            get; protected set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICollection                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the number of elements in this collection.</summary>
        public virtual int Count
        {
            get { return UnderlyingCollection.Count; }
        }


        /// <summary>Gets an object that is used to synchronize access to the instance.</summary>
        object ICollection.SyncRoot
        {
            get { return UnderlyingCollection.SyncRoot; }
        }


        /// <summary>Gets if the collection is synchronized.</summary>
        bool ICollection.IsSynchronized
        {
            get { return UnderlyingCollection.IsSynchronized; }
        }


        /// <summary>Copies the collection to an array.</summary>
        /// <param name="array">Array.</param>
        /// <param name="index">Start index.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            ((ICollection) UnderlyingCollection).CopyTo(array, index);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICollection<string>                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the collection is read-only.</summary>
        public virtual bool IsReadOnly
        {
            get { return UnderlyingCollection.IsReadOnly; }
        }


        /// <summary>Adds an item to the collection.</summary>
        /// <param name="item">Item.</param>
        public virtual void Add(string item)
        {
            UnderlyingCollection.Add(item);
        }


        /// <summary>Clears the collection.</summary>
        public virtual void Clear()
        {
            UnderlyingCollection.Clear();
        }


        /// <summary>Returns if the collection contains an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the collection contains the item, otherwise returns FALSE.</returns>
        public virtual bool Contains(string item)
        {
            return UnderlyingCollection.Contains(item);
        }


        /// <summary>Copies the collection to an array.</summary>
        /// <param name="array">Array.</param>
        /// <param name="arrayIndex">Start index.</param>
        public virtual void CopyTo(string[] array, int arrayIndex)
        {
            UnderlyingCollection.CopyTo(array, arrayIndex);
        }


        /// <summary>Removes the first occurrence of a specific object from the collection.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the item has been successfully removed, otherwise returns FALSE.</returns>
        public virtual bool Remove(string item)
        {
            int m = UnderlyingCollection.Count;
            UnderlyingCollection.Remove(item);

            return (UnderlyingCollection.Count < m);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IList                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets if the list has a fixed size.</summary>
        bool IList.IsFixedSize
        {
            get { return ((IList) UnderlyingCollection).IsFixedSize; }
        }


        /// <summary>Gets or sets the element at the specified index.</summary>
        /// <param name="index">Index.</param>
        object IList.this[int index]
        {
            get { return UnderlyingCollection[index]; }
            set { UnderlyingCollection[index] = (string) value; }
        }


        /// <summary>Adds an item to the list.</summary>
        /// <param name="value">Item.</param>
        /// <returns>Returns the zero-based index at which the new element is inserted.</returns>
        int IList.Add(object value)
        {
            return UnderlyingCollection.Add((string) value);
        }


        /// <summary>Gets a value indicating if the list contains an item.</summary>
        /// <param name="value">Item.</param>
        /// <returns>Returns TRUE if the list contains the item, otherwise returns FALSE.</returns>
        bool IList.Contains(object value)
        {
            return ((IList) UnderlyingCollection).Contains(value);
        }


        /// <summary>Returns the index of an item.</summary>
        /// <param name="value">Item.</param>
        /// <returns>Index.</returns>
        int IList.IndexOf(object value)
        {
            return ((IList) UnderlyingCollection).IndexOf(value);
        }


        /// <summary>nserts an item to the list at the specified index.</summary>
        /// <param name="index">Index.</param>
        /// <param name="value">Item.</param>
        void IList.Insert(int index, object value)
        {
            ((IList) UnderlyingCollection).Insert(index, value);
        }


        /// <summary>Removes an item from the list.</summary>
        /// <param name="value">Item.</param>
        void IList.Remove(object value)
        {
            ((IList) UnderlyingCollection).Remove(value);
        }


        /// <summary>Removes the item at the specified index.</summary>
        /// <param name="index">Index.</param>
        public virtual void RemoveAt(int index)
        {
            UnderlyingCollection.RemoveAt(index);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IList<string>                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the element at the specified index.</summary>
        /// <param name="index">Index.</param>
        public virtual string this[int index]
        {
            get { return UnderlyingCollection[index]; }
            set { UnderlyingCollection[index] = value; }
        }


        /// <summary>nserts an item to the list at the specified index.</summary>
        /// <param name="index">Index.</param>
        /// <param name="item">Item.</param>
        public virtual void Insert(int index, string item)
        {
            UnderlyingCollection.Insert(index, item);
        }


        /// <summary>Returns the index of an item.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Index.</returns>
        public virtual int IndexOf(string item)
        {
            return UnderlyingCollection.IndexOf(item);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<string>) this).GetEnumerator();
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<string>                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<string> IEnumerable<string>.GetEnumerator()
        {
            return new _StringEnumerator(UnderlyingCollection.GetEnumerator());
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] _StringEnumerator                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>This class provides an IEnumerable-wrapper for StringEnumerator.</summary>
        protected class _StringEnumerator: IEnumerator<string>, IEnumerator, IDisposable
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // protected members                                                                                            //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Underlying enumerator.</summary>
            protected StringEnumerator _UnderlyingEnumerator;



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                 //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="underlyingEnumerator">Underlying enumerator.</param>
            public _StringEnumerator(StringEnumerator underlyingEnumerator)
            {
                _UnderlyingEnumerator = underlyingEnumerator;
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [interface] IEnumerator<string>                                                                              //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            /// <summary>Gets the current element in the collection.</summary>
            public string Current
            {
                get { return _UnderlyingEnumerator.Current; }
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [interface] IEnumerator                                                                              //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets the current element in the collection.</summary>
            object IEnumerator.Current
            {
                get { return Current; }
            }


            /// <summary>Advances the enumerator to the next element of the collection.</summary>
            /// <returns>Returns TRUE if the enumerator was successfully advanced to the next element,
            ///          FALSE if the enumerator has passed the end of the collection.</returns>
            public bool MoveNext()
            {
                return _UnderlyingEnumerator.MoveNext();
            }


            /// <summary>Sets the enumerator to its initial position, which is before the first element in the collection.</summary>
            public void Reset()
            {
                _UnderlyingEnumerator.Reset();
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [interface] IDisposable                                                                                      //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
            public void Dispose()
            {}
        }
    }
}
