﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Util.Collections
{
    /// <summary>This class provides an IMutableDictionary container for collections.</summary>
    /// <typeparam name="TKey">Key type.</typeparam>
    /// <typeparam name="TValue">Value type.</typeparam>
    public class MutableDictionary<TKey, TValue>: ExtensibleDictionary<TKey, TValue>, IMutableDictionary<TKey, TValue>, IExtensibleDictionary<TKey, TValue>, IImmutableDictionary<TKey, TValue>, IImmutableList<TValue>, IDictionary<TKey, TValue>, IEnumerable<TValue>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MutableDictionary(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="elements">Elements.</param>
        public MutableDictionary(IEnumerable<KeyValuePair<TKey, TValue>> elements): base(elements)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDictionary<TKey, TValue>                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets an element by its index.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Element.</returns>
        public virtual new TValue this[TKey key] 
        {
            get { return _Items[key]; }
            set { _Items[key] = value; }
        }


        /// <summary>Removes an item from the dictionary by its key.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Returns TRUE if an item has been removed, otherwise returns FALSE.</returns>
        public virtual bool Remove(TKey key)
        {
            return _Items.Remove(key);
        }


        /// <summary>Gets the keys for this collection.</summary>
        ICollection<TKey> IDictionary<TKey, TValue>.Keys
        {
            get { return _Items.Keys; }
        }


        /// <summary>Gets the values for this collection.</summary>
        ICollection<TValue> IDictionary<TKey, TValue>.Values
        {
            get { return _Items.Values; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICollection<KeyValuePair<TKey, TValue>>                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the instance is read-only.</summary>
        bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly
        {
            get { return false; }
        }


        /// <summary>Adds an item to the dictionary.</summary>
        /// <param name="item">Item.</param>
        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
        {
            _Items.Add(item);
        }


        /// <summary>Clears the dictionary.</summary>
        public virtual void Clear()
        {
            _Items.Clear();
        }


        /// <summary>Gets if the dictionary contains a key-value-pair.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if the dictionary contains the item, otherwise returns FALSE.</returns>
        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
        {
            return _Items.Contains(item);
        }


        /// <summary>Copies the dictionary to an array.</summary>
        /// <param name="array">Array.</param>
        /// <param name="arrayIndex">Start index.</param>
        void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _Items.CopyTo(array, arrayIndex);
        }


        /// <summary>Removes an item from the collection.</summary>
        /// <param name="item">Item.</param>
        /// <returns>Returns TRUE if an item has been removed, otherwise returns FALSE.</returns>
        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
        {
            return _Items.Remove(item);
        }


        /// <summary>Gets an item by its key.</summary>
        /// <param name="key">Key.</param>
        /// <param name="value">Item.</param>
        /// <returns>Returns TRUE if the item has been returned, otherwise returns FALSE.</returns>
        bool IDictionary<TKey, TValue>.TryGetValue(TKey key, out TValue value)
        {
            return _Items.TryGetValue(key, out value);
        }
    }
}
