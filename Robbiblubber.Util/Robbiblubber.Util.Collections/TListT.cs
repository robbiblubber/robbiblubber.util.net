﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Util.Collections
{
    /// <summary>This class provides an implementation of List that implements the IMutableList interface.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public class TList<T>: List<T>, IMutableList<T>, IExtensibleList<T>, IImmutableList<T>, ICollection<T>, ICollection, IList<T>, IList, IEnumerable<T>, IEnumerable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public TList(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="collection">Collection.</param>
        public TList(IEnumerable<T> collection): base(collection)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="capacity">Capacity.</param>
        public TList(int capacity): base(capacity)
        {}
    }
}
