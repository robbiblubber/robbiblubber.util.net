﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Util.Collections
{
    /// <summary>This class provides an IExtensibleDictionary container for collections.</summary>
    /// <typeparam name="TKey">Key type.</typeparam>
    /// <typeparam name="TValue">Value type.</typeparam>
    public class ExtensibleDictionary<TKey, TValue>: ImmutableDictionary<TKey, TValue>, IExtensibleDictionary<TKey, TValue>, IImmutableDictionary<TKey, TValue>, IImmutableList<TValue>, IReadOnlyDictionary<TKey, TValue>, IEnumerable<TValue>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public ExtensibleDictionary(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="elements">Elements.</param>
        public ExtensibleDictionary(IEnumerable<KeyValuePair<TKey, TValue>> elements): base(elements)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IExtensibleDictionary<TKey, TValue>                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="key">Item key.</param>
        /// <param name="value">Item value.</param>
        public virtual void Add(TKey key, TValue value)
        {
            _Items.Add(key, value);
        }


        /// <summary>Adds a range of items to the list.</summary>
        /// <param name="items">Items.</param>
        public virtual void AddRange(IEnumerable<KeyValuePair<TKey, TValue>> items)
        {
            foreach(KeyValuePair<TKey, TValue> i in items) { _Items.Add(i.Key, i.Value); }
        }
    }
}
