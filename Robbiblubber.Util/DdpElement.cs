﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Robbiblubber.Util.Coding;
using Robbiblubber.Util.Collections;



namespace Robbiblubber.Util
{
    /// <summary>This class implements a ddp element.</summary>
    public class DdpElement: IEnumerable<DdpElement>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Child elements.</summary>
        protected ConcurrentDictionary<string, DdpElement> _ChildElements = new ConcurrentDictionary<string, DdpElement>();

        /// <summary>Value.</summary>
        protected Ddp.DdpValue _Value = "";

        /// <summary>Masked name.</summary>
        protected string _MaskedName = "";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent element.</param>
        /// <param name="maskedName">Masked name.</param>
        /// <param name="re">String reader.</param>
        protected DdpElement(DdpElement parent, string maskedName, StringRdr re = null)
        {
            Parent = parent;
            _MaskedName = maskedName;

            if(re != null) { _Read(maskedName, re); }
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent element.</param>
        /// <param name="maskedName">Masked name.</param>
        /// <param name="maskedValue">Masked value.</param>
        /// <param name="value">Value.</param>
        protected DdpElement(DdpElement parent, string maskedName, string maskedValue, string value)
        {
            Parent = parent;
            _MaskedName = maskedName;
            _Value = new Ddp.DdpValue(value, maskedValue);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a child element by its path.</summary>
        /// <param name="path">Path.</param>
        /// <returns>Child element.</returns>
        protected DdpElement _GetChildElement(List<string> path)
        {
            if(path.Count == 0) { return this; }

            string next = path[0];
            path.RemoveAt(0);

            if(next == "")  { return Root._GetChildElement(path); }
            if(next == ".") { return _GetChildElement(path); }
            if(next == "..")
            {
                if(Parent == null) { return _GetChildElement(path); }
                return Parent._GetChildElement(path);
            }
            if(_ChildElements.ContainsKey(next)) { return _ChildElements[next]._GetChildElement(path); }
            return _ChildElements.GetOrAdd(next, new DdpElement(this, next))._GetChildElement(path);
        }


        /// <summary>Merges an element with an existing element.</summary>
        /// <param name="maskedName">Masked element name.</param>
        /// <param name="elm">Element.</param>
        /// <returns></returns>
        protected DdpElement _MergeMove(string maskedName, DdpElement elm)
        {
            elm._Value._Update(_Value);

            foreach(DdpElement i in this._ChildElements.Values) { i.MoveTo(elm); }
            return elm;
        }


        /// <summary>Merges an element with an existing element.</summary>
        /// <param name="maskedName">Masked element name.</param>
        /// <param name="elm">Element.</param>
        /// <returns></returns>
        protected DdpElement _MergeCopy(string maskedName, DdpElement elm)
        {
            elm._Value._Update(_Value);

            foreach(DdpElement i in this._ChildElements.Values) { i.CopyTo(elm); }
            return elm;
        }


        /// <summary>Creates a string representation of this instance.</summary>
        /// <param name="str">String builder.</param>
        /// <param name="indent">Current indent.</param>
        /// <param name="singleLine">Creates the output as a single line.</param>
        /// <param name="skipComments">Creates the output without comments.</param>
        protected void _ToString(ref StringBuilder str, int indent, bool singleLine, bool skipComments)
        {
            bool hasName = !string.IsNullOrWhiteSpace(_MaskedName);
            string ind = "";
            if(!singleLine)
            {
                for(int i = 0; i < indent; i++) { ind += "    "; }
            }

            if(!(skipComments || string.IsNullOrWhiteSpace(Comment)))
            {
                str.Append(ind);
                str.Append("/* ");
                str.Append(Comment);
                str.Append(" */");
                str.Append(singleLine ? " " : "\r\n");
            }

            if(_ChildElements.Count == 0)
            {
                str.Append(ind);
                str.Append(hasName ? __DdpUtility.InternalToMask(_MaskedName) : ".");
                str.Append(" = ");
                str.Append(__DdpUtility.InternalToMask(Value._MaskedValue));
                str.Append(';');
                str.Append(singleLine ? " " : "\r\n");
            }
            else
            {
                if(hasName)
                {
                    str.Append(ind);
                    str.Append(__DdpUtility.InternalToMask(_MaskedName));
                    str.Append(singleLine ? " { " : "\r\n" + ind + "{\r\n");
                }

                bool hasEntries = false;
                if(!_Value._IsEmpty)
                {
                    hasEntries = true;
                    str.Append(ind);
                    if(hasName && (!singleLine)) { str.Append("    "); }
                    str.Append(". = ");
                    str.Append(__DdpUtility.InternalToMask(_Value._MaskedValue));
                    str.Append(singleLine ? "; " : ";\r\n");
                }

                foreach(DdpElement i in _ChildElements.Values.Where(m => !m.HasElements).OrderBy(m => m.Name))
                {
                    hasEntries = true;
                    i._ToString(ref str, hasName ? indent + 1 : indent, singleLine, skipComments);
                }

                foreach(DdpElement i in _ChildElements.Values.Where(m => m.HasElements).OrderBy(m => m.Name))
                {
                    if(hasEntries)
                    {
                        hasEntries = false;
                        if(!singleLine) { str.Append(ind + "\r\n"); }
                    }

                    i._ToString(ref str, hasName ? indent + 1 : indent, singleLine, skipComments);
                }

                if(hasName)
                {
                    str.Append(ind);
                    str.Append(singleLine ? " } " : "}\r\n");
                }

            }
        }


        /// <summary>Reads a section.</summary>
        /// <param name="maskedName">Masked name.</param>
        /// <param name="re">String data.</param>
        protected void _Read(string maskedName, StringRdr re)
        {
            string comment = null;
            DdpElement v = null;

            while(true)
            {
                __DdpLine line = __DdpUtility.ReadLine(re, ref comment);

                if(line.Termination == __DdpLine.BRACKET_OPEN)
                {
                    v = new DdpElement(this, __DdpUtility.ApplyInternal(line.Text.Trim()), re);
                    _ChildElements.AddOrUpdate(v._MaskedName, v, _MergeMove);
                }
                else
                {
                    if(!string.IsNullOrWhiteSpace(line.Text))
                    {
                        int eqpos = line.Text.IndexOf("=");
                        v = new DdpElement(this, __DdpUtility.ApplyInternal(line.Text.Substring(0, eqpos < 0 ? 0 : eqpos).Trim(), true),
                                                 __DdpUtility.ApplyInternal(line.Text.Substring(eqpos + 1).Trim(), true),
                                                 __DdpUtility.UnmaskSpecialChars(line.Text.Substring(eqpos + 1).Trim()));

                        if(v.Name == ".")
                        {
                            _Value = v._Value;
                        }
                        else { _ChildElements.AddOrUpdate(v._MaskedName, v, _MergeMove); }
                    }
                    if(line.Termination != __DdpLine.SEMICOLON) break;
                }

                if(comment != null)
                {
                    v.Comment = __DdpUtility.UnmaskSpecialChars(comment.Trim());
                    comment = null;
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the child element for a path.</summary>
        /// <param name="path">Path.</param>
        /// <returns>Element.</returns>
        public DdpElement this[string path]
        {
            get
            {
                return _GetChildElement(__DdpUtility.ParsePath(path));
            }
        }


        /// <summary>Gets the child element value for a path using a default value.</summary>
        /// <param name="path">Path.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns></returns>
        public Ddp.DdpValue this[string path, object defaultValue]
        {
            get
            {
                Ddp.DdpValue rval = this[path].Value;
                if(rval._IsEmpty)
                {
                    rval = new Ddp.DdpValue();

                    if(defaultValue is string)
                    {
                        rval._Value = (string) defaultValue;
                        rval._MaskedValue = __DdpUtility.MaskSpecialChars((string) defaultValue);
                    }
                    else if(defaultValue is DateTime)
                    {
                        rval._Value = rval._MaskedValue = ((DateTime) defaultValue).ToTimestamp();
                    }
                    else if(defaultValue is IEnumerable)
                    {
                        if(defaultValue is IEnumerable<string>)
                        {
                            rval._SetArrayValue((IEnumerable<string>) defaultValue);
                        }
                        else if(defaultValue is IEnumerable<int>)
                        {
                            rval._SetArrayValue((IEnumerable<int>) defaultValue);
                        }
                        if(defaultValue is IEnumerable<uint>)
                        {
                            rval._SetArrayValue((IEnumerable<uint>) defaultValue);
                        }
                        if(defaultValue is IEnumerable<long>)
                        {
                            rval._SetArrayValue((IEnumerable<long>) defaultValue);
                        }
                        if(defaultValue is IEnumerable<ulong>)
                        {
                            rval._SetArrayValue((IEnumerable<ulong>) defaultValue);
                        }
                        if(defaultValue is IEnumerable<short>)
                        {
                            rval._SetArrayValue((IEnumerable<short>) defaultValue);
                        }
                        if(defaultValue is IEnumerable<ushort>)
                        {
                            rval._SetArrayValue((IEnumerable<ushort>) defaultValue);
                        }
                        if(defaultValue is IEnumerable<byte>)
                        {
                            rval._SetArrayValue((IEnumerable<byte>) defaultValue);
                        }
                        if(defaultValue is IEnumerable<sbyte>)
                        {
                            rval._SetArrayValue((IEnumerable<sbyte>) defaultValue);
                        }
                        if(defaultValue is IEnumerable<float>)
                        {
                            rval._SetArrayValue((IEnumerable<float>) defaultValue);
                        }
                        if(defaultValue is IEnumerable<double>)
                        {
                            rval._SetArrayValue((IEnumerable<double>) defaultValue);
                        }
                        if(defaultValue is IEnumerable<decimal>)
                        {
                            rval._SetArrayValue((IEnumerable<decimal>) defaultValue);
                        }
                        if(defaultValue is IEnumerable<DateTime>)
                        {
                            rval._SetArrayValue((IEnumerable<DateTime>) defaultValue);
                        }
                    }
                    else
                    {
                        rval._Value = rval._MaskedValue = defaultValue.ToString();
                    }
                }

                return rval;
            }
        }


        /// <summary>Gets the parent element for this element.</summary>
        public DdpElement Parent
        {
            get; protected set;
        }


        /// <summary>Gets the root element.</summary>
        public DdpElement Root
        {
            get
            {
                DdpElement rval = this;
                while(rval.Parent != null)
                {
                    rval = rval.Parent;
                }

                return rval;
            }
        }


        /// <summary>Gets or sets the element name.</summary>
        public string Name
        {
            get { return __DdpUtility.UnmaskSpecialChars(_MaskedName); }
        }


        /// <summary>Gets or sets the comment for this element.</summary>
        public string Comment { get; set; }


        /// <summary>Gets or sets the text for this element.</summary>
        public string Text
        {
            get
            {
                return ToString();
            }
            set
            {
                StringRdr re = new StringRdr(value);
                _Read("", re);
            }
        }


        /// <summary>Gets if the element has child elements.</summary>
        public bool HasElements
        {
            get { return (_ChildElements.Count > 0); }
        }


        /// <summary>Gets the number of child elements for this element.</summary>
        public int ElementCount
        {
            get { return _ChildElements.Count; }
        }


        /// <summary>Gets or sets the element value.</summary>
        public Ddp.DdpValue Value
        {
            get { return _Value; }
            set { _Value = value; }
        }


        /// <summary>Gets the section child elements of this element.</summary>
        public IEnumerable<DdpElement> Sections
        {
            get { return _ChildElements.Values.Where(m => m.HasElements); }
        }


        /// <summary>Gets the plain value child elements of this element.</summary>
        public IEnumerable<DdpElement> Values
        {
            get { return _ChildElements.Values.Where(m => !m.HasElements); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the element value as a given type.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value.</returns>
        public T GetValue<T>(T defaultValue)
        {
            if(_Value._IsEmpty) return defaultValue;
            return GetValue<T>();
        }


        /// <summary>Gets the element value as a given type.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <returns>Value.</returns>
        public T GetValue<T>()
        {
            if(typeof(T) == typeof(string)) { return (T)(object) _Value._Value; }            
            if(typeof(T) == typeof(bool)) { return (T) (object) (bool) _Value; }

            if(typeof(T) == typeof(int)) { return (T)(object)(int) _Value; }
            if(typeof(T) == typeof(uint)) { return (T)(object)(uint) _Value; }
            if(typeof(T) == typeof(long)) { return (T)(object)(long) _Value; }
            if(typeof(T) == typeof(ulong)) { return (T)(object)(ulong) _Value; }
            if(typeof(T) == typeof(short)) { return (T)(object)(short) _Value; }
            if(typeof(T) == typeof(ushort)) { return (T)(object)(ushort) _Value; }
            if(typeof(T) == typeof(byte)) { return (T)(object)(byte) _Value; }
            if(typeof(T) == typeof(sbyte)) { return (T)(object)(sbyte) _Value; }

            if(typeof(T) == typeof(float)) { return (T)(object)(float) _Value; }
            if(typeof(T) == typeof(double)) { return (T)(object)(double) _Value; }
            if(typeof(T) == typeof(decimal)) { return (T)(object)(decimal) _Value; }

            if(typeof(T) == typeof(DateTime)) { return (T)(object)(DateTime) _Value; }

            if(typeof(T).IsEnum) { return (T) Enum.ToObject(typeof(T), (int) _Value); }

            if((typeof(T) == typeof(string[])) || (typeof(T) == typeof(IEnumerable<string>))) { return (T)(object)(string[]) _Value; }
            if((typeof(T) == typeof(bool[])) || (typeof(T) == typeof(IEnumerable<bool>))) { return (T) (object) (bool[]) _Value; }

            if((typeof(T) == typeof(int[])) || (typeof(T) == typeof(IEnumerable<int>))) { return (T)(object)(int[]) _Value; }
            if((typeof(T) == typeof(uint[])) || (typeof(T) == typeof(IEnumerable<uint>))) { return (T)(object)(uint[]) _Value; }
            if((typeof(T) == typeof(long[])) || (typeof(T) == typeof(IEnumerable<long>))) { return (T)(object)(long[]) _Value; }
            if((typeof(T) == typeof(ulong[])) || (typeof(T) == typeof(IEnumerable<ulong>))) { return (T)(object)(ulong[]) _Value; }
            if((typeof(T) == typeof(short[])) || (typeof(T) == typeof(IEnumerable<short>))) { return (T)(object)(short[]) _Value; }
            if((typeof(T) == typeof(ushort[])) || (typeof(T) == typeof(IEnumerable<ushort>))) { return (T)(object)(ushort[]) _Value; }
            if((typeof(T) == typeof(byte[])) || (typeof(T) == typeof(IEnumerable<byte>))) { return (T)(object)(byte[]) _Value; }
            if((typeof(T) == typeof(sbyte[])) || (typeof(T) == typeof(IEnumerable<sbyte>))) { return (T)(object)(sbyte[]) _Value; }

            if((typeof(T) == typeof(float[])) || (typeof(T) == typeof(IEnumerable<float>))) { return (T)(object)(float[]) _Value; }
            if((typeof(T) == typeof(double[])) || (typeof(T) == typeof(IEnumerable<double>))) { return (T)(object)(double[]) _Value; }
            if((typeof(T) == typeof(decimal[])) || (typeof(T) == typeof(IEnumerable<decimal>))) { return (T)(object)(decimal[]) _Value; }

            if((typeof(T) == typeof(DateTime[])) || (typeof(T) == typeof(IEnumerable<DateTime>))) { return (T)(object)(DateTime[]) _Value; }

            if(typeof(T).IsAssignableFrom(typeof(TList<string>))) { return (T)(object) new TList<string>((string[]) _Value); }

            if(typeof(T).IsAssignableFrom(typeof(TList<int>))) { return (T)(object) new TList<int>((int[]) _Value); }
            if(typeof(T).IsAssignableFrom(typeof(TList<uint>))) { return (T)(object) new TList<uint>((uint[]) _Value); }
            if(typeof(T).IsAssignableFrom(typeof(TList<long>))) { return (T)(object) new TList<long>((long[]) _Value); }
            if(typeof(T).IsAssignableFrom(typeof(TList<ulong>))) { return (T)(object) new TList<ulong>((ulong[]) _Value); }
            if(typeof(T).IsAssignableFrom(typeof(TList<short>))) { return (T)(object) new TList<short>((short[]) _Value); }
            if(typeof(T).IsAssignableFrom(typeof(TList<ushort>))) { return (T)(object) new TList<ushort>((ushort[]) _Value); }
            if(typeof(T).IsAssignableFrom(typeof(TList<byte>))) { return (T)(object) new TList<byte>((byte[]) _Value); }
            if(typeof(T).IsAssignableFrom(typeof(TList<sbyte>))) { return (T)(object) new TList<sbyte>((sbyte[]) _Value); }

            if(typeof(T).IsAssignableFrom(typeof(TList<float>))) { return (T)(object) new TList<float>((float[]) _Value); }
            if(typeof(T).IsAssignableFrom(typeof(TList<double>))) { return (T)(object) new TList<double>((double[]) _Value); }
            if(typeof(T).IsAssignableFrom(typeof(TList<decimal>))) { return (T)(object) new TList<decimal>((decimal[]) _Value); }

            if(typeof(T).IsAssignableFrom(typeof(TList<DateTime>))) { return (T)(object) new TList<DateTime>((DateTime[]) _Value); }

            throw new ArgumentException("Unable to cast value into type " + typeof(T).FullName + ".");
        }


        /// <summary>Sets the value.</summary>
        /// <param name="value">Value.</param>
        public void SetValue(object value)
        {
            if(value == null) { _Value._Value = _Value._MaskedValue = ""; return; }

            if(value is DdpElement) { ((DdpElement) value).CopyTo(this); return; }
            if(value is Ddp.DdpValue) { _Value._MaskedValue = ((Ddp.DdpValue) value)._MaskedValue; _Value._Value = ((Ddp.DdpValue) value)._Value; return; }

            if(value is string) { _Value = (string) value; return; }
            if(value is bool) { _Value = (bool) value; return; }

            if(value is int) { _Value = (int) value; return; }
            if(value is uint) { _Value = (uint) value; return; }
            if(value is long) { _Value = (long) value; return; }
            if(value is ulong) { _Value = (ulong) value; return; }
            if(value is short) { _Value = (short) value; return; }
            if(value is ushort) { _Value = (ushort) value; return; }
            if(value is byte) { _Value = (byte) value; return; }
            if(value is sbyte) { _Value = (sbyte) value; return; }

            if(value is float) { _Value = (float) value; return; }
            if(value is double) { _Value = (double) value; return; }
            if(value is decimal) { _Value = (decimal) value; return; }

            if(value is DateTime) { _Value = (DateTime) value; return; }

            if(value is IEnumerable<string>) { _Value._SetArrayValue<string>((IEnumerable<string>) value); return; }
            if(value is IEnumerable<bool>) { _Value._SetArrayValue<bool>((IEnumerable<bool>) value); return; }

            if(value is IEnumerable<int>) { _Value._SetArrayValue<int>((IEnumerable<int>) value); return; }
            if(value is IEnumerable<uint>) { _Value._SetArrayValue<uint>((IEnumerable<uint>) value); return; }
            if(value is IEnumerable<long>) { _Value._SetArrayValue<long>((IEnumerable<long>) value); return; }
            if(value is IEnumerable<ulong>) { _Value._SetArrayValue<ulong>((IEnumerable<ulong>) value); return; }
            if(value is IEnumerable<short>) { _Value._SetArrayValue<short>((IEnumerable<short>) value); return; }
            if(value is IEnumerable<ushort>) { _Value._SetArrayValue<ushort>((IEnumerable<ushort>) value); return; }
            if(value is IEnumerable<byte>) { _Value._SetArrayValue<byte>((IEnumerable<byte>) value); return; }
            if(value is IEnumerable<sbyte>) { _Value._SetArrayValue<sbyte>((IEnumerable<sbyte>) value); return; }

            if(value is IEnumerable<float>) { _Value._SetArrayValue<float>((IEnumerable<float>) value); return; }
            if(value is IEnumerable<double>) { _Value._SetArrayValue<double>((IEnumerable<double>) value); return; }
            if(value is IEnumerable<decimal>) { _Value._SetArrayValue<decimal>((IEnumerable<decimal>) value); return; }

            if(value is IEnumerable<DateTime>) { _Value._SetArrayValue<DateTime>((IEnumerable<DateTime>) value); return; }

            _Value._MaskedValue = _Value._Value = value.ToString();
        }


        /// <summary>Gets the element value for a path as a given type.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="path">Path.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value.</returns>
        public T Get<T>(string path, T defaultValue)
        {
            return this[path].GetValue<T>(defaultValue);
        }


        /// <summary>Gets the element value for a path as a given type.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="path">Path.</param>
        /// <returns>Value.</returns>
        public T Get<T>(string path)
        {
            return this[path].GetValue<T>();
        }


        /// <summary>Sets the element value for a path.</summary>
        /// <param name="path">Path.</param>
        /// <param name="value">Value.</param>
        /// <returns>This ddp element.</returns>
        public DdpElement Set(string path, object value)
        {
            this[path].SetValue(value);
            return this;
        }


        /// <summary>Removes this element.</summary>
        public void Remove()
        {
            if(Parent != null)
            {
                Parent._ChildElements.TryRemove(_MaskedName, out _);
            }
        }


        /// <summary>Removes all values and child elements from this element.</summary>
        public void Clear()
        {
            _Value._Update("");
            _ChildElements.Clear();
        }


        /// <summary>Moves this element to another element.</summary>
        /// <param name="target">Target element.</param>
        public void MoveTo(DdpElement target)
        {
            Remove();
            target._ChildElements.AddOrUpdate(_MaskedName, this, _MergeMove);
        }


        /// <summary>Copies this element to another element.</summary>
        /// <param name="target">Target element.</param>
        public void CopyTo(DdpElement target)
        {
            target._ChildElements.AddOrUpdate(_MaskedName, this, _MergeCopy);
        }


        /// <summary>Returns a string representation of this instance.</summary>
        /// <param name="encoder">Encoder.</param>
        /// <param name="singleLine">Creates the output as a single line.</param>
        /// <param name="skipComments">Creates the output without comments.</param>
        /// <returns>String.</returns>
        public string ToString(IEncoder encoder, bool singleLine = false, bool skipComments = false)
        {
            if(encoder == null) { return ToString(singleLine, skipComments); }

            return encoder.EncodeString(ToString(singleLine, skipComments));
        }


        /// <summary>Returns a string representation of this instance.</summary>
        /// <param name="singleLine">Creates the output as a single line.</param>
        /// <returns>String.</returns>
        public string ToString(bool singleLine)
        {
            return ToString(singleLine, false);
        }


        /// <summary>Returns a string representation of this instance.</summary>
        /// <param name="singleLine">Creates the output as a single line.</param>
        /// <param name="skipComments">Creates the output without comments.</param>
        /// <returns>String.</returns>
        public string ToString(bool singleLine, bool skipComments)
        {
            StringBuilder rval = new StringBuilder();

            _ToString(ref rval, 0, singleLine, skipComments);

            if(singleLine)
            {
                return rval.ToString().Trim();
            }

            return rval.ToString();
        }


        /// <summary>Saves the element to a file.</summary>
        /// <param name="file">File name.</param>
        public virtual void Save(string file)
        {
            Save(file, null);
        }


        /// <summary>Saves the element to a file.</summary>
        /// <param name="file">File name.</param>
        /// <param name="encoder">Encoder.</param>
        public virtual void Save(string file, IEncoder encoder)
        {
            File.WriteAllText(file, ToString(encoder));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Returns a string representation of this instance.</summary>
            /// <returns>String.</returns>
        public override string ToString()
        {
            return ToString(false);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<DdpElement>                                                                              //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<DdpElement> IEnumerable<DdpElement>.GetEnumerator()
        {
            return _ChildElements.Values.GetEnumerator();
        }




        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an enumerator for this instance.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _ChildElements.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // operators                                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Implicitly converts a DdpElement to a string.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator string(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a Boolean.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator bool(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to an integer.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator int(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to an unsigned integer.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator uint(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a short integer.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator short(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to an unsigned short integer.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator ushort(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a long integer.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator long(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to an unsigned long integer.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator ulong(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a byte.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator byte(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a signed byte.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator sbyte(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a float.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator float(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpValue to a float.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator double(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a decimal.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator decimal(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a decimal.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator DateTime(DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a string array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator string[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a Boolean array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator bool[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to an integer array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator int[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to an unsigned integer array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator uint[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a long integer array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator long[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to an unsigned long integer array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator ulong[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a short integer array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator short[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to an unsigned short integer array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator ushort[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a byte array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator byte[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a signed byte array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator sbyte[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a float array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator float[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a double array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator double[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to a decimal array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator decimal[](DdpElement value)
        {
            return value._Value;
        }


        /// <summary>Implicitly converts a DdpElement to an unsigned integer array.</summary>
        /// <param name="value">Value.</param>
        public static implicit operator DateTime[](DdpElement value)
        {
            return value._Value;
        }
    }
}
