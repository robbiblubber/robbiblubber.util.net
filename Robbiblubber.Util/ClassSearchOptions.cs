﻿using System;



namespace Robbiblubber.Util
{
    /// <summary>This enumeration defines options for class search methods.</summary>
    public enum ClassSearchOptions: int
    {
        /// <summary>Use default settings</summary>
        DEFAULT = 0,
        /// <summary>Load classes from loaded assemblies.</summary>
        LOADED_ASSEMBLIES = 1,
        /// <summary>Load classes from files.</summary>
        FILES = 2,
        /// <summary>Search directories recursively.</summary>
        RECURSIVE = 4,
        /// <summary>Try to load files with any extension (not only .exe and .dll)</summary>
        ALL_FILES = 8
    }
}
