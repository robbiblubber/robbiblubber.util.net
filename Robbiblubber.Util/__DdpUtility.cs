﻿using System.Collections.Generic;
using System.Text;



namespace Robbiblubber.Util
{
    /// <summary>This class provides ddp utility methods.</summary>
    internal static class __DdpUtility
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public constants                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Internal representation for the backslash character.</summary>
        public static readonly string BACKSLASH = new string(new char[] {(char) 0x1, (char) 0x16, (char) 0x16, (char) 0x16, (char) 0x11, (char) 0x1d});


        /// <summary>Internal representation for the slash character.</summary>
        public static readonly string SLASH = new string(new char[] { (char) 0x1, (char) 0x16, (char) 0x16, (char) 0x16, (char) 0x12, (char) 0x1d });


        /// <summary>Internal representation for the equals character.</summary>
        public static readonly string EQUALS = new string(new char[] { (char) 0x1, (char) 0x16, (char) 0x16, (char) 0x16, (char) 0x13, (char) 0x1d });


        /// <summary>Internal representation for the semicolon character.</summary>
        public static readonly string SEMICOLON = new string(new char[] { (char) 0x1, (char) 0x16, (char) 0x16, (char) 0x16, (char) 0x14, (char) 0x1d });


        /// <summary>Internal representation for the line break character.</summary>
        public static readonly string LINEBREAK = new string(new char[] { (char) 0x1, (char) 0x16, (char) 0x16, (char) 0x16, (char) 0x1f, (char) 0x1d });


        /// <summary>Internal representation for the comma character.</summary>
        public static readonly string COMMA = new string(new char[] { (char) 0x1, (char) 0x16, (char) 0x16, (char) 0x16, (char) 0xe, (char) 0x1d });


        /// <summary>Internal representation for the space character.</summary>
        public static readonly string SPACE = new string(new char[] { (char) 0x1, (char) 0x16, (char) 0x16, (char) 0x16, (char) 0x1, (char) 0x1d });


        /// <summary>Internal representation for the atb character.</summary>
        public static readonly string TAB = new string(new char[] { (char) 0x1, (char) 0x16, (char) 0x16, (char) 0x16, (char) 0x5, (char) 0x1d });


        /// <summary>Internal representation for the open bracket character.</summary>
        public static readonly string BRACKET_OPEN = new string(new char[] { (char) 0x1, (char) 0x16, (char) 0x16, (char) 0x16, (char) 0x2, (char) 0x1d });


        /// <summary>Internal representation for the closed bracket character.</summary>
        public static readonly string BRACKET_CLOSED = new string(new char[] { (char) 0x1, (char) 0x16, (char) 0x16, (char) 0x16, (char) 0x3, (char) 0x1d });



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reads and returns comment sections.</summary>
        /// <param name="re">String reader.</param>
        /// <param name="multiLine">Determines if the comment is multi-line.</param>
        /// <returns>Comment text.</returns>
        private static string _ReadOn(StringRdr re, bool multiLine)
        {
            StringBuilder rval = new StringBuilder();
            re.Read();

            while(re.Read())
            {
                if(multiLine)
                {
                    if((re.Current == '*') && (re.Next == '/'))
                    {
                        re.Read();
                        return rval.ToString();
                    }
                    else { rval.Append(re.Current); }
                }
                else if(re.Current == '\n')
                {
                    return rval.ToString();
                }
                else { rval.Append(re.Current); }
            }

            return rval.ToString();
        }


        /// <summary>Masks leading and tailing spaces in a string.</summary>
        /// <param name="str">String.</param>
        /// <param name="mask">Mask string.</param>
        /// <returns>Masked string.</returns>
        private static string _MaskPads(string str, string mask)
        {
            string pad = "";
            while(str.StartsWith(" "))
            {
                pad += mask;
                str = str.Substring(1);
            }
            str = (pad + str);

            pad = "";
            while(str.EndsWith(" "))
            {
                pad += mask;
                str = str.Substring(0, str.Length - 1);
            }
            str += pad;

            return str;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Masks special characters in a string.</summary>
        /// <param name="str">String.</param>
        /// <returns>Masked string.</returns>
        public static string MaskSpecialChars(string str)
        {
            if(str == null) { return ""; }

            str = str.Replace(@"\", @"\\")
                     .Replace("/", @"\/")
                     .Replace("=", @"\=")
                     .Replace(";", @"\;")
                     .Replace(",", @"\,")
                     .Replace("{", @"\{")
                     .Replace("}", @"\}")
                     .Replace("\r\n", "\n").Replace("\n", @"\n")
                     .Replace("\t", @"\t");

            return _MaskPads(str, @"\_");
        }


        /// <summary>Unmasks special characters in a string.</summary>
        /// <param name="str">Masked string.</param>
        /// <returns>Unmasked string.</returns>
        public static string UnmaskSpecialChars(string str)
        {
            if(str == null) { return ""; }

            return str.Replace(@"\/", "/")
                      .Replace(@"\=", "=")
                      .Replace(@"\;", ";")
                      .Replace(@"\,", ",")
                      .Replace(@"\{", "{")
                      .Replace(@"\}", "}")
                      .Replace(@"\n", "\r\n")
                      .Replace(@"\t", "\t")
                      .Replace(@"\_", " ")
                      .Replace(@"\\", @"\")
                      .Replace(SLASH, "/")
                      .Replace(EQUALS, "=")
                      .Replace(SEMICOLON, ";")
                      .Replace(COMMA, ",")
                      .Replace(BRACKET_OPEN, "{")
                      .Replace(BRACKET_CLOSED, "}")
                      .Replace(LINEBREAK, "\r\n")
                      .Replace(SPACE, " ")
                      .Replace(TAB, "\t")
                      .Replace(BACKSLASH, @"\");
        }


        /// <summary>Applies internal representation of special characters on a string.</summary>
        /// <param name="str">String.</param>
        /// <param name="masked">Determines if the string is already masked.</param>
        /// <returns>Masked string.</returns>
        public static string ApplyInternal(string str, bool masked = false)
        {
            if(str == null) { return ""; }

            if(masked)
            {
                str = str.Replace(@"\\", BACKSLASH)
                         .Replace(@"\/", SLASH)
                         .Replace(@"\=", EQUALS)
                         .Replace(@"\;", SEMICOLON)
                         .Replace(@"\,", COMMA)
                         .Replace(@"\{", BRACKET_OPEN)
                         .Replace(@"\}", BRACKET_CLOSED)
                         .Replace(@"\n", LINEBREAK)
                         .Replace(@"\t", TAB)
                         .Replace(@"\_", SPACE);
            }
            else
            {
                while(str.Contains("\r\n")) { str = str.Replace("\r\n", "\n"); }
                str = str.Replace("\\", BACKSLASH)
                         .Replace("/",  SLASH)
                         .Replace("=",  EQUALS)
                         .Replace(";",  SEMICOLON)
                         .Replace(",",  COMMA)
                         .Replace("{",  BRACKET_OPEN)
                         .Replace("}",  BRACKET_CLOSED)
                         .Replace("\n", LINEBREAK)
                         .Replace("\t", TAB);
            }

            return _MaskPads(str, SPACE);
        }


        /// <summary>Converts internally masked characters to masked characters.</summary>
        /// <param name="str">Internally masked string.</param>
        /// <returns>Masked string.</returns>
        public static string InternalToMask(string str)
        {
            if(str == null) { return null; }

            return str.Replace(BACKSLASH, @"\\")
                      .Replace(SLASH, @"\/")
                      .Replace(EQUALS, @"\=")
                      .Replace(SEMICOLON, @"\;")
                      .Replace(COMMA, @"\,")
                      .Replace(BRACKET_OPEN, @"\{")
                      .Replace(BRACKET_CLOSED, @"\}")
                      .Replace(LINEBREAK, @"\n")
                      .Replace(TAB, @"\t")
                      .Replace(SPACE, @"\_");
        }


        /// <summary>Parses a path expression.</summary>
        /// <param name="path">Path.</param>
        /// <returns>List of fully masked path elements.</returns>
        public static List<string> ParsePath(string path)
        {
            path = ApplyInternal(path, true);
            while(path.Contains("//")) { path = path.Replace("//", "/"); }
            return new List<string>(path.Split('/'));
        }


        /// <summary>Reads a line from stream.</summary>
        /// <param name="re">String reader.</param>
        /// <param name="comment">Comments read.</param>
        /// <returns>Line object.</returns>
        public static __DdpLine ReadLine(StringRdr re, ref string comment)
        {
            int term = __DdpLine.EOF;
            StringBuilder rval = new StringBuilder();

            while(re.Read())
            {
                if(re.Current == '\\')
                {
                    switch(re.Next)
                    {
                        case '\\': rval.Append(BACKSLASH); break;
                        case ';':  rval.Append(SEMICOLON); break;
                        case ',':  rval.Append(COMMA); break;
                        case '/':  rval.Append(SLASH); break;
                        case '=':  rval.Append(EQUALS); break;
                        case 'n':  rval.Append(LINEBREAK); break;
                        case 't':  rval.Append(TAB); break;
                        case '_':  rval.Append(SPACE); break;
                        case '{': rval.Append(BRACKET_OPEN); break;
                        case '}': rval.Append(BRACKET_CLOSED); break;
                        default:   rval.Append(re.Next); break;
                    }
                    re.Read();
                }
                else if(re.Current == '/')
                {
                    string c = null;
                    if(re.Next == '/')
                    {
                        c = _ReadOn(re, false);
                    }
                    else if(re.Next == '*')
                    {
                        c = _ReadOn(re, true);
                    }
                    else { rval.Append('/'); }

                    if(c != null)
                    { 
                        if(string.IsNullOrWhiteSpace(comment))
                        { comment = ""; }
                        else { comment += "\r\n"; }
                        comment += c;
                    }
                }
                else if(re.Current == ';')
                {
                    term = __DdpLine.SEMICOLON;
                    break;
                } 
                else if(re.Current == '{')
                {
                    term = __DdpLine.BRACKET_OPEN;
                    break;
                } 
                else if(re.Current == '}')
                {
                    term = __DdpLine.BRACKET_CLOSE;
                    break;
                }
                else
                {
                    rval.Append(re.Current);
                }
            }

            return new __DdpLine(rval.ToString(), term);
        }
    }
}
