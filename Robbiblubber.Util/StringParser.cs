﻿using System;



namespace Robbiblubber.Util
{
    /// <summary>This class implements a string parser.</summary>
    public class StringParser: StringRdr
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="str">Value.</param>
        /// <param name="position">Position.</param>
        public StringParser(string str = null, int position = 0): base(str, position)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets token delimiters.</summary>
        public virtual string[] Delimiters { get; set; } = new string[] { " ", "\t", "\r", "\n" };


        /// <summary>Gets the current string part.</summary>
        public virtual string CurrentPart { get; protected set; }


        /// <summary>Gets the current delimiter.</summary>
        public virtual string CurrentDelimiter { get; protected set; } = "";



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reads to the next occurance of a given token or any of a number of tokens.</summary>
        /// <param name="delimiters">Token or tokens.</param>
        /// <returns>Returns the part read.</returns>
        public virtual string ReadPart(params string[] delimiters)
        {
            if((delimiters == null) || (delimiters.Length == 0)) { delimiters = Delimiters; }

            int pos = Length;
            int n;
            string del = "";

            foreach(string i in delimiters)
            {
                n = _String.IndexOf(i, Position);

                if(n < 0) continue;
                if(n < pos)
                {
                    pos = n;
                    del = i;
                }
            }

            CurrentDelimiter = del;
            CurrentPart = _String.Substring(Position, pos - Position);
            Position = (pos + del.Length);
            
            return CurrentPart;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] StringRdr                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the preceding character.</summary>
        public override char Preceding
        {
            get { return String[Position - CurrentDelimiter.Length]; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // operators                                                                                                    //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Casts a string to a string parser.</summary>
        /// <param name="str">String.</param>
        public static explicit operator StringParser(string str)
        {
            return new StringParser(str);
        }


        /// <summary>Casts a string parser to a string.</summary>
        /// <param name="p">String parser.</param>
        public static explicit operator string(StringParser p)
        {
            return p.String;
        }
    }
}
