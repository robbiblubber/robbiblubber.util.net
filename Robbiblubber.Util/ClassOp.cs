﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Linq;


namespace Robbiblubber.Util
{
    /// <summary>This class provides class loader functionallity.</summary>
    public static class ClassOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private constants                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Default class search options.</summary>
        private const ClassSearchOptions _DEFAULT_OPTIONS = ClassSearchOptions.LOADED_ASSEMBLIES | ClassSearchOptions.FILES | ClassSearchOptions.RECURSIVE;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if the value is the default value of its type.</summary>
        /// <param name="value">Value.</param>
        /// <returns>Returns TRUE if the value is default, otherwise returns FALSE.</returns>
        public static bool IsDefault(object value)
        {
            if(value == null) { return true; }
            if(!value.GetType().IsValueType) { return false; }

            return value.Equals(Activator.CreateInstance(value.GetType()));
        }


        /// <summary>Loads a type.</summary>
        /// <param name="typeName">Type name.</param>
        /// <param name="filesOrDirectories">Files or directories to search.</param>
        /// <returns>Type.</returns>
        /// <remarks>Will search the application directory and its subdirectories when no directory is specified.</remarks>
        public static Type LoadType(string typeName, params string[] filesOrDirectories)
        {
            return LoadType(typeName, ClassSearchOptions.DEFAULT, filesOrDirectories);
        }


        /// <summary>Loads a type.</summary>
        /// <param name="typeName">Type name.</param>
        /// <param name="options">Specifies search options.</param>
        /// <param name="filesOrDirectories">Files or directories to search.</param>
        /// <returns>Type.</returns>
        /// <remarks>Will search the application directory and its subdirectories when no directory is specified.</remarks>
        public static Type LoadType(string typeName, ClassSearchOptions options, params string[] filesOrDirectories)
        {
            if(options == ClassSearchOptions.DEFAULT) { options = _DEFAULT_OPTIONS; }

            if((options & ClassSearchOptions.LOADED_ASSEMBLIES) == ClassSearchOptions.LOADED_ASSEMBLIES)
            {
                foreach(Assembly i in AppDomain.CurrentDomain.GetAssemblies())
                {
                    try
                    {
                        if(i.GetType(typeName) != null) { return i.GetType(typeName); }
                    }
                    catch(Exception) {}
                }
            }

            if((options & ClassSearchOptions.FILES) == ClassSearchOptions.FILES)
            {
                SearchOption rec = (((options & ClassSearchOptions.RECURSIVE) == ClassSearchOptions.RECURSIVE) ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
                if((filesOrDirectories == null) || (filesOrDirectories.Length == 0)) { filesOrDirectories = new string[] { PathOp.ApplicationDirectory }; }

                foreach(string i in filesOrDirectories)
                {
                    IEnumerable<string> files;
                    try
                    {
                        if(File.Exists(i))
                        {
                            files = new string[] { i };
                        }
                        else { files = Directory.GetFiles(PathOp.ApplicationDirectory, "*.*", rec); }
                    }
                    catch(Exception) { continue; }

                    foreach(string k in files)
                    {
                        if(!((options & ClassSearchOptions.ALL_FILES) == ClassSearchOptions.ALL_FILES))
                        {
                            if(!(k.ToLower().EndsWith(".exe") || k.ToLower().EndsWith(".dll"))) continue;
                        }

                        try
                        {
                            Assembly asm = Assembly.LoadFile(k);
                            if((asm != null) && (asm.GetType(typeName) != null)) { return asm.GetType(typeName); }
                        }
                        catch(Exception) {}
                    }
                }
            }

            return null;
        }


        /// <summary>Loads a type and creates an instance.</summary>
        /// <param name="typeName">Type name.</param>
        /// <param name="args">Constructor arguments.</param>
        /// <returns>Instance.</returns>
        public static object Load(string typeName, params object[] args)
        {
            return Load(typeName, ClassSearchOptions.DEFAULT, null, true, args);
        }


        /// <summary>Loads a type and creates an instance.</summary>
        /// <param name="typeName">Type name.</param>
        /// <param name="attach">Attach flag.</param>
        /// <param name="args">Constructor arguments.</param>
        /// <returns>Instance.</returns>
        public static object Load(string typeName, bool attach, params object[] args)
        {
            return Load(typeName, ClassSearchOptions.DEFAULT, null, attach, args);
        }


        /// <summary>Loads a type and creates an instance.</summary>
        /// <param name="typeName">Type name.</param>
        /// <param name="filesOrDirectories">Files or directories to search.</param>
        /// <param name="args">Constructor arguments.</param>
        /// <returns>Instance.</returns>
        public static object Load(string typeName, IEnumerable<string> filesOrDirectories, params object[] args)
        {
            return Load(typeName, ClassSearchOptions.DEFAULT, filesOrDirectories, true, args);
        }


        /// <summary>Loads a type and creates an instance.</summary>
        /// <param name="typeName">Type name.</param>
        /// <param name="filesOrDirectories">Files or directories to search.</param>
        /// <param name="attach">Attach flag.</param>
        /// <param name="args">Constructor arguments.</param>
        /// <returns>Instance.</returns>
        public static object Load(string typeName, IEnumerable<string> filesOrDirectories, bool attach, params object[] args)
        {
            return Load(typeName, ClassSearchOptions.DEFAULT, filesOrDirectories, attach, args);
        }


        /// <summary>Loads a type and creates an instance.</summary>
        /// <param name="typeName">Type name.</param>
        /// <param name="options">Specifies search options.</param>
        /// <param name="args">Constructor arguments.</param>
        /// <returns>Instance.</returns>
        public static object Load(string typeName, ClassSearchOptions options, params object[] args)
        {
            return Load(typeName, options, null, true, args);
        }


        /// <summary>Loads a type and creates an instance.</summary>
        /// <param name="typeName">Type name.</param>
        /// <param name="options">Specifies search options.</param>
        /// <param name="attach">Attach flag.</param>
        /// <param name="args">Constructor arguments.</param>
        /// <returns>Instance.</returns>
        public static object Load(string typeName, ClassSearchOptions options, bool attach, params object[] args)
        {
            return Load(typeName, options, null, attach, args);
        }


        /// <summary>Loads a type and creates an instance.</summary>
        /// <param name="typeName">Type name.</param>
        /// <param name="options">Specifies search options.</param>
        /// <param name="filesOrDirectories">Files or directories to search.</param>
        /// <param name="args">Constructor arguments.</param>
        /// <returns>Instance.</returns>
        public static object Load(string typeName, ClassSearchOptions options, IEnumerable<string> filesOrDirectories, params object[] args)
        {
            return Load(typeName, options, filesOrDirectories, true, args);
        }


        /// <summary>Loads a type and creates an instance.</summary>
        /// <param name="typeName">Type name.</param>
        /// <param name="options">Specifies search options.</param>
        /// <param name="filesOrDirectories">Files or directories to search.</param>
        /// <param name="attach">Attach flag.</param>
        /// <param name="args">Constructor arguments.</param>
        /// <returns>Instance.</returns>
        public static object Load(string typeName, ClassSearchOptions options, IEnumerable<string> filesOrDirectories, bool attach, params object[] args)
        {
            Type t = LoadType(typeName, options, (filesOrDirectories == null) ? new string[0] : filesOrDirectories.ToArray());
            object rval = null;

            if(t != null)
            {
                try
                {
                    if((args == null) || (args.Length == 0))
                    {
                        rval = Activator.CreateInstance(t);
                    }
                    else { rval = Activator.CreateInstance(t, args); }
                }
                catch(Exception) {}
            }

            if(attach && (rval != null))
            {
                Attach(rval);
            }

            return rval;
        }


        /// <summary>Attaches an object with all its references.</summary>
        /// <param name="obj">Object.</param>
        public static void Attach(object obj)
        {
            Type t = ((obj is Type) ? (Type) obj : obj.GetType());
            List<Assembly> asm = null;

            foreach(AssemblyName i in t.Assembly.GetReferencedAssemblies())
            {
                try
                {
                    AppDomain.CurrentDomain.Load(i);
                }
                catch(Exception)
                {
                    if(asm == null)
                    {
                        asm = new List<Assembly>();

                        foreach(string k in Directory.GetFiles(Path.GetDirectoryName(t.Assembly.Location), "*.*", SearchOption.AllDirectories))
                        {
                            if(k.ToLower().EndsWith(".exe") || k.ToLower().EndsWith(".dll"))
                            {
                                try { asm.Add(Assembly.LoadFrom(k)); } catch(Exception) {}
                            }
                        }

                        foreach(Assembly k in asm)
                        {
                            if(k.GetName().FullName == i.FullName)  { AppDomain.CurrentDomain.Load(i); }
                        }
                    }
                }
            }
        }


        /// <summary>Gets all subtypes of a type.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="filesOrDirectories">Files or directories to search.</param>
        /// <returns>Array of types.</returns>
        public static Type[] GetSubtypesOf<T>(params string[] filesOrDirectories)
        {
            return GetSubtypesOf<T>(ClassSearchOptions.DEFAULT, filesOrDirectories);
        }


        /// <summary>Gets all subtypes of a type.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="options">Specifies search options.</param>
        /// <param name="filesOrDirectories">Files or directories to search.</param>
        /// <returns>Array of types.</returns>
        public static Type[] GetSubtypesOf<T>(ClassSearchOptions options, params string[] filesOrDirectories)
        {
            return GetSubtypesOf(typeof(T), options, filesOrDirectories);
        }


        /// <summary>Gets all subtypes of a type.</summary>
        /// <param name="t">Type.</param>
        /// <param name="filesOrDirectories">Files or directories to search.</param>
        /// <returns>Array of types.</returns>
        /// <remarks>GetSubtypesOf() will search the application directory and its subdirectories when no directory is specified.</remarks>
        public static Type[] GetSubtypesOf(this Type t, params string[] filesOrDirectories)
        {
            return GetSubtypesOf(t, ClassSearchOptions.DEFAULT, filesOrDirectories);
        }


        /// <summary>Gets all subtypes of a type.</summary>
        /// <param name="t">Type.</param>
        /// <param name="options">Specifies search options.</param>
        /// <param name="filesOrDirectories">Files or directories to search.</param>
        /// <returns>Array of types.</returns>
        /// <remarks>GetSubtypesOf() will search the application directory and its subdirectories when no directory is specified.</remarks>
        public static Type[] GetSubtypesOf(this Type t, ClassSearchOptions options, params string[] filesOrDirectories)
        {
            List<Assembly> asms = new List<Assembly>();
            List<Type> rval = new List<Type>();

            if(options == ClassSearchOptions.DEFAULT) { options = _DEFAULT_OPTIONS; }
            if((options & ClassSearchOptions.LOADED_ASSEMBLIES) == ClassSearchOptions.LOADED_ASSEMBLIES) { asms.AddRange(AppDomain.CurrentDomain.GetAssemblies()); }

            if((options & ClassSearchOptions.FILES) == ClassSearchOptions.FILES)
            {
                SearchOption rec = (((options & ClassSearchOptions.RECURSIVE) == ClassSearchOptions.RECURSIVE) ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
                if((filesOrDirectories == null) || (filesOrDirectories.Length == 0)) { filesOrDirectories = new string[] { PathOp.ApplicationDirectory }; }

                foreach(string i in filesOrDirectories)
                {
                    IEnumerable<string> files;
                    try
                    {
                        if(File.Exists(i))
                        {
                            files = new string[] { i };
                        }
                        else { files = Directory.GetFiles(PathOp.ApplicationDirectory, "*.*", rec); }
                    }
                    catch(Exception) { continue; }

                    foreach(string k in files)
                    {
                        if(!((options & ClassSearchOptions.ALL_FILES) == ClassSearchOptions.ALL_FILES))
                        {
                            if(!(k.ToLower().EndsWith(".exe") || k.ToLower().EndsWith(".dll"))) continue;
                        }

                        try
                        {
                            Assembly asm = Assembly.LoadFile(k);
                            if((asm != null) && (!asms.Contains(asm))) { asms.Add(asm); }
                        }
                        catch(Exception) {}
                    }
                }
            }

            foreach(Assembly i in asms)
            {
                try
                {
                    foreach(Type j in i.GetTypes())
                    {
                        try
                        {
                            if(t.IsAssignableFrom(j)) { rval.Add(j); }
                        }
                        catch(Exception) {}
                    }
                }
                catch(Exception) {}
            }

            return rval.ToArray();
        }


        /// <summary>Loads all subtypes of a type and returns an array of instances.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="args">Arguments.</param>
        /// <returns>Array of objects.</returns>
        public static T[] LoadSubtypesOf<T>(params object[] args)
        {
            return LoadSubtypesOf<T>(ClassSearchOptions.DEFAULT, null, args);
        }


        /// <summary>Loads all subtypes of a type and returns an array of instances.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="options">Class search options.</param>
        /// <param name="args">Arguments.</param>
        /// <returns>Array of objects.</returns>
        public static T[] LoadSubtypesOf<T>(ClassSearchOptions options, params object[] args)
        {
            return LoadSubtypesOf<T>(options, null, args);
        }


        /// <summary>Loads all subtypes of a type and returns an array of instances.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="options">Class search options.</param>
        /// <param name="filesOrDirectories">Files or directories to search.</param>
        /// <param name="args">Arguments.</param>
        /// <returns>Array of objects.</returns>
        public static T[] LoadSubtypesOf<T>(ClassSearchOptions options, IEnumerable<string> filesOrDirectories, params object[] args)
        {
            return Array.ConvertAll(LoadSubtypesOf(typeof(T), options, filesOrDirectories, args), new Converter<object, T>(m => (T) m));
        }


        /// <summary>Loads all subtypes of a type and returns an array of instances.</summary>
        /// <param name="t">Type.</param>
        /// <param name="args">Arguments.</param>
        /// <returns>Array of objects.</returns>
        public static object[] LoadSubtypesOf(Type t, params object[] args)
        {
            return LoadSubtypesOf(t, ClassSearchOptions.DEFAULT, null, args);
        }


        /// <summary>Loads all subtypes of a type and returns an array of instances.</summary>
        /// <param name="t">Type.</param>
        /// <param name="directories">Directories to search.</param>
        /// <param name="args">Arguments.</param>
        /// <returns>Array of objects.</returns>
        public static object[] LoadSubtypesOf(Type t, IEnumerable<string> directories, params object[] args)
        {
            return LoadSubtypesOf(t, ClassSearchOptions.DEFAULT, directories, args);
        }


        /// <summary>Loads all subtypes of a type and returns an array of instances.</summary>
        /// <param name="t">Type.</param>
        /// <param name="options">Specifies search options.</param>
        /// <param name="args">Arguments.</param>
        /// <returns>Array of objects.</returns>
        public static object[] LoadSubtypesOf(Type t, ClassSearchOptions options, params object[] args)
        {
            return LoadSubtypesOf(t, options, null, args);
        }


        /// <summary>Loads all subtypes of a type and returns an array of instances.</summary>
        /// <param name="t">Type.</param>
        /// <param name="options">Specifies search options.</param>
        /// <param name="directories">Directories to search.</param>
        /// <param name="args">Arguments.</param>
        /// <returns>Array of objects.</returns>
        public static object[] LoadSubtypesOf(Type t, ClassSearchOptions options, IEnumerable<string> directories, params object[] args)
        {
            List<object> rval = new List<object>();

            foreach(Type i in GetSubtypesOf(t, options, (directories == null) ? new string[0] : directories.ToArray()))
            {
                if(i.IsAbstract) continue;

                try
                {
                    if((args == null) || (args.Length == 0))
                    {
                        rval.Add(Activator.CreateInstance(i));
                    }
                    else
                    {
                        rval.Add(Activator.CreateInstance(i, args));
                    }
                }
                catch(Exception) { }
            }

            return rval.ToArray();
        }
    }
}
