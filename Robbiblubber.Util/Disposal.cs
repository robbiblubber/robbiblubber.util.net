﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;



namespace Robbiblubber.Util
{
    /// <summary>Disposal helper class.</summary>
    public static class Disposal
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Releases and disposes COM objects.</summary>
        /// <param name="o">Object.</param>
        [SupportedOSPlatform("windows")]
        public static void ReleaseComObjects(params object[] o)
        {
            foreach(object i in o)
            {
                try
                {
                    Marshal.ReleaseComObject(i);
                    GC.Collect();
                }
                catch(Exception) {}
            }

            Recycle();
        }


        /// <summary>Disposes objects.</summary>
        /// <param name="o">Object.</param>
        public static void Dispose(params object[] o)
        {
            foreach(object i in o)
            {
                if(i == null) continue;

                MethodInfo m = i.GetType().GetMethod("Close");

                if(m != null)
                {
                    try
                    {
                        m.Invoke(i, new object[] {});
                    }
                    catch(Exception) {}
                }

                m = i.GetType().GetMethod("Dispose");

                if(m != null)
                {
                    try
                    {
                        m.Invoke(i, new object[] {});
                    }
                    catch(Exception) {}
                }
            }
        }


        /// <summary>Disposes objects and forces garbage collector to collect all disposed objects.</summary>
        /// <param name="o">Object.</param>
        public static void Recycle(params object[] o)
        {
            Dispose(o);
            Recycle();
        }


        /// <summary>Forces garbage collector to collect all disposed objects.</summary>
        public static void Recycle()
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}