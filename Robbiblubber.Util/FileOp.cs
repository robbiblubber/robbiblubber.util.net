﻿using System;
using System.Collections.Generic;
using System.IO;



namespace Robbiblubber.Util
{
    /// <summary>This class provides File utitility methods.</summary>
    public static class FileOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Copies a file.</summary>
        /// <param name="source">Source file name.</param>
        /// <param name="destination">Destination file name.</param>
        /// <param name="overwrite">Determines if existing file should be overwritten.</param>
        public static void CopyFile(string source, string destination, bool overwrite = true)
        {
            File.Copy(source, destination, overwrite);
        }


        /// <summary>Copies files.</summary>
        /// <param name="source">Source file. May include wildcards.</param>
        /// <param name="destination">Destination directory.</param>
        /// <param name="recursive">Determines if matching files in subdirectories will be copied.</param>
        /// <param name="overwrite">Determines if existing files should be overwritten.</param>
        public static void CopyFiles(string source, string destination, bool recursive = false, bool overwrite = true)
        {
            foreach(string i in Directory.GetFiles(Path.GetDirectoryName(source), Path.GetFileName(source), recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly))
            {
                string dir = i.Substring(Path.GetDirectoryName(source).Length + 1);
                if(dir.Contains(@"\"))
                {
                    dir = (destination.TrimEnd('\\') + @"\" + Path.GetDirectoryName(dir));
                }
                else { dir = destination.TrimEnd('\\'); }

                if(!Directory.Exists(dir)) { Directory.CreateDirectory(dir); }

                File.Copy(i, dir + @"\" + Path.GetFileName(i), overwrite);
            }
        }


        /// <summary>Tries to copy a file and returns a success value.</summary>
        /// <param name="source">Source file name.</param>
        /// <param name="destination">Destination file name.</param>
        /// <param name="overwrite">Determines if existing file should be overwritten.</param>
        /// <returns>Returns TRUE if the file has successfully been copied, otherwise returns FALSE.</returns>
        public static bool TryCopyFile(string source, string destination, bool overwrite = true)
        {
            try
            {
                File.Copy(source, destination, overwrite);
            }
            catch(Exception) { return false; }

            return true;
        }


        /// <summary>Deletes one or more files.</summary>
        /// <param name="fileName">File name. The file name may include wildcards.</param>
        /// <param name="recursive">Determines if files in subdircetories will be deleted.</param>
        /// <param name="throwException">Determines if the method will throw an exception if an error occurs.</param>
        /// <exception cref="IOException">Thrown when the files could not be deleted.</exception>
        public static void Delete(string fileName, bool recursive = false, bool throwException = true)
        {
            try
            {
                foreach(string i in Directory.GetFiles(Path.GetDirectoryName(fileName), Path.GetFileName(fileName), recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly))
                {
                    try
                    {
                        File.Delete(i);
                    }
                    catch(Exception) { if(throwException) throw; }
                }
            }
            catch(Exception) { if(throwException) throw; }
        }


        /// <summary>Copies a directory.</summary>
        /// <param name="source">Source directory name.</param>
        /// <param name="destination">Destination directory name.</param>
        /// <param name="recursive">Determines if subdirectories should be copied.</param>
        /// <param name="overwrite">Determines if existing files should be overwritten.</param>
        public static void CopyDirectory(string source, string destination, bool recursive = true, bool overwrite = true)
        {
            DirectoryInfo src = new DirectoryInfo(source);

            if(!src.Exists) { throw new DirectoryNotFoundException("Source directory not found: " + source); }

            if(!Directory.Exists(destination))
            {
                Directory.CreateDirectory(destination);
            }

            foreach(FileInfo i in src.GetFiles())
            {
                if(!overwrite)
                {
                    if(File.Exists(Path.Combine(destination, i.Name))) continue;
                }

                i.CopyTo(Path.Combine(destination, i.Name), overwrite);
            }

            if(recursive)
            {
                foreach(DirectoryInfo i in src.GetDirectories())
                {
                    CopyDirectory(i.FullName, Path.Combine(destination, i.Name), true, overwrite);
                }
            }
        }


        /// <summary>Tries to copy a directory and returns a success value.</summary>
        /// <param name="source">Source directory name.</param>
        /// <param name="destination">Destination directory name.</param>
        /// <param name="recursive">Determines if subdirectories should be copied.</param>
        /// <param name="overwrite">Determines if existing files should be overwritten.</param>
        /// <returns>Returns TRUE if the directory has successfully been copied, otherwise returns FALSE.</returns>
        public static bool TryCopyDirectory(string source, string destination, bool recursive = true, bool overwrite = true)
        {
            bool rval = true;
            DirectoryInfo src = new DirectoryInfo(source);

            if(!src.Exists) { return false; }

            if(!Directory.Exists(destination))
            {
                try
                {
                    Directory.CreateDirectory(destination);
                }
                catch(Exception) { return false; }
            }

            foreach(FileInfo i in src.GetFiles())
            {
                try
                {
                    i.CopyTo(Path.Combine(destination, i.Name), overwrite);
                }
                catch(Exception) { rval = false; }
            }

            if(recursive)
            {
                foreach(DirectoryInfo i in src.GetDirectories())
                {
                    rval = rval && TryCopyDirectory(i.FullName, Path.Combine(destination, i.Name), true, overwrite);
                }
            }

            return rval;
        }


        /// <summary>Deletes all contents of a directory.</summary>
        /// <param name="directoryName">Directory name.</param>
        /// <param name="keep">Files or directoryies to keep, allows wildcards.</param>
        /// <returns>Returns an array of files that haven't been deleted.</returns>
        /// <exception cref="IOException">Thrown when one or more files could not be deleted.</exception>
        public static string[] PurgeDirectory(string directoryName, params string[] keep)
        {
            return PurgeDirectory(directoryName, false, keep);
        }


        /// <summary>Deletes all contents of a directory.</summary>
        /// <param name="directoryName">Directory name.</param>
        /// <param name="ignoreErrors">Determines if errors will be ignored.</param>
        /// <param name="keep">Files or directoryies to keep, allows wildcards.</param>
        /// <returns>Returns an array of files that haven't been deleted.</returns>
        /// <exception cref="IOException">Thrown when one or more files could not be deleted. No exception will be thrown if ignoreErrors flag is set.</exception>
        public static string[] PurgeDirectory(string directoryName, bool ignoreErrors, params string[] keep)
        {
            if(keep == null) { keep = new string[0]; }
            List<string> faulty = new List<string>();

            if(ignoreErrors && (!Directory.Exists(directoryName))) { return new string[0]; }

            foreach(string i in Directory.GetDirectories(directoryName))
            {
                if(Path.GetFileName(i).Matches(keep)) continue;

                try
                {
                    Directory.Delete(i, true);
                }
                catch(Exception) { faulty.Add(i); }
            }

            foreach(string i in Directory.GetFiles(directoryName))
            {
                if(Path.GetFileName(i).Matches(keep)) continue;

                try
                {
                    File.Delete(i);
                }
                catch(Exception) { faulty.Add(i); }
            }

            if(ignoreErrors || (faulty.Count == 0)) { return faulty.ToArray(); }

            string m = "";
            foreach(string i in faulty)
            {
                if(m != "") { m += ", "; }
                m += i;
            }

            throw new IOException("Failed to delete " + m + ".");
        }


        /// <summary>Creates a temporary path.</summary>
        /// <returns>Temporary path.</returns>
        public static string CreateTempPath()
        {
            string rval;

            while(true)
            {
                rval = PathOp.TempRoot + '\\' + StringOp.Random(32);
                if(!Directory.Exists(rval)) break;
            }
            Directory.CreateDirectory(rval);

            return rval;
        }


        /// <summary>Cleans up temporary PathOp.</summary>
        public static void CleanupTemporaryPaths()
        {
            foreach(string i in Directory.GetDirectories(PathOp.TempRoot))
            {
                if(DateTime.Now - Directory.GetCreationTime(i) > new TimeSpan(1, 0, 0))
                {
                    try
                    {
                        Directory.Delete(i);
                    }
                    catch(Exception) {}
                }
            }
        }


        /// <summary>Returns a relative path from one path to another.</summary>
        /// <param name="relativeTo">The source path the result should be relative to. This path is always considered to be a directory.</param>
        /// <param name="path">The destination path.</param>
        /// <returns>The relative path, or path if the paths don't share the same root.</returns>
        public static string GetRelativePath(string relativeTo, string path)
        {
            string filePart = Path.GetFileName(path);
            path = Path.GetDirectoryName(path) + '\\';
            relativeTo = relativeTo.TrimEnd('\\') + '\\';

            string common = "";
            while(relativeTo.IndexOf('\\', common.Length) >= 0)
            {
                string part = relativeTo.Substring(0, relativeTo.IndexOf('\\', common.Length) + 1).ToLower();
                if(!path.ToLower().StartsWith(part)) break;
                common = part;
            }
            if(string.IsNullOrWhiteSpace(common)) { return path; }

            string rval = "";
            path = path.Substring(common.Length);
            relativeTo = relativeTo.Substring(common.Length);

            if(!string.IsNullOrWhiteSpace(relativeTo))
            {
                while(relativeTo.IndexOf('\\') >= 0)
                {
                    rval += @"..\";
                    relativeTo = relativeTo.Substring(relativeTo.IndexOf('\\') + 1);
                }
            }

            return rval + path + filePart;
        }


        /// <summary>Gets if a file name is valid.</summary>
        /// <param name="fileName">File name.</param>
        /// <returns>Returns TRUE if the file name is possibly valid, otherwise returns FALSE.</returns>
        public static bool ValidFileName(string fileName)
        {
            FileInfo rval = null;
            try
            {
                rval = new FileInfo(fileName);
            }
            catch(Exception) {}

            return (rval != null);
        }
    }
}
