﻿using System;
using System.Reflection;



namespace Robbiblubber.Util
{
    /// <summary>This class provides versioning utility functionality.</summary>
    public static class VersionOp
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the application version.</summary>
        public static Version ApplicationVersion
        {
            get { return Assembly.GetEntryAssembly().GetName().Version; }
        }


        /// <summary>Gets the calling assembly version.</summary>
        public static Version AssemblyVersion
        {
            get { return Assembly.GetCallingAssembly().GetName().Version; }
        }


        /// <summary>Gets the application copyright string.</summary>
        public static string ApplicationCopyright
        {
            get
            {
                object[] attributes = Assembly.GetEntryAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if(attributes.Length == 0) return "";

                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }


        /// <summary>Converts a string to a version.</summary>
        /// <param name="version">Version string.</param>
        /// <param name="defaultValue">Default version.</param>
        /// <returns>Version.</returns>
        public static Version ToVersion(string version, Version defaultValue)
        {
            try
            {
                return new Version(version);
            }
            catch(Exception) {}

            return defaultValue;
        }


        /// <summary>Converts a string to a version.</summary>
        /// <param name="version">Version string.</param>
        /// <param name="defaultValue">Default version string.</param>
        /// <returns>Version.</returns>
        public static Version ToVersion(string version, string defaultValue = "1.0.0")
        {
            return ToVersion(version, new Version(defaultValue));
        }


        /// <summary>Returns if a string can be converted into a version.</summary>
        /// <param name="version">Version.</param>
        /// <returns>Returns TRUE if the string is a version, otherwise returns FALSE.</returns>
        public static bool IsVersion(string version)
        {
            try 
            {
                _ = new Version(version); return true;
            }
            catch(Exception) {}

            return false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // extension methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a three-part version string.</summary>
        /// <param name="version">Version.</param>
        /// <returns>Version string.</returns>
        public static string ToVersionString(this Version version)
        {
            if(version is null) { return "0.0"; }

            if(version.Build < 0)
            {
                return (version.Major.ToString() + "." + version.Minor.ToString());
            }

            return (version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString());
        }



        /// <summary>Returns the assembly version for an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Version.</returns>
        public static Version GetVersion(this object obj)
        {
            return ((obj is Type) ? ((Type)obj) : obj.GetType()).Assembly.GetName().Version;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the assembly version for an object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Version.</returns>
        public static Version VersionOf(object obj)
        {
            return GetVersion(obj);
        }
    }
}
