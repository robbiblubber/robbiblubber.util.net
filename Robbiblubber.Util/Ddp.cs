﻿using Robbiblubber.Util.Coding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;



namespace Robbiblubber.Util
{
    /// <summary>This class represents a ddp root element.</summary>
    public class Ddp: DdpElement
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Ddp(): base(null, null)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="s">Source string.</param>
        public Ddp(string s): this(Source.UNKNOWN, s, null, null)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        /// <param name="s">Source string.</param>
        public Ddp(string s, IEncoder encoder) : this(Source.UNKNOWN, s, encoder, null, null)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="s">Source string.</param>
        /// <param name="user">User name.</param>
        /// <param name="password">Password.</param>
        public Ddp(string s, string user, string password): this(Source.UNKNOWN, s, user, password)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        /// <param name="s">Source string.</param>
        /// <param name="user">User name.</param>
        /// <param name="password">Password.</param>
        public Ddp(string s, IEncoder encoder, string user, string password) : this(Source.UNKNOWN, s, encoder, user, password)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="source">Source type.</param>
        /// <param name="s">Source string.</param>
        public Ddp(Source source, string s): this(source, s, null, null)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        /// <param name="source">Source type.</param>
        /// <param name="s">Source string.</param>
        public Ddp(Source source, string s, IEncoder encoder): this(source, s, encoder, null, null)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="source">Source type.</param>
        /// <param name="s">Source string.</param>
        /// <param name="user">User name.</param>
        /// <param name="password">Password.</param>
        public Ddp(Source source, string s, string user, string password): this(source, s, null, user, password)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        /// <param name="source">Source type.</param>
        /// <param name="s">Source string.</param>
        /// <param name="user">User name.</param>
        /// <param name="password">Password.</param>
        public Ddp(Source source, string s, IEncoder encoder, string user, string password): base(null, null)
        {
            if(source == Source.UNKNOWN)
            {
                if(s == null)
                {
                    s = "";
                    source = Source.TEXT;
                }
                else if(s.ToLower().StartsWith("http://") || s.ToLower().StartsWith("https://"))
                {
                    source = Source.HTTP;
                }
                else if(s.ToLower().StartsWith("ftp://"))
                {
                    source = Source.FTP;
                }
                else
                {
                    try
                    {
                        if(File.Exists(s)) 
                        { 
                            source = Source.FILE; 
                        }
                    }
                    catch(Exception) {}

                    if(source == Source.UNKNOWN) { source = Source.TEXT; }
                }
            }

            Encoder = encoder;
            WebRequest wr;
            switch(source)
            {
                case Source.FILE:
                    if(encoder == null)
                    {
                        Text = File.ReadAllText(FileName = s);
                    }
                    else { Text = encoder.DecodeToString(File.ReadAllText(FileName = s)); }
                    break;
                case Source.HTTP:
                    wr = WebRequest.Create(s);
                    if(user != null) { wr.Credentials = new NetworkCredential(user, password); }

                    Text = _FromWebRequest(wr, encoder);
                    break;
                case Source.FTP:
                    wr = WebRequest.Create(s);
                    if(user != null) { wr.Credentials = new NetworkCredential(user, password); }

                    Text = _FromWebRequest(wr, encoder);
                    break;
                default:
                    if(encoder == null)
                    {
                        Text = s;
                    }
                    else { Text = encoder.DecodeToString(s); }
                    break;
            }
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="wr">Web request.</param>
        public Ddp(WebRequest wr) : base(null, null)
        {
            Text = _FromWebRequest(wr, null);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="encoder">Encoder.</param>
        /// <param name="wr">Web request.</param>
        public Ddp(WebRequest wr, IEncoder encoder): base(null, null)
        {
            Text = _FromWebRequest(wr, encoder);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the file name.</summary>
        public string FileName
        {
            get; set;
        }


        /// <summary>Gets or sets the encoder.</summary>
        public IEncoder Encoder
        {
            get; set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the element to a file.</summary>
        public void Save()
        {
            Save(FileName, Encoder);
        }


        /// <summary>Saves the element to a file.</summary>
        /// <param name="encoder">Encoder.</param>
        public void Save(IEncoder encoder)
        {
            base.Save(FileName, Encoder = encoder);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] DdpElement                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Saves the element to a file.</summary>
        /// <param name="file">File name.</param>
        /// <param name="encoder">Encoder.</param>
        public override void Save(string file, IEncoder encoder)
        {
            base.Save(FileName = file, Encoder = encoder);
        }


        /// <summary>Saves the element to a file.</summary>
        /// <param name="file">File name.</param>
        public override void Save(string file)
        {
            base.Save(FileName = file, Encoder);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a ddp element from a file, network resource, or text.</summary>
        /// <param name="s">Source string.</param>
        public static Ddp Create(string s)
        { 
            return new Ddp(s);
        }


        /// <summary>Creates a ddp element from a file or network resource.</summary>
        /// <param name="s">Source string.</param>
        /// <param name="user">User name.</param>
        /// <param name="password">Password.</param>
        public static Ddp Open(string s, string user = null, string password = null)
        { 
            return new Ddp(s, user, password);
        }


        /// <summary>Creates a ddp element from a file or network resource.</summary>
        /// <param name="s">Source string.</param>
        /// <param name="encoder">Encoder.</param>
        /// <param name="user">User name.</param>
        /// <param name="password">Password.</param>
        public static Ddp Open(string s, IEncoder encoder, string user = null, string password = null)
        {
            return new Ddp(s, encoder, user, password);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static methods                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the text for a file from a web request.</summary>
        /// <param name="encoder">Encoder.</param>
        /// <param name="wr">Web request.</param>
        /// <returns>String.</returns>
        protected static string _FromWebRequest(WebRequest wr, IEncoder encoder)
        {
            if(wr is HttpWebRequest)
            {
                wr.Method = WebRequestMethods.Http.Get;
            }
            else if(wr is FtpWebRequest)
            {
                wr.Method = WebRequestMethods.Ftp.DownloadFile;
            }

            StreamReader re = new StreamReader(wr.GetResponse().GetResponseStream());
            string rval = re.ReadToEnd();
            Disposal.Recycle(re);

            if(encoder == null) return rval;
            return encoder.DecodeToString(rval);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [enum] DppSource                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This enumeration defines ddp source types.</summary>
        public enum Source: int
        {
            /// <summary>ddp source is unknown.</summary>
            UNKNOWN = 0,
            /// <summary>ddp source is text data.</summary>
            TEXT = 1,
            /// <summary>ddp source is a file system file.</summary>
            FILE = 2,
            /// <summary>ddp source is a file via HTTP.</summary>
            HTTP = 3,
            /// <summary>ddp source is a file via FTP.</summary>
            FTP =4
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [class] DdpValue                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This class implements a ddp variant value.</summary>
        public sealed class DdpValue
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private members                                                                                                  //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets or sets the value.</summary>
            internal string _Value;

            /// <summary>Gets or sets the masked value.</summary>
            internal string _MaskedValue;



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // constructors                                                                                                     //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="value">Value.</param>
            /// <param name="maskedValue">Masked value.</param>
            internal DdpValue(string value, string maskedValue)
            {
                _Value = value;
                _MaskedValue = maskedValue;
            }


            /// <summary>Creates a new instance of this class.</summary>
            /// <param name="value">Value.</param>
            internal DdpValue(string value = null)
            {
                _MaskedValue = _Value = value;
            }




            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private static methods                                                                                           //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Returns a ddp value for an array.</summary>
            /// <typeparam name="T">Type.</typeparam>
            /// <param name="value">Array.</param>
            /// <returns>ddp value.</returns>
            private static DdpValue _FromArray<T>(IEnumerable<T> value)
            {
                DdpValue rval = new DdpValue();
                rval._SetArrayValue(value);

                return rval;
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private properties                                                                                               //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets if the value is empty.</summary>
            internal bool _IsEmpty
            {
                get { return string.IsNullOrWhiteSpace(_MaskedValue); }
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // private methods                                                                                                  //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Gets an array value.</summary>
            /// <typeparam name="T">Type.</typeparam>
            /// <returns>Array.</returns>
            private T[] _GetArrayValue<T>()
            {
                List<T> rval = new List<T>();

                foreach(string i in _MaskedValue.Split(','))
                {
                    if(typeof(T) == typeof(string))
                    {
                        rval.Add((T)(object) __DdpUtility.UnmaskSpecialChars(i.Trim()));
                    }
                    else if(typeof(T) == typeof(bool))
                    {
                        rval.Add((T)(object) __DdpUtility.UnmaskSpecialChars(i.Trim()).ToBoolean());
                    }
                    else if(typeof(T) == typeof(int))
                    {
                        try
                        {
                            rval.Add((T)(object) int.Parse(__DdpUtility.UnmaskSpecialChars(i.Trim())));
                        }
                        catch(Exception) { rval.Add((T)(object) 0); }
                    }
                    else if(typeof(T) == typeof(uint))
                    {
                        try
                        {
                            rval.Add((T)(object) uint.Parse(__DdpUtility.UnmaskSpecialChars(i.Trim())));
                        }
                        catch(Exception) { rval.Add((T)(object) 0); }
                    }
                    else if(typeof(T) == typeof(long))
                    {
                        try
                        {
                            rval.Add((T)(object) long.Parse(__DdpUtility.UnmaskSpecialChars(i.Trim())));
                        }
                        catch(Exception) { rval.Add((T)(object) 0); }
                    }
                    else if(typeof(T) == typeof(ulong))
                    {
                        try
                        {
                            rval.Add((T)(object) ulong.Parse(__DdpUtility.UnmaskSpecialChars(i.Trim())));
                        }
                        catch(Exception) { rval.Add((T)(object) 0); }
                    }
                    else if(typeof(T) == typeof(byte))
                    {
                        try
                        {
                            rval.Add((T)(object) byte.Parse(__DdpUtility.UnmaskSpecialChars(i.Trim())));
                        }
                        catch(Exception) { rval.Add((T)(object) 0); }
                    }
                    else if(typeof(T) == typeof(sbyte))
                    {
                        try
                        {
                            rval.Add((T)(object) sbyte.Parse(__DdpUtility.UnmaskSpecialChars(i.Trim())));
                        }
                        catch(Exception) { rval.Add((T)(object) 0); }
                    }
                    else if(typeof(T) == typeof(float))
                    {
                        try
                        {
                            rval.Add((T)(object) float.Parse(__DdpUtility.UnmaskSpecialChars(i.Trim()).Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator)));
                        }
                        catch(Exception) { rval.Add((T)(object) 0); }
                    }
                    else if(typeof(T) == typeof(double))
                    {
                        try
                        {
                            rval.Add((T)(object) double.Parse(__DdpUtility.UnmaskSpecialChars(i.Trim()).Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator)));
                        }
                        catch(Exception) { rval.Add((T)(object) 0); }
                    }
                    else if(typeof(T) == typeof(decimal))
                    {
                        try
                        {
                            rval.Add((T)(object) decimal.Parse(__DdpUtility.UnmaskSpecialChars(i.Trim()).Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator)));
                        }
                        catch(Exception) { rval.Add((T)(object) 0); }
                    }
                    else if(typeof(T) == typeof(DateTime))
                    {
                        rval.Add((T)(object) __DdpUtility.UnmaskSpecialChars(i.Trim()).ParseTimestamp());
                    }
                }

                return rval.ToArray();
            }


            /// <summary>Sets an array value.</summary>
            /// <typeparam name="T">Type.</typeparam>
            /// <param name="value">Array value.</param>
            internal void _SetArrayValue<T>(IEnumerable<T> value)
            {
                _MaskedValue = _Value = "";
                bool first = true;
                foreach(T i in value)
                {
                    if(first) { first = false; } else { _MaskedValue += ", "; _Value += ", "; }

                    if(typeof(T) == typeof(string))
                    {
                        _MaskedValue += __DdpUtility.ApplyInternal((string) (object) i);
                        _Value += (string) (object) i;
                    }
                    else if(typeof(T) == typeof(bool))
                    {
                        _MaskedValue += ((bool) (object) i ? "true" : "false");
                        _Value += ((bool) (object) i ? "true" : "false");
                    }
                    else if(typeof(T) == typeof(DateTime))
                    {
                        _MaskedValue += ((DateTime) (object) i).ToTimestamp();
                        _Value += ((DateTime) (object) i).ToTimestamp();
                    }
                    else if((typeof(T) == typeof(float)) || (typeof(T) == typeof(double)) || (typeof(T) == typeof(decimal)))
                    {
                        _MaskedValue += i.ToString().Replace(',', '.');
                        _Value += i.ToString().Replace(',', '.');
                    }
                    else
                    {
                        _MaskedValue += i.ToString();
                        _Value += i.ToString();
                    }
                }
            }


            /// <summary>Updates the object.</summary>
            /// <param name="value">Value.</param>
            /// <param name="maskedValue">Masked value.</param>
            internal void _Update(string value, string maskedValue)
            {
                _Value = value;
                _MaskedValue = maskedValue;
            }



            /// <summary>Updates the object.</summary>
            /// <param name="value">Value.</param>
            internal void _Update(DdpValue value)
            {
                _Update(value._Value, value._MaskedValue);
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // [override] object                                                                                                //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Returns a string representation of this instance.</summary>
            /// <returns>String.</returns>
            public override string ToString()
            {
                return _Value;
            }



            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // operators                                                                                                        //
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            /// <summary>Implicitly converts a string to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(string value)
            {
                return new DdpValue(value, __DdpUtility.ApplyInternal(value));
            }


            /// <summary>Implicitly converts a DdpValue to a string.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator string(DdpValue value)
            {
                return value._Value;
            }


            /// <summary>Implicitly converts a Boolean to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(bool value)
            {
                return new DdpValue(value ? "true" : "false");
            }


            /// <summary>Implicitly converts a DdpValue to a Boolean.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator bool(DdpValue value)
            {
                return value._Value.ToBoolean();
            }


            /// <summary>Implicitly converts an integer to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(int value)
            {
                return new DdpValue(value.ToString());
            }


            /// <summary>Implicitly converts a DdpValue to an integer.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator int(DdpValue value)
            {
                try
                {
                    return int.Parse(value._Value);
                }
                catch(Exception) { return 0; }
            }


            /// <summary>Implicitly converts an unsigned integer to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(uint value)
            {
                return new DdpValue(value.ToString());
            }


            /// <summary>Implicitly converts a DdpValue to an unsigned integer.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator uint(DdpValue value)
            {
                try
                {
                    return uint.Parse(value._Value);
                }
                catch(Exception) { return 0; }
            }


            /// <summary>Implicitly converts a short integer to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(short value)
            {
                return new DdpValue(value.ToString());
            }


            /// <summary>Implicitly converts a DdpValue to a short integer.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator short(DdpValue value)
            {
                try
                {
                    return short.Parse(value._Value);
                }
                catch(Exception) { return 0; }
            }


            /// <summary>Implicitly converts an unsigned short integer to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(ushort value)
            {
                return new DdpValue(value.ToString());
            }


            /// <summary>Implicitly converts a DdpValue to an unsigned short integer.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator ushort(DdpValue value)
            {
                try
                {
                    return ushort.Parse(value._Value);
                }
                catch(Exception) { return 0; }
            }


            /// <summary>Implicitly converts a long intger to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(long value)
            {
                return new DdpValue(value.ToString());
            }


            /// <summary>Implicitly converts a DdpValue to a long integer.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator long(DdpValue value)
            {
                try
                {
                    return long.Parse(value._Value);
                }
                catch(Exception) { return 0; }
            }


            /// <summary>Implicitly converts an unsigned long integer to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(ulong value)
            {
                return new DdpValue(value.ToString());
            }


            /// <summary>Implicitly converts a DdpValue to an unsigned long integer.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator ulong(DdpValue value)
            {
                try
                {
                    return ulong.Parse(value._Value);
                }
                catch(Exception) { return 0; }
            }


            /// <summary>Implicitly converts a byte to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(byte value)
            {
                return new DdpValue(value.ToString());
            }


            /// <summary>Implicitly converts a DdpValue to a byte.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator byte(DdpValue value)
            {
                try
                {
                    return byte.Parse(value._Value);
                }
                catch(Exception) { return 0; }
            }


            /// <summary>Implicitly converts a signed byte to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(sbyte value)
            {
                return new DdpValue(value.ToString());
            }


            /// <summary>Implicitly converts a DdpValue to a signed byte.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator sbyte(DdpValue value)
            {
                try
                {
                    return sbyte.Parse(value._Value);
                }
                catch(Exception) { return 0; }
            }


            /// <summary>Implicitly converts a float to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(float value)
            {
                return new DdpValue(value.ToString().Replace(',', '.'));
            }


            /// <summary>Implicitly converts a DdpValue to a float.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator float(DdpValue value)
            {
                try
                {
                    return float.Parse(value._Value.Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                }
                catch(Exception) { return 0; }
            }


            /// <summary>Implicitly converts a double to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(double value)
            {
                return new DdpValue(value.ToString().Replace(',', '.'));
            }


            /// <summary>Implicitly converts a DdpValue to a float.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator double(DdpValue value)
            {
                try
                {
                    return double.Parse(value._Value.Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                }
                catch(Exception) { return 0; }
            }


            /// <summary>Implicitly converts a decimal to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(decimal value)
            {
                return new DdpValue(value.ToString().Replace(',', '.'));
            }


            /// <summary>Implicitly converts a DdpValue to a decimal.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator decimal(DdpValue value)
            {
                try
                {
                    return decimal.Parse(value._Value.Replace(".", Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                }
                catch(Exception) { return 0; }
            }


            /// <summary>Implicitly converts a decimal to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(DateTime value)
            {
                return new DdpValue(value.ToTimestamp());
            }


            /// <summary>Implicitly converts a DdpValue to a decimal.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DateTime(DdpValue value)
            {
                return value._Value.ParseTimestamp();
            }


            /// <summary>Implicitly converts a string array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(string[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to a string array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator string[](DdpValue value)
            {
                return value._GetArrayValue<string>();
            }


            /// <summary>Implicitly converts a Boolean array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(bool[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to a Boolean array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator bool[](DdpValue value)
            {
                return value._GetArrayValue<bool>();
            }


            /// <summary>Implicitly converts an integer array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(int[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to an integer array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator int[](DdpValue value)
            {
                return value._GetArrayValue<int>();
            }


            /// <summary>Implicitly converts an unsigned integer array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(uint[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to an unsigned integer array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator uint[](DdpValue value)
            {
                return value._GetArrayValue<uint>();
            }


            /// <summary>Implicitly converts a long integer array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(long[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to a long integer array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator long[](DdpValue value)
            {
                return value._GetArrayValue<long>();
            }


            /// <summary>Implicitly converts an unsigned long integer array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(ulong[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to an unsigned long integer array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator ulong[](DdpValue value)
            {
                return value._GetArrayValue<ulong>();
            }


            /// <summary>Implicitly converts a short integer array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(short[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to a short integer array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator short[](DdpValue value)
            {
                return value._GetArrayValue<short>();
            }


            /// <summary>Implicitly converts an unsigned short integer array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(ushort[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to an unsigned short integer array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator ushort[](DdpValue value)
            {
                return value._GetArrayValue<ushort>();
            }


            /// <summary>Implicitly converts a byte array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(byte[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to a byte array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator byte[](DdpValue value)
            {
                return value._GetArrayValue<byte>();
            }


            /// <summary>Implicitly converts a signed byte array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(sbyte[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to a signed byte array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator sbyte[](DdpValue value)
            {
                return value._GetArrayValue<sbyte>();
            }


            /// <summary>Implicitly converts a float array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(float[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to a float array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator float[](DdpValue value)
            {
                return value._GetArrayValue<float>();
            }


            /// <summary>Implicitly converts a double array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(double[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to a double array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator double[](DdpValue value)
            {
                return value._GetArrayValue<double>();
            }


            /// <summary>Implicitly converts a decimal array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(decimal[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to a decimal array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator decimal[](DdpValue value)
            {
                return value._GetArrayValue<decimal>();
            }


            /// <summary>Implicitly converts a DateTime array to a DdpValue.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DdpValue(DateTime[] value)
            {
                return _FromArray(value);
            }


            /// <summary>Implicitly converts a DdpValue to an unsigned integer array.</summary>
            /// <param name="value">Value.</param>
            public static implicit operator DateTime[](DdpValue value)
            {
                return value._GetArrayValue<DateTime>();
            }
        }
    }
}
